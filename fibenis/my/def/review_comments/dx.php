<?PHP
	
	$application_id = $_GET['default_addon'];
    		
	$LAYOUT = 'layout_narrow_down';
              
        $D_SERIES       =   array(
                                   'title'=>'View Comments',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
				    
				    'gx' => 1,
				    
                                    #table data
                                    
                                    'data'=> array( 
							
							 
							2=>array('th'=>'Name',
								
								'field'	=> "get_eav_addon_varchar((SELECT is_internal FROM user_info WHERE id = exav_addon_text.user_id),'COFN')",
							                                                         
								'td_attr' => ' class=" align_LM no_wrap" width="10%"',
								
								'is_sort' => 0,
								
								'filter_out' => function($data_in){
									
											//$temp = explode(':',$data_in);
											
											return "<div class='block normal txt_case_uppercase txt_size_11 clr_gray_a'>$data_in</div>";;
											       
									
								                }
								
								),
							
							
							3=>array('th'=>'Comments ',
								
								'field' => 'exa_value',
								
								'field'     => "concat_ws(':',substring_index(exa_value,' ',7),exa_value)",
								
								'td_attr' => ' class="label_father align_LM" width="50%"',
								
								'is_sort' => 0,
								
								'filter_out'=>function($data_in){
									
									$temp = explode(':',$data_in);
									
									return "<a class='tip clr_gray_5'>$temp[0]...</a><span class='tooltiptext'>$temp[1]</span>";
										
								}
								
								),
							
							 1=>array('th'=>'Time',
								
								'field'	=> "date_format(timestamp_punch,'%d-%b-%Y %T')",
                                                                
								'td_attr' => ' class="label_father align_LM" width="40%"',
								
								'is_sort' => 1,
								
								'filter_out' => function($data_in){
									
											//$temp = explode(':',$data_in);
											
											return "<div class='block normal txt_case_uppercase txt_size_11 clr_gray_a'>$data_in</div>";;
											       
									
								                }
								
								),
							
					),
				    
					
                                    #Table Info
                                    
                                    'table_name' =>'exav_addon_text',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
				    
				    'key_filter'     =>	 "AND exa_token = 'COMM' AND parent_id = $application_id",
				 
				    'is_narrow_down' => 0,
				    
                                    # Communication
                                
                                    'prime_index'   => 1,
				    
				    'custom_filter' => array( ),
                                
				
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>0,'page_link'=>'', 'b_name' => '' ),
								
					'del_permission' => array('able_del'=>0,'user_flage'=>0), 
								
					'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  ''),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'COMM',
				
				'show_query'=>0,
				
				'hide_show_all' => 1,
				
				'search_filter_off' => 1,
				
				'hide_pager'=>1,
				
				'show_all_rows'=>1,
				
				'filter_off'=>1,
				
                            
                            );
    
?>