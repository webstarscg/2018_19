<?PHP 

    $D_SERIES   =   array(

                        'title'         =>  'Patient Table',
                        'gx'            =>  1,
                        'table_name'    =>  'patient',
                        'key_id'        =>  'id',
                        #'show_query'   =>  1,
                        'avoid_trans_key_direct'    =>  1,
                        'add_button'    =>  array(

                                                'is_add'    =>  1,
                                                'page_link' =>  'fx=patient_detail',
                                                'b_name'    =>  'Add Patient'
                        ),        


                        'data'  =>  array(

                                        '1' =>  array(

                                                    'th'        =>  'Patient Name',
                                                    'field'     =>  'name',
                                                    'is_sort'   =>  1,
                                        ),

                                        '2' =>  array(

                                                    'th'        =>  'Age',
                                                    'field'     =>  'age'
                                        ),

                                        '3' =>  array(

                                                    'th'        =>  'Gender',
                                                    'field'     =>  'gender'
                                        ),

                                        '4' =>  array(

                                                    'th'        =>  'Town',
                                                    'field'     =>  'town'
                                        ),

                                        '5' =>  array(

                                                    'th'        =>'Date',
                                                    'field'     =>  'date',
                                                    'is_sort'   => 1,
                                        ),

                                        '6' =>  array(

                                                    'th'        =>'Visits',
                                                    'field'     => 'visits'
                                        ),
        

                        ),


    );

?>
