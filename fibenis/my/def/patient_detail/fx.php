<?PHP 

    $F_SERIES   =   array(

                        #Desk title

                        'title'         => 'Patient',

                        #table Name

                        'table_name'    =>  'entity_child',

                        #Unique Key ID

                        'key_id'        =>  'id',

                        #'session_off'   =>  1,

                        'show_query'     =>  1,

                        'divider'   =>  'tab',

                        #'flat_message'  =>  'Patient Added Successfully',

                        # 'prime_index'   =>  1,

                        'avoid_trans_key_direct'    =>  1,

                        'back_to'  => array( 
                                             'is_back_button' =>1, 
                                             'back_link'=>'?dx=patient_detail',
                                             'BACK_NAME'=>'Back'
                                         ),

                        

                        #Table Field

                        'data'          =>  array(

                                                /*'10' =>  array(

                                                            'field_name'    => 'Patient Data Heading',
                                                            'type'          => 'heading',
                                                ),

                                                '9' =>  array(

                                                            'field_name'    => 'Patient Data Sub Heading',
                                                            'type'          => 'sub_heading',
                                                ),*/

                                                '1' =>  array(

                                                            'field_name'            => 'Patient Name',
                                                            'field_id'              =>  'exa_value',
                                                            'type'                  => 'text',
                                                            'is_mandatory'          => 1,
                                                            'hint'                  => 'Enter Name',
                                                            'parent_field_id'       => 'parent_id',
                                                            'child_table'           =>  'exav_addon_varchar',
                                                            'child_attr_field_id'   =>  'exa_token',
                                                            'child_attr_code'       =>  'PTNA',
                                                ),

                                                '2' =>  array(

                                                            'field_name'            => 'Age',
                                                            'field_id'              => 'exa_value',
                                                            'type'                  => 'text',
                                                            'is_mandatory'          => 1,
                                                            'allow'                 =>  'd2',
                                                            'parent_field_id'       => 'parent_id',
                                                            'child_table'           =>  'exav_addon_varchar',
                                                            'child_attr_field_id'   =>  'exa_token',
                                                            'child_attr_code'       =>  'PTAG',

                                                    
                                                ),


                                                /*'3' =>  array(

                                                    'field_name'    =>  'Gender',
                                                    'type'          =>  'option',
                                                    'field_id'      =>  'gender',
                                                    'is_list'       =>  0,
                                                    'option_data'   =>  '<option value = 0>No</option>
                                                                         <option value = 1>Yes</option>',
                                                    'avoid_default_option'  =>  1,
                                                ),


                                                '4' =>  array(

                                                            'field_name'    => 'Entity',
                                                            'field_id'      => 'entity_code',
                                                            'type'          => 'option',
                                                            'option_data'   =>'<option value=PT>Patient</option>',
                                                            'avoid_default_option' => 1,
                                                            'option_default'   => array('label' => 'Patient',
                                                                                        'value' => 'PT'),
                                                    
                                                ),

                                                '5' =>  array(

                                                            'field_name'    =>  'Date',
                                                            'field_id'      =>  'date',
                                                            'type'          =>  'date',
                                                            'default_date'  =>  '25.12.1996',
                                                            'year_range'    =>  '1996 : 2019',
                                                            #'is_mandatory'  => 1,
                                                    
                                                ),

                                                '6' =>  array(

                                                            'field_name'    => 'Visits',
                                                            'field_id'      => 'visits',
                                                            'type'          => 'file',

                                                ),

                                                '7' =>  array(

                                                            'field_name'    =>  'Email',
                                                            'field_id'      =>  'email',
                                                            'type'          =>  'text',
                                                            'allow'         =>  'w20[@.]'
                                            
                                                ),

                                                '8' =>  array(

                                                            'field_name'    =>  'Content',
                                                            'type'          =>  'label'
                                                ),

                                                '9' =>  array(

                                                            'field_name'    =>  'Text',
                                                            'type'          =>  'textarea_editor',
                                                           
                                                ),

                                                '10' => array(

                                                            'field_name'    =>  'Code',
                                                            'type'          =>  'code_editor',

                                                ),

                                                '11' => array(

                                                            'field_name'    =>  'Toggle Switch',
                                                            'type'          =>  'toggle',
                                                            'is_round'      =>  1,
                                                            'on_label'      =>  'On',
                                                            'off_label'     =>  'Off',
                                                            'is_default_on' =>  1,  

                                                ),

                                                '12' => array(

                                                            'field_name'    =>  'Table',
                                                            'type'          =>  'fibenistable',
                                                            'is_fibenistable'=> 1,
                                                            'colHeaders'=> array(array(
                                                                'column'    => 'Name',
                                                                'width'     => '50',
                                                                'type'      => 'text',
                                                                                                    ),
                                                            array(
                                                                'column'    => 'Purpose',
                                                                'width'     => '50',
                                                                'type'      => 'text',
                                                                                                    ),
                                                        )
                                                ),*/


                                            ),

                                            'default_fields'    =>  array(

                                                        'entity_code'   =>  "'PT'",

                                                ),

                                                'is_user_id'    =>  'user_id'



    );

?>
