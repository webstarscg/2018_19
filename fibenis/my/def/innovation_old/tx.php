<?PHP
        	
	$key        	= @$_GET['key'];
	
	$LAYOUT = "layout_full";
				
	$T_SERIES       =   array(
		
                                'table' => 'entity_child',
				 
				'data'  => array(
						 
						'key_id'=>array('field'=>'id'),
						
						//title
						'title_label'     =>array('field'=>"get_ecb_sn_by_token('FAINOTIT')"),
						'title'           =>array('field'=>"get_exav_addon_varchar(id,'FAINOTIT')"),
						
						//address
						'address_for_comm'=>array('field'=>"get_exav_addon_text(id,'FACOM')"),
						
						//date_of_birth
						'dob_title'=>array('field'=>"get_ecb_sn_by_token('FADOB')"),
						'dob'=>array('field'=>"date_format(get_exav_addon_date(id,'FADOB'),'%d-%b-%y')"),
						
						//gender
						'gender_title'=>array('field'=>"get_ecb_sn_by_token('FAGEN')"),
						'gender'=>array('field'=>"get_ecb_sn_by_token(get_exav_addon_varchar(id,'FAGEN'))"),
						
						//applicant_category
						'applicant_category_title'=>array('field'=>"get_ecb_sn_by_token('FATEAMHEAD')"),
						
						//Category of Applicant
						'coa_title'=>array('field'=>"get_ecb_sn_by_token('FACT')"),
						'coa'=>array('field'=>"get_ecb_sn_by_token(get_exav_addon_varchar(id,'FACT'))"),
					
						//number_of_team_members
						'number_of_team_members_title'=>array('field'=>"get_ecb_sn_by_token('FATM')"),
						'number_of_team_members'      =>['field'=>"get_exav_addon_decimal(id,'FATM')"],
						 
						//team_detail
						'team_detail_title'=>array('field'=>"get_ecb_sn_by_token('FATD')"),
						'team_detail'=>['field'=>"get_exav_addon_text(id,'FATD')",
								
								   'data'  => array( '1'=>array('key'=>'Name'),
										     '2'=>array('key'=>'Role'),
										     '3'=>array('key'=>'Noy'),
										     '4'=>array('key'=>'Email'),
										     '5'=>array('key'=>'Mobile'))
									
									
							       ],
										  		
						//heading
						'details_of_primary_applicant_title'=>array('field'=>"get_ecb_sn_by_token('FADPA')"),
						
						//years_of_exp
						'years_of_exp_title'=>array('field'=>"get_ecb_sn_by_token('FAEX')"),
						'years_of_exp'      =>['field'=>"get_exav_addon_decimal(id,'FAEX')"],
						
						//professional_detail
						'professional_detail_title'=>array('field'=>"get_ecb_sn_by_token('FAPDG')"),
						'professional_detail'=>['field'=>"get_exav_addon_text(id,'FAPDG')",
									
									'data'  => array( '1'=>array('key'=>'Orga_name'),
											'2'=>array('key'=>'Dep'),
											'3'=>array('key'=>'Role'),
											'4'=>array('key'=>'From'),
											'5'=>array('key'=>'To'))
									],
						
						 //if_startup
						'if_startup_title'=>array('field'=>"get_ecb_sn_by_token('FAIS')"),
						'if_startup'=>array('field'=>"get_ecb_sn_by_token(get_exav_addon_varchar(id,'FAIS'))"),
						
						//name_of_startup
						'name_of_startup_title'=>array('field'=>"get_ecb_sn_by_token('FASN')"),
						'name_of_startup'      =>array('field'=>"get_exav_addon_varchar(id,'FASN')"),
						
						//date_of_in_corporation
						'date_of_in_corporation_title'=>array('field'=>"get_ecb_sn_by_token('FASD')"),
						'date_of_in_corporation'      =>array('field'=>"get_exav_addon_varchar(id,'FASD')"),
						
						//number_of_directors
						'number_of_directors_title'=>array('field'=>"get_ecb_sn_by_token('FASNOD')"),
						'number_of_directors'      =>array('field'=>"get_exav_addon_varchar(id,'FASNOD')"),
						
						//number_of_employees
						'number_of_employees_title'=>array('field'=>"get_ecb_sn_by_token('FASNOE')"),
						'number_of_employees'      =>array('field'=>"get_exav_addon_varchar(id,'FASNOE')"),
						
						//website
						'website_title'=>array('field'=>"get_ecb_sn_by_token('FASWEB')"),
						'website'      =>array('field'=>"get_exav_addon_varchar(id,'FASWEB')"),
						
						//heading
						'details_of_innovation_title'=>array('field'=>"get_ecb_sn_by_token('FADI')"),
						
						//title
						'title_title'=>array('field'=>"get_ecb_sn_by_token('FAINOTIT')"),
						'title'      =>array('field'=>"get_exav_addon_varchar(id,'FAINOTIT')"),
						
						//problem_addressed
						
						'problem_addressed_title'=>array('field'=>"get_ecb_sn_by_token('FAPROADD')"),
						'problem_addressed'	 =>array('field'=>"get_exav_addon_text(id,'FAPROADD')"),
						
						// Alternative Solutions Available
						'asa_title'=>array('field'=>"get_ecb_sn_by_token('FAALTSOL')"),
						'asa'	 =>array('field'=>"get_exav_addon_text(id,'FAALTSOL')"),
						
						//  Advantage of Proposed Solution 
						'aps_title'=>array('field'=>"get_ecb_sn_by_token('FAADVSOL')"),
						'aps'	 =>array('field'=>"get_exav_addon_text(id,'FAADVSOL')"),
						
						// Novelty/Inventive/Differentiation
						'nid_title'=>array('field'=>"get_ecb_sn_by_token('FAINVDIF')"),
						'nid'	 =>array('field'=>"get_exav_addon_text(id,'FAINVDIF')"),
						
						//  Reason for Support 
						'rfs_title'=>array('field'=>"get_ecb_sn_by_token('FAREASUP')"),
						'rfs'	 =>array('field'=>"get_exav_addon_text(id,'FAREASUP')"),
						
						// Infrastructure/Special Requirement from PSG-CBMI
						'isrfpc_title'=>array('field'=>"get_ecb_sn_by_token('FAINFRA')"),
						'isrfpc'      =>array('field'=>"get_exav_addon_text(id,'FAINFRA')"),
						
						// Any other detail which would help in evaluating your proposal
						'aodwwheyp_title'=>array('field'=>"get_ecb_sn_by_token('FAEVAL')"),
						'aodwwheyp'      =>array('field'=>"get_exav_addon_text(id,'FAEVAL')"),
						
						// Advice or mentoring support from our clinician specialist
						'aomsfocs_title'=>array('field'=>"get_ecb_sn_by_token('FAADVCLI')"),
						'aomsfocs'=>array('field'=>"get_ecb_sn_by_token(get_exav_addon_varchar(id,'FAADVCLI'))"),
						
						// Does your study involve animals or cell lines etc.
						'dysiaocle_title'=>array('field'=>"get_ecb_sn_by_token('FAANICELL')"),
						'dysiaocle'=>array('field'=>"get_ecb_sn_by_token(get_exav_addon_varchar(id,'FAANICELL'))"),
						
						// Will you be collecting Human Data (clinical trial)
						'wybchd_title'=>array('field'=>"get_ecb_sn_by_token('FACOLHUM')"),
						'wybchd'=>array('field'=>"get_ecb_sn_by_token(get_exav_addon_varchar(id,'FACOLHUM'))"),
						
						// Select your Project State
						'syps_title'=>array('field'=>"get_ecb_sn_by_token('FAPROSTA')"),
						'syps'=>array('field'=>"get_ecb_sn_by_token(get_exav_addon_varchar(id,'FAPROSTA'))"),
						
						// Budget
						'budget_title'=>array('field'=>"get_ecb_sn_by_token('FABUDGET')"),
						'budget'=>['field'=>"get_exav_addon_text(id,'FABUDGET')",
							   'data'  => array( '1'=>array('key'=>'Total'),
										'2'=>array('key'=>'Amount'))
									
									],
						
						//Pay Detail
						'pay_detail_title'=>array('field'=>"get_ecb_sn_by_token('FAPAYDET')"),
						'pay_detail'      =>['field'=>"get_exav_addon_varchar(id,'FAPAYDET')"]
						
						
						),	
				
				
				'key_id' => 'id',
				
				'key_filter'=>' ',
				
				'show_query'=>0,
				
				# if data stored in array way like [ ['ins','1'],['ins B',2]]
  
                                'template'       => 'def/innovation/tx.html',
				
				// save data 
				'save_as'=> array(
						
						array('type'=>'html',
						      'file_name'=>'home',
						      'path'=>'def/innovation/')
						
					)

                            );
	
	
    
?>