<?PHP
//			
//	$default_addon = @$_GET['default_addon'];
//    
//	$LAYOUT	    	= 'layout_full';
//               
//        $D_SERIES       =   array(
//                                   'title'=>'Review',
//                                    
//                                    #query display depend on the user
//                                    
//                                    'is_user_base_query'=>0,
//				    
//				    'gx' => 1,
//				    
//                                    
//                                    #table data
//                                    
//                                    'data'=> array( 
//						        1=>array('th'=>'Assigned Time',
//								
//								'field'	=> "date_format(timestamp_punch,'%d-%b-%Y %T')",
//                                                                
//								'td_attr' => ' class="label_father align_LM" width="20%"',
//								
//								'is_sort' => 0,	
//								
//								),
//							
//							2=>array('th'=>'Title',
//								
//								'field'	=> "get_exav_addon_varchar(ec_id,'TITLE')",
//							                                                         
//								'td_attr' => ' class="label_father align_LM" width="50%"',
//								
//								'is_sort' => 0,	
//								
//								),
//							
//							3=>array(       'th'=>'Document',
//								 
//									'td_attr' => ' class="align_LM no_wrap " width="10%"',
//                                
//									'field'	=> "ec_id",
//									
//									//'field'	=> "id",
//								
//									'filter_out'=>function($data_in){
//										
//										global $rdsql;
//										
//										$find_ec_code = $rdsql->exec_query("SELECT entity_code FROM entity_child WHERE id = $data_in",
//														   "Selection of ec in reviewer home page Fails");
//										
//										$find_ec_code_val = $rdsql->data_fetch_row($find_ec_code);
//										
//										
//										$data_in = $find_ec_code_val[0].":".$data_in;
//										
//										return '<a class="pointer clr_gray_b txt_size_11" onclick="JavaScript:view_project_doc('."'".$data_in."'".');">'.
//											'<i class="fa fa-file-pdf-o clr_red txt_size_15" aria-hidden="true"></i>&nbsp;'.
//											'View</a>';
//											//''.$data_in.'</a>';
//									}
//                                                                
//								),
//							
//							4=>array(	'th'=>'Comments ',
//								 
//									'th_attr'=>' colspan=2 ',
//								 
//									'field'	=> "concat(id,':',ec_id)",
//									
//									'filter_out'=>function($data_in){
//										
//													$temp = explode(':',$data_in);
//                                                                            
//													$data_out = array(	'id'   => $temp[0],
//																'link_title'=>'Add Comments',
//																'is_fa'=>'fa-plus-square txt_size_15 clr_orange ',
//																'title'=>'Add Comments',
//																'src'=>"?fx=review_comments&menu_off=1&mode=simple&default_addon=$temp[1]",
//																'style'=>"border:none;width:100%;height:600px;",
//																'refresh_on_close'=>0
//															);
//															return json_encode($data_out);
//														
//												 },
//												 
//									'js_call'=>'d_series.set_nd'
//                                                                
//								),
//							
//							5=>array(       'th'=>'',
//								 
//									'field'	=> "concat(id,':',ec_id)",
//									
//									'filter_out'=>function($data_in){
//                                                                            
//													$temp = explode(':',$data_in);
//                                                                            
//													$data_out = array(	'id'   => $temp[0],
//																'link_title'=>'View comments',
//																'is_fa'=>'fa-folder txt_size_15 clr_orange ',
//																'title'=>'View comments',
//																'src'=>"?dx=review_comments&menu_off=1&mode=simple&default_addon=$temp[1]",
//																'style'=>"border:none;width:100%;height:600px;",
//																'refresh_on_close'=>0
//															);
//															return json_encode($data_out);
//														
//												 },
//									 
//                                                                        'js_call'=>'d_series.set_nd'
//                                                                
//								),  
//							
//                                                    ),
//				    
//					
//                                    #Table Info
//                                    
//                                    'table_name' =>'exav_addon_ec_id',
//                                    
//                                    'key_id'    =>'id',
//                                    
//                                    # Default Additional Column
//                                
//                                    'is_user_id'       => 'user_id',
//				    
//				    'is_narrow_down'	=> 1,
//				    
//				    'js' => array('is_top' => 1, 'top_js' => "def/review_view_hp/dx"),
//				    
//				    'key_filter'     =>	 "AND exa_token = 'ASPRO' AND parent_id = (SELECT is_internal FROM user_info WHERE id = $USER_ID)",
//				    
//                                    # Communication
//                                
//                                    'prime_index'   => 1,
//                                
//				
//				#check_field
//								
//					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
//								
//					'add_button' => array( 'is_add' =>0,'page_link'=>'', 'b_name' => '' ),
//								
//					'del_permission' => array('able_del'=>0,'user_flage'=>0), 
//								
//					'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  ''),	
//								
//				#export data
//				
//				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
//								
//				'page_code'    => 'REVI',
//				
//				'show_query'=>0,
//				
//				'hide_show_all' => 1,
//				
//				'search_filter_off'	=>1,
//                            
//                            );

	include("def/adm_prog_dev/dx.php");
	
	if($USER_ROLE == 'REW'){
		
		$D_SERIES['del_permission']['able_del'] = 0 ;
		
		$D_SERIES['key_filter'] =  " AND (entity_code='NP' OR entity_code='EI' OR entity_code='FA' OR entity_code='NA')
					     AND id IN (SELECT parent_id FROM exav_addon_ec_id WHERE exa_token = 'REVW'
					     AND ec_id = (SELECT is_internal FROM user_info WHERE id = $USER_ID))";
		
		unset($D_SERIES['data'][9]);
		
		unset($D_SERIES['data'][10]);
				    
		unset($D_SERIES['custom_filter'][1]);
		
		unset($D_SERIES['summary_data'][1]);
	}
	
	
    
?>