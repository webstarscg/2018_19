<?PHP
			
    
	$LAYOUT	    	= 'layout_full';
               
        $D_SERIES       =   array(
                                   'title'=>'Status',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
				    
				    'gx' => 1,
				    
                                    
                                    #table data
                                    
                                    'data'=> array( 
						//        0=>array('th'=>'ID ',
						//		
						//		'field' =>"id",
						//		
						//		'td_attr' => ' class="label_father align_LM" width="5%"',
						//		
						//		'is_sort' => 0,	
						//		
						//		),
							
							1=>array('th'=>'Date & Time ',
								
								'field'	=> "date_format(updated_on,'%d-%b-%Y %T')",
                                                                
								'td_attr' => ' class="label_father align_LM" width="10%"',
								
								'is_sort' => 1,	
								
								),
							
							2=>array('th'=>'Name ',
								
								'field' =>"get_user_internal_name(user_id)",
								
								'td_attr' => ' class="label_father align_LM" width="10%"',
								
								'is_sort' => 0,	
								
								),
							
							3=>array('th'=>'Image',
									 
								'field'=>'image',
									 
								'td_attr' => ' class="label_child align_CM" width="5%"',
								
								'filter_out'   =>	function($data_out){
									
                                                                        $temp = explode(',',$data_out);
                                                                        
									return '<img class="w_25" src="'. $temp[0].'">';
									
									},
									 
								),
							
							4=>array('th'=>'Information ',
								
								'field' =>"get_exav_addon_text(id,'PANO')",
								
								'td_attr' => ' class="label_father align_LM" width="50%"',
								
								'is_sort' => 0,	
								
								),
							
							7=>array('th'=>'Status ',
								 
								'td_attr' => ' class="align_LM" width="5%"',
                                
								'field'	=> "concat(id,':',parent_id,':',get_exav_addon_exa_token(entity_child.parent_id,'FASA'))",
								
								'filter_out'=>function($data_in){
					
								    $temp = explode(':',$data_in);
				
								
								    if($temp[2]=='SASU'){
					
									$data_out = array('id'   => $temp[0],
									'link_title'=>'Updtae Status',
									'title'=>'Add Payment Details',
									'is_fa'=>' fa fa-plus-square-o clr_red fa-lg',
									'src'=>"?fx=status&menu_off=1&mode=simple&default_addon=$temp[2]:$temp[1]",
									'style'=>"border:none;width:100%;height:600px;",
									'refresh_on_close'=>1);
									return json_encode($data_out);
						
								    }
								    elseif($temp[2]=='SAVF'){
					
									$data_out = array(
									'link_title'=>'Payment Verified',
									'is_fa'=>' fa fa-check clr_green fa-lg',
									);
									return json_encode($data_out);
					
								    }
								    elseif($temp[2]=='SARU'){
					
									$data_out = array(
									'link_title'=>'Requested For Update',
									'is_fa'=>' fa fa-ban clr_red fa-lg',
									);
									return json_encode($data_out);
					
								    }
								    else{
						
									$data_out = array('link_title'=>'Payment Not Submitted',
									'is_fa'=>' fa fa-ban clr_red fa-lg');
									return json_encode($data_out);
					
								    }
				
								
								},
                                                                        
								'js_call'=>'d_series.set_nd'
								           
								),
							
							
					
                                        ),
				    
					
                                    #Table Info
                                    
                                    'table_name' =>'entity_child',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
				    
				    'key_filter'     =>	 " AND entity_code='PA'",
				    
				    'is_narrow_down' => 1,
				    
                                    # Communication
                                
                                    'prime_index'   => 1,
				    
				    'custom_filter' => array(  			     						   
							      
									array(  'field_name' => 'Status:',
									      
										'field_id' => 'cf1', // 
										
										'filter_type' =>'option_list', 
												    
										'option_value'=> $G->option_builder('entity_child_base','token,sn'," WHERE entity_code = 'SA'"),
							    
										'html'=>'  title="Select Type"   data-width="100px"  ',
								    
										'cus_default_label'=>'Show All',
							    
										'filter_by'  => "get_exav_addon_exa_token(parent_id,'FASA')" // main table value
									)
									
								),
                                
				
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>0,'page_link'=>'', 'b_name' => '' ),
								
					'del_permission' => array('able_del'=>0,'user_flage'=>0), 
								
					'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  ''),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'CHKPAYMENT',
				
				'show_query'=>0,
				
				'hide_show_all' => 1,
				
				'search_filter_off'	=>1,
                            
                            );
    
?>