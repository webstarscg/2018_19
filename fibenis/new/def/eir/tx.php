<?PHP
        	
	$key        	= @$_GET['key'];
	
	$LAYOUT = "layout_full";
				
	$T_SERIES       =   array(
		
                                'table' => 'entity_child',
				 
				'data'  => array(
						 
						'key_id'=>array('field'=>'id'),
						
						'domain'=> array('field'=>"'http://psgstep.fibenis.org/'"),
						
						//Basic Application_Heading
						'basic_application_form'=>array('field'=>"get_ecb_sn_by_token('EI1H')"),
						
						//Name_of_the_applicant
						'name_of_the_applicant_title'=>array('field'=>"get_ecb_sn_by_token('EI1AP')"),
						'name_of_the_applicant'=>array('field'=>"get_exav_addon_varchar(id,'EI1AP')"),

						//Photograph
						'photograph_title'=>array('field'=>"get_ecb_sn_by_token('EI1PO')"),
						'photograph'=>array('field'=>"get_exav_addon_varchar(id,'EI1PO')"),
						
						//Full Postal Address
						'full_postal_address_title'=>array('field'=>"get_ecb_sn_by_token('EI1FPA')"),
						'full_postal_address'	 =>array('field'=>"get_exav_addon_text(id,'EI1FPA')"),
						
						//Email Address
						'email_address_title'=>array('field'=>"get_ecb_sn_by_token('EI1EA')"),
						'email_address'	 =>array('field'=>"get_exav_addon_varchar(id,'EI1EA')"),
						
						//Mobile Number
						'mobile_number_title'=>array('field'=>"get_ecb_sn_by_token('EI1MB')"),
						'mobile_number'	     =>['field'=>"get_exav_addon_varchar(id,'EI1MB')"],
						
						//Age
						'age_title'=>array('field'=>"get_ecb_sn_by_token('EI1AG')"),
						'age'      =>['field'=>"get_exav_addon_decimal(id,'EI1AG')"],
						
						//Gender
						'gender_title'=>array('field'=>"get_ecb_sn_by_token('EI1GE')"),
						'gender'=>array('field'=>"get_ecb_sn_by_token(get_exav_addon_varchar(id,'EI1GE'))"),
						
						//Aadhar No
						'aadhar_no_title'=>array('field'=>"get_ecb_sn_by_token('EI1AN')"),
						'aadhar_no'	     =>['field'=>"get_exav_addon_varchar(id,'EI1AN')"],
						
						//About The Applicant Heading
						'about_the_applicant_title'=>array('field'=>"get_ecb_sn_by_token('EI2H')"),
						
						//Basic Undergraduate Training / Education
						'bute_title'=>array('field'=>"get_ecb_sn_by_token('EI2ED')"),
						'bute'=>array('field'=>"get_exav_addon_varchar(id,'EI2ED')"),
						
						//Any experiences relating to entrepreneurship, leadership
						'aertel_title'=>array('field'=>"get_ecb_ln_by_token('EI2EX')"),
						'aertel'=>array('field'=>"get_exav_addon_text(id,'EI2EX')"),
						
						//Attach CV / Resume with details of education and work experience, if any
						'attach_cv_title'=>array('field'=>"get_ecb_sn_by_token('EI2CV')"),
						'attach_cv'=>array('field'=>"get_exav_addon_varchar(id,'EI2CV')"),
						
						//About The Application's Technology / Business Idea
						'abtaptbi_title'=>array('field'=>"get_ecb_sn_by_token('EI3H')"),
						
						//Please describe the technology for which you are seeking market opportunity 
						'pdtwso_title'=>array('field'=>"get_ecb_ln_by_token('EI3TECH')"),
						'pdtwso'=>array('field'=>"get_exav_addon_text(id,'EI3TECH')"),
						
						//Please attach a concept note of the Technology / Business Idea you propose to pursue
						'attach_tech_busin_idea_title'=>array('field'=>"get_ecb_ln_by_token('EI3IDEA')"),
						'attach_tech_busin_idea'=>array('field'=>"get_exav_addon_varchar(id,'EI3IDEA')"),
						
						//Please attach a note describing the knowledge or technology intensity aspects of the idea.
						'attach_tech_inten_idea_title'=>array('field'=>"get_ecb_ln_by_token('EI3KNW')"),
						'attach_tech_inten_idea'=>array('field'=>"get_exav_addon_varchar(id,'EI3KNW')"),
						
						//Technology Business Ideas with larger technology uncertainties and / or long gestation periods
						'tech_business_idea_title'=>array('field'=>"get_ecb_ln_by_token('EI3TECHYN')"),
						'tech_business_idea'=>array('field'=>"get_exav_addon_num(id,'EI3TECHYN')"),
						
						//Technology Business Ideas leveraging technology or IP from publicly funded research or academic organizations
						'tech_busi_idea_lever_tech_title'=>array('field'=>"get_ecb_ln_by_token('EI3IDEAYN')"),
						'tech_busi_idea_lever_tech'=>array('field'=>"get_exav_addon_num(id,'EI3IDEAYN')"),
						
						//Technology Business Ideas with considerable social impact
						'tech_busi_idea_with_consi_title'=>array('field'=>"get_ecb_ln_by_token('EI3KNWYN')"),
						'tech_busi_idea_with_consi'=>array('field'=>"get_exav_addon_num(id,'EI3KNWYN')"),
						
						//Check List for Essential Criteria
						'check_list_for_ec_title'=>array('field'=>"get_ecb_sn_by_token('EI4H')"),
						
						//Applicant confirms that he/ she has not been the recipient of the NIDHI-EIR previously. NIDHI-EIR can be used only once.
						'appli_conf_title'=>array('field'=>"get_ecb_ln_by_token('E101')"),
						'appli_conf'=>array('field'=>"get_exav_addon_num(id,'E101')"),
						
						//Applicant is planning to pursue NIDHI-EIR full time with no othe	
						'appli_planning_title'=>array('field'=>"get_ecb_ln_by_token('E102')"),
						'appli_planning'=>array('field'=>"get_exav_addon_num(id,'E102')"),
						
						//Applicant confirms that he/she will not be in receipt of any oth	
						'appli_conf_that_title'=>array('field'=>"get_ecb_ln_by_token('E103')"),
						'appli_conf_that'=>array('field'=>"get_exav_addon_num(id,'E103')"),
						
						//Applicant confirms that he/she is fully committed to exploring
						'appli_conf_that_commit_title'=>array('field'=>"get_ecb_ln_by_token('E104')"),
						'appli_conf_that_commit'=>array('field'=>"get_exav_addon_num(id,'E104')"),
						
						//Applicant confirms that he/she is not the promoter or significa
						'appli_conf_that_promote_title'=>array('field'=>"get_ecb_ln_by_token('E105')"),
						'appli_conf_that_promote'=>array('field'=>"get_exav_addon_num(id,'E105')"),
						
						//The NIDHI-EIR has or is planning to register for the pre-incubat
						'the_nidhi_eir_title'=>array('field'=>"get_ecb_ln_by_token('E106')"),
						'the_nidhi_eir'=>array('field'=>"get_exav_addon_num(id,'E106')"),
						
						//Undertaking and Signatures	
						'under_and_sign_title'=>array('field'=>"get_ecb_sn_by_token('EI5H')"),
						
						//Confirmation Letter for Pre-incubation Support 
						'conf_letter_title'=>array('field'=>"get_ecb_sn_by_token('EI5CL')"),
						'conf_letter'=>array('field'=>"get_exav_addon_varchar(id,'EI5CL')"),
						
						//Place
						'place_title'=>array('field'=>"get_ecb_sn_by_token('EI5PLA')"),
						'place'	 =>array('field'=>"get_exav_addon_varchar(id,'EI5PLA')"),
						
						//Date
						'date_title'=>array('field'=>"get_ecb_sn_by_token('EI5DATE')"),
						'date'=>array('field'=>"date_format(get_exav_addon_date(id,'EI5DATE'),'%d-%b-%y')"),
						
						//Signature
						'signature_title'=>array('field'=>"get_ecb_sn_by_token('EI5SIGN')"),
						'signature'=>array('field'=>"get_exav_addon_varchar(id,'EI5SIGN')")
						
						
						
						),	
				
				'image_path' => '../../../images/logo/psgstep.jpg',
				'key_id' => 'id',
				
				'key_filter'=>' ',
				
				'show_query'=>0,
				
				# if data stored in array way like [ ['ins','1'],['ins B',2]]
  
                                'template'       => 'def/eir/tx.html',
				
				// save data 
				'save_as'=> array(
						
						array('type'=>'pdf',
						      'file_name'=>'home',
						      'path'=>'def/eir/',
						
							// optional
						      'header' => [
								    
								     'image_path'     => 'images/logo/psgstep.jpg',
								     //'title'	      => 'Tite',
								     'margin_left'    => '15', # optional
								     'margin_right'   => '25',
								     'margin_top'     => '5', # optional
								     'height'         => '15', # optional								     
								     'is_disable'      => 0,	
								     							   
								],
						      
                                                      // optional
						      'footer' => [
								     
								     'glue'=>  ' of ', //{{CURRENET_PAGE_NO}} of {{TOTAL_PAGES}},
								     'align'  => 'C', # C->center, R->right, L->left
								     'is_disable'      => 0,
								 ]
						)
						
					)

                            );
	
	
    
?>