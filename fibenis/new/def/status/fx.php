<?PHP

    $temp = explode(':',$_GET['default_addon']);
    
    $status_code = $temp[0]; 
    
    $child_comm_id=$temp[1];
    
    $entity_code = substr($temp[0],0,2);
   
    $F_SERIES	=	array(
				#Title
				
				'title'	=>'Status',
				
				#Table field
                    
				'data'	=>   array(
						   
						   
						   '1' =>array( 'field_name'=> 'Detail ',
                                                               
								'field_id' => 'note',
                                                               
								'type' => 'textarea',
                                                               
                                                                'is_mandatory'=>0,
                                                               
                                                             ),
						   
						   '2' =>array(     'field_name'=>'Status',
                                                               
								    'field_id'=>'status_code',
                                                               
								    'type' => 'option',
							       
								    'option_data'=>$G->option_builder('entity_child_base','token,sn',"WHERE token IN (SELECT status_code_end FROM status_map WHERE status_code_start='$status_code') ORDER BY sn ASC"),
								 
								    'is_mandatory'=>1,
                                                               
								    'input_html'=>'class="w_200"'
								    
                                                               ),
						   
					    ),
				
                                    
				#Table Name
				
				'table_name'    => 'status_info',
				
				#Primary Key
                                
			        'key_id'        => 'id',
				
				'default_fields'    => array(
							    'entity_code' => "'$entity_code'",
							    'child_comm_id' => "'$child_comm_id'",
							    ),
				
				# Default Additional Column
                                
				'is_user_id'       => 'user_id',
				
				'back_to'  => NULL,
				
				# Communication
								
				//'prime_index'   => 1,
                                
				//'message'	=> '(SELECT sn FROM entity_child_base WHERE token = status_code)',
				
				'flat_message' => 'Sucesssfully Updated',
				
				# File Include
                                
				'page_code'	=> 'FASA',
				
				'show_query'    =>0,
				
				'after_add_update'=>1,
                                
			);
    
    function after_add_update($key_id){
	
	    global $F_SERIES,$rdsql,$USER_ROLE,$USER_ID,$F_SERIES;
	    
	    $status_info = $rdsql->exec_query("SELECT status_code,entity_code,child_comm_id FROM status_info WHERE id=$key_id","SELECT Status Information");
		
	    $status_info_row=$rdsql->data_fetch_row($status_info);
	    
	    $payment_status = $rdsql->exec_query("SELECT get_ec_parent_id_ifnull($status_info_row[2],'PA')","Payment Status");
		
	    $payment_status_row=$rdsql->data_fetch_row($payment_status);
	
	    if(($status_info_row[1] == 'SA') && ($payment_status_row[0]!=0)){
		
		    $ec = $rdsql->exec_query("SELECT code FROM entity_attribute WHERE entity_code = '$status_info_row[1]'","SELECTION Failed");
		
		    $ec_val = $rdsql->data_fetch_row($ec);
		    
		    $update = $rdsql->exec_query("UPDATE exav_addon_exa_token SET exa_value_token = '$status_info_row[0]' WHERE exa_token = '$ec_val[0]'  AND parent_id = '$status_info_row[2]'","Updation of status Failed");
		
	    }
	    
	    return null;	
		
    } 
?>