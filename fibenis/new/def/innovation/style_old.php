<style>
    
    
				body{
					background-color: #f9f9f9 !important;
				}
				
				fieldset{
					width:75% !important;
				}
				
				.form_action_row,
				.form_action_row .label,
				.form_action_row .value{
					background-color: #f9f9f9 !important;
				}
				
				.form_action_row .value{
				    padding-left:30px !important;
				}
				
				#SAVE,
				#ADD{
				    float: right;
				    
				}
				#SAVE{
				     margin-right: 10px;
				}
    
    
		    /*External Attribute*/
    
			    
			    
    /*fibentable label*/    .FATD .label,
			    .FAEX .label,
    /*fibentable label*/    .FAPDG .label,
			    .FAIS .label,
			    .FASN span.label,
			    .FALS .label,
			    /*.FAPROSTA .label,*/
  /*Fiben Table Heading*/   .FABUDGET .label,
			    .FAPAYDET .label,
			    .FAPAYIMG .label{
				    white-space: normal !important;
				    color: #000000;
				    line-height: 22px;
				    display	:inline-block !important;
				    margin-left :3% !important;
				    margin-right:2% !important;
				    width	:20% !important;	
			    }
			    
			    
			    
			    .FAEX .value,
			    .FAIS .value,
			    .FASN span.value,
			    .FALS .value,
			    /*.FAPROSTA .value,*/
			    .FAPAYDET .value,
			    .FAPAYIMG .value{
				    color: #0056b7; 
				    display	:inline-block !important;
				    width	:60% !important;
			    }
			    
			    /*Basic Application Form*/
			    
			    .FADOB .label,
			    .FAGEN .label,
			    .FACOM .label{
				    white-space: normal !important;
				    color: #000000;
				    line-height: 22px;
				    display	:inline-block;
				    margin-left :3% !important;
				    margin-right:2% !important;
				    width	:30%;
			    }
			    
			    .FADOB  .value,
			    .FAGEN .value,
			    .FACOM .value{
				    color: #0056b7; 
				    display	:inline-block;
				    width	:50%;
			    }
			    
			    /*If Start Up*/
			    /*-----------*/
			    
			    .FASN .label,
			    .FASD .label,
			    .FASNOD .label,
			    .FASNOE .label{
				white-space: normal !important;
				    color: #000000;
				    line-height: 22px;
				    display	:inline-block;
				    margin-left :3% !important;
				    margin-right:2% !important;
				    width	:20%;
				
			    }
			    
			     .FASN .value,
			     .FASD .value,
			     .FASNOD .value,
			     .FASNOE .value{
				color: #0056b7; 
				    display	:inline-block;
				    width	:60%;
			     }
			    
			    /*Applicant Category & Team Detail*/
			    
			    
			    .FACT .label,
			    .FATM .label{
				    white-space: normal !important;
				    color: #000000;
				    line-height: 22px;
				    display	:inline-block;
				    margin-left :3% !important;
				    margin-right:2% !important;
				    width	:30%;
			    }
			    
			    
			    .FACT .value,
			    .FATM .value{
				    color: #0056b7; 
				    display	:inline-block;
				    width	:50%;
			    }
			    
			    
			    
			    /*Details of Innovation*/
			    
			    .TITLE .label,
			    .FAPROADD .label,
			    .FAALTSOL .label,
			    .FAADVSOL .label,
			    .FAINVDIF .label{
				    white-space: normal !important;
				    color: #000000;
				    line-height: 22px;
				    display	:inline-block;
				    margin-left :3% !important;
				    margin-right:2% !important;
				    width	:30%;
			    }
			    
			    .TITLE .value,
			    .FAPROADD .value,
			    .FAALTSOL .value,
			    .FAADVSOL .value,
			    .FAINVDIF .value{
				    color: #0056b7; 
				    display	:inline-block;
				    width	:50%;
			    }
			    
			    /*Details of Incubation & Other Support*/
			    
			    
			    .FAREASUP .label,
			    .FAINFRA .label,
			    .FAEVAL .label,
			    .FAADVCLI .label,
			    .FAANICELL .label,
			    .FACOLHUM .label{
				white-space: normal !important;
				    color: #000000;
				    line-height: 22px;
				    display	:inline-block;
				    margin-left :3% !important;
				    margin-right:2% !important;
				    width	:35%;	
			    }
			    
			    
			    .FAREASUP .value,
			    .FAINFRA .value,
			    .FAEVAL .value,
			    .FAADVCLI .value,
			    .FAANICELL .value,
			    .FACOLHUM .value{
				    color: #0056b7; 
				    display	:inline-block;
				    width	:50%;
			    }
			    
			    /*Upload Document*/
			    .FAPROSTA .label,
			    .FAPROSTAII .label,
			    .FAPROSTAIII .label,
			    .FAPROSTAIV .label,
			    .FAPROSTAV .label,
			    .FAPROSTAVI .label,
			    .FAPROSTAVII .label,
			    .FAPROSTAVIII .label,
			    .FAPROSTAIX .label{
				    white-space: normal !important;
				    color: #000000;
				    line-height: 22px;
				    display	:inline-block;
				    margin-left :3% !important;
				    margin-right:2% !important;
				    width	:30%;
			    }
			    
			     .FAPROSTA .value,
			     .FAPROSTAII .value,
			     .FAPROSTAIII .value,
			     .FAPROSTAIV .value,
			     .FAPROSTAV .value,
			     .FAPROSTAVI .value,
			     .FAPROSTAVII .value,
			     .FAPROSTAVIII .value,
			     .FAPROSTAIX .value{
				    color: #0056b7; 
				    display	:inline-block;
				    width	:50%;
			     }
			     
			     
			     
			     /*Footer*/
			     
			     footer{
				display:none !important;
			    }
			    
			    footer .container{
				display:none !important;
			    }
  
    
    
</style>