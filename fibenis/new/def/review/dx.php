<?PHP
			
	$default_addon = $_GET['default_addon'];
    
	$LAYOUT	    	= 'layout_full';
               
        $D_SERIES       =   array(
                                   'title'=>'Review',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
				    
				    'gx' => 1,
				    
                                    
                                    #table data
                                    
                                    'data'=> array( 
						        1=>array('th'=>'Date & Time ',
								
								'field'	=> "date_format(updated_on,'%d-%b-%Y %T')",
                                                                
								'td_attr' => ' class="label_father align_LM" width="10%"',
								
								'is_sort' => 0,	
								
								),
							
							3=>array('th'=>'Note ',
								
								'field' =>"get_exav_addon_text(id,'RENO')",
								    
								'td_attr' => ' class="label_father align_LM" width="90%"',
								
								'is_sort' => 0,	
								
								),
							
							
					
                                                    ),
				    
					
                                    #Table Info
                                    
                                    'table_name' =>'entity_child',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
				    
				    'key_filter'     =>	 " AND entity_code='RE' AND parent_id=$default_addon",
				    
                                    # Communication
                                
                                    'prime_index'   => 1,
                                
				
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>0,'page_link'=>'', 'b_name' => '' ),
								
					'del_permission' => array('able_del'=>0,'user_flage'=>0), 
								
					'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  ''),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'REVI',
				
				'show_query'=>0,
				
				'hide_show_all' => 1,
				
				'search_filter_off'	=>1,
                            
                            );
    
?>