<?PHP

    $parent_id = $_GET['default_addon'];
    
    $F_SERIES	=	array(
				#Title
				
				'title'	=>'Payment',
				
				#Table field
                    
				'data'	=>   array(
						   
						   
						   
						   '1' 	=>array('field_name'=> 'Image ',
                                                               
							       'field_id' => 'image',
                                                               
							       'type' => 'file',
							                                                                      
							       'upload_type' => 'image',
							    
							       'allow_ext'   => array('jpg','jpeg','png'),
							       							       
							       'save_file_name_prefix'=> 'pay_',

							       'location'    => 'images/entity_child/',          // attribute code
                                                               
							       'is_mandatory'=>1,
                                                               
							       'input_html'=>'class="w_200"',
								    
                                                        ),
						   
						    '2' =>array( 'field_name'=> 'Note ',
                                                               
                                                               'field_id' => 'exa_value',
                                                               
                                                               'type' => 'textarea',
							       
							       'child_table'         => 'exav_addon_text', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'exa_token',   // attribute code field
							       
							       'child_attr_code'     => 'PANO',           // attribute code
							       
							       'is_mandatory'=>1,
							       
							       'allow'	=> 'x1028',
                                                               
                                                               ),
						   
					    ),
				
                                    
				#Table Name
				
				'table_name'    => 'entity_child',
				
				#Primary Key
                                
			        'key_id'        => 'id',
				
				'default_fields'    => array(
							    'entity_code' => "'PA'",
							    'parent_id' => "'$parent_id'",
							    ),
				
				# Default Additional Column
                                
				'is_user_id'       => 'user_id',
				
				'back_to'  => NULL,
				
				# Communication
								
				//'prime_index'   => 1,
                               
			       'flat_message' => 'Sucesssfully Updated',
				
				# File Include
                                
				'page_code'	=> 'PAYM',
				
				'show_query'    =>0,
				
				'after_add_update'=>1,
                                
			);
    
     function after_add_update($key_id){
	
	header('Location:?fx=payment&menu_off=1&mode=simple');
    
    }
?>