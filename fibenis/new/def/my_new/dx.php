<?PHP 

    $D_SERIES   =   array(

                        #Table Title
                        'title'                 =>  'New Data Table',

                        #Table Name 
                        'table_name'            =>  'entity_child',

                        #Key ID
                        'key_id'                =>  1,

                        #Query Show
                        #'show_query'            =>  1,

                        #To avoid transcation key on url bar
                        'avoid_trans_key_direct'    =>  1,

                        'add_button'            =>  array(

                                                        'is_add'    =>  1,
                                                        'page_link' =>  'fx=my_new',
                                                        'b_name'    =>  'Add Patient',

                                                        ),//end of add button...

                        'key_filter'            =>  "AND entity_code = 'NW'",
                        
                        
                        #custom filter
                    'custom_filter'         =>  array(

                                                    array(

                                                        'field_name'        =>  'Gender',
                                                        'field_id'          =>  'cf1',
                                                        'field_type'        =>  'option_list',
                                                        'option_value'      =>  $G->option_builder('entity_gender','code,sn'," WHERE 1=1"),
                                                        'html'              =>  'title="Select Gender"  data-width="160px"',
                                                        'cus_default_label' =>  'Show All',
                                                        'filter_by'         => "get_exav_addon_varchar(entity_child.id,'PTGN')",
                                                    ),

                                    ),//custom filter ends here...
                    
                    
                    #search info
                    'search'        =>      array(

                                                array(

                                                    'data'  =>  array(

                                                        'table_name'    =>  'entity_child',
                                                        'field_id'      =>  'id',
                                                        'field_name'    =>  "get_exav_addon_varchar(entity_child.id,'PTNA1')",
                                                        'filter'        =>  "AND entity_code = 'PT'",

                                                    ),//data array ends here...

                                                        'title'                 => 'Name',
                                                        'search_key'            =>  'id',
                                                        'is_search_by_text'     =>  0, 

                                                    ),

                                                array(

                                                    'data'  =>  array(

                                                        'table_name'    =>  'entity_child',
                                                        'field_id'      =>  'id',
                                                        'field_name'    =>  "get_exav_addon_varchar(entity_child.id,'PTMN')",
                                                        'filter'        =>  "AND entity_code = 'PT'",

                                                    ),//data array ends here...

                                                        'title'                 => 'Mobile Number',
                                                        'search_key'            =>  'id',
                                                        'is_search_by_text'     =>  0, 

                                                ),

                        ),//search array ends here...



                        'data'                  =>  array(

                                                '1' =>  array(

                                                        'th'        =>  'Name',
                                                        'is_sort'   =>  1,
                                                        'field'     =>  "get_exav_addon_varchar
                                                                        (entity_child.id,'NWNA')",

                                                ),

                                                '2' =>  array(

                                                                'th'        =>  'Age',
                                                                #'field'    ss=>  "get_exav_addon_varchar
                                                                                #(entity_child.id,'NWAG')",
                                                                'field'     =>  "(SELECT exa_value FROM exav_addon_varchar
                                                                                        WHERE parent_id = entity_child.id AND exa_token = 'NWAG')" ,

                                                        ),

                                                '3' =>  array(

                                                                'th'        =>  'Gender',
                                                                'is_sort'   =>  1,
                                                                'field'     =>  "(SELECT exa_value FROM exav_addon_varchar
                                                                                                WHERE parent_id = entity_child.id AND exa_token = 'NWGN')" ,

                                                        ),

                                                '4' =>  array(

                                                                'th'        =>  'Date',
                                                                'is_sort'   =>  1,
                                                                'field'     =>  "get_exav_addon_date
                                                                                (entity_child.id,'NWDA')",

                                                        ),

                                                '5' =>  array(

                                                                'th'    =>  'Purpose of Visit',
                                                                'field' =>  "get_exav_addon_varchar
                                                                                (entity_child.id,'NWVI')",

                                                        ),

                                                /* '' =>  array(
                                                        
                                                                'th'    =>  'Picture',
                                                                'field' =>  "get_exav_addon_varchar
                                                                                (entity_child.id,'NWPH')", 

                                                        ),*/
                                                        
                                                '6' =>  array(

                                                                'th'        =>    'Payment Status',
                                                                'field'     =>    "get_exav_addon_varchar
                                                                                        (entity_child.id,'NWPY')",
                                                        ),

                                                ),//End of data array....

                        );//end of D_Series...
                                                   
?>