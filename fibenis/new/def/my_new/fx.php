<?PHP 

    $F_SERIES   =   array(

                    $today      =   date("d-m-y"),
                
                    #form title
                    'title'                     =>  'New Data',

                    #table name
                    'table_name'                =>  'entity_child',

                    #Unique Key ID
                    'key_id'                    =>  'id',

                    #session
                    #'session_off'              =>1,

                    #To show query
                    'show_query'                =>  1,

                    #Tab view of multiple forms
                    'divider'                   =>  'tab',

                    #message to display
                    'flat_message'              =>  "New Data Successfully Added!!!",
                    
                    #To display particluar Data added...
                    #'prime_index'              =>  1,

                    #To avoid transcation key on url bar
                    #'avoid_trans_key_direct'    =>  1,

                    #back button

                    'back_to'                   =>  array(

                                                    'is_back_button'    =>  1,
                                                    'back_link'         =>  '?dx=my_new',
                                                    'BACK_NAME'         =>  'Back',
 
                                                ),  //End of Back button array....

                    #default fields
                    'default_fields'            =>  array(

                                                    'entity_code'   =>  "'NW'",

                                                ), //End of default fields array....

    
                    #user id
                    'is_user_id'                =>  'user_id',


                    #Form Fields...

                    'data'                      =>  array(

                                                    '0' =>  array(

                                                            'field_name'            =>  'New Form',
                                                            'type'                  =>  'heading', 
                                                ),

                                                    '1' =>  array(

                                                            'field_name'            =>  'Name',
                                                            'field_id'              =>  'exa_value',
                                                            'type'                  =>  'text',
                                                            'is_mandatory'          =>  1,
                                                            'hint'                  =>  'Enter Name',
                                                            'parent_field_id'       =>  'parent_id',
                                                            'child_table'           =>  'exav_addon_varchar',
                                                            'child_attr_field_id'   => 'exa_token',
                                                            'child_attr_code'       =>  'NWNA',

                                                ),

                                                    '2' =>  array(

                                                            'field_name'            =>  'Gender',
                                                            'hint'                  =>  'Select Gender',
                                                            'field_id'              =>  'exa_value',
                                                            'type'                  =>  'option',
                                                            'option_data'           =>  '<option value  = Male>     Male   </option>
                                                                                         <option value  = Female>   Female </option>
                                                                                         <option value  = Others>   Others </option>',
                                                            'avoid_default_option'  =>  1,
                                                            'is_mandatory'          =>  1,
                                                            'parent_field_id'       =>  'parent_id',
                                                            'child_table'           =>  'exav_addon_varchar',
                                                            'child_attr_field_id'   => 'exa_token',
                                                            'child_attr_code'       =>  'NWGN',

                                                ),


                                                    '3' =>  array(

                                                            'field_name'            =>  'Age',
                                                            'hint'                  =>  'Enter Age',
                                                            'field_id'              =>  'exa_value',
                                                            'type'                  =>  'text',
                                                            'allow'                 =>  'd2',
                                                            'avoid_default_option'  =>  1,
                                                            'is_mandatory'          =>  1,
                                                            'parent_field_id'       =>  'parent_id',
                                                            'child_table'           =>  'exav_addon_varchar',
                                                            'child_attr_field_id'   => 'exa_token',
                                                            'child_attr_code'       =>  'NWAG',


                                                ),

                                                    '4' =>  array(

                                                            'field_name'            =>  'Date',
                                                            'hint'                  =>  'Enter Date',
                                                            'default_date'          =>  $today,
                                                            'year_range'            =>  '2019 : 2035',
                                                            'field_id'              =>  'exa_value',
                                                            'type'                  =>  'date',
                                                            'is_mandatory'          =>  1,
                                                            'parent_field_id'       =>  'parent_id',
                                                            'child_table'           =>  'exav_addon_date',
                                                            'child_attr_field_id'   => 'exa_token',
                                                            'child_attr_code'       =>  'NWDA',


                                                ),

                                                    '5' =>  array(

                                                            'field_name'            =>  'Purpose of Visit',
                                                            'field_id'              =>  'exa_value',
                                                            'type'                  =>  'textarea',
                                                            'parent_field_id'       =>  'parent_id',
                                                            'child_table'           =>  'exav_addon_varchar',
                                                            'child_attr_field_id'   => 'exa_token',
                                                            'child_attr_code'       =>  'NWVI',


                                                ),

                                                    /*'' =>  array(

                                                            'field_name'            =>  'Patient Photo',
                                                            'field_id'              =>  'exa_value',
                                                            'type'                  =>  'file',
                                                            'upload_type'           =>  'image',
                                                            'allow_ext'             =>  array('png','jpg','jpeg'),
                                                            'location'              =>  'images/profile/',
                                                            'parent_field_id'       =>  'parent_id',
                                                            'child_table'           =>  'exav_addon_varchar',
                                                            'child_attr_field_id'   =>  'exa_token',
                                                            'child_attr_code'       =>  'NWPH',


                                                ),*/

                                                    '6' =>  array(

                                                            'field_name'            =>  'Payment Status',
                                                            'field_id'              =>  'exa_value',
                                                            'type'                  =>  'toggle',
                                                            'is_round'              =>  1,
                                                            'on_label'              =>  'PAID',
                                                            'off_label'             =>  'NOT PAID',
                                                            'hint'                  =>  'Patient Type',
                                                            'parent_field_id'       =>  'parent_id',
                                                            'child_table'           =>  'exav_addon_varchar',
                                                            'child_attr_field_id'   =>  'exa_token',
                                                            'child_attr_code'       =>  'NWPY',
                                                ),

                        ),//end of data...

    );//end of F_series...

?>
