-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: db87a.pair.com
-- Generation Time: Feb 05, 2019 at 06:25 AM
-- Server version: 5.6.31-log
-- PHP Version: 5.6.30

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Stand-in structure for view `application_count`
-- (See below for the actual view)
--
CREATE TABLE `application_count` (
`entity_code` char(2)
,`count` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `application_status`
-- (See below for the actual view)
--
CREATE TABLE `application_status` (
`entity_code` char(2)
,`total_count` bigint(21)
,`submitted_count` bigint(21)
,`under_review_count` bigint(21)
,`reviewed_count` bigint(21)
,`accepted_count` bigint(21)
,`rejected_count` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `communication`
--

CREATE TABLE `communication` (
  `id` int(11) NOT NULL,
  `comm_group_code` char(4) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL COMMENT 'Parent Contact id',
  `sn` varchar(63) DEFAULT NULL,
  `ln` varchar(255) DEFAULT NULL,
  `add_i` varchar(255) DEFAULT NULL,
  `add_ii` varchar(255) DEFAULT NULL,
  `pincode` varchar(11) DEFAULT NULL,
  `phone` varchar(63) DEFAULT NULL,
  `mobile` varchar(63) DEFAULT NULL,
  `fax_no` varchar(63) DEFAULT NULL,
  `email` varchar(127) DEFAULT NULL,
  `website` varchar(63) DEFAULT NULL,
  `is_org` tinyint(1) NOT NULL,
  `detail` text,
  `user_id` int(11) NOT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_img` text,
  `is_active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `eav_addon_date`
--

CREATE TABLE `eav_addon_date` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `ea_code` varchar(16) DEFAULT NULL,
  `ea_value` date DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `eav_addon_ec_id`
--

CREATE TABLE `eav_addon_ec_id` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `ea_code` varchar(16) DEFAULT NULL,
  `ec_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `eav_addon_exa_token`
--

CREATE TABLE `eav_addon_exa_token` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `ea_code` varchar(16) NOT NULL,
  `exa_value_token` varchar(32) NOT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `eav_addon_text`
--

CREATE TABLE `eav_addon_text` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `ea_code` char(4) DEFAULT NULL,
  `ea_value` text,
  `user_id` int(11) NOT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `eav_addon_varchar`
--

CREATE TABLE `eav_addon_varchar` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `ea_code` char(4) DEFAULT NULL,
  `ea_value` varchar(1028) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ecb_av_addon_varchar`
--

CREATE TABLE `ecb_av_addon_varchar` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `ea_code` char(4) DEFAULT NULL,
  `ea_value` varchar(1028) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ecb_parent_child_matrix`
--

CREATE TABLE `ecb_parent_child_matrix` (
  `id` int(11) NOT NULL,
  `ecb_parent_id` int(11) NOT NULL,
  `ecb_child_id` int(11) NOT NULL,
  `parent_child_hash` char(32) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `entity`
--

CREATE TABLE `entity` (
  `id` int(11) NOT NULL,
  `code` char(2) DEFAULT NULL,
  `sn` varchar(100) DEFAULT NULL,
  `ln` varchar(100) DEFAULT NULL,
  `creation` datetime DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `parent_code` char(2) DEFAULT 'BS'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `entity_attribute`
--

CREATE TABLE `entity_attribute` (
  `id` int(11) NOT NULL,
  `entity_code` char(2) DEFAULT NULL,
  `code` char(4) DEFAULT NULL,
  `sn` varchar(50) DEFAULT NULL,
  `ln` varchar(50) DEFAULT NULL,
  `line_order` smallint(6) NOT NULL DEFAULT '0',
  `creation` datetime DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `entity_child`
--

CREATE TABLE `entity_child` (
  `id` int(11) NOT NULL,
  `sn` varchar(150) DEFAULT NULL,
  `ln` varchar(150) DEFAULT NULL,
  `detail` text,
  `code` varchar(50) DEFAULT NULL,
  `entity_code` char(2) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `status_code` char(4) DEFAULT NULL,
  `line_order` decimal(6,2) NOT NULL,
  `date` datetime DEFAULT NULL,
  `date_II` datetime DEFAULT NULL,
  `created_by` int(6) DEFAULT NULL,
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(4) DEFAULT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `addon` text,
  `image` text,
  `image_a` varchar(1024) DEFAULT NULL,
  `image_b` varchar(1024) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `entity_child_base`
--

CREATE TABLE `entity_child_base` (
  `id` int(11) NOT NULL,
  `entity_code` char(2) DEFAULT NULL,
  `token` varchar(16) DEFAULT NULL,
  `sn` varchar(64) DEFAULT NULL,
  `ln` varchar(1024) DEFAULT NULL,
  `note` text,
  `parent_id` int(11) DEFAULT NULL,
  `dna_code` char(4) NOT NULL DEFAULT 'EBUC',
  `created_by` int(11) NOT NULL,
  `creation` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `line_order` decimal(6,2) NOT NULL DEFAULT '0.00',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `user_id` int(11) NOT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `entity_key_value`
--

CREATE TABLE `entity_key_value` (
  `id` int(11) NOT NULL,
  `entity_code` char(2) DEFAULT NULL,
  `domain_hash` varchar(32) NOT NULL,
  `entity_key` varchar(100) DEFAULT NULL,
  `entity_value` varchar(255) DEFAULT NULL,
  `creation` datetime DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `EP_technology_count`
-- (See below for the actual view)
--
CREATE TABLE `EP_technology_count` (
`technology` varchar(1028)
,`count` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `exav_addon_date`
--

CREATE TABLE `exav_addon_date` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `exa_token` varchar(16) DEFAULT NULL,
  `exa_value` date DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exav_addon_decimal`
--

CREATE TABLE `exav_addon_decimal` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `exa_token` varchar(16) DEFAULT NULL,
  `exa_value` decimal(14,4) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exav_addon_ec_id`
--

CREATE TABLE `exav_addon_ec_id` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `exa_token` varchar(16) DEFAULT NULL,
  `ec_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exav_addon_exa_token`
--

CREATE TABLE `exav_addon_exa_token` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `exa_token` varchar(32) NOT NULL,
  `exa_value_token` varchar(32) NOT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exav_addon_num`
--

CREATE TABLE `exav_addon_num` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `exa_token` varchar(32) NOT NULL,
  `exa_value` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exav_addon_text`
--

CREATE TABLE `exav_addon_text` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `exa_token` varchar(16) DEFAULT NULL,
  `exa_value` text,
  `user_id` int(11) NOT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exav_addon_varchar`
--

CREATE TABLE `exav_addon_varchar` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `exa_token` varchar(16) DEFAULT NULL,
  `exa_value` varchar(128) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `page_info`
--

CREATE TABLE `page_info` (
  `id` int(11) NOT NULL,
  `page_family` enum('D','F','E','A','M') DEFAULT NULL,
  `code` char(4) DEFAULT NULL,
  `sn` varchar(100) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `is_parent` tinyint(1) NOT NULL DEFAULT '0',
  `creation` datetime DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `report_setup`
--

CREATE TABLE `report_setup` (
  `id` int(11) NOT NULL,
  `report_type_code` char(4) DEFAULT NULL,
  `sn` text,
  `data_map` text,
  `map_navigator` text,
  `user_id` int(11) NOT NULL,
  `timestamp_punch` timestamp NULL DEFAULT NULL,
  `group_type` tinyint(1) DEFAULT '0',
  `series_type` varchar(255) DEFAULT NULL,
  `json_box` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `status_info`
--

CREATE TABLE `status_info` (
  `id` int(11) NOT NULL,
  `status_code` varchar(4) DEFAULT NULL,
  `entity_code` char(2) DEFAULT NULL,
  `child_comm_id` varchar(40) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `status_map`
--

CREATE TABLE `status_map` (
  `id` smallint(4) NOT NULL,
  `entity_code` char(2) DEFAULT NULL,
  `status_code_start` char(4) DEFAULT NULL,
  `status_code_base_10_start` int(11) NOT NULL DEFAULT '0',
  `status_code_end` char(4) DEFAULT NULL,
  `status_code_base_10_end` int(11) NOT NULL DEFAULT '0',
  `user_id` int(6) NOT NULL,
  `creation` datetime NOT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_log`
--

CREATE TABLE `sys_log` (
  `id` int(11) NOT NULL,
  `sys_access_ip` varchar(30) DEFAULT NULL,
  `sys_access_name` varchar(30) DEFAULT NULL,
  `page_code` char(32) DEFAULT NULL,
  `action` text,
  `action_type` char(4) DEFAULT NULL,
  `access_key` varchar(63) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `technology_count`
-- (See below for the actual view)
--
CREATE TABLE `technology_count` (
`technology` varchar(1028)
,`count` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `TP_technology_count`
-- (See below for the actual view)
--
CREATE TABLE `TP_technology_count` (
`technology` varchar(1028)
,`count` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `user_ec_info`
--

CREATE TABLE `user_ec_info` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `child_id` varchar(1024) NOT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `login_name` char(32) DEFAULT NULL,
  `email` varchar(75) DEFAULT NULL,
  `password` char(32) DEFAULT NULL,
  `user_role_id` int(11) DEFAULT NULL,
  `gender_code` char(4) DEFAULT NULL,
  `user_comm` char(32) DEFAULT NULL,
  `is_mail_check` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_send_mail` tinyint(1) NOT NULL,
  `is_send_welcome_mail` tinyint(1) NOT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_internal` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `user_info`
--
DELIMITER $$
CREATE TRIGGER `trg_user_info_after_ins` AFTER INSERT ON `user_info` FOR EACH ROW BEGIN
                                                                                                                        INSERT INTO user_info_log
                                                                                                                                  (email,gender_code,id,is_active,is_mail_check,is_send_mail,is_send_welcome_mail,last_login,login_name,password,timestamp_punch,user_comm,user_id,user_name,user_role_id,log_type_code)
                                                                                                                        VALUES
                                                                                                                                  (new.email,new.gender_code,new.id,new.is_active,new.is_mail_check,new.is_send_mail,new.is_send_welcome_mail,new.last_login,new.login_name,new.password,new.timestamp_punch,new.user_comm,new.user_id,new.user_name,new.user_role_id,'LTAD'); 
                                                                                                            END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trg_user_info_after_upd` AFTER UPDATE ON `user_info` FOR EACH ROW BEGIN
                                                                                                                                    INSERT INTO user_info_log
                                                                                                                                              (email,gender_code,id,is_active,is_mail_check,is_send_mail,is_send_welcome_mail,last_login,login_name,password,timestamp_punch,user_comm,user_id,user_name,user_role_id,log_type_code)
                                                                                                                                    VALUES
                                                                                                                                              (new.email,new.gender_code,new.id,new.is_active,new.is_mail_check,new.is_send_mail,new.is_send_welcome_mail,new.last_login,new.login_name,new.password,new.timestamp_punch,new.user_comm,new.user_id,new.user_name,new.user_role_id,'LTUD'); 
                                                                                                                        END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trg_user_info_before_del` BEFORE DELETE ON `user_info` FOR EACH ROW BEGIN
                                                                                                                                    INSERT INTO user_info_log
                                                                                                                                              (email,gender_code,id,is_active,is_mail_check,is_send_mail,is_send_welcome_mail,last_login,login_name,password,timestamp_punch,user_comm,user_id,user_name,user_role_id,log_type_code)
                                                                                                                                    VALUES
                                                                                                                                              (old.email,old.gender_code,old.id,old.is_active,old.is_mail_check,old.is_send_mail,old.is_send_welcome_mail,old.last_login,old.login_name,old.password,old.timestamp_punch,old.user_comm,old.user_id,old.user_name,old.user_role_id,'LTDL'); 
                                                                                                                        END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `user_info_log`
--

CREATE TABLE `user_info_log` (
  `log_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `login_name` char(32) DEFAULT NULL,
  `email` varchar(75) DEFAULT NULL,
  `password` char(32) DEFAULT NULL,
  `user_role_id` int(11) DEFAULT NULL,
  `gender_code` char(4) DEFAULT NULL,
  `user_comm` char(32) DEFAULT NULL,
  `is_mail_check` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_send_mail` tinyint(1) NOT NULL,
  `is_send_welcome_mail` tinyint(1) NOT NULL,
  `timestamp_punch` datetime DEFAULT NULL,
  `last_login` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log_type_code` enum('LTAD','LTUD','LTDL') DEFAULT NULL,
  `timestamp_log` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_permission`
--

CREATE TABLE `user_permission` (
  `id` int(11) NOT NULL,
  `sn` varchar(255) DEFAULT NULL,
  `code` char(255) DEFAULT NULL,
  `is_admin` tinyint(1) NOT NULL,
  `is_common` tinyint(1) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `creation` datetime DEFAULT CURRENT_TIMESTAMP,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `sn` char(3) DEFAULT NULL,
  `ln` varchar(100) DEFAULT NULL,
  `home_page_url` varchar(64) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `creation` datetime DEFAULT CURRENT_TIMESTAMP,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_role_permission`
--

CREATE TABLE `user_role_permission` (
  `id` int(6) NOT NULL,
  `user_role_id` tinyint(2) NOT NULL,
  `user_permission_ids` varchar(255) DEFAULT NULL,
  `creation` datetime NOT NULL,
  `user_id` int(6) NOT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_role_permission_matrix`
--

CREATE TABLE `user_role_permission_matrix` (
  `id` int(11) NOT NULL,
  `user_role_id` int(11) DEFAULT NULL,
  `user_permission_id` int(11) DEFAULT NULL,
  `creation` datetime DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  `timestamp_punch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure for view `application_count`
--
DROP TABLE IF EXISTS `application_count`;

CREATE ALGORITHM=UNDEFINED DEFINER=`1030551_14`@`209.68.5.46` SQL SECURITY DEFINER VIEW `application_count`  AS  select `entity_child`.`entity_code` AS `entity_code`,count(0) AS `count` from `entity_child` where `entity_child`.`entity_code` in (select `entity_child_base`.`token` from `entity_child_base` where (`entity_child_base`.`entity_code` in ('EP','TP')) order by `entity_child_base`.`line_order`) group by `entity_child`.`entity_code` order by `entity_child`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `application_status`
--
DROP TABLE IF EXISTS `application_status`;

CREATE ALGORITHM=UNDEFINED DEFINER=`1030551_14`@`209.68.5.46` SQL SECURITY DEFINER VIEW `application_status`  AS  select `entity_child`.`entity_code` AS `entity_code`,count(0) AS `total_count`,count((select `exavt`.`id` from `exav_addon_exa_token` `exavt` where ((`exavt`.`parent_id` = `entity_child`.`id`) and (`exavt`.`exa_token` = 'STAT') and (`exavt`.`exa_value_token` = 'SASU')))) AS `submitted_count`,count((select `exavt`.`id` from `exav_addon_exa_token` `exavt` where ((`exavt`.`parent_id` = `entity_child`.`id`) and (`exavt`.`exa_token` = 'STAT') and (`exavt`.`exa_value_token` = 'SAUR')))) AS `under_review_count`,count((select `exavt`.`id` from `exav_addon_exa_token` `exavt` where ((`exavt`.`parent_id` = `entity_child`.`id`) and (`exavt`.`exa_token` = 'STAT') and (`exavt`.`exa_value_token` = 'SARE')))) AS `reviewed_count`,count((select `exavt`.`id` from `exav_addon_exa_token` `exavt` where ((`exavt`.`parent_id` = `entity_child`.`id`) and (`exavt`.`exa_token` = 'STAT') and (`exavt`.`exa_value_token` = 'SAAC')))) AS `accepted_count`,count((select `exavt`.`id` from `exav_addon_exa_token` `exavt` where ((`exavt`.`parent_id` = `entity_child`.`id`) and (`exavt`.`exa_token` = 'STAT') and (`exavt`.`exa_value_token` = 'SARJ')))) AS `rejected_count` from `entity_child` where `entity_child`.`entity_code` in (select `entity_child_base`.`token` from `entity_child_base` where (`entity_child_base`.`entity_code` = 'EP') order by `entity_child_base`.`line_order`) group by `entity_child`.`entity_code` order by `entity_child`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `EP_technology_count`
--
DROP TABLE IF EXISTS `EP_technology_count`;

CREATE ALGORITHM=UNDEFINED DEFINER=`1030551_14`@`209.68.5.46` SQL SECURITY DEFINER VIEW `EP_technology_count`  AS  select `get_exav_addon_varchar`(`exav_addon_ec_id`.`ec_id`,'TDTN') AS `technology`,count(`exav_addon_ec_id`.`id`) AS `count` from `exav_addon_ec_id` where (`exav_addon_ec_id`.`ec_id` in (select `exav_addon_varchar`.`parent_id` from `exav_addon_varchar` where (`exav_addon_varchar`.`exa_token` = 'TDTN')) and (`exav_addon_ec_id`.`exa_token` = 'TDPT') and `exav_addon_ec_id`.`parent_id` in (select `entity_child`.`id` from `entity_child` where `entity_child`.`entity_code` in (select `entity_child_base`.`token` from `entity_child_base` where (`entity_child_base`.`entity_code` = 'EP')))) group by `exav_addon_ec_id`.`ec_id` order by `get_exav_addon_varchar`(`exav_addon_ec_id`.`ec_id`,'TDTN') ;

-- --------------------------------------------------------

--
-- Structure for view `technology_count`
--
DROP TABLE IF EXISTS `technology_count`;

CREATE ALGORITHM=UNDEFINED DEFINER=`1030551_14`@`209.68.5.46` SQL SECURITY DEFINER VIEW `technology_count`  AS  select `get_exav_addon_varchar`(`exav_addon_ec_id`.`ec_id`,'TDTN') AS `technology`,count(`exav_addon_ec_id`.`id`) AS `count` from `exav_addon_ec_id` where (`exav_addon_ec_id`.`ec_id` in (select `exav_addon_varchar`.`parent_id` from `exav_addon_varchar` where (`exav_addon_varchar`.`exa_token` = 'TDTN')) and (`exav_addon_ec_id`.`exa_token` = 'TDPT')) group by `exav_addon_ec_id`.`ec_id` order by `get_exav_addon_varchar`(`exav_addon_ec_id`.`ec_id`,'TDTN') ;

-- --------------------------------------------------------

--
-- Structure for view `TP_technology_count`
--
DROP TABLE IF EXISTS `TP_technology_count`;

CREATE ALGORITHM=UNDEFINED DEFINER=`1030551_14`@`209.68.5.46` SQL SECURITY DEFINER VIEW `TP_technology_count`  AS  select `get_exav_addon_varchar`(`exav_addon_ec_id`.`ec_id`,'TDTN') AS `technology`,count(`exav_addon_ec_id`.`id`) AS `count` from `exav_addon_ec_id` where (`exav_addon_ec_id`.`ec_id` in (select `exav_addon_varchar`.`parent_id` from `exav_addon_varchar` where (`exav_addon_varchar`.`exa_token` = 'TDTN')) and (`exav_addon_ec_id`.`exa_token` = 'TDPT') and `exav_addon_ec_id`.`parent_id` in (select `entity_child`.`id` from `entity_child` where `entity_child`.`entity_code` in (select `entity_child_base`.`token` from `entity_child_base` where (`entity_child_base`.`entity_code` = 'TP')))) group by `exav_addon_ec_id`.`ec_id` order by `get_exav_addon_varchar`(`exav_addon_ec_id`.`ec_id`,'TDTN') ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `communication`
--
ALTER TABLE `communication`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eav_addon_date`
--
ALTER TABLE `eav_addon_date`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_eav_addon_date_ea_code` (`ea_code`),
  ADD KEY `fk_eav_addon_date_parent_id` (`parent_id`);

--
-- Indexes for table `eav_addon_ec_id`
--
ALTER TABLE `eav_addon_ec_id`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_eav_addon_ec_id_ea_code` (`ea_code`),
  ADD KEY `fk_eav_addon_ec_id_ec_id` (`ec_id`),
  ADD KEY `fk_eav_addon_ec_id_parent_id` (`parent_id`);

--
-- Indexes for table `eav_addon_exa_token`
--
ALTER TABLE `eav_addon_exa_token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eav_addon_text`
--
ALTER TABLE `eav_addon_text`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `parent_id_2` (`parent_id`,`ea_code`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `fk_eav_addon_text_ea_code` (`ea_code`);

--
-- Indexes for table `eav_addon_varchar`
--
ALTER TABLE `eav_addon_varchar`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `parent_id_2` (`parent_id`,`ea_code`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `fk_eav_addon_varchar_ea_code` (`ea_code`);

--
-- Indexes for table `ecb_av_addon_varchar`
--
ALTER TABLE `ecb_av_addon_varchar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `fk_ecb_av_addon_varchar_ea_code` (`ea_code`);

--
-- Indexes for table `ecb_parent_child_matrix`
--
ALTER TABLE `ecb_parent_child_matrix`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ecb_parent_id` (`ecb_parent_id`),
  ADD KEY `ecb_child_id` (`ecb_child_id`);

--
-- Indexes for table `entity`
--
ALTER TABLE `entity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `code` (`code`),
  ADD KEY `id` (`id`),
  ADD KEY `timestamp_punch` (`timestamp_punch`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `entity_attribute`
--
ALTER TABLE `entity_attribute`
  ADD PRIMARY KEY (`id`),
  ADD KEY `code` (`code`),
  ADD KEY `entity_code` (`entity_code`),
  ADD KEY `id` (`id`),
  ADD KEY `timestamp_punch` (`timestamp_punch`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `entity_child`
--
ALTER TABLE `entity_child`
  ADD PRIMARY KEY (`id`),
  ADD KEY `code` (`code`),
  ADD KEY `entity_code` (`entity_code`),
  ADD KEY `id` (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `status_code` (`status_code`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `entity_child_base`
--
ALTER TABLE `entity_child_base`
  ADD PRIMARY KEY (`id`),
  ADD KEY `token` (`token`);

--
-- Indexes for table `entity_key_value`
--
ALTER TABLE `entity_key_value`
  ADD PRIMARY KEY (`id`),
  ADD KEY `entity_code` (`entity_code`),
  ADD KEY `id` (`id`),
  ADD KEY `timestamp_punch` (`timestamp_punch`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `exav_addon_date`
--
ALTER TABLE `exav_addon_date`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_exav_addon_date_parent_id` (`parent_id`),
  ADD KEY `fk_exav_addon_date_exa_code` (`exa_token`);

--
-- Indexes for table `exav_addon_decimal`
--
ALTER TABLE `exav_addon_decimal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `fk_exav_addon_decimal_exa_token` (`exa_token`);

--
-- Indexes for table `exav_addon_ec_id`
--
ALTER TABLE `exav_addon_ec_id`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_exav_addon_ec_id_ec_id` (`ec_id`),
  ADD KEY `fk_exav_addon_ec_id_parent_id` (`parent_id`),
  ADD KEY `fk_exav_addon_ec_id_exa_token` (`exa_token`);

--
-- Indexes for table `exav_addon_exa_token`
--
ALTER TABLE `exav_addon_exa_token`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_exav_addon_exa_token_exa_code` (`exa_token`),
  ADD KEY `fk_exav_addon_exa_token_parent_id` (`parent_id`);

--
-- Indexes for table `exav_addon_num`
--
ALTER TABLE `exav_addon_num`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exav_addon_num_ibfk_1` (`exa_token`),
  ADD KEY `exav_addon_num_ibfk_2` (`parent_id`);

--
-- Indexes for table `exav_addon_text`
--
ALTER TABLE `exav_addon_text`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `fk_exav_addon_text_exa_token` (`exa_token`);

--
-- Indexes for table `exav_addon_varchar`
--
ALTER TABLE `exav_addon_varchar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `fk_exav_addon_varchar_exa_token` (`exa_token`);

--
-- Indexes for table `page_info`
--
ALTER TABLE `page_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code_2` (`code`),
  ADD UNIQUE KEY `code_3` (`code`),
  ADD UNIQUE KEY `code_4` (`code`),
  ADD KEY `code` (`code`),
  ADD KEY `id` (`id`),
  ADD KEY `timestamp_punch` (`timestamp_punch`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `report_setup`
--
ALTER TABLE `report_setup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_type` (`group_type`),
  ADD KEY `id` (`id`),
  ADD KEY `report_type_code` (`report_type_code`),
  ADD KEY `timestamp_punch` (`timestamp_punch`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `status_info`
--
ALTER TABLE `status_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `entity_code` (`entity_code`),
  ADD KEY `id` (`id`),
  ADD KEY `status_code` (`status_code`),
  ADD KEY `timestamp_punch` (`timestamp_punch`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `status_map`
--
ALTER TABLE `status_map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sys_log`
--
ALTER TABLE `sys_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `action_type` (`action_type`),
  ADD KEY `id` (`id`),
  ADD KEY `page_code` (`page_code`),
  ADD KEY `timestamp_punch` (`timestamp_punch`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user_ec_info`
--
ALTER TABLE `user_ec_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `is_active` (`is_active`),
  ADD KEY `timestamp_punch` (`timestamp_punch`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_role_id` (`user_role_id`);

--
-- Indexes for table `user_info_log`
--
ALTER TABLE `user_info_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `id` (`id`),
  ADD KEY `is_active` (`is_active`),
  ADD KEY `timestamp_punch` (`timestamp_punch`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_role_id` (`user_role_id`);

--
-- Indexes for table `user_permission`
--
ALTER TABLE `user_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sn` (`sn`),
  ADD UNIQUE KEY `sn_2` (`sn`),
  ADD UNIQUE KEY `sn_3` (`sn`),
  ADD KEY `code` (`code`),
  ADD KEY `id` (`id`),
  ADD KEY `timestamp_punch` (`timestamp_punch`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sn` (`sn`),
  ADD KEY `id` (`id`),
  ADD KEY `timestamp_punch` (`timestamp_punch`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user_role_permission`
--
ALTER TABLE `user_role_permission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_role_id` (`user_role_id`);

--
-- Indexes for table `user_role_permission_matrix`
--
ALTER TABLE `user_role_permission_matrix`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `timestamp_punch` (`timestamp_punch`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_permission_id` (`user_permission_id`),
  ADD KEY `user_role_id` (`user_role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `communication`
--
ALTER TABLE `communication`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `eav_addon_date`
--
ALTER TABLE `eav_addon_date`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=528;

--
-- AUTO_INCREMENT for table `eav_addon_ec_id`
--
ALTER TABLE `eav_addon_ec_id`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1201;

--
-- AUTO_INCREMENT for table `eav_addon_exa_token`
--
ALTER TABLE `eav_addon_exa_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=642;

--
-- AUTO_INCREMENT for table `eav_addon_text`
--
ALTER TABLE `eav_addon_text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `eav_addon_varchar`
--
ALTER TABLE `eav_addon_varchar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6984;

--
-- AUTO_INCREMENT for table `ecb_av_addon_varchar`
--
ALTER TABLE `ecb_av_addon_varchar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37087;

--
-- AUTO_INCREMENT for table `ecb_parent_child_matrix`
--
ALTER TABLE `ecb_parent_child_matrix`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=487;

--
-- AUTO_INCREMENT for table `entity`
--
ALTER TABLE `entity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;

--
-- AUTO_INCREMENT for table `entity_attribute`
--
ALTER TABLE `entity_attribute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=408;

--
-- AUTO_INCREMENT for table `entity_child`
--
ALTER TABLE `entity_child`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4573;

--
-- AUTO_INCREMENT for table `entity_child_base`
--
ALTER TABLE `entity_child_base`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1349;

--
-- AUTO_INCREMENT for table `entity_key_value`
--
ALTER TABLE `entity_key_value`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=371;

--
-- AUTO_INCREMENT for table `exav_addon_date`
--
ALTER TABLE `exav_addon_date`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2627;

--
-- AUTO_INCREMENT for table `exav_addon_decimal`
--
ALTER TABLE `exav_addon_decimal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=409;

--
-- AUTO_INCREMENT for table `exav_addon_ec_id`
--
ALTER TABLE `exav_addon_ec_id`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=738;

--
-- AUTO_INCREMENT for table `exav_addon_exa_token`
--
ALTER TABLE `exav_addon_exa_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=882;

--
-- AUTO_INCREMENT for table `exav_addon_num`
--
ALTER TABLE `exav_addon_num`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18738;

--
-- AUTO_INCREMENT for table `exav_addon_text`
--
ALTER TABLE `exav_addon_text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86117;

--
-- AUTO_INCREMENT for table `exav_addon_varchar`
--
ALTER TABLE `exav_addon_varchar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37992;

--
-- AUTO_INCREMENT for table `page_info`
--
ALTER TABLE `page_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `report_setup`
--
ALTER TABLE `report_setup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `status_info`
--
ALTER TABLE `status_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `status_map`
--
ALTER TABLE `status_map`
  MODIFY `id` smallint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sys_log`
--
ALTER TABLE `sys_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129715;

--
-- AUTO_INCREMENT for table `user_ec_info`
--
ALTER TABLE `user_ec_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=616;

--
-- AUTO_INCREMENT for table `user_info_log`
--
ALTER TABLE `user_info_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6606;

--
-- AUTO_INCREMENT for table `user_permission`
--
ALTER TABLE `user_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user_role_permission`
--
ALTER TABLE `user_role_permission`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_role_permission_matrix`
--
ALTER TABLE `user_role_permission_matrix`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5231;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `eav_addon_date`
--
ALTER TABLE `eav_addon_date`
  ADD CONSTRAINT `fk_eav_addon_date_ea_code` FOREIGN KEY (`ea_code`) REFERENCES `entity_attribute` (`code`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_eav_addon_date_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `entity_child` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `eav_addon_ec_id`
--
ALTER TABLE `eav_addon_ec_id`
  ADD CONSTRAINT `fk_eav_addon_ec_id_ea_code` FOREIGN KEY (`ea_code`) REFERENCES `entity_attribute` (`code`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_eav_addon_ec_id_ec_id` FOREIGN KEY (`ec_id`) REFERENCES `entity_child` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_eav_addon_ec_id_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `entity_child` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `eav_addon_text`
--
ALTER TABLE `eav_addon_text`
  ADD CONSTRAINT `fk_eav_addon_text_ea_code` FOREIGN KEY (`ea_code`) REFERENCES `entity_attribute` (`code`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_eav_addon_text_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `entity_child` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `eav_addon_varchar`
--
ALTER TABLE `eav_addon_varchar`
  ADD CONSTRAINT `fk_eav_addon_varchar_ea_code` FOREIGN KEY (`ea_code`) REFERENCES `entity_attribute` (`code`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_eav_addon_varchar_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `entity_child` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `ecb_av_addon_varchar`
--
ALTER TABLE `ecb_av_addon_varchar`
  ADD CONSTRAINT `fk_ecb_av_addon_varchar_ea_code` FOREIGN KEY (`ea_code`) REFERENCES `entity_attribute` (`code`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ecb_av_addon_varchar_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `entity_child_base` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `ecb_parent_child_matrix`
--
ALTER TABLE `ecb_parent_child_matrix`
  ADD CONSTRAINT `fk_ecb_parent_child_matrix_ecb_child_id` FOREIGN KEY (`ecb_child_id`) REFERENCES `entity_child_base` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ecb_parent_child_matrix_ecb_parent_id` FOREIGN KEY (`ecb_parent_id`) REFERENCES `entity_child_base` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `entity_attribute`
--
ALTER TABLE `entity_attribute`
  ADD CONSTRAINT `fk_entity_attribute_entity_code` FOREIGN KEY (`entity_code`) REFERENCES `entity` (`code`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `entity_child`
--
ALTER TABLE `entity_child`
  ADD CONSTRAINT `fk_entity_child_entity_code` FOREIGN KEY (`entity_code`) REFERENCES `entity` (`code`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `entity_key_value`
--
ALTER TABLE `entity_key_value`
  ADD CONSTRAINT `fk_entity_key_value_entity_code` FOREIGN KEY (`entity_code`) REFERENCES `entity` (`code`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `exav_addon_date`
--
ALTER TABLE `exav_addon_date`
  ADD CONSTRAINT `fk_exav_addon_date_exa_code` FOREIGN KEY (`exa_token`) REFERENCES `entity_child_base` (`token`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_exav_addon_date_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `entity_child` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `exav_addon_decimal`
--
ALTER TABLE `exav_addon_decimal`
  ADD CONSTRAINT `fk_exav_addon_decimal_exa_token` FOREIGN KEY (`exa_token`) REFERENCES `entity_child_base` (`token`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_exav_addon_decimal_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `entity_child` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `exav_addon_ec_id`
--
ALTER TABLE `exav_addon_ec_id`
  ADD CONSTRAINT `fk_exav_addon_ec_id_ec_id` FOREIGN KEY (`ec_id`) REFERENCES `entity_child` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_exav_addon_ec_id_exa_token` FOREIGN KEY (`exa_token`) REFERENCES `entity_child_base` (`token`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_exav_addon_ec_id_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `entity_child` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `exav_addon_exa_token`
--
ALTER TABLE `exav_addon_exa_token`
  ADD CONSTRAINT `fk_exav_addon_exa_token_exa_code` FOREIGN KEY (`exa_token`) REFERENCES `entity_child_base` (`token`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_exav_addon_exa_token_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `entity_child` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `exav_addon_num`
--
ALTER TABLE `exav_addon_num`
  ADD CONSTRAINT `exav_addon_num_ibfk_1` FOREIGN KEY (`exa_token`) REFERENCES `entity_child_base` (`token`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `exav_addon_num_ibfk_2` FOREIGN KEY (`parent_id`) REFERENCES `entity_child` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `exav_addon_text`
--
ALTER TABLE `exav_addon_text`
  ADD CONSTRAINT `fk_exav_addon_text_exa_token` FOREIGN KEY (`exa_token`) REFERENCES `entity_child_base` (`token`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_exav_addon_text_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `entity_child` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `exav_addon_varchar`
--
ALTER TABLE `exav_addon_varchar`
  ADD CONSTRAINT `fk_exav_addon_varchar_exa_token` FOREIGN KEY (`exa_token`) REFERENCES `entity_child_base` (`token`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_exav_addon_varchar_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `entity_child` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `user_info`
--
ALTER TABLE `user_info`
  ADD CONSTRAINT `fk_user_info_user_role_id` FOREIGN KEY (`user_role_id`) REFERENCES `user_role` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `user_role_permission_matrix`
--
ALTER TABLE `user_role_permission_matrix`
  ADD CONSTRAINT `fk_user_role_permission_matrix_user_role_id` FOREIGN KEY (`user_role_id`) REFERENCES `user_role` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
