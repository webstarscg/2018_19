<?PHP
$F_SERIES   =   array(

#Desk title

'title'         => 'Patient',

#table Name

'table_name'    =>  'patient',

#Unique Key ID

'key_id'        =>  'id',

#'session_off'   =>  1,

#'show_query'     =>  1,

'divider'   =>  'tab',

#'flat_message'  =>  'Patient Added Successfully',

'prime_index'   =>  1,

#'avoid_trans_key_direct'    =>  1,

'back_to'  => array( 
                     'is_back_button' =>1, 
                     'back_link'=>'?dx=',
                     'BACK_NAME'=>'Back'
                 ),

'default_fields'    =>  array(

                            'user_id'   =>  '1',

                        ),

#Table Field

'data'          =>  array(

                        '0' =>  array(

                                    'field_name'    => '1-Demo-Form',
                                    'type'          => 'heading',
                        ),
                        '1' =>  array(

                                    'field_name'    =>  'Text',
                                    'type'          =>  'text',
                        ),
                        '2' =>  array(
                            
                                    'field_name'    =>  'toggle',
                                    'type'          =>  'toggle',

                        ),
                        '3' =>  array(

                                    'field_name'    =>  'date',
                                    'type'          =>  'date',
                        ),
                        '4' =>  array(

                                    'field_name'    =>  'code-editor',
                                    'type'          =>  'code_editor',
                        ),
                        '5' =>  array(

                                    'field_name'    => 'Level1',
                                    'type'          => 'heading',
                        ),
                        '6' =>  array(

                                    'field_name'    =>  'password',
                                    'type'          =>  'password',
                        ),
                        '7' =>  array(
                            
                                    'field_name'    =>  'hidden',
                                    'type'          =>  'hidden',

                        ),
                        '8' =>  array(

                                    'field_name'    =>  'autocomplete',
                                    'type'          =>  'autocomplete',

                        ),
                        '9' =>  array(

                                    'field_name'    =>  'range',
                                    'type'          =>  'range',

                        ),
                        '10' =>  array(

                                    'field_name'    =>  'hms',
                                    'type'          =>  'hms',

                        ),
                        '11' => array(

                                    'field_name'    =>  'file',
                                    'type'          =>  'file',
                        ),
                        '12' => array(

                                    'field_name'    =>  'Text-Area',
                                    'type'          =>  'textarea',

                        ),
                        '13' => array(

                                    'field_name'    =>  'Option',
                                    'type'          =>  'option',

                        ),
                        '14' => array(

                                    'field_name'    =>  'Left Right',
                                    'type'          =>  'list_left_right',
                        ),
                        '15' => array(

                                    'field_name'    =>  'handsontable',
                                    'type'          =>  'handsontable',
                                    'min_rows'      =>  '1',
                                    'max_rows'      =>  '5',

                        ),
                        '16' => array(

                                    'field_name'        =>  'FibenisTable',
                                    'type'              =>  'fibenistable',
                                    'is_fibenistable'   => 1,
                                    'colHeaders'        => array(
                                                            array(
                                                                'column'    => 'Name',
                                                                'width'     => '50',
                                                                'type'      => 'text',
                                                            ),
                                                            array(
                                                                'column'    => 'Purpose',
                                                                'width'     => '50',
                                                                'type'      => 'text',
                                                            ),
                                                        )
                        ),

        ),//data array ends here...
    );//F_series ends here..
