<?PHP
        	
	$key        	= @$_GET['key'];
	
	$LAYOUT = "layout_full";
				
	$T_SERIES       =   array( 
		
                                'table' => 'entity_child',
				 
				'data'  => array(
						 
						//'key_id'=>array('field'=>'id'),
						
						'domain'=> array('field'=>"'http://psgstep.fibenis.org/'"),
						
						//Basic Application_Heading
						'basic_application_form'=>array('field'=>"get_ecb_sn_by_token('NP1H')"),
						
						//Name_of_the_applicant
						'name_of_the_applicant_title'=>array('field'=>"get_ecb_sn_by_token('NP1NOA')"),
						'name_of_the_applicant'=>array('field'=>"get_exav_addon_varchar(id,'NP1NOA')"),

						//Photograph
						'photograph_title'=>array('field'=>"get_ecb_sn_by_token('NP1POA')"),
						'photograph'=>array('field'=>"get_exav_addon_varchar(id,'NP1POA')"),
						
						//Father's Name / Husband's Name
						'fathers_name_or_husbands_name_title'=>array('field'=>"get_ecb_sn_by_token('NP1CN')"),
						'fathers_name_or_husbands_name'=>array('field'=>"get_exav_addon_varchar(id,'NP1CN')"),

						//date Of Birth 
						'dob_title'=>array('field'=>"get_ecb_sn_by_token('NP1DOB')"),
						'dob'	   =>array('field'=>"date_format(get_exav_addon_date(id,'NP1DOB'),'%d-%b-%y')"),
						
						
						//Email Address
						'email_address_title'=>array('field'=>"get_ecb_sn_by_token('NP1EM')"),
						'email_address'	 =>array('field'=>"get_exav_addon_varchar(id,'NP1EM')"),
						
						//Mobile Number
						'mobile_number_title'=>array('field'=>"get_ecb_sn_by_token('NP1MB')"),
						'mobile_number'	     =>['field'=>"get_exav_addon_varchar(id,'NP1MB')"],
						
						//Landline Number
						'landline_number_title'=>array('field'=>"get_ecb_sn_by_token('NP1LN')"),
						'landline_number'	     =>['field'=>"get_exav_addon_varchar(id,'NP1LN')"],
						
						//Gender
						'gender_title'=>array('field'=>"get_ecb_sn_by_token('NP1GN')"),
						'gender'=>array('field'=>"get_ecb_sn_by_token(get_exav_addon_varchar(id,'NP1GN'))"),
						
						//Education Qualification
						'education_qualification_title'=>array('field'=>"get_ecb_sn_by_token('NP1EQ')"),
						'education_qualification'=>array('field'=>"get_exav_addon_varchar(id,'NP1EQ')"),
						
						//Category
						'category_title'=>array('field'=>"get_ecb_sn_by_token('NP1CT')"),
						'category'=>array('field'=>"get_ecb_sn_by_token(get_exav_addon_varchar(id,'NP1CT'))"),
						
						//Full Postal Address
						'full_postal_address_title'=>array('field'=>"get_ecb_sn_by_token('NP1POS')"),
						'full_postal_address'	 =>array('field'=>"get_exav_addon_text(id,'NP1POS')"),
						
						//permanent Address
						'permanent_address_title'=>array('field'=>"get_ecb_sn_by_token('NP1PER')"),
						'permanent_address'	 =>array('field'=>"get_exav_addon_text(id,'NP1PER')"),
						
						//Please enclose proof of residence
						'please_enclose_proof_title'=>array('field'=>"get_ecb_sn_by_token('NP1PROOF')"),
						'please_enclose_proof'=>array('field'=>"get_exav_addon_varchar(id,'NP1PROOF')"),
						
						//Profession of the Applicant
						'prof_of_app_title'=>array('field'=>"get_ecb_sn_by_token('NP1PROF')"),
						'prof_of_app'=>array('field'=>"get_exav_addon_varchar(id,'NP1PROF')"),
						
						//Name & Address of the Institution or Organization
						'name_and_address_of_inst_title'=>array('field'=>"get_ecb_sn_by_token('NP1WKADDR')"),
						'name_and_address_of_inst'	 =>array('field'=>"get_exav_addon_text(id,'NP1WKADDR')"),
						
						//Upload the copy of the NOC
						'upload_the_copy_of_noc_title'=>array('field'=>"get_ecb_sn_by_token('NP1NOC')"),
						'upload_the_copy_of_noc'=>array('field'=>"get_exav_addon_varchar(id,'NP1NOC')"),
						
						//Annual Income of the Applicant
						'annu_inc_title'=>array('field'=>"get_ecb_sn_by_token('NP1INC')"),
						'annu_inc'	     =>['field'=>"get_exav_addon_varchar(id,'NP1INC')"],
						
						//Attach a copy of the latest Income Tax Returns filed
						'att_copy_of_itr_title'=>array('field'=>"get_ecb_sn_by_token('NP1IT')"),
						'att_copy_of_itr'=>array('field'=>"get_exav_addon_varchar(id,'NP1IT')"),
						
						//PAN Card No.
						'pan_card_number_title'=>array('field'=>"get_ecb_sn_by_token('NP1PAN')"),
						'pan_card_number'	     =>['field'=>"get_exav_addon_varchar(id,'NP1PAN')"],

						//Aadhar No
						'aadhar_no_title'=>array('field'=>"get_ecb_sn_by_token('NP1ADDR')"),
						'aadhar_no'	     =>['field'=>"get_exav_addon_varchar(id,'NP1ADDR')"],
						
						
						//Team Member Detail
						'team_member_detail_title'=>array('field'=>"get_ecb_sn_by_token('NP1TEAM')"),
						'team_member_detail'=>array('field' => 'get_exav_addon_text(entity_child.id,"NP1TEAM")',
							       
										'data'   =>array(
												  '1'=>array('key'=>'name'),
												  '2'=>array('key'=>'aadhar_no'),
												  '3'=>array('key'=>'pan_card'),
												  '4'=>array('key'=>'gender'),
												  '5'=>array('key'=>'contact'),
												  '6'=>array('key'=>'email_id'),
												  '7'=>array('key'=>'qualification'),
												 ),
								),
						
						
						//About Project
						'about_project_title'=>array('field'=>"get_ecb_sn_by_token('NP3H')"),
						
						
						//Title of the Project
						'title_of_the_project_title'=>array('field'=>"get_ecb_sn_by_token('NPTOP')"),
						'title_of_the_project'=>array('field'=>"get_exav_addon_varchar(id,'NPTOP')"),
						
						//Description
						'description_title'=>array('field'=>"get_ecb_sn_by_token('NP2DS')"),
						'description'	 =>array('field'=>"get_exav_addon_text(id,'NP2DS')"),
						
						// Status of Work Carried out (if any) label
						'status_of_work_title'=>array('field'=>"get_ecb_ln_by_token('NP3WCO')"),
						
						//Participation in Competition
						'pre_in_comp_title'=>array('field'=>"get_ecb_sn_by_token('NP3WCO1')"),
						'pre_in_comp'	 =>array('field'=>"get_exav_addon_num(id,'NP3WCO1')"),
						
						//Making a Model
						'marking_a_model_title'=>array('field'=>"get_ecb_sn_by_token('NP3WCO2')"),
						'marking_a_model'	 =>array('field'=>"get_exav_addon_num(id,'NP3WCO2')"),
						
						////Provisional Application for Patent
						'prov_appli_for_pat_title'=>array('field'=>"get_ecb_ln_by_token('NP3WCO3')"),
						'prov_appli_for_pat'	 =>array('field'=>"get_exav_addon_num(id,'NP3WCO3')"),
						//
						//Paper Presentations
						'paper_present_title'=>array('field'=>"get_ecb_sn_by_token('NP3WCO4')"),
						'paper_present'	 =>array('field'=>"get_exav_addon_num(id,'NP3WCO4')"),
						
						//Publication
						'publication_title'=>array('field'=>"get_ecb_sn_by_token('NP3WCO5')"),
						'publication'	 =>array('field'=>"get_exav_addon_num(id,'NP3WCO5')"),
						
						//College Project
						'college_project_title'=>array('field'=>"get_ecb_sn_by_token('NP3WCO6')"),
						'college_project'	 =>array('field'=>"get_exav_addon_num(id,'NP3WCO6')"),
						
						//Any other
						'any_other_title'=>array('field'=>"get_ecb_sn_by_token('NP3WCO7')"),
						'any_other'	 =>array('field'=>"get_exav_addon_num(id,'NP3WCO7')"),
						
						//Science and Working Principle behind this Idea
						'sci_and_work_pri_title'=>array('field'=>"get_ecb_sn_by_token('NP3IDEA')"),
						'sci_and_work_pri'	 =>array('field'=>"get_exav_addon_text(id,'NP3IDEA')"),
						
						//Final Outcome / Deliverable of the Project
						'final_out_deli_of_pro_title'=>array('field'=>"get_ecb_sn_by_token('NP3OUT')"),
						'final_out_deli_of_pro'	 =>array('field'=>"get_exav_addon_text(id,'NP3OUT')"),
						
						//Who would be the beneficiary of this innovation and why?
						'who_would_be_the_benefi_title'=>array('field'=>"get_ecb_sn_by_token('NP3BENF')"),
						'who_would_be_the_benefi'	 =>array('field'=>"get_exav_addon_text(id,'NP3BENF')"),
						
						//Cost And Duration
						'cost_and_duration_title'=>array('field'=>"get_ecb_sn_by_token('NP4H')"),
						
						//Proposed Costs and Time Frame
						'proposed_costs_and_time_title'=>array('field'=>"get_ecb_sn_by_token('NP4COTI')"),
						'proposed_costs_and_time'=>array('field' => 'get_exav_addon_text(entity_child.id,"NP4COTI")',
							       
										'data'   =>array(
												  '1'=>array('key'=>'name'),
												  '2'=>array('key'=>'own_share'),
												  '3'=>array('key'=>'prayas_support'),
												  '4'=>array('key'=>'total_cost'),
												 ),
								),
						
						//Detailed Justification for the proposed budget
						'detail_just_for_pro_budget_title'=>array('field'=>"get_ecb_sn_by_token('NP4BDJUS')"),
						'detail_just_for_pro_budget'=>array('field'=>"get_exav_addon_varchar(id,'NP4BDJUS')"),
						
						//Project Period in Months
						'project_period_in_month_title'=>array('field'=>"get_ecb_sn_by_token('NP4PERI')"),
						'project_period_in_month'	 =>array('field'=>"get_exav_addon_varchar(id,'NP4PERI')"),
						
						
						//Activity Plan
						'activity_plan_title'=>array('field'=>"get_ecb_sn_by_token('NP7H')"),
						
						// Activity Details / Work Plan
						'activity_details_title'=>array('field'=>"get_ecb_sn_by_token('NP7FT')"),
						'activity_details'=>array('field' => 'get_exav_addon_text(entity_child.id,"NP7FT")',
							       
										'data'   =>array(
												  '1'=>array('key'=>'activities'),
												  '2'=>array('key'=>'monitorable_milestones'),
												  '3'=>array('key'=>'duration'),
												  '4'=>array('key'=>'budget'),
												 ),
								),
						
						//Justification for the budget against milestones
						'just_for_the_budget_title'=>array('field'=>"get_ecb_sn_by_token('NP7JBA')"),
						'just_for_the_budget'=>array('field'=>"get_exav_addon_varchar(id,'NP7JBA')"),
						
						//Have you received financial support / award for your present work from any other sources?
						//'have_you_rec_fin_sup_title'=>array('field'=>"get_ecb_sn_by_token('NP8FSPW')"),
						'have_you_rec_fin_sup'	 =>array('field'=>"get_exav_addon_num(id,'NP8FSPW')"),
						
						//If yes, Please furnish details
						'if_yes_ple_fur_det_title'=>array('field'=>"get_ecb_sn_by_token('NP8PFD')"),
						'if_yes_ple_fur_det'=>array('field'=>"get_exav_addon_varchar(id,'NP8PFD')"),
						
						//Essential Criteria
						'essen_cri_title'=>array('field'=>"get_ecb_sn_by_token('NP9ES')"),
						
						//Applicant Confirms that he / she has not been a recipient of any NIDHI-PRAYAS or similar support previously for the same innovative concept as proposed in this form. NIDHI-PRAYAS can be used only once. 
						//'appli_conf_that_title'=>array('field'=>"get_ecb_sn_by_token('NP9ES1')"),
						'appli_conf_that'	 =>array('field'=>"get_exav_addon_num(id,'NP9ES1')"),
						
						//Applicant is planning to pursue NIDHI-PRAYAS full time with no other concurrent commitments
						//'appli_is_plan_to_pur_title'=>array('field'=>"get_ecb_sn_by_token('NP9ES2')"),
						'appli_is_plan_to_pur'	 =>array('field'=>"get_exav_addon_num(id,'NP9ES2')"),
						
						//Applicant confirms that he / she is fully committed to work towards the prototype development for which the support is being sought and should not treat this as a stop gap arrangement to support any other pursuits.
						//'appli_conf_that_he_title'=>array('field'=>"get_ecb_sn_by_token('NP9ES3')"),
						'appli_conf_that_he'	 =>array('field'=>"get_exav_addon_num(id,'NP9ES3')"),
						
						//Applicant has or is planning to register for the pre-incubation or incubation program at PSG-STEP for the entire duration of PRAYAS support
						//'appli_has_or_is_plan_title'=>array('field'=>"get_ecb_sn_by_token('NP9ES4')"),
						'appli_has_or_is_plan'	 =>array('field'=>"get_exav_addon_num(id,'NP9ES4')"),
						
						'appli_conf_pc'	 =>array('field'=>"get_exav_addon_num(id,'NP9ES5')"),
						
						'appli_conf_that_they_will_not'	 =>array('field'=>"get_exav_addon_num(id,'NP9ES6')"),
						
	
						//Declaration
						'declaration_title'=>array('field'=>"get_ecb_sn_by_token('NP10H')"),
						
						// label
						//'label_heading'=>array('field'=>"get_ecb_sn_by_token('NP10LA')"),
						//'label_content'	 =>array('field'=>"get_exav_addon_varchar(id,'NP10LA')"),
						
						//Place
						'place_title'=>array('field'=>"get_ecb_sn_by_token('NP10PL')"),
						'place'	 =>array('field'=>"get_exav_addon_varchar(id,'NP10PL')"),
						
						//Date
						'date_title'=>array('field'=>"get_ecb_sn_by_token('NP10DA')"),
						'date'=>array('field'=>"date_format(get_exav_addon_date(id,'NP10DA'),'%d-%b-%y')"),
						
						//Signature
						'signature_title'=>array('field'=>"get_ecb_sn_by_token('NP10SG')"),
						'signature'=>array('field'=>"get_exav_addon_varchar(id,'NP10SG')")
						
						
						
						),	
				
				
				'key_id' => 'id',
				
				'key_filter'=>' ',
				
				'show_query'=>0,
				
				# if data stored in array way like [ ['ins','1'],['ins B',2]]
  
                                'template'       => 'def/prayas/tx.html',
				
				// save data 
				'save_as'=> array(
						
						array('type'=>'pdf',
						      'file_name'=>'home',
						      'path'=>'def/prayas/')
						
					)

                            );
	
	
    
?>