<?PHP

    $application_id = $_GET['default_addon'];
    
    $LAYOUT = 'layout_narrow_down';
    
    $F_SERIES	=	array(
				#Title
				
				'title'	=>'Set Reviewer',
				
				#Table field
                    
				'data'	=>   array(
						   
						     
						     '1' =>array( 'field_name'=> 'Select Reviewer', 
                                                               
								'field_id' => 'ec_id',
                                                               
                                                                'type' => 'option',
							       
								'option_data'=> $G->option_builder('entity_child','id,get_eav_addon_varchar(id,"COFN")',
												   "WHERE id IN (SELECT is_internal FROM
												   user_info WHERE user_role_id = (SELECT id FROM
												   user_role WHERE sn ='REW') AND is_internal NOT IN
												   (SELECT ec_id FROM exav_addon_ec_id WHERE exa_token='REVW'
												   AND parent_id = $application_id)
												   ) ORDER BY get_eav_addon_varchar(id,'COFN') ASC"),
								
							       'is_mandatory'=>1,
							       
							       'input_html'=>'class="w_100"',
							       
							   	
							    ),
					    ),
				
                                    
				'table_name'    => 'exav_addon_ec_id',
				
				'key_id'        => 'id',
				
				'default_fields'    => array(
							    'parent_id' => "'$application_id'",
							    'exa_token' => "'REVW'",
							    ),
				
				'is_user_id'       => 'user_id',
				
				'back_to'  => NULL,
				
				'flat_message' => 'Sucesssfully Updated',
				
				'page_code'	=> 'REIW',
				
				'show_query'    =>1,
				
				'after_add_update'=>0,
				
				'before_add_update'=>0,
                                
			);
    
    
    
     function after_add_update($key_id){
	
		global $rdsql,$USER_ID;
		
		
		$application_id = $_GET['default_addon'];
    
		$reviewer_id = $_POST['X1'];
	    
	    //
	    // Insert for reviewer 
	    //
		
		//$insert_reviewer_projects = $rdsql->exec_query("INSERT INTO exav_addon_ec_id (parent_id,exa_token,ec_id,user_id)
		//				    VALUES ($reviewer_id,'ASPRO',$application_id,$USER_ID)",
		//				    "Assigning for reviewer Failed");
		
		return null;
    }

?>