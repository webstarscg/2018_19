<?PHP

    $LAYOUT	    = 'layout_full';
    
    // current year
    
    $temp_year = date("Y");
    
    $temp_min_year  = ($temp_year-18);
    
    $temp_max_year  = ($temp_year-70);
    
    
    $F_SERIES	=	array(
				#Form Title
				
				'title'	=>'My Profile',
				
				#Table field
                    
				'data'	=>   array(

						   '1' =>array( 'field_name'=> 'Name ',
                                                               
                                                               'field_id' => 'ea_value',
                                                               
                                                               'type' => 'text',
							       
							       'child_table'         => 'eav_addon_varchar', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => 'COFN',           // attribute code
							       
							       'input_html'=>'class="w_200" readonly',
							       
							       'allow'		=> 'w128[ .]',
							       
							       'ro'	=> 1,
                                                               
							       'is_mandatory'=>1,
							       
							       'hint'   => '(No changes allowed)',
                                                               
                                                               ),
						   
						   
						   '2' =>array( 'field_name'=> 'Gender ',
                                                               
                                                               'field_id' => 'exa_value_token',
                                                               
                                                               'type' => 'option',
							       
							       'type'                => 'option',
							       
                                                               'option_data'         => "<option value=''>Select</option><option value='GEFM'>Female</option><option value='GEMA'>Male</option><option value='GETR'>Transgender</option>",  
							       
							       //'option_data' => $G->option_builder('entity_child_base','token,sn'," WHERE entity_code='GE'  ORDER BY line_order ASC"),
							       
							       'option_default'=> array('label'=>'Select','value'=>'NANA'),
							       
							       'child_table'         => 'eav_addon_exa_token', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => 'COGE',           // attribute code
                                                               
                                                               'is_mandatory'=>1,
							       
							       'avoid_default_option' => 1,
                                                               
                                                               'input_html'=>'class="w_125" "'
                                                               
                                                               ),
						   
						   
						   '3' =>array('field_name'=> 'Date of Birth',
                                                               
                                                             'field_id' => 'ea_value',
                                                               
                                                             'type' => 'date',
							       
							     //'set'  => array('min_date'=>'-100Y','max_date'=>'0D'),
							       
                                                             'child_table'         => 'eav_addon_date', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => 'CODB',           // attribute code
                                                      
							       'is_mandatory'=>1,
							       
							       //'default_date'=>date("d-m-$temp_min_year"),
							       //
							       //'default_date' => "dd-mm-yyyy",
							       
							       'default_date' => "",
                                                               
                                                               'input_html'=>'class="w_100"' ,
							       
							       'year_range' => "$temp_max_year:$temp_min_year"
							       
                                                          ),
						   
						   
//						    '4' =>array( 'field_name'=> 'Landline ',
//                                                               
//                                                               'field_id' => 'ea_value',
//                                                               
//                                                               'type' => 'text',
//							       
//							       'child_table'         => 'eav_addon_varchar', // child table
//							       
//							       'parent_field_id'     => 'parent_id',    // parent field
//										       
//							       'child_attr_field_id' => 'ea_code',   // attribute code field
//							       
//							       'child_attr_code'     => 'COLD',           // attribute code
//							       
//							       'input_html'=> "class='w_100'  onchange='check_mobile(this);'",
//                                                               
//							       'is_mandatory'=>0,
//							       
//							       'allow' => 'd10',
//                                                               
//                                                               ),
						    
						    '5' =>array( 'field_name'=> 'Mobile ',
                                                               
                                                               'field_id' => 'ea_value',
                                                               
                                                               'type' => 'text',
							       
							       'child_table'         => 'eav_addon_varchar', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => 'COMB',           // attribute code
							       
							       //'input_html'=>'class="w_100"',
							       
							       'input_html'=> "class='w_100' readonly onchange='check_mobile(this);'",
                                                               
							       'is_mandatory'=>1,
							       
							       'allow' => 'd10',
                                                               
                                                               ),
						    
						    '6' =>array( 'field_name'=> 'Alternate Mobile ',
                                                               
                                                               'field_id' => 'ea_value',
                                                               
                                                               'type' => 'text',
							       
							       'child_table'         => 'eav_addon_varchar', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => 'COAM',           // attribute code
							       
							       'input_html'=>'class="w_100"',
							       
							       'input_html'=> "class='w_100'  onchange='check_mobile(this);'",
                                                               
							       'is_mandatory'=>0,
							       
							       'allow' => 'd10',
                                                               
                                                               ),
						    
						    '7' =>array( 'field_name'=> 'Email ',
                                                               
                                                               'field_id' => 'ea_value',
                                                               
                                                               'type' => 'text',
							       
							       'child_table'         => 'eav_addon_varchar', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => 'COEM',           // attribute code
							       
							       'input_html'=>"class='w_200' readonly onchange='check_email(this);'",
							       
							       //'allow'		=> 'x128',
                                                               
							       'is_mandatory'=>1,
							       
							       'ro'	=> 1,
							       
							       'hint'   => '(No changes allowed)',
                                                               
                                                               ),
						    
						    '8' =>array( 'field_name'=> 'Residential / Permanent Address',
                                                               
                                                               'field_id' => 'ea_value',
                                                               
                                                               'type' => 'textarea',
							       
							       'child_table'         => 'eav_addon_varchar', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => 'CORA',           // attribute code
							       
							       'input_html'=>'class="w_300" rows=4 placeholder="Door No / Street or Road / City / State / Pincode"',
							       
							       'hint'	=> '(Door No / Street or Road / City / State / Pincode)',
                                                               
							       'is_mandatory'=>1,
							       
							       'allow'		=> 'x1000',
                                                               
                                                               ),
						    
//						    '9' =>array( 'field_name'=> 'Permanent Address',
//                                                               
//                                                               'field_id' => 'ea_value',
//                                                               
//                                                               'type' => 'textarea',
//							       
//							       'child_table'         => 'eav_addon_varchar', // child table
//							       
//							       'parent_field_id'     => 'parent_id',    // parent field
//										       
//							       'child_attr_field_id' => 'ea_code',   // attribute code field
//							       
//							       'child_attr_code'     => 'CORB',           // attribute code
//							       
//							       'input_html'=>'class="w_200"',
//                                                               
//							       'is_mandatory'=>1,
//							       
//							       'allow'		=> 'x1028',
//                                                               
//                                                               ),
					    
					    ),
                                    
				#Table Name
				
				'table_name'    => 'entity_child',
				
				#Primary Key
                                
			        'key_id'        => 'id',
				
				'deafult_value'    => array('entity_code' => "'CO'"),
				
				'js'=> ['is_top'=>1, 'top_js'=>"def/my_profile/fx"], 
                                
				# Default Additional Column
                                
				'is_user_id'       => 'created_by',
								
				# Communication
								
				'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?dx=my_profile', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 3,
                                
				//'flat_message'	=> 'Successfully Added',
				
				# File Include
				
				//'before_add_update' => 0,
				
				'after_add_update' => 1,
				
				'show_query'    =>0,
				
				'divider'       => 'tab',
				
				//'is_save_form'   => 0,
				//
				//'button_name'=>'Submit'
				
                                
			);
    
    	//function after_add_update($key_id){
        $F_SERIES['after_add_update'] = function(){

		   global $F_SERIES;
		    
		   $F_SERIES['avoid_trans_key_direct']=1;

		    header('Location:?dx=programme');
		    
		    return null;
		    
	}
    
	
?>