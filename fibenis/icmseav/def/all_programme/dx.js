    

    // Add Project details
    function project_doc(code) {
        
        switch(code) {
            
            case "FA":
            
                    window.location = '?fx=innovation&menu_off=0&default_addon=new',"_blank",'width=800,height=600,left=10,top=50,toolbar=0,scrollbars=1,menubar=0,status=0';
                    break;
                
            case "EI":
            
                   window.location = '?fx=eir&menu_off=0&default_addon=new',"_blank",'width=800,height=600,left=10,top=50,toolbar=0,scrollbars=1,menubar=0,status=0';
                    break;
                
            case "NP":
            
                    window.location = '?fx=prayas&menu_off=0&default_addon=new',"_blank",'width=800,height=600,left=10,top=50,toolbar=0,scrollbars=1,menubar=0,status=0';
                    break;
            
            case "NA":
            
                    window.location = '?fx=nano&menu_off=0&default_addon=new',"_blank",'width=800,height=600,left=10,top=50,toolbar=0,scrollbars=1,menubar=0,status=0';
                    break;
        
        }
        
    }
    
    // View Project details
    function view_project_doc(input) {
        
        var data = input.split(':');
        
        switch(data[0]) {
            
            case "FA":
            
                    window.open('?tx=innovation&menu_off=0&key='+data[2],"_blank",'width=800,height=600,left=10,top=50,toolbar=0,scrollbars=1,menubar=0,status=0');
                    
                    break;
                
            case "EI":
            
                   window.open('?tx=eir&menu_off=0&key='+data[2],"_blank",'width=800,height=600,left=10,top=50,toolbar=0,scrollbars=1,menubar=0,status=0');
                    
                    break;
                
            case "NP":
            
                    window.open('?tx=prayas&menu_off=0&key='+data[2],"_blank",'width=800,height=600,left=10,top=50,toolbar=0,scrollbars=1,menubar=0,status=0');
                    
                    break;
            
            case "NA":
            
                    window.open('?tx=nano&menu_off=0&key='+data[2],"_blank",'width=800,height=600,left=10,top=50,toolbar=0,scrollbars=1,menubar=0,status=0');
                    
                    break;
        
        }
        
    }
    
    // Edit Project details
    function project_doc_edit(input) {
        
        var data = input.split(':');
        
        switch(data[0]) {
            
            case "FA":
            
                    window.location = '?fx=innovation&menu_off=0&key='+data[2],"_blank",'width=800,height=600,left=10,top=50,toolbar=0,scrollbars=1,menubar=0,status=0';
                    break;
                
            case "EI":
            
                   window.location = '?fx=eir&menu_off=0&key='+data[2],"_blank",'width=800,height=600,left=10,top=50,toolbar=0,scrollbars=1,menubar=0,status=0';
                    break;
                
            case "NP":
            
                    window.location = '?fx=prayas&menu_off=0&key='+data[2],"_blank",'width=800,height=600,left=10,top=50,toolbar=0,scrollbars=1,menubar=0,status=0';
                    break;
                
            case "NA":
            
                    window.location = '?fx=nano&menu_off=0&key='+data[2],"_blank",'width=800,height=600,left=10,top=50,toolbar=0,scrollbars=1,menubar=0,status=0';
                    break;
        
        }
        
    }    
    
    
    
  