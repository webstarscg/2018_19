<?PHP
			
//     if(($user_role != 'ADM') || ($user_role != 'SAD')){
//		
//		header('Location:?dx=programme');
//     }
	
	
	$LAYOUT	    	= 'layout_full';
               
        $D_SERIES       =   array(
                                   'title'=>'Programme',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
				    
				    'gx' => 1,
				    
                                    
                                    #table data
                                    
                                    'data'=> array(
						       
						       
							
							
							1=>array('th'=>'Programme',
								
								'field' =>"ln",
								
								'td_attr' => ' class="clr_gray_8 align_LM" width="10%"',
								
								'is_sort' => 1,	
								
								),
							
							
							0=>array('th'=>'Reference No',
								
								'field' =>"concat(code,':',$USER_ID)",
								
								'td_attr' => ' class="num align_LM" width="10%"',
								
								'is_sort' => 0,
								
								'filter_out'=>function($data_in){
										
										global $rdsql;
										
										$temp = explode(':',$data_in);
										
										//$token = $temp[0].'RF';
										
										$find_status = $rdsql->exec_query("SELECT exa_value FROM exav_addon_varchar WHERE exa_token = 'REFN' AND parent_id = 
														  (SELECT id FROM entity_child WHERE entity_code = '$temp[0]' AND user_id = $temp[1])","Selection Fails");
							
										$value = $rdsql->data_fetch_row($find_status);
										
										if(!$value[0]){
												return '<span class="clr_gray_a">NA</span>';
										}else{
											return $value[0];
										}
										
								  }
								
								),
							
							
							6=>array('th'=>'Title',
								
								'field' =>"concat(code,':',$USER_ID)",
								
								'td_attr' => ' class="align_LM" width="25%"',
								
								'is_sort' => 0,
								
								'filter_out'=>function($data_in){
										
										global $rdsql;
										
										$temp = explode(':',$data_in);
										
										$find_title = $rdsql->exec_query("SELECT exa_value FROM exav_addon_varchar WHERE exa_token = 'TITLE'
														  AND parent_id = (SELECT id FROM entity_child WHERE entity_code = '$temp[0]' AND user_id = $temp[1])","Selection Fails");
							
										$find_title_val = $rdsql->data_fetch_row($find_title);
										
										if(!$find_title_val[0]){
												return '<span class="clr_gray_a">NA</span>';
										}else{
											return "<span class='txt_size_13'>$find_title_val[0]</span>";
										}
										
								}
								
								),
							
							2=>array('th'=>'Action ',
								
								'field' =>"concat(code,':',$USER_ID)",
								
								'td_attr' => ' class=" align_CM" width="15%"',
								
								'filter_out'=>function($data_in){
										
										global $rdsql;
										
										$temp = explode(':',$data_in);
										
										$find_id = $rdsql->exec_query("SELECT id FROM entity_child WHERE entity_code = '$temp[0]' AND user_id = $temp[1]","Selection Fails");
	    
										$value = $rdsql->data_fetch_row($find_id);
										
										if($value[0]==NULL){
											
											return '<a class="btn btn-success clr_gray_f w_100" onclick="JavaScript:project_doc('."'$temp[0]'".');">'.
												'<i class="fa fa-chevron-circle-right " aria-hidden="true"></i>&nbsp;'.
												'Apply</a>'.
												'<div class="pad_tb_5"><span class="clr_red">*</span>'.
												'<a class="clr_gray_9 txt_size_11" target="_blank" href="doc/'.$temp[0].'.pdf">Check Eligibility Guideline</a></div>';
						
										}	
                                        
										else{
											
											$to_pass= $data_in.":".$value[0];
										
										//        return '<a class="btn btn-primary clr_gray_f w_100" onclick="JavaScript:view_project_doc('."'$to_pass'".');">'.
										//		'<i class="fa fa-file" aria-hidden="true"></i>&nbsp;'.
										//		'View</a>';
												
											return '<a class="pointer clr_gray_b txt_size_11 tip_bottom" onclick="JavaScript:view_project_doc('."'$to_pass'".');">'.
												      '<i class="fa fa-file-pdf-o clr_red txt_size_15" aria-hidden="true"></i>&nbsp;'.
												      'View</a><span class="tooltiptext">View Project Report</span>';
						
										}
								}
							),
							
							4=>array('th'=>'Status',
								
								'field' =>"concat(code,':',$USER_ID)",
								
								'td_attr' => ' class="clr_gray_a txt_case_upper align_LM" width="10%"',
								
								'is_sort' => 0,
								
								'filter_out'=>function($data_in){
										
										global $rdsql;
										
										$temp = explode(':',$data_in);
										
										//$token = $temp[0].'ST';
										
										$find_status = $rdsql->exec_query("SELECT exa_value_token FROM exav_addon_exa_token WHERE exa_token = 'STAT' AND parent_id = 
														  (SELECT id FROM entity_child WHERE entity_code = '$temp[0]' AND user_id = $temp[1])","Selection Fails");
							
										$value = $rdsql->data_fetch_row($find_status);
										
										$code_val = $rdsql->exec_query("SELECT upper(sn) FROM entity_child_base WHERE token = '$value[0]'","Selection Fails");
										
										$code_value = $rdsql->data_fetch_row($code_val);
										
										if(!$code_value[0]){
												return '<span class="clr_gray_a">NA</span>';
										}else{
											$temp_status_code=$code_value[0];
											
											$temp=['NEW'=>'file-o clr_orange','SUBMITTED'=>'file-text-o clr_dark_blue'];
									
											return "<span><i class='fa fa-$temp[$temp_status_code] txt_size_15' aria-hidden='true'>&nbsp;&nbsp;</i>".
											       "$temp_status_code</span>";
									     
										}
										
								  }
								
								),
							
							5=>array('th'=>'Updated On',
								
								'field' =>"concat(code,':',$USER_ID)",
								
								'td_attr' => ' class="clr_gray_a txt_size_11 align_RM no_wrap" width="10%"',
								
								'is_sort' => 1,
								
								'filter_out'=>function($data_in){
										
										global $rdsql;
										
										$temp = explode(':',$data_in);
										
										$find_upd_on = $rdsql->exec_query("SELECT date_format(updated_on,'%d-%b-%Y-%T') FROM entity_child WHERE entity_code = '$temp[0]' AND user_id = $temp[1]","Selection Fails");
							
										$value_upd = $rdsql->data_fetch_row($find_upd_on);
										
										if(!$value_upd[0]){
												return '<span class="clr_gray_a">NA</span>';
										}else{
											return $value_upd[0];
										}
										
								}
								
								),
							
							3=>array('th'=>'Edit ',
								
								'field' =>"concat(code,':',$USER_ID)",
								
								'td_attr' => ' class=" align_RM" width="10%"',
								
								'filter_out'=>function($data_in){
										
										global $rdsql;
										
										$temp = explode(':',$data_in);
										
										//$token = $temp[0].'ST';
										
										$find_id = $rdsql->exec_query("SELECT id FROM entity_child WHERE entity_code = '$temp[0]' AND user_id = $temp[1]","Selection Fails");
	    
										$value = $rdsql->data_fetch_row($find_id);
										
										
										
										$find_status = $rdsql->exec_query("SELECT exa_value_token FROM exav_addon_exa_token WHERE exa_token = 'STAT' AND parent_id = 
														  (SELECT id FROM entity_child WHERE entity_code = '$temp[0]' AND user_id = $temp[1])","Selection Fails");
							
										$status_value = $rdsql->data_fetch_row($find_status);
										
										
										if($value[0]!=NULL){
										
										
										       $to_pass= $data_in.":".$value[0];	
											
											return '<a class="pointer clr_gray_b tip_bottom" onclick="JavaScript:project_doc_edit('."'$to_pass'".');">'.
												'<i class="fa fa-edit clr_green txt_size_15" aria-hidden="true"></i>&nbsp;'.
												'Edit</a><span class="tooltiptext">Edit Application</span>';
						
										}	
                                        
										else{
											return '<span class="clr_gray_a">NA</span>';
						
						
										}
								}
							),
					),
				    
					
                                    #Table Info
                                    
                                    'table_name' =>'entity',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
				    
    				    'key_filter'     =>	 " AND (code='NP' OR code='EI' OR code='NA' OR code='FA') ORDER BY id",
				    
				    //'key_filter'     =>	 " AND code='NP'",

				    'is_narrow_down' => 0,
				    
				    'js' => array('is_top' => 1, 'top_js' => "def/programme/dx"),
				    
                                    # Communication
                                
                                    'prime_index'   => 1,
				    
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>0,'page_link'=>'', 'b_name' => '' ),
								
					'del_permission' => array('able_del'=>0,'user_flage'=>0), 
								
					'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  ''),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'show_query'=>0,
				
				'hide_show_all' => 1,
				
				'hide_pager'     =>1,
				
				'filter_off' =>0,
				 
				'search_filter_off'	=>1,
                            
                            );
    
?>