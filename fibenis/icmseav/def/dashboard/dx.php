<?PHP

    $D_SERIES   =   array(
                                    
				    
				    
                                    'data'=> array(

							
							
							1=> array(
								  
								   'field_composite'=>[
											  
											  'fields'=>['x'=>' (SELECT sn FROM entity WHERE code = entity_code )',
												     'Total'=>'count'
												     
												     ],
											  
											  'table' => 'application_count'
											  
											  ],
								   
								   'js_call'	=> 'c3001.graphDonut',
								   
								   
								   //'js_call'=> 'c3001.graphBase',
								   
								   'attr' 	=> ['class'	   	=> "col-md-6 pad_lr mar_top_25",
										     'data-title' 	=> "Application Registered",
										      'data-type'  	=> 'bar',
										    ]
							),
							
							
							2=> array(
								  
								   'field_composite'=>[
										         'fields'=>['Application'=>'(SELECT sn FROM entity WHERE code = entity_code )',
												     'Total'=>'total_count',
												     'Submitted'=>'submitted_count'
												     ],
											  
											  'table' => 'application_status'
											  
											  ],
								   
								   'js_call'=> 'c3001.graphBase',
								   
								   //'js_call'	=> 'c3001.graphDonut',
								   
								   'attr' => [
									      
									      'class'      	=> "col-md-5 pad_lr mar_top_25",
									      'data-title' 	=> "Application submitted",
									      'data-type'  	=> 'bar',
									      #'data-bar-width'	=> 50,
									      'data-grid-x'	=> true,
									      'data-grid-y'	=> true
									      ]
							),
							
							
											
							
                                                  ),
            
                                                                     
                                    # Communication
                                    
				    'mode' => 'graph'
				    
				   
	    );
	    

?>