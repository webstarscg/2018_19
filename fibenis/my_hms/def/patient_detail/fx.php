<?PHP

        $today  =   date("d-m-Y");

    $F_SERIES   =   array(

                    #title
                    'title'             =>  'Patient Details',

                    #table name
                    'table_name'        =>  'entity_child',

                    #unique key
                    'key_id'            =>  'id',

                    #To Show DB Query
                    'show_query'        =>  1,

                    #message to display
                    'flat_message'      =>  'Patient Added Successfully !!!',

                    #form divider
                    'divider'           =>  'accordion',

                    'button_name'       =>  'Add Patient',   

                    #default fields
                    'default_fields'    =>  array(

                                            'entity_code'   =>  "'PT'",

                    ),//default fields ends here...

                    #user id
                    'is_user_id'        =>  'user_id',

                    'back_to'           =>  array(

                                                'is_back_button'    =>  1,
                                                'back_link'         =>  '?dx=patient_detail',
                                                'BACK_NAME'         =>  'Back',

                        ),//back button ends here...


                    #form data goes here

                    'data'          =>  array(

                                        '0' =>  array(

                                                'field_name'            =>  'Personal Details',
                                                'type'                  =>  'heading',

                                            ),

                                        '1' =>  array(

                                                'field_name'            =>  'First Name',
                                                'field_id'              =>  'exa_value',
                                                'type'                  =>  'text',
                                                'hint'                  =>  'Enter First Name',
                                                'is_mandatory'          =>  1,
                                                'parent_field_id'       =>  'parent_id',
                                                'child_table'           =>  'exav_addon_varchar',
                                                'child_attr_field_id'   =>  'exa_token',
                                                'child_attr_code'       =>  'PTNA1',

                                        ),

                                        '2' =>  array(

                                                'field_name'            =>  'Last Name',
                                                'field_id'              =>  'exa_value',
                                                'type'                  =>  'text',
                                                'hint'                  =>  'Enter Last Name',
                                                'is_mandatory'          =>  0,
                                                'parent_field_id'       =>  'parent_id',
                                                'child_table'           =>  'exav_addon_varchar',
                                                'child_attr_field_id'   =>  'exa_token',
                                                'child_attr_code'       =>  'PTNA2',

                                        ),

                                        '3' =>  array(

                                                'field_name'            =>  'Gender',
                                                'field_id'              =>  'exa_value',
                                                'type'                  =>  'option',
                                                'option_data'           =>  '<option value = Male> Male </option>
                                                                             <option value = Female> Female </option>
                                                                             <option value = Transgender> Transgender </option>',
                                                'hint'                  =>  'Select Gender',
                                                'is_mandatory'          =>  1,
                                                'parent_field_id'       =>  'parent_id',
                                                'child_table'           =>  'exav_addon_varchar',
                                                'child_attr_field_id'   =>  'exa_token',
                                                'child_attr_code'       =>  'PTGN',

                                        ),


                                        '4' =>  array(

                                                'field_name'            =>  'Image',
                                                'field_id'              =>  'exa_value',
                                                'type'                  =>  'file',
                                                'hint'                  =>  'Upload Image Which is < 1 MB',
                                                #'location'             =>  '/images/patients/',
                                                'parent_field_id'       =>  'parent_id',
                                                'child_table'           =>  'exav_addon_varchar',
                                                'child_attr_field_id'   =>  'exa_token',
                                                'child_attr_code'       =>  'PTIM',

                                        ),
                                        
                                        '24'=>  array(

                                                'field_name'            =>  'Age',
                                                'field_id'              =>  'exa_value',
                                                'type'                  =>  'text',
                                                'allow'                 =>  'd2',
                                                'parent_field_id'       =>  'parent_id',
                                                'child_table'           =>  'exav_addon_varchar',
                                                'child_attr_field_id'   =>  'exa_token',
                                                'child_attr_code'       =>  'PTAG',

                                        ), 

                                        '5' =>  array(

                                                'field_name'            =>  'Visiting Date',
                                                'field_id'              =>  'date',
                                                'type'                  =>  'date',
                                                'default_date'          =>   $today,
                                                'is_mandatory'          =>  1,

                                        ),

                                        '6' =>  array(

                                                'field_name'            =>  'Marital Status',
                                                'field_id'              =>  'exa_value',
                                                'type'                  =>  'option',
                                                'option_data'           =>  '<option value = Single> Single </option>
                                                                             <option value = Married> Married </option>',
                                                'parent_field_id'       =>  'parent_id',
                                                'child_table'           =>  'exav_addon_varchar',
                                                'child_attr_field_id'   =>  'exa_token',
                                                'child_attr_code'       =>  'PTMST',

                                        ),

                                        '7' =>  array(

                                                'field_name'            =>  'E-mail',
                                                'field_id'              =>  'exa_value',
                                                'type'                  =>  'text',
                                                'allow'                 =>  'w[@.]',
                                                'parent_field_id'       =>  'parent_id',
                                                'child_table'           =>  'exav_addon_varchar',
                                                'child_attr_field_id'   =>  'exa_token',
                                                'child_attr_code'       =>  'PTEM',

                                        ),

                                        '8' =>  array(

                                                'field_name'            =>  'Contact details',
                                                'type'                  =>  'heading',
                                        ),
                                        
                                        '9' =>  array(

                                                'field_name'            =>  'Mobile',
                                                'field_id'              =>  'exa_value',
                                                'type'                  =>  'text',
                                                'allow'                 =>  'd10',
                                                'is_mandatory'          =>  1,
                                                'parent_field_id'       =>  'parent_id',
                                                'child_table'           =>  'exav_addon_varchar',
                                                'child_attr_field_id'   =>  'exa_token',
                                                'child_attr_code'       =>  'PTMN',

                                        ),

                                        '10' =>  array(

                                                'field_name'            =>  'Alternate Mobile',
                                                'field_id'              =>  'exa_value',
                                                'type'                  =>  'text',
                                                'allow'                 =>  'd10',
                                                'parent_field_id'       =>  'parent_id',
                                                'child_table'           =>  'exav_addon_varchar',
                                                'child_attr_field_id'   =>  'exa_token',
                                                'child_attr_code'       =>  'PTAM',

                                        ),


                                        '11'    =>  array(

                                                'field_name'            =>  'Address',
                                                'type'                  =>  'sub_heading',

                                        ),

                                        '12' =>  array(

                                                'field_name'            =>  'Street Address',
                                                'field_id'              =>  'exa_value',
                                                'type'                  =>  'text',
                                                'is_mandatory'          =>  1,
                                                'parent_field_id'       =>  'parent_id',
                                                'child_table'           =>  'exav_addon_varchar',
                                                'child_attr_field_id'   =>  'exa_token',
                                                'child_attr_code'       =>  'PTSTA',

                                        ),

                                        '13' =>  array(

                                                'field_name'            =>  'City',
                                                'field_id'              =>  'exa_value',
                                                'type'                  =>  'text',
                                                'is_mandatory'          =>  1,
                                                'parent_field_id'       =>  'parent_id',
                                                'child_table'           =>  'exav_addon_varchar',
                                                'child_attr_field_id'   =>  'exa_token',
                                                'child_attr_code'       =>  'PTCY',

                                        ),

                                        '14'    =>  array(

                                                    'field_name'            =>  'State',
                                                    'field_id'              =>  'exa_value',
                                                    'type'                  =>  'option',
                                                    'is_mandatory'          =>  1,
                                                    'option_data'           =>  $G->option_builder('entity_state','code, value','WHERE 1=1'),
                                                    #'default'              =>  'Tamilnadu',
                                                    'parent_field_id'       =>  'parent_id',
                                                    'child_table'           =>  'exav_addon_varchar',
                                                    'child_attr_field_id'   =>  'exa_token',
                                                    'child_attr_code'       =>  'PTSTE',

                                        ),

                                        '15'    =>  array(

                                                    'field_name'            =>  'Postal Code',
                                                    'field_id'              =>  'exa_value',
                                                    'type'                  =>  'text',
                                                    'allow'                 =>  'd6',
                                                    'parent_field_id'       =>  'parent_id',
                                                    'child_table'           =>  'exav_addon_varchar',
                                                    'child_attr_field_id'   =>  'exa_token',
                                                    'child_attr_code'       =>  'PTPIN',

                                        ),

                                        '16'     => array(

                                                    'field_name'            =>  'Care Taker Details',
                                                    'type'                  =>  'sub_heading',
                                        ),

                                        '17'    =>  array(

                                                    'field_name'            =>  'Care Taker Type',
                                                    'field_id'              =>  'exa_value',
                                                    'is_mandatory'          =>  1,
                                                    'type'                  =>  'option',
                                                    'option_data'           =>  '<option value = Father> Father </option>
                                                                                <option value = Mother>  Mother </option>
                                                                                <option value = Husband> Husband </option>
                                                                                <option value = Husband> Others </option>',
                                                    'parent_field_id'       =>  'parent_id',
                                                    'child_table'           =>  'exav_addon_varchar',
                                                    'child_attr_field_id'   =>  'exa_token',
                                                    'child_attr_code'       =>  'PTCTT',

                                        ),


                                        '18'    =>  array(

                                                    'field_name'            =>  'Care Taker Name',
                                                    'field_id'              =>  'exa_value',
                                                    'is_mandatory'          =>  1,
                                                    'type'                  =>  'text',
                                                    'parent_field_id'       =>  'parent_id',
                                                    'child_table'           =>  'exav_addon_varchar',
                                                    'child_attr_field_id'   =>  'exa_token',
                                                    'child_attr_code'       =>  'PTCTN',

                                        ),

                                        '19'    =>  array(

                                                    'field_name'            =>  'Other Details',
                                                    'type'                  =>  'heading',
                                        ),

                                        '20'    =>  array(

                                                    'field_name'            =>  'Blood Group',
                                                    'field_id'              =>  'exa_value',
                                                    'type'                  =>  'option',
                                                    'is_mandatory'          =>  1,
                                                    'option_data'           =>  $G->option_builder('entity_blood_group','code, value','WHERE 1=1'),
                                                    #'default'              =>  'Tamilnadu',
                                                    'parent_field_id'       =>  'parent_id',
                                                    'child_table'           =>  'exav_addon_varchar',
                                                    'child_attr_field_id'   =>  'exa_token',
                                                    'child_attr_code'       =>  'PTBG',

                                        ),

                                        '21'    =>  array(

                                                    'field_name'            =>  'Category',
                                                    'field_id'              =>  'exa_value',
                                                    'type'                  =>  'option',
                                                    'option_data'           =>  '<option value = In Patient> In Patient </option>
                                                                                 <option value = Out Patient> Out Patient </option>',
                                                    'is_mandatory'          =>  1,
                                                    'parent_field_id'       =>  'parent_id',
                                                    'child_table'           =>  'exav_addon_varchar',
                                                    'child_attr_field_id'   =>  'exa_token',
                                                    'child_attr_code'       =>  'PTCTR',

                                        ),

                                        '22'    =>  array(

                                                    'field_name'            =>  'Referred to',
                                                    'field_id'              =>  'exa_value',
                                                    'type'                  =>  'option',
                                                    'is_mandatory'          =>  1,
                                                    'option_data'           =>  $G->option_builder('entity_doctor','code, value','WHERE 1=1'),
                                                    'parent_field_id'       =>  'parent_id',
                                                    'child_table'           =>  'exav_addon_varchar',
                                                    'child_attr_field_id'   =>  'exa_token',
                                                    'child_attr_code'       =>  'PTREF',

                                        ),

                                        '23'    =>  array(

                                                    'field_name'            =>  'Comments',
                                                    'field_id'              =>  'exa_value',
                                                    'is_mandatory'          =>  0,
                                                    'type'                  =>  'textarea',
                                                    'parent_field_id'       =>  'parent_id',
                                                    'child_table'           =>  'exav_addon_varchar',
                                                    'child_attr_field_id'   =>  'exa_token',
                                                    'child_attr_code'       =>  'PTCOM',

                                        ),

                    ),//data ends here... 
                    
    );//end of F_Series...

?>