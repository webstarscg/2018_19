<?PHP 

    $D_SERIES   =   array(

                    #Table Title
                    'title'                 =>  'New Data Table',

                    #Table Name 
                    'table_name'            =>  'entity_child',

                    #Key ID
                    'key_id'                =>  'id',

                    #Query Show
                    'show_query'            =>  1,

                    #To avoid transcation key on url bar
                    'avoid_trans_key_direct'    =>  1,

                    'add_button'            =>  array(

                                                    'is_add'    =>  1,
                                                    'page_link' =>  'fx=patient_detail',
                                                    'b_name'    =>  'Add Patient',

                                                    ),//end of add button...

                    'key_filter'            =>  "AND entity_code = 'PT'",


                    
                    #custom filter
                    'custom_filter'     =>  array(

                                                array(

                                                    'field_name'        =>  'Filter by Gender',
                                                    'field_id'          =>  'cf1',
                                                    'filter_type'       =>  'option_list',
                                                    'option_value'      =>  $G->option_builder('entity_gender','code , sn','WHERE 1=1'),
                                                    'html'              =>  'title="Select Gender" data-width ="180px"',
                                                    'cus_default_label' =>  "Show All",
                                                    'filter_by'         =>  "get_exav_addon_varchar
                                                                                (entity_child.id,'PTGN')",

                                                ),

                    ),//custom filter ends here...


                    'data'                  =>  array(

                                            '1' =>  array(

                                                    'th'        =>  'Name',
                                                    'is_sort'   =>  1,
                                                    'field'     =>  "get_exav_addon_varchar
                                                                    (entity_child.id,'PTNA1')",
                                            ),

                                            '2' =>  array(

                                                    'th'        =>  'Gender',
                                                    'field'     =>  "get_exav_addon_varchar
                                                                    (entity_child.id,'PTGN')",
                                            ),

                                            '3' =>  array(

                                                            
                                                    'th'        =>  'Mobile',
                                                    'field'     =>  "get_exav_addon_varchar
                                                                    (entity_child.id,'PTMN')",

                                            ),
                                                        
                                            '4' =>  array(

                                                    'th'    =>  'City',
                                                    'field' =>  "get_exav_addon_varchar
                                                                    (entity_child.id,'PTCY')",

                                            ),

                                            '5' =>  array(

                                                    'th'        =>  'Visiting Date',
                                                    'field'     =>  "(SELECT date_format(date,'%d-%b-%Y'))",                                    
                                                    //'field'     =>  'date',//Raw data from DB
                                                    'is_sort'   =>  1,

                                            ),

                                ),//data array ends here....

                    #search info
                    'search'        =>      array(

                                                array(

                                                    'data'  =>  array(

                                                        'table_name'    =>  'entity_child',
                                                        'field_id'      =>  'id',
                                                        'field_name'    =>  "get_exav_addon_varchar(entity_child.id,'PTNA1')",
                                                        'filter'        =>  "AND entity_code = 'PT'",

                                                    ),//data array ends here...

                                                    'title'                 => 'Name',
                                                    'search_key'            =>  'id',
                                                    'is_search_by_text'     =>  0, 

                                                ),

                                                array(

                                                    'data'  =>  array(

                                                        'table_name'    =>  'entity_child',
                                                        'field_id'      =>  'id',
                                                        'field_name'    =>  "get_exav_addon_varchar(entity_child.id,'PTMN')",
                                                        'filter'        =>  "AND entity_code = 'PT'",

                                                    ),//data array ends here...

                                                    'title'                 => 'Mobile Number',
                                                    'search_key'            =>  'id',
                                                    'is_search_by_text'     =>  0, 

                                                ),

                    ),//search array ends here...


        
                    'del_permission'    => array( 'able_del'=>1),
                    'date_filter'  		=> array( 'is_date_filter'  =>  1,
                                                  'date_field'      =>  'date'),


                    

    );//end of D_Series...


?>