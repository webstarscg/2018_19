<?PHP
    
    $select_user_detail = " SELECT entity_child.id,
				    get_eav_addon_varchar(entity_child.id,'COFN') AS name,
				    get_eav_addon_date(entity_child.id,'CODB') AS date,
				    get_eav_addon_varchar(entity_child.id,'COEM') AS email,
				    get_eav_addon_varchar(entity_child.id,'COMB') AS mobile,
				    get_eav_addon_varchar(entity_child.id,'COLD') AS landline,
				    get_eav_addon_exa_token(entity_child.id,'COGE') AS gender,
				    get_eav_addon_varchar(entity_child.id,'CORA') AS postal_addr,
				    get_eav_addon_varchar(entity_child.id,'CORB') AS perman_addr
				FROM entity_child WHERE entity_child.entity_code = 'CO' AND id = (SELECT is_internal FROM user_info WHERE id = $USER_ID)";
	
	
	$exe_user_query= $rdsql->exec_query($select_user_detail,'Select user detail');
	
	$exe_user_query_val = $rdsql->data_fetch_assoc($exe_user_query);
	
	print_r ($exe_user_query_val['name']);
    
    
    
    
    
    			
    $default_addon = $_GET['default_addon'];
    
    $temp = explode(':',$default_addon);
    
    $status_code = $temp[0]; //ASBK
    
    $entity_child_id=$temp[1]; //17
    
    $entity_code = substr($temp[0],0,2); //AS
    
        $LAYOUT	    	= 'layout_full';
               
        $D_SERIES       =   array(
                                   'title'=>'Status',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
				    
				    'gx' => 1,
				    
                                    
                                    #table data
                                    
                                    'data'=> array( 
						        2=>array('th'=>'Status ',
								
								'field' =>"(SELECT sn FROM entity_child_base WHERE token = status_code)",
								
								'td_attr' => ' class="label_father align_LM" width="30%"',
								
								'is_sort' => 0,	
								
								),
							
							3=>array('th'=>'Note ',
								
								'field' =>"note",
								    
								'td_attr' => ' class="label_father align_LM" width="30%"',
								
								'is_sort' => 0,	
								
								),
							
							4=>array('th'=>'Date & Time ',
								
								'field'	=> "date_format(timestamp_punch,'%d-%b-%Y %T')",
                                                                
								'td_attr' => ' class="label_father align_LM" width="50%"',
								
								'is_sort' => 0,	
								
								),
					
                                                    ),
				    
					
                                    #Table Info
                                    
                                    'table_name' =>'status_info',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
				    
				    'key_filter'     =>	 " AND entity_code='$entity_code' AND entity_child_id=$entity_child_id",
				    
                                    # Communication
                                
                                    'prime_index'   => 1,
                                
				
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>0,'page_link'=>'', 'b_name' => '' ),
								
					'del_permission' => array('able_del'=>0,'user_flage'=>0), 
								
					'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  ''),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'STATUS',
				
				'show_query'=>0,
				
				'hide_show_all' => 1,
				
				'search_filter_off'	=>1,
                            
                            );
    
?>