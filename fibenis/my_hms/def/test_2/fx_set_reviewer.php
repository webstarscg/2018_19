<?PHP

    $parent_id = $_GET['default_addon'];
    
    $F_SERIES	=	array(
				#Title
				
				'title'	=>'Set Reviewer',
				
				#Table field
                    
				'data'	=>   array(
						   
						     
						     '1' =>array( 'field_name'=> 'Select Reviewer', 
                                                               
								'field_id' => 'ec_id',
                                                               
                                                                'type' => 'option',
							       
								'option_data'=> $G->option_builder('entity_child','id,get_contact_name(id)',
												   "WHERE id IN (SELECT is_internal FROM
												   user_info WHERE user_role_id = (SELECT id FROM
												   user_role WHERE sn ='REW')
												   ) ORDER BY get_eav_addon_varchar(id,'COFN') ASC"),
								
							       'child_table'         => 'exav_addon_ec_id', // child table
							       
							       //'parent_field_id'     => 'parent_id',    // parent field
							          
							       'child_attr_field_id' => 'exa_token',   // attribute code field
							       
							       'child_attr_code'     => 'REVW',           // attribute code
							       
							       'is_mandatory'=>1,
							       
							       'input_html'=>'class="w_100"', 
								
							    ),
					    ),
				
                                    
				'table_name'    => 'exav_addon_ec_id',
				
				'key_id'        => 'id',
				
				'default_fields'    => array(
							    'parent_id' => "'$parent_id'",
							    ),
				
				'is_user_id'       => 'user_id',
				
				'back_to'  => NULL,
				
				'flat_message' => 'Sucesssfully Updated',
				
				'page_code'	=> 'REIW',
				
				'show_query'    =>1,
				
				'after_add_update'=>1,
				
				'before_add_update'=>0,
                                
			);
    
    
    if($a=='modify'){
	
	$F_SERIES['before_add_update'] = 1;
    }
    
    function before_add_update(){
    
	//$parent_id = $_GET['default_addon'];
	
	$new_reviewer_id = $_POST['X1'];
	
	$project_id = $_GET['key'];
	
	$existing_reviewer_id = $rdsql->exec_query("SELECT ec_id FROM exav_addon_ec_id WHERE parent_id = $project_id
				     AND exa_token = 'REVW'","Selection of existing reviewer Failed");
    
	
	$existing_reviewer_id_val = $rdsql->data_fetch_row($existing_reviewer_id);
	
	//$reviewer_for_project = implode(',',[$new_reviewer_id,$existing_reviewer_id_val[0]]);
	
	$reviewer_project = $rdsql->exec_query("SELECT child_id FROM user_ec_info WHERE
						parent_id = $existing_reviewer_id_val[0] ",
					       "Selection of existing reviewer project Failed");
	
	
	$reviewer_project_val = $rdsql->data_fetch_row($reviewer_project);
	
	$projects_assigned = implode(',',array_diff(explode(',',$reviewer_project_val),[$project_id]));
	
	//$projects_assigned = explode(',',$reviewer_project_val);
	//
	//$projects_assigned = array_diff($projects_assigned,[$project_id]);
	//
	//$projects_assigned = implode(',',$projects_assigned);
	
	$update_reviewer_projects = $rdsql->exec_query("UPDATE user_ec_info SET child_id = '$projects_assigned'
								   WHERE id parent_id = $existing_reviewer_id_val[0] ","Updation of reviewer projects Failed");
		
     }
    
    
    
     function after_add_update($key_id){
	
		global $rdsql,$USER_ID;
	    
	    //
	    // To update projects for reviewer 
	    //
		
		$reviewer_id = $_POST['X1'];
		
		
		$reviewer_project = $rdsql->exec_query("SELECT child_id FROM user_ec_info WHERE
						       parent_id = $reviewer_id ",
						       "Selection of reviewer project Failed");
		
		$reviewer_project_val = $rdsql->data_fetch_row($reviewer_project);
		
		//First entry
		if(@$reviewer_project_val[0]==null){
		   
		    $insert_reviewer_projects = $rdsql->exec_query("INSERT INTO user_ec_info (parent_id,child_id,user_id)
						    VALUES ($reviewer_id,$key_id,$USER_ID)",
						    "Insertion in user_ec_info Failed");
		}
		
		//updation of project list
		else{
		    
		    $project_id = $reviewer_project_val[0].','.$key_id;
		    
		    $update_reviewer_projects = $rdsql->exec_query("UPDATE user_ec_info SET child_id = '$project_id'
								   WHERE id parent_id = $reviewer_id ","Updation of reviewer projects Failed");
		
		}
		  
		return null;
    }

?>