<?PHP


if(isset($_GET['default_addon'])){
    
    $find_id = $rdsql->exec_query("SELECT id FROM entity_child WHERE entity_code = 'NP' AND user_id = $USER_ID","Selection Fails");
	    
    $value = $rdsql->data_fetch_row($find_id);
        
    if(($value[0]==NULL)|| (!$value[0])){
        
	$set_query  = "INSERT INTO entity_child (entity_code,created_on,user_id) VALUES ('NP',NOW(),$USER_ID)";
       
        $exe_query= $rdsql->exec_query($set_query,'new_entry');
       
        $get_last_id = $rdsql->last_insert_id('entity_child');
	
	
	
	//Insert Batch
	
	//$Select_batch = $rdsql->exec_query("SELECT sn FROM entity_child_base WHERE token = (SELECT entity_value FROM entity_key_value WHERE entity_key = 'current_batch')","Selection of batch failed");
	
	$Select_batch = $rdsql->exec_query("SELECT entity_value FROM entity_key_value WHERE entity_key = 'current_batch'","Selection of batch failed");
	
	$batch = $rdsql->data_fetch_row($Select_batch);
	
	$insert_batch_query  = "INSERT INTO exav_addon_varchar (parent_id,exa_token,exa_value,user_id) VALUES ($get_last_id,'BATCH','$batch[0]',$USER_ID)";
       
        $exe_insert_batch_query= $rdsql->exec_query($insert_batch_query,'new_entry');
       
	
	
	//User information to pre-fill in the form
	
	
	$select_user_detail = " SELECT entity_child.id,
				    get_eav_addon_varchar(entity_child.id,'COFN') AS name,
				    get_eav_addon_date(entity_child.id,'CODB') AS date,
				    get_eav_addon_varchar(entity_child.id,'COEM') AS email,
				    get_eav_addon_varchar(entity_child.id,'COMB') AS mobile,
				    get_eav_addon_varchar(entity_child.id,'COLD') AS landline,
				    get_eav_addon_exa_token(entity_child.id,'COGE') AS gender,
				    get_eav_addon_varchar(entity_child.id,'CORA') AS postal_addr,
				    get_eav_addon_varchar(entity_child.id,'CORB') AS perman_addr
				FROM entity_child WHERE entity_child.entity_code = 'CO' AND id = (SELECT is_internal FROM user_info WHERE id = $USER_ID)";
	
	
	$exe_user_query= $rdsql->exec_query($select_user_detail,'Select user detail');
	
	$exe_user_query_val = $rdsql->data_fetch_assoc($exe_user_query);
	
	$insert_varchar = "INSERT INTO exav_addon_varchar (parent_id,exa_token,exa_value,user_id) VALUES
			   ($get_last_id,'NP1NOA','$exe_user_query_val[name]',$USER_ID),
			   ($get_last_id,'NP1EM','$exe_user_query_val[email]',$USER_ID),
			   ($get_last_id,'NP1MB','$exe_user_query_val[mobile]',$USER_ID),
			   ($get_last_id,'NP1LN','$exe_user_query_val[landline]',$USER_ID),
			   ($get_last_id,'NP1GN','$exe_user_query_val[gender]',$USER_ID)";
	      
	$exe_varchar_query= $rdsql->exec_query($insert_varchar,'insert_varchar');
	
	$insert_date  = "INSERT INTO exav_addon_date (parent_id,exa_token,exa_value,user_id) VALUES ($get_last_id,'NP1DOB','$exe_user_query_val[date]',$USER_ID)";
	      
	$exe_date_query= $rdsql->exec_query($insert_date,'insert_date');
	
	$insert_token  = "INSERT INTO exav_addon_exa_token (parent_id,exa_token,exa_value_token,user_id) VALUES
			  ($get_last_id,'STAT','SANW',$USER_ID)";
	      
	$exe_insert_token = $rdsql->exec_query($insert_token,'insert_token');
	
	$insert_text  = "INSERT INTO exav_addon_text (parent_id,exa_token,exa_value,user_id) VALUES 
			($get_last_id,'NP1POS','$exe_user_query_val[postal_addr]',$USER_ID),
			($get_last_id,'NP1PER','$exe_user_query_val[perman_addr]',$USER_ID)";
	      
	$exe_text_query = $rdsql->exec_query($insert_text,'insert_text');
	
	
	header('location:?fx=prayas&menu_off=0&key='.$get_last_id);

    }
}



//To re-direct if project is submitted

$find_project_status = $rdsql->exec_query("SELECT exa_value_token FROM exav_addon_exa_token
					  WHERE user_id = $USER_ID AND exa_token ='STAT'
					  AND parent_id =
					  (SELECT id FROM entity_child WHERE user_id = $USER_ID
					  AND entity_code ='NP')","Selection Fails");
	    
$find_project_status_value = $rdsql->data_fetch_row($find_project_status);

if($find_project_status_value[0]=='SASU'){

    header("Location:?dx=programme");
    
}




//To allow only specified user and admin to view record

$find_user_project_id = $rdsql->exec_query("SELECT id FROM entity_child WHERE user_id = $USER_ID AND entity_code ='NP'","Selection Fails");
	    
$find_user_project_id_value = $rdsql->data_fetch_row($find_user_project_id);


  
if((($_GET['key'])==$find_user_project_id_value[0])||($user_role == 'ADM')){
    
    
    
    include_once($LIB_PATH."/inc/lib/f_addon.php");
                        
    $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Form',
                                
                                'gx'=>1,
				
				#Table field
                    
				'data'	=>   array(
						   
						//   '1'  => ['field_name'=>'Application Details',
						//		 'type'=>'heading'					 
						//		],
						//   
						
				    
                                ),
                                    
				'table_name'    => 'entity_child',
				
			        'key_id'        => 'id',
                                
				'is_user_id'       => 'created_by',
								
				#'add_button' => array( 'is_add' =>1,'page_link'=>'f=entity_child', 'b_name' => 'Add Innovation' ),
				
				'button_name' => 'Submit Form',
                     
                                'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?dx=programme', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 2,
                                
                                'form_layout'   => 'form_100',
				
				'js'=> ['is_top'=>1, 'top_js'=>"def/prayas/fx"], 
                                
                                'after_add_update'	=>1,
				
				'session_off'           =>0, 
				
				'divider' => 'accordion', 
				
				'page_code'	=> 'PRAYAS',
				
				'is_save_form'=>1,
				
				//'avoid_trans_key_direct' => 1,
				
				'button_name' => 'Submit Form'
				
                                
			);
    
	    $default_addon = 'NP';
	    	    
	    //$F_SERIES['data'][2]['option_data']          = $G->option_builder('entity','code,sn'," WHERE code='$default_addon' ORDER by sn ASC "); 
	    //
	    //$F_SERIES['data'][2]['avoid_default_option'] = 1;
	    
	    $F_SERIES['deafult_value']    = array('entity_code' => "'$default_addon'");
	    
	   
	    @$F_SERIES['temp']=f_addon(['g'		   => $G,
					'rdsql'		   => $rdsql,
					'field_label'      => 'ln',
					'f_series'     	   => ['data'=>$F_SERIES['data']],
					'default_addon'	   => json_encode(['en'=>$default_addon])	
				]);
		    
	    $F_SERIES['data']=$F_SERIES['temp']['data'];
	    
	    if ($F_SERIES['session_off'] == 1){
		
		$USER_ID = 0;
 	    }
	    
}
	    //print_r ($F_SERIES);
	    #  grid pre-fill
	    //$F_SERIES['data'][43]['default_data']= json_encode([['A'],['B']]);

	    // header
	    $F_SERIES['header'] = array('header_content'=>'<div>&nbsp;</div><div><img src="images/logo/nidhi_prayas.jpg" width="150px" class="brdr_clr_gray_8"><h4 class="pad_10_t">Application for PSG-STEP : NIDHI-PRAYAS</h4>
					                   <span class="clr_gray_b txt_size_12">Fellowship Support for Aspiring Entrepreneurs </span></div>
							   <div class="col-md-2 pad_lr align_LM">&nbsp;</div>
							   <div class="col-md-8 pad_10_tb pad_lr align_CM clr_gray_a"><span class="clr_red txt_size_18">*</span>Please refer the NIDHI-PRAYAS eligibility guideline before you apply. <a target="_blank" href="doc/nidhi_prayas_eligibility.pdf" class="clr_red">Click here to check..</a></div>
							   <div class="col-md-2 pad_lr align_LM">&nbsp;</div>',				   
					
					'header_style'=>'col-md-12 align_CM row');
	    
	    
	//    $F_SERIES['footer'] = array('footer_content'=>'<div class="col-md-12 brdr_bottom">&nbsp;</div>Please give the details properly.'.
	//				                  '<br>Referenece Number will be sent to the registered e-mail address, for further clarification kindly mail to : step@psgtech.edu .<br>
	//						  <u>NOTE</u><br>click  "SAVE FORM"  button to save the information. <br>click "SUBMIT FORM" button to complete the registration process. <br><br><br><br><br>',
	//				 
	//				 'footer_style'  =>'align_CM clr_gray_7 label_grand_child txt_size_15 mar_TB_5');
	//			   
	//    $F_SERIES['footer'] = array('footer_content'=>'<div class="col-md-12 brdr_bottom">&nbsp;</div>
	//						  <u>NOTE:</u><br>click  "SAVE FORM"  button to save the information. <br>click "SUBMIT FORM" button to complete the registration process. <br><br><br><br><br><br><br>',
	//				 
	//				 'footer_style'  =>'align_left clr_gray_7 label_grand_child txt_size_15 mar_TB_5');
	//			   
	   

	    
	    // after add update
	    
	    
	    //redirect to all_programme desk while chosing back if user is admin or super_admin
	    
	    if(($USER_ROLE == 'ADM')||($USER_ROLE == 'SAD')){	
	     
		$F_SERIES['back_to']  = array( 'is_back_button' =>1, 'back_link'=>'?dx=all_programme', 'BACK_NAME'=>'Back');
	    
	    }
	    
	    $F_SERIES['after_add_update'] = function($last_insert_id){
		
			global $G,$rdsql,$USER_ID,$USER_NAME,$USER_EMAIL,$CONFIG,$F_SERIES,$COACH;
			
			$param = array('user_id'=>$USER_ID,
							       
								'page_code'=>$F_SERIES['page_code'],
								
								'action_type'=>'FVEW',
								
								'action'=>'View '.$last_insert_id.' page');
						
			$G->set_system_log($param);
			
			$domain_name = $COACH['name_hash'];
			
			$code = 'NP';
			
			//To update application stataus
			
			$update_status = $rdsql->exec_query("UPDATE exav_addon_exa_token SET exa_value_token = 'SASU' WHERE parent_id = $last_insert_id AND exa_token = 'STAT'","Updation Of Status Failed");
			
			
			
			//To generate reference number
			
			$gen_ref_no = $rdsql->exec_query("SELECT NP_get_ref_no('$domain_name')","Generate reference no Failed");
			
			$gen_ref_no_val = $rdsql->data_fetch_row($gen_ref_no);
			
			
			
			//To avoid duplication of reference number
			
			$select_ref_no = $rdsql->exec_query("SELECT exa_value FROM exav_addon_varchar WHERE exa_token = 'REFN' AND parent_id = $last_insert_id","Selection of ref no");
			
			$select_ref_no_val = $rdsql->data_fetch_row($select_ref_no);
			
			if(!$select_ref_no_val[0]){
			    
			    $ins_ref = $rdsql->exec_query("INSERT into exav_addon_varchar (parent_id,exa_token,exa_value,user_id) VALUES ($last_insert_id,'REFN','$gen_ref_no_val[0]',$USER_ID)","Selection of Count Failed");
			
			}
			
			$CONFIG['title'] = 'PSG STEP - NIDHI PRAYAS - Admin';
			
			$MAIL=array(
								'from'    => get_config('smtp_mail').' PSG STEP - NIDHI PRAYAS - Admin',					
								'to'      => $USER_EMAIL, //'ratbew@gmail.com',
								'cc'	  =>  get_config('cc_mail'),
								'bcc'	  => get_config('bcc_mail'),
								'subject' => 'PSG-STEP | NIDHI-PRAYAS | Registration Confirmation',					
								'message' => 'Dear '.$USER_NAME.',<br/><br/>Thank you for submitting your proposal with us. Kindly make a note of your application reference number: <b>'.$gen_ref_no_val[0].'.</b><br><br>We will reach out to you very soon.</br></br><br/><br/>Regards,<br/>Team PSG-STEP<br/>'
											  
			);
			   
			mail_send_smtp($MAIL);
		    
		    $temp_key = md5($G->encrypt($last_insert_id,'registration'.time().rand()));
			
		    setcookie($temp_key,
			      'Thank you for your Registration.',
			      (time()+360));
		
		    $F_SERIES['avoid_trans_key_direct']=1;
	   
		    //header("Location:?dx=programme");
		    echo '<script>location.href="?dx=programme";</script>';
		    
		
	    } // end of update
	    
?><style type="text/css">
	
	
	/* @media Css Start */
	/* ====== === ===== */

	
	@media only screen and (max-width:700px)
	{
	  
	    fieldset span.label{
		
		width	:100% !important;
		    }
	
	    fieldset span.value{
		
		width	:100% !important;

		    }
		    
	    fieldset{
		width:10% !important;
		    }
		    
		    fieldset .row {
			border-bottom: 1px solid #d3d3d3 !important;
			display: block;
			font-size: 13px;
			width: 100% !important;
		    }
		   
	}
	
	/* @media Css end */
	/* ====== === === */
	
	
	body,
	fieldset{
		background-color: #f9f9f9 !important;
	}
	
	fieldset{
		width:75% !important;
	}
	
	.form_action_row,
	.form_action_row .label,
	.form_action_row .value{
		background-color: #f9f9f9 !important;
	}
	
	.form_action_row .value{
	    padding-left:30px !important;
	}
	
	#SAVE,
	#ADD{
	    float: right;
	    
	}
	#SAVE{
	     margin-right: 10px;
	}
	
	
	.NP7JBA,
	.NP8FSPW,
	.NP8PFD,
	.NP4PERI,
	.NP4BDJUS,
	.NP4COTI
	
		{
	    font-style: normal;
	    border-bottom:1px solid #f1f1f1;
	    float:left;
	    width:100%;
	}
	
	#X1 span.label,
	.NP1H .label,
	.NP10H .label,
	.NP9ES .label,
	.NP7JBA .label,
	.NP8FSPW .label,
	.NP8PFD .label,
	.NP4PERI .label,
	.NP4BDJUS .label,
	.NP4COTI .label,
	.NP3H .label,
	.NP2H .label,
	.NP7FT .label
		    {
	    white-space: normal !important;
	    color: #000000;
	    line-height: 22px;
	    display	:inline-block;
	    margin-left :3% !important;
	    margin-right:2% !important;
	    width	:72%;	    
	}
	
	#X1 span.value,
	.NP1H .value,
	.NP10H .value,
	.NP9ES .value,
	.NP7JBA .value,
	.NP8FSPW .value,
	.NP8PFD .value,
	.NP4PERI .value,
	.NP4BDJUS .value,
	.NP3H .value,
	.NP2H .value
		    {
	    color: #0056b7; 
	    display	:inline-block;
	    width	:20%;	    
	}
	
	/* About Project Toogle */
	/* ===== ======= ====== */
	
	.NP3WCO span{
	    
	    font-size:14px !important;
	}
	
	.NP3WCO7 .label,
	.NP3WCO6 .label,
	.NP3WCO5 .label,
	.NP3WCO4 .label,
	.NP3WCO3 .label,
	.NP3WCO2 .label,
	.NP3WCO1 .label{
	    color: #808080 !important;
	    width: 50% !important;
	}
	
	.NP3WCO7 .value,
	.NP3WCO6 .value,
	.NP3WCO5 .value,
	.NP3WCO4 .value,
	.NP3WCO3 .value,
	.NP3WCO2 .value,
	.NP3WCO1 .value{
	    width: 40% !important;
	}
	
	
	/*X1 Tab Fiben Table Only*/
	
	.NP1TEAM .label{
		width: 100% !important;
    	}
	
	.NP1TEAM .value{
		width: 100% !important;
    	}
	
	/*For  X1 Tab Only*/
	/*=== ==== ==== ====*/
	
	.NP1H,
	.NP10H,
	.NP3H,
	.NP2h
	{
	    
	    border-bottom:1px solid #f1f1f1 !important;
	    display: block;
	    font-size: 13px;
	    width: 100% ;
	    
	     
	}
	
	#X1 span.label,
	.NP1NOA .label,
	.NP1H .label,
	.NP10H .label,
	.NP3H .label,
	.NP2H .label{
	    
	    width	:20%; 	    
	}
	
	
	#X1 span.value,
	.NP1NOA .value,
	.NP1H .value,
	.NP10H .value,
	.NP3H .value,
	.NP2H .value{
	   
	    text-align: left !important;
    	    width	:70%;
	    
	    
	}
	
	/* Special for below fiben table*/
	/* ======= === ===== ===== =====*/
	.NP4PERI .label,
	.NP4BDJUS .label,
	.NP7JBA .label,
	.NP8FSPW .label,
	.NP8PFD .label{
	    
	    width	:30%; 	    
	}
	
	.NP4PERI .value,
	.NP4BDJUS .value,
	.NP7JBA .value,
	.NP8FSPW .value,
	.NP8PFD .value{
	   
	    text-align: left !important;
    	    width	:60% ;
	    
	    
	}
	
	/*For  Custom Correction Only*/
	/*===  ====== ========== ====*/
	
	.NP9ES1 .label,
	.NP9ES2 .label,
	.NP9ES3 .label,
	.NP9ES4 .label,
	.NP9ES5 .label,
	.NP9ES6 .label
	{
	    white-space: normal !important;
	    color: #000000;
	    line-height: 22px;
	    display	:inline-block;
	    margin-left :3% !important;
	    margin-right:2% !important;
	    width	:60%;	    
	}
	
	
	
	
	/*Special class for lable only*/
	/*======= ===== === ===== ====*/
	.block{
	    display: block !important;
	    margin-left: 2% !important;
	    color: #000000 !important;
	}
	
	.NP9ES1 .value,
	.NP9ES2 .value,
	.NP9ES3 .value,
	.NP9ES4 .value,
	.NP9ES5 .value,
	.NP9ES6 .value
	{
	    
	    display	:inline-block !important;
	    width	:30% !important;
	    margin-left :2% !important;
	    padding-top :10px;
	}
	
	.NP4COTI .value{
	    margin-left :2% !important;
	}
	
	footer{
	    display:none !important;
	}
	
	footer .container{
	    display:none !important;
	}
	
	
    
</style>
