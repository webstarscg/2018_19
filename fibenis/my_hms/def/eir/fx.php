<?PHP


    // default addon case
    if(isset($_GET['default_addon'])){
	
	$find_id = $rdsql->exec_query("SELECT id FROM entity_child WHERE entity_code = 'EI' AND user_id = $USER_ID","Selection Fails");
		
	$value = $rdsql->data_fetch_row($find_id);
	    
	if(($value[0]==NULL)|| (!$value[0])){
	    
	    $set_query  = "INSERT INTO entity_child (entity_code,created_on,user_id) VALUES ('EI',NOW(),$USER_ID)";
	   
	    $exe_query= $rdsql->exec_query($set_query,'new_entry');
	   
	    $get_last_id = $rdsql->last_insert_id('entity_child');
	    
	    
	    
	    //Insert Batch
	    
	    //$Select_batch = $rdsql->exec_query("SELECT sn FROM entity_child_base WHERE token = (SELECT entity_value FROM entity_key_value WHERE entity_key = 'current_batch')","Selection of batch failed");
	    
	    $Select_batch = $rdsql->exec_query("SELECT entity_value FROM entity_key_value WHERE entity_key = 'current_batch'","Selection of batch failed");
	    
	    $batch = $rdsql->data_fetch_row($Select_batch);
	    
	    $insert_batch_query  = "INSERT INTO exav_addon_varchar (parent_id,exa_token,exa_value,user_id) VALUES ($get_last_id,'BATCH','$batch[0]',$USER_ID)";
	   
	    $exe_insert_batch_query= $rdsql->exec_query($insert_batch_query,'new_entry');
	   
	    
	    
	    //User information to pre-fill in the form
	    
	    
	    $select_user_detail = " SELECT entity_child.id,
					get_eav_addon_varchar(entity_child.id,'COFN') AS name,
					get_eav_addon_date(entity_child.id,'CODB') AS date,
					get_eav_addon_varchar(entity_child.id,'COEM') AS email,
					get_eav_addon_varchar(entity_child.id,'COMB') AS mobile,
					get_eav_addon_varchar(entity_child.id,'COLD') AS landline,
					get_eav_addon_exa_token(entity_child.id,'COGE') AS gender,
					get_eav_addon_varchar(entity_child.id,'CORA') AS postal_addr,
					get_eav_addon_varchar(entity_child.id,'CORB') AS perman_addr
				    FROM entity_child WHERE entity_child.entity_code = 'CO' AND id = (SELECT is_internal FROM user_info WHERE id = $USER_ID)";
	    
	    
	    $exe_user_query= $rdsql->exec_query($select_user_detail,'Select user detail');
	    
	    $exe_user_query_val = $rdsql->data_fetch_assoc($exe_user_query);
	    
	    $insert_varchar = "INSERT INTO exav_addon_varchar (parent_id,exa_token,exa_value,user_id) VALUES
			       ($get_last_id,'EI1AP','$exe_user_query_val[name]',$USER_ID),
			       ($get_last_id,'EI1EA','$exe_user_query_val[email]',$USER_ID),
			       ($get_last_id,'EI1MB','$exe_user_query_val[mobile]',$USER_ID),
			       ($get_last_id,'EI1GE','$exe_user_query_val[gender]',$USER_ID)";
		  
	    $exe_varchar_query= $rdsql->exec_query($insert_varchar,'insert_varchar');
	    
	    //$insert_date  = "INSERT INTO exav_addon_date (parent_id,exa_token,exa_value,user_id) VALUES ($get_last_id,'NP1DOB','$exe_user_query_val[date]',$USER_ID)";
		  
	    //$exe_date_query= $rdsql->exec_query($insert_date,'insert_date');
	    
	    $insert_token  = "INSERT INTO exav_addon_exa_token (parent_id,exa_token,exa_value_token,user_id) VALUES
			      ($get_last_id,'STAT','SANW',$USER_ID)";
		  
	    $exe_insert_token = $rdsql->exec_query($insert_token,'insert_token');
	    
	    $insert_text  = "INSERT INTO exav_addon_text (parent_id,exa_token,exa_value,user_id) VALUES 
			    ($get_last_id,'EI1FPA','$exe_user_query_val[postal_addr]',$USER_ID)";
		  
	    $exe_text_query = $rdsql->exec_query($insert_text,'insert_text');
	    
	    
	    header('location:?fx=eir&menu_off=0&key='.$get_last_id);
    
	}
    } // end of default addin case
    
    
    
    //To re-direct if project is submitted
    
    $find_project_status = $rdsql->exec_query("SELECT exa_value_token FROM exav_addon_exa_token
					      WHERE user_id = $USER_ID AND exa_token ='STAT'
					      AND parent_id =
					      (SELECT id FROM entity_child WHERE user_id = $USER_ID
					      AND entity_code ='EI')","Selection Fails");
		
    $find_project_status_value = $rdsql->data_fetch_row($find_project_status);
    
    if($find_project_status_value[0]=='SASU'){
	
	header("Location:?dx=programme");
	
    }
    

    //To allow only specified user and admin to view record

    $find_user_project_id = $rdsql->exec_query("SELECT id FROM entity_child WHERE user_id = $USER_ID AND entity_code ='EI'","Selection Fails");
		
    $find_user_project_id_value = $rdsql->data_fetch_row($find_user_project_id);

  
    if((($_GET['key'])==$find_user_project_id_value[0])||($user_role == 'ADM')){
    
	include_once($LIB_PATH."/inc/lib/f_addon.php");
			    
	$F_SERIES	=	array(
				   
				  
				    
				    'title'	=>'Form',
				    
				    'gx'=>1,
				    
				    #Table field
			
				    'data'	=>   array(),
					
				    'table_name'    => 'entity_child',
				    
				    'key_id'        => 'id',
				    
				    'is_user_id'       => 'created_by',
								    
				    #'add_button' => array( 'is_add' =>1,'page_link'=>'f=entity_child', 'b_name' => 'Add Innovation' ),
				    
				    //'is_custom_button' => 'Submit Form',
			 
				    'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?dx=programme', 'BACK_NAME'=>'Back'),
				    
				    'prime_index'   => 2,
				    
				    'form_layout'   => 'form_100',
				    
				    //'deafult_value'    => array('entity_code' => "'EI'"),
				    
				    'js'=> ['is_top'=>1, 'top_js'=>"def/eir/fx"], 
				    
				    'after_add_update'	=>1,
				    
				    'session_off'     =>0, 
				    
				    'divider' => 'accordion', 
				    
				    'page_code'	=> 'NEIR',
				    
				    'button_name'	=> 'Submit Form',
				    
				    'is_save_form'=>1
				    
				    
			    );
	
		$default_addon = 'EI';
			
		$F_SERIES['deafult_value']    = array('entity_code' => "'$default_addon'");
		
		@$F_SERIES['temp']=f_addon(['g'		   => $G,
					    'rdsql'		   => $rdsql,
					    'field_label'      => 'ln',
					    'f_series'     	   => ['data'=>$F_SERIES['data']],
					    'default_addon'	   => json_encode(['en'=>$default_addon])	
				    ]);
			
		$F_SERIES['data']=$F_SERIES['temp']['data'];
    
				
		// header
		$F_SERIES['header'] = array('header_content'=>'<div>&nbsp;</div><div><img src="images/logo/nidhi_eir.jpg" width="200px" class="brdr_clr_gray_8"><h4 class="pad_10_t">Application for PSG-STEP : NIDHI-EIR</h4>
					                   <span class="clr_gray_b txt_size_12">Nurturing Budding Entrepreneurs </span></div>
							   <div class="col-md-2 pad_lr align_LM">&nbsp;</div>
							   <div class="col-md-8 pad_10_tb pad_lr align_CM clr_gray_a"><span class="clr_red txt_size_18">*</span>Please refer the NIDHI-EIR eligibility guideline before you apply. <a target="_blank" href="doc/nidhi_eir_eligibility.pdf" class="clr_red">Click here to check..</a></div>
							   <div class="col-md-2 pad_lr align_LM">&nbsp;</div>',				   
					
					'header_style'=>'col-md-12 align_CM row');
		
		//     $F_SERIES['footer'] = array('footer_content'=>'<div class="col-md-12 brdr_bottom">&nbsp;</div>
		//						  <u>NOTE:</u><br>click  "SAVE FORM"  button to save the information. <br>click "SUBMIT FORM" button to complete the registration process. <br><br><br><br><br><br><br>',
		//				 
		//				 'footer_style'  =>'align_left clr_gray_7 label_grand_child txt_size_15 mar_TB_5');
		//			   
				   
		   
	       
		
		
    } // end of project pre-fill
    
	    //redirect to all_programme desk while chosing back if user is admin or super_admin
	    
	    if(($USER_ROLE == 'ADM')||($USER_ROLE == 'SAD')){	
	     
		$F_SERIES['back_to']  = array( 'is_back_button' =>1, 'back_link'=>'?dx=all_programme', 'BACK_NAME'=>'Back');
	    
	    }
    
    
	    // after add update
	    
	    $F_SERIES['after_add_update'] = function($last_insert_id){
		
		global $G,$rdsql,$USER_ID,$USER_NAME,$USER_EMAIL,$CONFIG,$F_SERIES,$COACH;
		
		//////////////////////////////////////////////////////////
		
		
		$code = 'EI';
		
		$domain_name = $COACH['name_hash'];
			
			//To update application stataus
			
			$update_status = $rdsql->exec_query("UPDATE exav_addon_exa_token SET exa_value_token = 'SASU' WHERE parent_id = $last_insert_id AND exa_token = 'STAT'","Updation Of Status Failed");
			
			
			
			//To generate reference number
			
			$gen_ref_no = $rdsql->exec_query("SELECT EI_get_ref_no('$domain_name')","Generate reference no Failed");
			
			$gen_ref_no_val = $rdsql->data_fetch_row($gen_ref_no);
			
			
			
			//To avoid duplication of reference number
			
			$select_ref_no = $rdsql->exec_query("SELECT exa_value FROM exav_addon_varchar WHERE exa_token = 'REFN' AND parent_id = $last_insert_id","Selection of ref no");
			
			$select_ref_no_val = $rdsql->data_fetch_row($select_ref_no);
			
			if(!$select_ref_no_val[0]){
			    
			    $ins_ref = $rdsql->exec_query("INSERT into exav_addon_varchar (parent_id,exa_token,exa_value,user_id) VALUES ($last_insert_id,'REFN','$gen_ref_no_val[0]',$USER_ID)","Selection of Count Failed");
			
			}
		
		//////////////////////////////////////////////////////////////
		
		
			
		$CONFIG['title'] = 'PSG STEP - NIDHI EIR - Admin';
			
		$MAIL=array(
					    'from'    => get_config('smtp_mail').'PSG STEP - NIDHI EIR - Admin',
					    'to'      => $USER_EMAIL, //'ratbew@gmail.com',
					    'cc'	  =>  get_config('cc_mail'),
					    'bcc'	  => get_config('bcc_mail'),
					    'subject' => 'PSG-STEP | NIDHI-EIR | Registration Confirmation',					
					    'message' => 'Dear '.$USER_NAME.',<br/><br/>Thank you for submitting your application with us. Kindly make a note of your application reference number: <b>'.$gen_ref_no_val[0].'.</b><br><br>We will reach out to you very soon.</br></br><br/><br/>Regards,<br/>Team PSG-STEP<br/>'
											  
			);
			   
		mail_send_smtp($MAIL);
	
		$temp_key = md5($G->encrypt($last_insert_id,'registration'.time().rand()));
			
		setcookie($temp_key,
			      'Thank you for your Registration. Please check your e-mail for further proceedings.',
			      (time()+360));
		    
		
		$F_SERIES['avoid_trans_key_direct']=1;
	   
		//header('Location:?dx=programme');
		echo '<script>location.href="?dx=programme";</script>';
		
	    } // end of update
	
	    
	
?>

<style type="text/css">
	
	/* @media Css Start */
	/* ====== === ===== */

	
	@media only screen and (max-width:700px)
	{
	  
	    fieldset span.label{
		
		width	:100% !important;
		    }
	
	    fieldset span.value{
		
		width	:100% !important;

		    }
		    
	    fieldset{
		width:10% !important;
		    }
	}
	
	/* @media Css end */
	/* ====== === === */
	
	
	body,
	fieldset{
		background-color: #f9f9f9 !important;
	}
	
	fieldset{
		width:75% !important;
	}
	
	.form_action_row,
	.form_action_row .label,
	.form_action_row .value{
		background-color: #f9f9f9 !important;
	}
	
	.form_action_row .value{
	    padding-left:30px !important;
	}
	
	#SAVE,
	#ADD{
	    float: right;
	    
	}
	#SAVE{
	     margin-right: 10px;
	}
	.TITLE,
	.EI3IDEA,
	.EI3KNW,
	.EI3TECHYN,
	.EI3IDEAYN,
	.EI3KNWYN,
	.E101,
	.E102,
	.E103,
	.E104,
	.E105,
	.E106{
	    font-style: normal;
	    border-bottom:1px solid #f1f1f1;
	    float:left;
	    width:100% !important;
	}
	
	.TITLE .label,
	.EI3IDEA .label,
	.EI3KNW .label,
	.EI3TECHYN .label,
	.EI3IDEAYN .label,
	.EI3KNWYN .label,
	.E101 .label,
	.E102 .label,
	.E103 .label,
	.E104 .label,
	.E105 .label,
	.E106 .label{
	    white-space: normal !important;
	    color: #000000;
	    line-height: 22px;
	    display	:inline-block;
	    margin-left :3% !important;
	    margin-right:2% !important;
	    width	:72%;	    
	}
	
	.TITLE .value,
	.EI3IDEA .value,
	.EI3KNW .value,
	.EI3TECHYN .value,
	.EI3IDEAYN .value,
	.EI3KNWYN .value,
	.E101 .value,
	.E102 .value,
	.E103 .value,
	.E104 .value,
	.E105 .value,
	.E106 .value{
	    color: #0056b7;
	    display	:inline-block;
	    width	:20%;	    
	}
	
	
	/*For Text Area Only*/
	/*=== ==== ==== ====*/
	.EI1BA,
	.EI5CL,
	.EI5SIGN,
	.EI5DATE,
	.EI5PLA,
	.EI1AN,
	.EI1GE,
	.EI1AG,
	.EI1MB,
	.EI1EA,
	.EI1FPA,
	.EI1PO,
	.EI1AP,
	.EI2ED,
	.EI2EX,
	.EI2CV,
	.EI3TECH{
	    font-style: normal;
	    border-bottom:1px solid #f1f1f1;
	    float:left;
	    width:100%;
	}
	
	.EI1BA .label,
	.EI5CL .label,
	.EI5SIGN .label,
	.EI5DATE .label,
	.EI5PLA .label,
	.EI1AN .label,
	.EI1GE .label,
	.EI1AG .label,
	.EI1MB .label,
	.EI1EA .label,
	.EI1FPA .label,
	.EI1PO .label,
	.EI1AP .label,
	.EI2ED .label,
	.EI2EX .label,
	.EI2CV .label,
	.EI3TECH .label{
	    white-space: normal !important;
	    color: #000000;
	    line-height: 22px;
	    display	:inline-block;
	    margin-left :3% !important;
	    margin-right:2% !important;
	    width	:50%;	    
	}
	
	.EI1BA .value,
	.EI5CL .value,
	.EI5SIGN .value,
	.EI5DATE .value,
	.EI5PLA .value,
	.EI1AN .value,
	.EI1GE .value,
	.EI1AG .value,
	.EI1MB .value,
	.EI1EA .value,
	.EI1FPA .value,
	.EI1PO .value,
	.EI1AP .value,
	.EI2ED .value,
	.EI2EX .value,
	.EI2CV .value,
	.EI3TECH .value{
	    color: #0056b7;
	    display	:inline-block;
	    width	:40%;
	    margin-left :2% !important;
	}
	
	
	/*For  Custom Correction Only*/
	/*=== ==== ==== ======== ====*/
	.TITLE .label,
	.EI2EX .label,
	.EI2ED .label,
	.EI2CV .label,
	.EI3TECH .label,
	.EI3IDEA .label,
	.EI3KNW .label,
	.EI3TECHYN .label,
	.EI3IDEAYN .label,
	.EI3KNWYN .label,
	.E101 .label,
	.E102 .label,
	.E103 .label,
	.E104 .label,
	.E105 .label,
	.E106 .label{
	    white-space: normal !important;
	    color: #000000;
	    line-height: 22px;
	    display	:inline-block;
	    margin-left :3% !important;
	    margin-right:2% !important;
	    width	:40%;	    
	}
	
	.TITLE .value,
	.EI2EX .value,
	.EI2ED .value,
	.EI2CV .value,
	.EI3TECH .value,
	.EI3IDEA .value,
	.EI3KNW .value,
	.EI3TECHYN .value,
	.EI3IDEAYN .value,
	.EI3KNWYN .value,
	.E101 .value,
	.E102 .value,
	.E103 .value,
	.E104 .value,
	.E105 .value,
	.E106 .value{
	    
	    display	:inline-block !important;
	    width	:50%;
	    margin-left :2% !important;
	}
	
	/*For  X3 Tab Only*/
	/*=== ==== ==== ====*/
	#X0 span.label,
	#X1 span.label,
	.EI5H span.label{
	    
	    width	:20%; 	    
	}
	
	#X0 span.value,
	#X1 span.value,
	.EI5H span.value{
	   
	    text-align: left !important;
    	    width	:70%;
	    
	}
	
	
	
	footer{
	    display:none !important;
	}
	
	footer .container{
	    display:none !important;
	}
	
    
</style>
