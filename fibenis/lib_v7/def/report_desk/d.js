
       // Ajax Instance
        
       var request_report_creation = new ajax();
       

       //Create Option
       
       function create_option(td_value){
              
              var lv = new Object({});
         
              // param
         
              var column_value      = td_value.split('[R]');
              
              lv.report_id         =  column_value[0];
              
              lv.report_name       =  column_value[1];
              
              lv.option_text       =  column_value[2];
    
              lv.navigator_code    =  column_value[3];
              
              lv.option_data       = [];
              
              lv.option_counter    = 0;
              
              
              // lv option data
              
              lv.option_temp_data  = lv.option_text.split('[S]');
              
              // option data
              
              for(var idx in lv.option_temp_data){
                     
                     lv.option_data[lv.option_counter] = lv.option_temp_data[idx].split(',');
                     
                     lv.option_counter++;
                     
              } // end of option data
              
              
              lv.select_III         = '';
              
              // action
              
              lv.action = {
                     
                     
                             'CG':function(param){
                                   
                                          var temp = ({'content':''});
                                          
                                          // build
                                          
                                          temp.content = lv.build.select_box({'id':'map_nav_'+param.report_id,
                                                                             'html':'',
                                                                             'options_title':'0:Select Customer Group',
                                                                             'options_text':lv.option_data[0]
                                                                });
                                          
                                          return {'write':1,
                                                  'content':temp.content};
                     
                            }, // customer group
                            
                            
                            'CY':function(param){
                                   
                                          var temp = ({'content':''});
                                          
                                          // build
                                          
                                          temp.content = lv.build.select_box({'id':'map_nav_'+param.report_id,
                                                                             'html':'',
                                                                             'options_title':'0:Select Year',
                                                                             'options_text':lv.option_data[0]
                                                                });
                                          
                                           return {'write':1,
                                                  'content':temp.content};       
                                   
                            },
                            
                            'DSRFYM':function(param){
                                                 
                                                  var temp = ({'content':''});
                                          
                                                 // build
                                                 
                                                 temp.content = lv.build.select_box({'id':'map_nav_'+param.report_id,
                                                                                    'html':'onchange=JavaScript:year_month_build(this,'+'map_nav_II_'+param.report_id+')',
                                                                                    'options_title':'0:Select Fin year',
                                                                                    'options_text':lv.option_data[1]
                                                                       });
                                                 
                                                  // secondbox
                                          
                                                 temp.content+=lv.build.select_box({'id':'map_nav_II_'+param.report_id,
                                                                             'html':'',
                                                                             'options_title':'0:Select Month',
                                                                             'options_text':''
                                                                });
                                           
                                                 temp.content+='<br>';
                                           
                                                 temp.content+=lv.build.select_box({'id':'map_nav_III_'+param.report_id,
                                                                                    'html':'',
                                                                                    'options_title':'0:Select DSR',
                                                                                    'options_text':lv.option_data[0]
                                                                       });
                                                 
                                                  return {'write':1,
                                                  'content':temp.content};
                                   
                                          },
                            
                            'FT':function(param){ var temp = ({'content':''});
                            
                            
                                                 // build
                                          
                                                 temp.content = lv.build.text_box({'id':'map_nav_'+param.report_id,
                                                                                    'html':' class="w_75"'
                                                                                   });
                                                 
                                                 
                                                 temp.content+=' to ';
                                                 
                                                 temp.content+= lv.build.text_box({'id':'map_nav_II_'+param.report_id,
                                                                                    'html':' class="w_75"'
                                                                                   });
                                                 document.write(temp.content);
                                                 
                                                 $("#map_nav_"+param.report_id).datepicker({
                                                        firstDay: 1,
                                                        dateFormat:'dd-mm-yy',
                                                        numberOfMonths:1,
                                                        //altField: "#s_date_hidden",
                                                        //altFormat: "yy-mm-dd",
                                                 }).datepicker("setDate", "0");
                                                 
                                                  $("#map_nav_II_"+param.report_id).datepicker({
                                                        firstDay: 1,
                                                        dateFormat:'dd-mm-yy',
                                                        numberOfMonths:1,
                                                        //altField: "#s_date_hidden",
                                                        //altFormat: "yy-mm-dd",
                                                 }).datepicker("setDate", "0");
                                                 
                                                 
                                                 
                                           return {'write':0};
                                                 
                                          },
                            
                            'FYM':function(param){
                                   
                                          
                                          
                                          // build
                                          
                                          temp.content = lv.build.select_box({'id':'map_nav_'+param.report_id,
                                                                             'html':'onchange=JavaScript:year_month_build(this,'+'map_nav_II_'+param.report_id+')',
                                                                             'options_title':'0:Select Fin year',
                                                                             'options_text':lv.option_data[0]
                                                                });
                                          
                                          // secondbox
                                          
                                           temp.content+=lv.build.select_box({'id':'map_nav_II_'+param.report_id,
                                                                             'html':'',
                                                                             'options_title':'0:Select Month',
                                                                             'options_text':''
                                                                });
                                          
                                           return {'write':1,
                                                  'content':temp.content};
                                   },
                                   
                            // offer + fin year
                                   
                            'OFT':function(param){ var temp = ({'content':''});
                                                        
                                                 // build
                                          
                                                 temp.content='From:'+lv.build.text_box({'id':'map_nav_'+param.report_id,
                                                                                    'html':' class="w_75"'
                                                                                   });
                                                 
                                                 
                                                 temp.content+=' to ';
                                                 
                                                 temp.content+= lv.build.text_box({'id':'map_nav_II_'+param.report_id,
                                                                                    'html':' class="w_75"'
                                                                                   });
                                                 
                                                 
                                                 // offer
                                                 temp.content+='<br><br>Offer:<br>'+lv.build.select_box({'id':'map_nav_III_'+param.report_id,
                                                                                    'html':'',
                                                                                    'options_title':'0:Select Offer',
                                                                                    'options_text':lv.option_data[0]
                                                                       });
                                                 
                                                 // product application                                                 
                                                 temp.content+='<br><br>Product Application:<br>'+lv.build.list_box({'id':'map_nav_IV_'+param.report_id,
                                                                                    'html':' size=9 ',
                                                                                    'options_title':'',
                                                                                    'options_text':lv.option_data[1]
                                                                       });
                                                 
                                                 document.write(temp.content);
                                                 
                                                 $("#map_nav_"+param.report_id).datepicker({
                                                        firstDay: 1,
                                                        dateFormat:'dd-mm-yy',
                                                        numberOfMonths:1,
                                                        //altField: "#s_date_hidden",
                                                        //altFormat: "yy-mm-dd",
                                                 }).datepicker("setDate", "0");
                                                 
                                                  $("#map_nav_II_"+param.report_id).datepicker({
                                                        firstDay: 1,
                                                        dateFormat:'dd-mm-yy',
                                                        numberOfMonths:1,
                                                        //altField: "#s_date_hidden",
                                                        //altFormat: "yy-mm-dd",
                                                 }).datepicker("setDate", "0");
                                                 
                                                 
                                                 
                                          return {'write':0};
                                                 
                            }, // end
                            
                            
                            'OD':function(param){
                                   
                                          var temp = ({'content':''});
                                          
                                          // build
                                          
                                          temp.content = lv.build.select_box({'id':'map_nav_'+param.report_id,
                                                                             'html':'',
                                                                             'options_title':'1:Select Over Due Offset',
                                                                             'options_text':'10:+ 10 days,30:+ 30 days'
                                                                });
                                          
                                          return {'write':1,
                                                  'content':temp.content};
                     
                            }, // customer group
                     
              };
              
              // element
              
              lv.build= {
                     
                            // lits box
                            
                            'list_box':function(param){
                     
                     
                    
                                          // build selectbox
                                          
                                          var select_map_nav    = document.createElement("SELECT");
                                          
                                          select_map_nav.id     = 'temp_'+param.id;                                          
                                          
                                          select_map_nav.name   = 'temp_'+param.id;
                                          
                                          param.options_text    =  (param.options_text.length>0)?param.options_title+param.options_text:'';
                                          
                                          Option_Builder_Packet({select_box:select_map_nav,                              
   							          options_csv:param.options_text});
                                          
                                         
                                           
                                         return  '<input type="hidden" id="'+param.id+'" name="'+param.id+'">'+
                                                 '<select id="temp_'+param.id+'" name="temp_'+param.id+'" '+param.html+' multiple="true"'+
                                                 ' onchange=JavaScript:selectbox_capture_selected_value(this,ELEMENT("'+param.id+'"),\',\')>'+
                                                 ''+select_map_nav.innerHTML+''+
                                                 '</select>';
                                                 
                     
                                   }, // customer group
                     
                     
                            'select_box':function(param){
                     
                                          // build selectbox
                                          
                                          var select_map_nav    = document.createElement("SELECT");
                                          
                                          select_map_nav.id     = param.id;                                          
                                          
                                          select_map_nav.name   = param.id;
                                          
                                          param.options_text    =  (param.options_text.length>0)?','+param.options_text:'';
                                          
                                          Option_Builder_Packet({select_box:select_map_nav,                              
   							          options_csv:param.options_title+param.options_text});
                     
                                         return '<select id="'+param.id+'" name="'+param.id+'" '+param.html+' >'+
                                                 ''+select_map_nav.innerHTML+''+
                                                 '</select>';
                     
                                   }, // customer group
                                   
                                   
                            'text_box':function(param){
                     
                                          // build selectbox
                                          
                                          var select_map_nav    = document.createElement("INPUT");
                                          
                                          select_map_nav.id     = param.id;                                          
                                          
                                          select_map_nav.name   = param.id;
                                          
                                          select_map_nav.type   = 'text';
                           
                                         return '<input type="text" id="'+param.id+'" name="'+param.id+'" '+param.html+'>'
                     
                            } // customer group
                     
              };
              
              
              
              lv.response = lv.action[lv.navigator_code](lv);
              
              // write content
              
              if (Number(lv.response.write)==1){
                     document.write(lv.response.content);
              }
              
              
       }//end
       
       
       
       function map_nav_action(id){
        
             alert(document);
       }
       
	   
	   
	   
       
       // year month build
       
       function year_month_build(parent,child){
              
                     var lv = new Object({});
                     
                     lv.month_option = new Array();
                     
                     lv.month_option.push({id:0,name:'Select Month '});
                     
                     lv.month        = [['01','Jan'],
                                        ['02','Feb'],
                                        ['03','Mar'],
                                        
                                        ['04','Apr'],
                                        ['05','May'],
                                        ['06','Jun'],
                                        
                                        ['07','Jul'],
                                        ['08','Aug'],
                                        ['09','Sep'],
                                        
                                        ['10','Oct'],
                                        ['11','Nov'],
                                        ['12','Dec']
                                        
                                        ];
                     
                     lv.month_start = lv.month.slice(3,12);
                     
                     lv.month_end   = lv.month.slice(0,3);
                     
                     // clean the old option
                     
                     remove_e_first(child);
                     
                     if(parent.value.length==9){
                     
                            lv.year = parent.value.split('-');
                            
                            // start year
                            
                            for(key in lv.month_start){
                                   lv.month_option.push({id:lv.year[0]+lv.month_start[key][0],
                                                        name:lv.month_start[key][1]+'-'+lv.year[0]});
                            }
                            
                            // end year
                            
                            for(key in lv.month_end){
                                    lv.month_option.push({id:lv.year[1]+lv.month_end[key][0],
                                                        name:lv.month_end[key][1]+'-'+lv.year[1]});
                            }                                          
                            
                     } // end
                     
                      // alert(child.id);
                     
                     Option_Builder({'select_box':child,
                                     'options'   :lv.month_option
                            });
                     
       } // end
       
       
	   // Group Option
       
       function group_option(td_value){
              
                            var column_value      = td_value.split('[R]');  // id[R]group_type                          
                            var lv                = new Object();                          
                          
                            if( (Number(column_value[1])==1) &&
                                (String(column_value[2]).length==0) ){
	   
                                   var select_map_nav       = document.createElement("SELECT");
              
                                   select_map_nav.id        = 'group_type_'+column_value[0];
                                   
                                   select_map_nav.name      = 'group_type_'+column_value[0];
                                   
                                   select_map_nav.className = 'w_150';
                                   
                                   select_map_nav.innerHTML = '<option value="0">None</option>'+
                                                                  '<option value="S">Single Sheet</option>'+
                                                                  '<option value="M">Multiple Sheet</option>';                                   
                                               
                                   document.write('<select id="'+select_map_nav.id+'" name="'+select_map_nav.name+'" class="w_100">'+
                                                    select_map_nav.innerHTML+
                                                   '</select>');
                                   
                                   // end
                                   
                            }else if(String(column_value[2]).length>0){ 
                                   
                                   //alert(column_value[2]);
                                   
                                   var select_map_nav       = document.createElement("SELECT");
                                   
                                   var group_option         = JSON.parse(column_value[2]);
                                   
                                   lv.opt_keys              = Object.keys(group_option);
								   
                                   // ouline element
                                   
                                   lv.outline_ele_id_name   = 'outline_'+column_value[0];
                                   
                                   lv.outline               = '';
                                   
                                   
                                   lv.outline_limit_ele_id_name   = 'outline_limit_'+column_value[0];
                                   
                                   //lv.outline_limit               = '';
                                   
                                   
                                   lv.custom_group_options_single    = '';
                                   
                                   lv.custom_group_options_multiple  = '';
                                   
                                   select_map_nav.id        = 'group_type_'+column_value[0];
                                   
                                   select_map_nav.name      = 'group_type_'+column_value[0];
                                   
                                   select_map_nav.className = 'w_150';
                                   
                                   // dynamic group opton
                                   
                                   for(var opt_idx in lv.opt_keys){                                          
                                          lv.custom_group_options_single+= '<option value="S'+lv.opt_keys[opt_idx]+'">Single Sheet '+group_option[lv.opt_keys[opt_idx]]+'</option>';
                                          lv.custom_group_options_multiple+='<option value="M'+lv.opt_keys[opt_idx]+'">Multiple Sheet '+group_option[lv.opt_keys[opt_idx]]+'</option>';
                                   }  // eachoption
								   
								   
								   // if has groupby
								   
								   if(lv.custom_group_options_single.length>0){
								   
										lv.outline='<br><input id="'+lv.outline_ele_id_name+'"  name="'+lv.outline_ele_id_name+'"   type="checkbox" value="1" >&nbsp;Outline';
                                                                                lv.outline+='&nbsp;&nbsp;<span class="clr_gray_9">|&nbsp;&nbsp;Items:</span><select id="'+lv.outline_limit_ele_id_name+'"  name="'+lv.outline_limit_ele_id_name+'" >'+
                                                                                           '<option value=0>All</option><option value=5>5 Only</option><option value=10>10 Only</option><option value=-1>No Items</option>'+
                                                                                           '</select>';
								   } // end
                                   
                                   
                                   select_map_nav.innerHTML = '<option value="0">None</option>'+
                                                                  '<option value="S">Single Sheet</option>'+
                                                                  lv.custom_group_options_single+
                                                                  '<option disbaled=true>----------------------</option>'+
                                                                  '<option value="M">Multiple Sheet</option>'+
                                                                  lv.custom_group_options_multiple;
                                                                     
                                               
                                   document.write('<select id="'+select_map_nav.id+'" name="'+select_map_nav.name+'" class="w_200" onchange="JavaSript:report_group_action(this,\''+lv.outline_ele_id_name+'\')" >'+
                                                    select_map_nav.innerHTML+
                                                   '</select>'+lv.outline);
                                   
                            } // end
	   
	   } // end
	   
	   // report group action
	   
	   function report_group_action(element,outline_ele_id){
	   
				var lv = new Object({});
				
				lv.is_single_group=element.value.indexOf('S');
				
				if(Number(lv.is_single_group)!=0){
				
					ELEMENT(outline_ele_id).checked=false;
					ELEMENT(outline_ele_id).disabled=true;
				
				}else{
						ELEMENT(outline_ele_id).disabled=false;

			    } // end
				
				//alert(element.value+'--'+lv.is_single_group+'--'+outline_ele_id);
	   
	   
	   } // end
	   
	   
       
       //Create Excel Link
       
       //Input: td_value=> id 
       function create_excel_link(td_value){
              
                   
           var column_value = td_value;
           
           var split_content = column_value.split('[R]');
           
           var id = split_content[0];
           
           var pass_id = "'"+split_content[1]+"'";
           
              if (id>0) {
                
                document.write('<a class="icon excel  hint--left"'+
                                            'data-hint="Create Report"'+
                                    'href ="JavaScript:get_counter_report('+id+','+pass_id+')";>&nbsp;Create</a>');
              }
              
              else{
                document.write('&nbsp;');
              }
       }//end 
       
       
       function get_counter_report(id,pass_id){
        
           
            
            var counter = document.getElementById('row_idx_'+id).value;
            
            create_excel_report(counter,pass_id);
       }
       
       
       //Excel Creation
       
       // query -> &e_series=p_appl222ication&filter_1=CGHR
       
       function create_excel_report(counter,pass_id){
              
              var lv = new Object({});       
         
              lv.report_id = document.getElementById('HX_'+counter+'_1').value;
              
              lv.data_map = document.getElementById('HX_'+counter+'_2').value;
              
              lv.series_type = document.getElementById('HX_'+counter+'_3').value;
              
              lv.pass_id = pass_id;
              
              lv.req_str = '&report_id='+lv.report_id ;
                    
              // filter elements
           
              lv.filter_I   = document.getElementById('map_nav_'+lv.report_id).value;
              
              lv.filter_II  = (ELEMENT('map_nav_II_'+lv.report_id)!=undefined)?ELEMENT('map_nav_II_'+lv.report_id).value:undefined;
              
              lv.filter_III = (ELEMENT('map_nav_III_'+lv.report_id)!=undefined)?ELEMENT('map_nav_III_'+lv.report_id).value:undefined;
              
              lv.filter_IV  = (ELEMENT('map_nav_IV_'+lv.report_id)!=undefined)?ELEMENT('map_nav_IV_'+lv.report_id).value:undefined;
			  
	     // outline
              
	      lv.outline = (ELEMENT('outline_'+lv.report_id)!=undefined)?((ELEMENT('outline_'+lv.report_id).checked==true)?1:0):undefined;
              
              lv.outline_limit  = (ELEMENT('outline_limit_'+lv.report_id)!=undefined)?(ELEMENT('outline_limit_'+lv.report_id).value):undefined;
              
              // filter creation              
              
                     if(lv.filter_I.length>0){
                            lv.req_str+="&filter_I="+lv.filter_I+"";                  
                     }
                     
                                                         
                     if(lv.filter_II){
                            lv.req_str+="&filter_II="+lv.filter_II+"";                  
                     }
                     
                     if(lv.filter_III){
                           lv.req_str+="&filter_III="+lv.filter_III+"";                  
                     }
                     
                     if(lv.filter_IV){
                           lv.req_str+="&filter_IV="+lv.filter_IV+"";                  
                     }
              
              // outline	  
                                      
                     if(Number(lv.outline)==Number(1)){
                                   lv.req_str+="&outline="+lv.outline+"";                  
                     } // end
                     
                     // outline limit
                     
                     if(lv.outline_limit){
                                   lv.req_str+="&outline_limit="+lv.outline_limit+"";                  
                     } // end
              
              // Group Type
              
                     lv.req_str+="&group_type="+ELEMENT('group_type_'+lv.report_id).value+""; 
		
              //inner_frame.show();
                                
                     G.showLoader();    
                     
                     request_report_creation.set_request("my_perl.pl","&"+lv.series_type+"="+lv.data_map+lv.req_str+"&PASS_ID="+pass_id);              
                     
                     request_report_creation.send_get(excel_report_server_response);
              
       }//end
       
       
       // i.p -> <status>:<filename_with_full_path>:<file_name>
       
       function excel_report_server_response(response){
              
              //alert(response);
        
                  var lv = new Object({});
                  
                  lv.response = response.split(':');
                  
                  lv.response_status = lv.response[0];
                  
                  lv.response_path   = lv.response[1];
                  
                  if (lv.response_status==1) {
                    
                         G.hideLoader();      
                    
                         G.bs_alert_success('Report: Created Successfully');
                         
                         window.open(url=lv.response_path,'Part','toolbar=0,scrollbars=1,menubar=1,status=0,innerHeight=800,innerWidth=800');                         
                  }
                  
                  else{
                    G.hideLoader();     
                    G.bs_alert_error('Report: Not Created');
                  }
        
       }//end server response
       
       
       
       
       