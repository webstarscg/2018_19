<?PHP
       
       $pass_id = $_SESSION['PASS_ID'];
       
       #D Series definition:
       
            $D_SERIES   =   array(
			           #Title
			          
				   'title'=>'Report',
                                    
				    #Page Code
				   
				    'page_code'   =>'RPD',
				   
                                    #query display depend on the user
                                   
                                    'is_user_base_query'=>0,
				    
				    # generation
				    
				    'gx'=>1,
                                    
                                    #table data
                                    
                                    'data'=> array(
                                                   
                                                   1=>array('th'	=> 'Report Name',
                                                            'field'	=> 'sn',
							    'td_attr'   =>  ' class="txt_size_11 no_wrap b" ' ),
                                                   
                                                   2=>array('th'	=> 'Category',
                                                            'field'	=> "concat(id,'[R]',sn,'[R]','<option></option>','[R]',map_navigator)",
                                                            'js_call'	=> 'create_option'),
															
						   4=>array('th'	=> 'Group',
                                                            'field'	=> "concat(id,'[R]',group_type::int,'[R]',json_box->'group_by'::text)",
                                                            'js_call'	=> 'group_option'),   			
                                                   
                                                   3=>array('th'	=> 'Action',
                                                            'field'	=> 'concat(id,\'[R]\',\''.$pass_id.'\')',
                                                            'js_call'	=> 'create_excel_link'),                                                            
                                                 ),
				    
				    #Table Info
                                    
                                    'table_name' =>'report_setup',
                                    
                                    'key_id'    =>'id',
				    
				    #Hidden Field:
				    
				    'hidden_data'=>array('id','data_map','series_type'),
                                    
                                    #Filter Off:
                                    
                                    'search_filter_off'=>1,
				    
				    #create search field
									
                                        'search_text' => array(
								
                                                                
                                                                
				
                                                            ),
                                    
                                    #Search filter 
				
                                    //'search_field' =>'',
                                    
                                    //'search_id'    =>array('id'),
				    
				    #Date
				    
				    'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  'timestamp_punch'),
				    
				    #check_field
								
				    'check_field'   =>  array('id' => @$_GET['id']),
                                    
                                    
                                    #custom_filter:
                                    
                                    
                                    
                                    
				    #Sorting:
				    
				    
				    #Default option:
				    
				    'order_by'   =>'ORDER BY sn ASC',
				    
				    #Js File Include:

				    'js'        =>array('is_top'=>1,'top_js'=>"$LIB_PATH/def/report_desk/d"),
				    
				    #Narrowdown:
				    
				    'is_narrow_down'=>1,
				
				    #Action:
				
                                    'action' => array('is_action'=>0, 'is_edit' => 0, 'is_view' =>0,'is_add_more_info'=>0),
				    
				    #Custom Action:
				    
                                    /*'custom_action'=>array(
                                                           array('fun_name'=>'create_excel_report',
                                                                 'action_name'=>'Create Excel',
                                                                 'html'=>'class="icon excel"'
                                                                 )
                                                            ),
				    
				    */
				    #Add:
				    
				    'add_button' =>array( 'is_add' =>0,'page_link'=>'', 'b_name' => ''),
				    
				    #Delete:
				    
				    'del_permission' => array('able_del'=>0,'user_flage'=>1), 
                                    
                                    # Communication
                                
                                    'prime_index'   => 1,
                                
                                    			
				    #export data
				    
                                    'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
                                
                                    
                                 );
	    
	
?>