<?PHP

    # Default Addon
    
    if(@$_GET['key']){        
        $temp_entity_id=$_GET['key'];
        
    }

    // F_series definition:
                            
    $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Parent Child',
				
				#Table field
                    
				'data'	=>   array(
                                                  
                                                   
                                                   '1' =>array( 'field_name'=> 'Entity',
                                                               
                                                               'field_id' => 'parent_code',
                                                               
                                                               'type' => 'option',
                                                               
                                                               'option_data'=>$G->option_builder('entity','code,sn'," ORDER by sn ASC"),
                                                               
                                                               'avoid_default_option'=>1,
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'is_ro'=>1,
                                                               
                                                               'input_html'=>'class="w_100"'
                                                            ),
                                              
												
						   '2' =>array('field_name'=>'Child',
                                                               
                                                               'field_id'=>'parent_code',
                                                               
                                                                'type'         => 'list_left_right',
                                                                
                                                                'option_data'  =>'',
                                                                
                                                                'is_mandatory' =>  0,
                                                                
                                                                'input_html'   =>  ' class="w_200" rows="5"  style="height:200px !important"  ',
                                                                
                                                                'is_ro'         =>1
						   
                                                    ),
//                                               
                                                   
                                    
                                ),
                                    
				#Table Name
				
				'table_name'    => 'entity',
				
				#Primary Key
                                
			        'key_id'        =>  'id',
                                
                                                              
				# Default Additional Column
                                
				'is_user_id'    =>  'user_id',
								
				# Communication
								
				'back_to'  => array( 'is_back_button' =>0, 'back_link'=>'', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 1,
                                
				# File Include
                                'after_add_update'	=>1,
				
			
				
                                
			);
    
    
    if(@$temp_entity_id){    before_update($temp_entity_id);  }
    
   # else{   $F_SERIES['data']['5']['option_data'] = $G->option_builder('entity_child_base',"id,sn",""); }
    
    // before_update
    
    function before_update($key_id){
	
	global $G,$F_SERIES,$rdsql;
        
	if($key_id){
	    
	    # option prefill data
	    
            $F_SERIES['temp']['option_data']                 = "WHERE parent_code NOT IN (SELECT code FROM entity WHERE id=$key_id)";
	    
	    $F_SERIES['data']['2']['option_data']		= $G->option_builder('entity','code,sn',$F_SERIES['temp']['option_data'].' ORDER BY sn ASC');
	    
            
            # existing
            
	    $F_SERIES['temp']['where_option_existing_data']  = "WHERE parent_code=(SELECT code FROM entity WHERE id=$key_id)";
              
            $F_SERIES['data']['2']['option_existing_data']	= $G->option_builder('entity ','code,sn',$F_SERIES['temp']['where_option_existing_data'].' ORDER by sn ASC');
	    
	    
	} # edit
	
    } # end of add update action
    
    
    // after_add_update
    
    function after_add_update($key_id){
        
        global $G,$F_SERIES,$rdsql,$USER_ID;
        
        $lv;
        $lv['content']=[];
               
        
        $key_code = $G->get_one_column(['table'  => 'entity',
                                    'field'  => 'code',
                                    'manipulation'=>" WHERE id=$key_id"                                                                       
                    ]);
        
        
                 
        
        # insert data
        
        $lv['temp_options'] = $_POST['X2'];
       
        if($lv['temp_options']){
        
            foreach(preg_split("/,/",$lv['temp_options']) as $key){
                
                $rdsql->exec_query("UPDATE
                                            entity
                                    SET
                                            parent_code='$key_code'
                                    WHERE
                                            code='$key'
                                   
                                   ","");
            }
            
        } // end
        
      
        
        before_update($key_id);       
        
    } # end
    
    
    
?>