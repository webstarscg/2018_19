<?PHP
        $LAYOUT	    = 'layout_full';
        
	$PV['gallery_path'] = 'images/gallery/';
	 
        $D_SERIES       =   array(
                                   'title'=>'',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
                                    
                                    #table data
                                    
                                    'data'=> array(
					                
							3=>array('th'=>'Title',
									 
								'field'=>'sn as v3',
                                                                   
									 
								'td_attr' => ' class="label_father align_LM" ',
									
									
								 
								),
							
							/*1=>array('th'=>'Image',
									 
								'field'=>'CONCAT("<img width=\"10%\" src=images/gallery/",image,"1.jpg >")  as v1',
                                                                   
									 
								'td_attr' => ' class="label_father align_LM" ',
									
									
								 
								),*/
							
							2=>array('th'=>'Status',
									 
								'field'=>'CONCAT(id,"[C]",is_active) as v2',
								
								'filter_out'=>function($data_in){
										
											$temp     = explode('[C]',$data_in);
											
											$flag     = [1,0];
											
											$data_out = array(
													  'data'=>array('id'   => $temp[0],
															'key'  => md5($temp[0]),
															'label'=> 'Gallery',
															'cv'   => $temp[1],
															'fv'   => $flag[$temp[1]],
															'series'=>'a',
															'action'=>'entity_child',
															'token' =>'ECAI'
															)
													);
											
											return json_encode($data_out);
										},
                                                                   
								/*'html'      =>  'style	= "cursor:pointer" onclick="JavaScript:E_V_PASS(\'sort_field\',2);E_V_PASS(\'sort_direction\',GET_E_VALUE(\'sort_col_2\'));filter_data();"',
								  
								'font'      =>  'class="sort"',*/
																
								'span'      =>  '<span id="sort_icon_2" name="sort_icon_2"></span>',
									 
								'td_attr' => ' class="label_child align_LM  txt_case_upper b" style="padding-right:10px;" width="3%"',
								
								'js_call'=> 'd_series.inline_on_off',
									 
								),
					
								
                                                    ),
				    
                                    
                                    #Sort Info
                                      
                                       'sort_field' =>array('code',
							    
							    'sn'),
                                        
                                       'action' => array('is_action'=>0, 'is_edit' =>1, 'is_view' =>0 ),
                                       
                                       'order_by'   =>'ORDER BY id ASC ' ,
				       		
                                
                                    #Table Info
                                    
                                    'table_name' =>'entity_child',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
                                
                                    # Communication
                                
                                    'prime_index'   => 1,
                                
                                    # File Include
                                
                                  'js'            => array('is_top'=>1, 'top_js' => $LIB_PATH.'def/plug_gallery/d'),
                                    
                                    #create search field
									
                                        'search_text' => array(
								
																						
								1=>array('get_search_text'  => get_search_array('entity_child','id as ST1,sn as ST2','Title',1,0,' WHERE entity_code="ET" AND parent_id=0')),
								
								//2=>array('get_search_text'  => get_search_array('entity','id as ST1,code as ST2','Code',2,1)), 
								
                                                              ),
						
				
				#Search filter 
				
				'search_field' => 'sn',
				
				'search_id' 	=> array('id'),
				'is_narrow_down'=>1,
				'key_filter'   => ' AND entity_code = "GL" AND parent_id=0', 
				#summary:
				
				/*'summary_data'=>array(
							array(  'name'=>'No Data','field'=>'count(id)','html'=>'class=summary'),
				
				                   ),*/
				
				'custom_action'=>array(
						
							      array(

                                                                    'is_fun'      => 1,	

                                                                    'fun_name' 	  => 'view_gal_image',

                                                                    'action_name' => 'View Image',

                                                                    'html'        => 'class="icon page hint--bottom" data-hint="Edit"'

                                                                   ),
							      
							          array(

                                                                    'is_fun'      => 1,	

                                                                    'fun_name' 	  => 'add_gal_image',

                                                                    'action_name' => 'Add Image |',

                                                                    'html'        => 'class="icon camera hint--bottom" data-hint="Edit"'

                                                                   ),
						      ),
				
				
				'custom_filter' => array(
							
						/*array(  'field_name' => 'Category',
						      
						      'field_id' => 'cat', 'filter_type' =>'option_list', 
											
							'option_value'=> $G->option_builder('entity','code,sn',' WHERE code="PG" OR code="BL"  ORDER by sn ASC'),
						
							//'default_option'=>$G->get_entity_value('MP','fy_id'),
							
							'input_html'=>' class="w_100" ',
							
							'cus_default_label'=>'Show All',
						
							'filter_by'  => 'entity_code'
						),*/
				),
				
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>1,'page_link'=>'f=plug_gallery', 'b_name' => 'Add Gallery' ),
								
					'del_permission' => array('able_del'=>1,'user_flage'=>1), 
								
					'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  ''),	
								
				#export data
				'after_delete'    =>1,
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'DMET'
                            
                            );
	
	
	function after_delete(){
		
		global $check;
		
		global $rdsql ;
		global $PV;
		// dele parent image
		
		 $image_I = $PV['gallery_path'].$check.'1.jpg';
		 
		 $image_II = $PV['gallery_path'].$check.'2.jpg';
		  
		 if(is_file($image_I)){
			unlink($image_I);
			
			unlink($image_II);
		 }
		$select_child = "SELECT id  FROM entity_child WHERE parent_id= $check";
		
		$select_exe_query  = $rdsql->exec_query($select_child,"CSV Error!");
		
		$child_id = '';
		
		while($get_row = $rdsql->data_fetch_object($select_exe_query)){
			
		     $child_id =$get_row->id;

		     $image_I = 'images/gallery/'.$child_id.'1.jpg';
		     $image_II = 'images/gallery/'.$child_id.'2.jpg';
 
		     if(is_file($image_I)){
			
			unlink($image_I);
			
			unlink($image_II);
		    }
		}
				
		$delete_child = "DELETE FROM entity_child WHERE parent_id= $check";
		
		$rdsql->exec_query($delete_child,"Dele dchild");
		
	}
    
?>