<?PHP

        $LAYOUT		= 'layout_full';
	       
        $D_SERIES       =   array(
                                   'title'=>'',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
				    
				    'gx'	=> 1,
                                    
                                    #table data
                                    
                                    'data'=> array(
					                
							3=>array('th'=>'Entity',
									 
								'field'=>'(SELECT sn FROM entity WHERE entity.code=entity_code)',
                                                                   
								'td_attr' => ' class="label_father align_LM" width="15%"',
									 
								),
					
                                                        1=>array('th'=>'Ref. Code',
								 
								'field'=>'sn',
								     
								'is_sort' => 1,
								
								'td_attr' => ' width="12%" ',
                                                                            
								), 													
//                                                      
							  5=>array('th'=>'Label',
									 
								'field'=>'ln',
                                                            	
								'is_sort' => 1,
								
								'td_attr' => ' class="label_child align_LM" width="25%"',
									 
								),
							
							 6=>array('th'=>'Detail',
									 
								'field'=>'detail',                                                         
									 
								'td_attr' => ' class="label_child align_LM" width="25%"',
									 
								),
							
							 7=>array('th'=>'Order',
									 
								'field'=>'line_order',
                                                                
								'td_attr' => ' class="label_child align_LM" width="25%"',
								
								'js_call'=>'na'
									 
								),
								
								
							
								
                                                    ),
				    
                                       'action' => array('is_action'=>0, 'is_edit' =>1, 'is_view' =>0 ),
                                       
                                       'order_by'   =>'ORDER BY entity_code,code,line_order ASC ' ,
				       		
                                
                                    #Table Info
                                    
                                    'table_name' =>'entity_child',
                                    
                                    'key_id'    =>'id',
				    
				          
					#key filter
                                
				     'key_filter'        => " AND parent_id=0 AND entity_code='MP' ",
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
                                
                                    # Communication
                                
                                    'prime_index'   => 1,
				    
				    'search'=> array(
							  
							array(  'data'  =>array('table_name' 	=> 'entity_child',
										'field_id'	=> 'sn',
										'field_name' 	=> 'sn',										
									     ),
												     
								'title' 		=> 'Ref.code',										
								'search_key' 		=> 'sn',													       
								'is_search_by_text' 	=> 1,
							     ),
							
							array(  'data'  =>array('table_name' 	=> 'entity_child',
										'field_id'	=> 'ln',
										'field_name' 	=> 'ln',										
									     ),
												     
								'title' 		=> 'Label',										
								'search_key' 		=> 'ln',													       
								'is_search_by_text' 	=> 1,
							     ),
							
						),
				    
				    'custom_filter' => array(  			     						   
							      
									array(  'field_name' => 'Entity:',
									      
										'field_id' => 'cf1', // 
										
										'filter_type' =>'option_list', 
												    
										'option_value'=> $G->option_builder('entity','code,sn'," ORDER BY sn ASC"),
							    
										'html'=>'  title="Select Client"   data-width="160px"  ',
								    
										'cus_default_label'=>'Show All',
							    
										'filter_by'  => "entity_code"  // main table value
									),
							),
				    
				
                                    # File Include
                                
                                  'js'            => array('is_top'=>1, 'top_js' => 'js/d_series/entity_child_apex'),
                                    
				#summary:
				
				'summary_data'=>array(
							array(  'name'=>'No.','field'=>'count(id)','html'=>'class=summary'),
				
				                   ),
				
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>1,'page_link'=>'f=entity_child_apex', 'b_name' => 'Add Child' ),
								
					'del_permission' => array('able_del'=>1,'user_flage'=>1), 
								
					'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  ''),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'DECH'
                            
                            );
    
?>