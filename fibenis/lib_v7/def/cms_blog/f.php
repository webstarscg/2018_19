<?PHP
      
      //define('PAGE_PERM',$_SESSION['CMS_BLOG']);

      $LAYOUT='layout_full';	
	
      use GuzzleHttp\Client;
				
      use GuzzleHttp\Query;
      
      $blog_image =  $SG->get_cookie("blog_image");
   
      $status_option = '<option value="2">Active</option><option value="1">In-Active</option>';
   
      $new_option = '<option value=-1 class="clr_green">+ Add New</option>';
   
      $no_row = $G->table_no_rows( array('table_name'=>'entity_child','WHERE_FILTER'=>" AND entity_code='BL' AND parent_id=0"));
	
      $line_order= $no_row[0]+1;
   
      //F_series definition:
                            
      $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Blog',
				
				#Table field
                    
				'data'	=>   array(
				 
						   '0'  => ['field_name'=>'Basic',
								 'type'=>'heading'					 
								],
						   
						    
						   '1' =>array( 'field_name'=> 'Category',
                                                               
                                                               'field_id' => 'parent_id',
                                                               
                                                               'type' => 'option',
                                                               
                                                               'option_data'=>$G->option_builder('entity_child','id,sn'," WHERE entity_code='BL' AND parent_id=0 AND is_active=1  ORDER by sn ASC").$new_option,
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_100" onchange="add_new_master(this,2)"'
                                                                
                                                               ),
						     
						     '2' => array( 'field_name' => 'New Category',
							      
								'field_id'=>'sn',
                                                               
								'type'=>'text',
								
		                                                'is_hide'=>1,
	               
	                                                        'is_mandatory'=>0,
								
								'ro'	=>1,
								
								'allow'		=> 'x128',
								
								'input_html'=> "  onchange='check_category(this);'",
								
							),
				    
						    '3' =>array('field_name'=>'Date',
                                                               
                                                               'field_id'=>'date',
                                                               
                                                               'type'=>'date',
                                                               
                                                               'is_mandatory'=>1,
							       
							       'default_date'=>date("d-m-Y"),
                                                               
                                                               'input_html'=>'class="w_100"'                                                               
                                                              
							       ),
						    
						    '4' =>array('field_name'=>'Page Title',
                                                               
                                                               'field_id'=>'ln',
                                                               
                                                               'type'=>'text',
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_350" maxlength="120" onkeypress = "return PR_All_Alpha_Numeric(event,\' -_.\');"'
                                                               
                                                               ),
						   
						     '5' =>array('field_name'=>'Page Content',
                                                               
                                                               'field_id'=>'detail',
                                                               
                                                               'type'=>'textarea_editor',
							       
							       'editor'=>array('height'=>300),
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               ),
						     
						    '6'  => ['field_name'=>'Meta',
								 'type'=>'heading'					 
								],
				    
						    
						   '7' =>array('field_name'=>'Page Description',
                                                               
                                                               'field_id'=>'ea_value',
                                                               
                                                               'type'=>'textarea',
							       
							       'child_table'         => 'eav_addon_varchar', // child table 
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => 'PGPD',           // attribute code
                                                               
                                                               'is_mandatory'=>0,
                                                               
                                                               'input_html'=>' class="w_350" maxlength="120" onkeypress = "return PR_All_Alpha_Numeric(event,\' -_.\');"'
                                                               
                                                               ),
						   
						   '8' =>array('field_name'=>'Page Keyword',
                                                               
                                                               'field_id'=>'ea_value',
                                                               
                                                               'type'=>'textarea',
							       
							       'child_table'         => 'eav_addon_varchar', // child table 
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => 'PGKW',           // attribute code
                                                               
                                                               'is_mandatory'=>0,
                                                               
                                                               'input_html'=>' class="w_350" maxlength="120" onkeypress = "return PR_All_Alpha_Numeric(event,\' -_.\');"'
                                                               
                                                               ),
						   
						  			
						   '9' => array( 'field_name'=> 'Status', 
                                                               
                                                                'field_id' => 'is_active',
                                                               
                                                                'type' => 'option',
                                                                
                                                                'option_data'=> $status_option,//$G->option_builder('entity_attribute','code,sn',' WHERE entity_code="PP"  ORDER BY sn ASC'),
								
								'filter_in'=>function($data_in){
								  
									return ($data_in-1);
								  
								},
								
								'filter_out'=>function($data_in){
								  
									return ($data_in+1);
								},
								
								'avoid_default_option'=>1,
                                                               
                                                                'is_mandatory'=>1,
                                                                
                                                                'input_html'=>' class="W_150"'
                                                                
                                                                ),
						   
                                                   '10' => array( 'field_name'=> 'Line Order', 
                                                               
								  'field_id' => 'line_order',
				       
								  'type' => 'text',
					
								  'is_mandatory'=>1,
					
								  'input_html'=>'class="w_75"  onkeypress = "return PR_All_Numeric(event);" maxlength="4"   value='.$line_order,
							       ),
						   
						   '11' => array( 'field_name'=> 'Event Image ',
                                                               
								  'field_id' => 'image',
                                                               
								  'type' => 'file',
                                                               
								  'upload_type' => 'image',
							    
								  'allow_ext'   => array('jpg','jpeg','png'),
								  
								  ///'image_size'	=> $blog_image,

								  'location'    => 'images/gallery/',          // attribute code
                                                               
								  'is_mandatory'=>0,
                                                               
								  'input_html'=>'class="w_200"',
                                                                
                                                                ),
				
                                ),
                                    
				#Table Name
				
				'table_name'    => 'entity_child',
				
				#Primary Key
                                
			        'key_id'        => 'id',
				
				'divider'       => 'tab',
				
				'after_add_update'=>1,
				
				'before_add_update'=>1,
				
				# Default Additional Column
                                
				'is_user_id'       => 'created_by',
				
				'deafult_value'    => array('entity_code' => "'BL'",
							    'addon'       => '\'[["","layout_right"]]\''
							    ),
								
				# Communication
				
				'back_to' =>  array( 'is_back_button' =>1, 'back_link'=>'?d=cms_blog', 'BACK_NAME'=>'Back'),
       				
				'page_code'=> 'FMBL',
				
				'prime_index'   => 1,
                                
				'js'=> ['is_top'=>1,'top_js'=>$LIB_PATH.'def/cms_blog/f'],
				
				'show_query'    =>0,
                                
			);
      
      function before_add_update(){
	    
	    global $G,$rdsql;
	    
	    if($_POST['X1']==-1){
		
			$value = $_POST['X2'];
		
			$insert = $rdsql->exec_query("INSERT INTO entity_child (sn,entity_code,parent_id,is_active) VALUES ('$value','BL',0,1)","Insertion Of New Category Failed");
		 
			$temp = $rdsql->last_insert_id('entity_child');
		
			//$get = $rdsql->exec_query("SELECT sn FROM entity_child WHERE id = $temp","Selection Failed");
		
			//$get_val = $rdsql->data_fetch_object($get);
		
			$_POST['X1'] = $temp ;
			
		}
      
	    if(strlen($_POST['X8']) == 0){
		  
			$_POST['X8'] = $_POST['X7'];
		  
		  }
      }
    
    # after add update
    
	function after_add_update($key_id){
	 
		  global $rdsql,$G,$LIB_PATH,$USER_ID;
		  
		  $title = $_POST['X4'];
		  
		  $code = str_replace(' ','_',strtolower($title));
		  
		  $insert_title_code = $rdsql->exec_query("UPDATE entity_child SET code='$code',sn='$title' WHERE id = $key_id","Insert_title_code Failed");
		 
		  $lv         = [];		  
		  
		  $lv['temp'] = [];		
		  
		  $lib = $LIB_PATH.'/comp/guzzle_rest/guzzle/vendor/autoload.php';			
		  
		  require_once $lib;
		  
		  $lv['temp'] = $G->get_key_value('code','entity_child'," AND id=$key_id");
	       	
		  $client = new Client([
		       // You can set any number of default request options.
		       'timeout'  => 2.0,
		  ]);
		  
		  #print_r($lv['temp']);
		  		  
		  $node_res = $client->GET($_SERVER["HTTP_HOST"].$_SERVER["SCRIPT_NAME"],['query'=>['t_series'=>'basic',
												    'key'     =>$lv['temp']['code']
												   ]
											  ]
					   );
		  
	} // end
    
          
?>