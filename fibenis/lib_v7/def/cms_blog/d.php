<?PHP
       
        //define('PAGE_PERM',$_SESSION['CMS_BLOG']);
	
	$LAYOUT='layout_full';
	    
	//include($LIB_PATH."def/cms_page/d.php");
	
	$LAYOUT	    = 'layout_full';
               
        $D_SERIES       =   array(
                                   'title'=>'',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
                                    
                                    #table data
                                    
                                    'data'=> array(
					                
							3=>array('th'=>'Page Title ',
									 
								'field'=>'ln as v3',
                                                                   
									 
								'td_attr' => ' class="label_father align_LM" width="30%"',
									 
								),
							
							6=>array('th'=>'Parent',
									 
								'field'=>" ifnull((SELECT sn as e_child FROM entity_child as e_child WHERE e_child.id=entity_child.parent_id),' NA ')  as v6",
                                                                   
									 
								'td_attr' => ' class="label_grand_child align_LM" width="15%"',
									 
								),
							
					
                                                        1=>array('th'=>'Menu Name',
								 
								'field'=>'sn as v1',
								     
								/*'html'      =>  'style	= "cursor:pointer" onclick="JavaScript:E_V_PASS(\'sort_field\',1);E_V_PASS(\'sort_direction\',GET_E_VALUE(\'sort_col_1\'));filter_data();"',
								  
								'font'      =>  'class="sort"',*/
																
								'span'      =>  '<span id="sort_icon_1" name="sort_icon_1"></span>',
									
								'td_attr' => ' width="15%" class="clr_dark_blue"',
                                                                            
								), 
													
                                                        2=>array('th'=>'Status',
									 
								'field'=>"CONCAT(id,'[C]',is_active) as v2",
								
								'filter_out'=>function($data_in){
										
											$temp     = explode('[C]',$data_in);
											
											$flag     = [1,0];
											
											$data_out = array(
													  'data'=>array('id'   => $temp[0],
															'key'  => md5($temp[0]),
															'label'=> 'Page',
															'cv'   => $temp[1],
															'fv'   => $flag[$temp[1]],
															'action'=>'entity_child',
															'series'=>'a',
															'token' =>'ECAI'
															)
													);
											
											return json_encode($data_out);
										},
                                                                   
								/*'html'      =>  'style	= "cursor:pointer" onclick="JavaScript:E_V_PASS(\'sort_field\',2);E_V_PASS(\'sort_direction\',GET_E_VALUE(\'sort_col_2\'));filter_data();"',
								  
								'font'      =>  'class="sort"',*/
																
								'span'      =>  '<span id="sort_icon_2" name="sort_icon_2"></span>',
									 
								'td_attr' => ' class="label_child align_LM  txt_case_upper b" style="padding-right:10px;" width="3%"',
								
								'js_call'=> 'd_series.inline_on_off',
									 
								),
							
							
							5=>array('th'=>'Updation',
									 
								'field'=>"concat((SELECT user_name FROM user_info WHERE user_info.id=entity_child.user_id),',',date_format(updated_on,'%d-%b-%y %T')) as v5",
							        									 
								'td_attr' => 'width="10%"',
								
								'js_call'=> 'show_user_info',
									 
								),
						
							 
							//  5=>array('th'=>'Content',
							//		 
							//	'field'=>'detail as v5',
							//                                                            
							//	//'html'      =>  'style	= "cursor:pointer" onclick="JavaScript:E_V_PASS(\'sort_field\',2);E_V_PASS(\'sort_direction\',GET_E_VALUE(\'sort_col_2\'));filter_data();"',
							//	  
							//	//'font'      =>  'class="sort"',
							//									
							//	
							//		 
							//	'td_attr' => ' class="label_child align_LM" width="50%"',
							//		 
							//	),
							//
							
								
								
							
								
                                                    ),
				    
                                    
                                    #Sort Info
                                      
                                       'sort_field' =>array('code',
							    
							    'sn'),
                                        
                                       'action' => array('is_action'=>0, 'is_edit' =>1, 'is_view' =>0 ),
                                       
                                       'order_by'   =>'ORDER BY id ASC ' ,
				       		
                                
                                    #Table Info
                                    
                                    'table_name' =>'entity_child',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
				    
				    'key_filter'     =>	 " AND  entity_code='PG' ",
                                
                                    # Communication
                                
                                    'prime_index'   => 1,
                                
                                    # File Include
                                
                                  'js'            => array('is_top'=>1, 'top_js' => 'js/d_series/manage_cms'),
                                    
                                    #create search field
									
                                        'search_text' => array(
								
																						
								1=>array('get_search_text'  => get_search_array('entity_child','id as ST1,ln as ST2','Title',1,0," WHERE entity_code='PG'")),
								
								//2=>array('get_search_text'  => get_search_array('entity','id as ST1,code as ST2','Code',2,1)), 
								
                                                              ),
						
				
				#Search filter 
				
				'search_field' => 'sn',
				
				'search_id' 	=> array('id'),
				
				'is_narrow_down'=>1,
				
				'before_delete'=>1,
				
				
				# include
                                
				'js'            => array('is_top'=>1, 'top_js' => $LIB_PATH.'def/cms_page/d',
															   
							),
			   
				
				'bulk_action' => array(
							array('is_bulk_button' =>0,
							      'button_name'    => ' Update Content ',
							      'js_call'        => 'recreate_page'
							      )
							
						),
				
				
				
				# summary:
				
				/*'summary_data'=>array(
							array(  'name'=>'No Data','field'=>'count(id)','html'=>'class=summary'),
				
				                   ),*/
				
				 'custom_filter' => array(
							
							    array(  'field_name' => 'Category.:', 'field_id' => 'cf_i', 'filter_type' =>'option_list', 
									
								    'option_value'=> $G->option_builder('entity_child','id,sn'," WHERE entity_code='BL' AND parent_id=0 AND is_active=1  ORDER by sn ASC"),							   
								    
								    'input_html'=>' class="WI_100" ',
								    
								    'cus_default_label'=>'Show All',
							    
								    'filter_by'  => 'parent_id'
							    ),
							    
							 ),
				
				
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>1,'page_link'=>'f=cms_page', 'b_name' => 'Add Pages' ),
								
					'del_permission' => array('able_del'=>1,'user_flage'=>1), 
								
					'date_filter'  => array( 'is_date_filter' =>1,'date_field' =>  'updated_on'),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'DMPG',
				
				'show_query'=>0,
                            
                            );
	
	// after delete
	function before_delete($param){
		
		$lv 	      = array();
		
		
		$lv['result'] = $param['g']->get_one_columm(['table'=>'entity_child',
						    'field'=>'code',
						    'manipulation'=>" WHERE id=$param[key_id]" 
						    ]);
		
		$content_page = 'template/page_content/'.$lv['result'].".html";		     
 
		if(is_file($content_page)){
		   
			unlink($content_page);
		}  		
		
	} // end
	
	
	    # addon
	if(@$_GET['default_addon']){
		
		global $G;
                global $D_SERIES;
		
		//$temp = json_encode($_GET['default_addon']);
		$temp['addon'] = json_decode($_GET['default_addon']);
                
                if($temp['addon']->at){
                    
                        $D_SERIES['key_filter'].=" AND addon='".$temp['addon']->at."'";
                    
                } // end
                
        }
	    
	$D_SERIES['key_filter']  = 	 " AND  entity_code='BL' AND parent_id>0 ";
	
	
	$D_SERIES['data'][3]=array('th'=>'Blog title ',
									 
								'field'=>'ln as v3',
                                                                	 
								'td_attr' => ' class="label_father align_LM" width="30%"',
									 
								);
	
	
	$D_SERIES['data'][6]=array('th'=>'Category',
									 
				'field'   => '(SELECT sn as e_child FROM entity_child as e_child WHERE e_child.id=entity_child.parent_id) as v6',
				 
				'td_attr' => ' class="label_grand_child align_LM" width="15%"',
					 
				);
	
	$D_SERIES['data'][8] =array('th'=>'Image',
									 
								'field'=>'image as v8',
									 
								'td_attr' => ' class="label_child align_CM" width="5%"',
								
								'filter_out'   =>	function($data_out){
									
                                                                        $temp = explode(',',$data_out);
                                                                        
									return '<img class="w_25" src="'. $temp[0].'">';
									
									},
									 
								);
	
	// search
	
	$D_SERIES['search_text'] = array(																       
						1=>array('get_search_text'  => get_search_array('entity_child','id as ST1,ln as ST2','Blog Title',1,0," WHERE entity_code='BL' AND parent_id > 0 AND is_active=1")),
				);
	
	$D_SERIES['search_field'] = 'sn';
				
	$D_SERIES['search_id'] 	= array('id');
	
	$D_SERIES['add_button']= array( 'is_add' =>1,'page_link'=>'f=cms_blog', 'b_name' => 'Add Blog' );
	
	$D_SERIES['page_code']  = 'DMBL';
	
	$D_SERIES['show_query'] = 0;
	
	$D_SERIES['del_permission'] = array('able_del'=>1,'user_flage'=>1);
      
     
?>