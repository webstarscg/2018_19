<?PHP

$D_SERIES = array(
	
	'title' => 'Demo F_addon',

	'is_user_base_query' => 0,
        
        'gx'=>1,

	#table data

	'data' => array(
	    
		1 => array(
                                'th' => 'ID',
                                
                                'field' => "id",			
                                
                                'attr' =>['class'=>'label_grand_father align_LM',
                                                    'width'=> '5%'
                                        ] 
		),

		2 => array(
                                'th' => 'Text',
                                
                                'field' => "get_exav_addon_varchar(id,'TFTX')",			
                                
                                'attr' =>['class'=>'label_grand_father align_LM',
                                                    'width'=> '10%'
                                        ] 
		),
		
		3=>array('th'=>'Image',
									 
			'field'=>"get_exav_addon_varchar(id,'TFUI')",
									 
			'td_attr' => 'class="label_child align_CM" width="5%"',
								
			'filter_out'   =>	function($data_out){
									
                        $temp = explode(',',$data_out);
                                                                        
				return '<img class="w_25" src="'. $temp[0].'">';
									
			},
									 
			),
		
                4 => array(	'th' => 'Date',
                                
                                'field' => "get_exav_addon_date(id,'TFDT')",			
                                
                                'attr' =>['class'=>'label_grand_father align_LM',
                                                    'width'=> '10%'
                                        ] 
		),
		
		5 => array(	'th' => 'Textarea',
                                
                                'field' => "get_exav_addon_text(id,'TFTA')",			
                                
                                'attr' =>['class'=>'label_grand_father align_LM',
                                                    'width'=> '10%'
                                        ] 
		),
		
		6 => array(	'th' => 'Fiben Table',
                                
                                'field' => "get_exav_addon_text(id,'TFFT')",			
                                
                                'attr' =>['class'=>'label_grand_father align_LM',
                                                    'width'=> '20%'
                                        ] 
		),
		
		
		7 => array(	'th' => 'Simple List',
                                
                                'field' => "get_exav_addon_varchar(id,'TFSL')",			
                                
                                'attr' =>['class'=>'label_grand_father align_LM',
                                                    'width'=> '10%'
                                        ] 
		),
                
                8 => array(	'th' => 'Editor',
                                
                                'field' => "get_exav_addon_text(id,'TFTE')",			
                                
                                'attr' =>['class'=>'label_grand_father align_LM',
                                                    'width'=> '10%'
                                        ] 
		),
		
				
		
	),

	#Sort Info

	'action' => array('is_action' => 0, 'is_edit' => 1, 'is_view' => 0),

	'order_by' => 'ORDER BY id ASC',

	#Table Info

	'table_name' => 'entity_child',

	'key_id' => 'id',

	'key_filter' => " AND entity_code='TF' ",

	# Default Additional Column

	'is_user_id' => 'user_id',

	# Communication

	'prime_index' => 1,
        
        'is_narrow_down' => 0,
    
	'check_field' => array('id' => @$_GET['id']),								

	'add_button' => array('is_add' => 1, 'page_link' => 'f=f_addon', 'b_name' => 'Add' ),

	'del_permission' => array('able_del' => 1,'user_flage' => 0),
        
        'date_filter' => array('is_date_filter' => 1, 'date_field' => 'updated_on'),	

	#export data
	
	'export_csv' => array('is_export_file' => 0, 'button_name' => 'Create CSV', 'csv_file_name' => 'csv/log_'.time().'.csv'),

	'page_code' => 'FADDON',
        
        'search_filter_off' => 1,
        
        'show_query' => 0

);

?>