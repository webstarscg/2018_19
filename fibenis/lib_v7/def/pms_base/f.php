<?PHP

    $LAYOUT	= 'layout_full';
    
    $new_option = '<option value=-1 class="clr_green">+ Add New</option>';
                            
    $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Project Details',
				
				#Table field
                    
				'data'	=>   array(
						   
						   
						   '1' =>array( 'field_name'=> 'Project Name ',
                                                               
                                                               'field_id' => 'exa_value',
                                                               
                                                               'type' => 'text',
                                                               
							       'child_table'         => 'exav_addon_varchar', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'exa_token',   // attribute code field
							       
							       'child_attr_code'     => 'PBNA',           // attribute code
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=> "  onchange='check_project(this);'",
								
                                                               
                                                               ),
						   
						   
						   '2' =>array( 'field_name'=> 'Start date',
                                                               
                                                               'field_id' => 'exa_value',
                                                               
                                                               'type' => 'date',
							       
                                                               'child_table'         => 'exav_addon_date', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'exa_token',   // attribute code field
							       
							       'child_attr_code'     => 'PBSD',           // attribute code
							       
							       'set'		     => array('min_date' => '-1Y',
											      'max_date' => '10M'),
                                                               
							       'is_mandatory'=>0,
                                                               
                                                               'input_html'=>'class="w_100"' ,
							       
                                                               ),
						   
						    '3' =>array( 'field_name'=> 'End date',
                                                               
                                                               'field_id' => 'exa_value',
                                                               
                                                               'type' => 'date',
							       
							       'child_table'         => 'exav_addon_date', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'exa_token',   // attribute code field
							       
							       'child_attr_code'     => 'PBED',           // attribute code
							       
							       'set'		     => array('min_date' => '0D'),
                                                               
							       'is_mandatory'=>0,
                                                               
                                                               'input_html'=>'class="w_100"' ,
							       
                                                               ),
						    
						    '4' =>array( 'field_name'=> 'Description ',
                                                               
                                                               'field_id' => 'exa_value',
                                                               
                                                               'type' => 'text',
                                                               
							       'child_table'         => 'exav_addon_text', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'exa_token',   // attribute code field
							       
							       'child_attr_code'     => 'PBDS',           // attribute code
                                                               
                                                               'is_mandatory'=>0,
                                                               
                                                               'input_html'=>'class="w_250"'
                                                               
                                                               ),
						  
						   
					   ),
                                    
				#Table Name
				
				'table_name'    => 'entity_child',
				
				#Primary Key
                                
			        'key_id'        => 'id',
				
				'deafult_value'    => array('entity_code' => "'PB'"),
				
				'js'=> ['is_top'=>1,'top_js'=>$LIB_PATH.'def/pms_base/p_a/f'],
				
				# Default Additional Column
                                
				'is_user_id'       => 'created_by',
				
				'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?d=pms_base', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 1,
                                
				'page_code'	=> 'PRJT', //irrelevant information
				
				'show_query'    =>0,
                                
			);
 
   
    
?>