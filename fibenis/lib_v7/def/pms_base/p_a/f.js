
var element;

function check_task(ele,parent_id) {
        
        var lv =  new Object({});
        
        element = ele;
       
        lv.value   = ele.value;
        lv.p_id  = parent_id;
        
        var req = new ajax();
        
        req.set_request('router.php','&series=a&action=pms_base__p_a&token=TASK&param='+JSON.stringify(lv)+'');
        
        req.send_get(request_response_task);
            
}

function request_response_task(response){
        
        var msg = 'message_'+element.id;
                
        if (response == 1) {
                
                element.value = '';
                
                document.getElementById(msg).innerHTML = "<span style='color: red;'>Already Exist</span>";
                      
        }
        
        else{
                document.getElementById(msg).innerHTML = "<span style='color: green;'>Added</span>";
                
        }
        
}

function check_project(ele) {
    
        element = ele;
        
        var req = new ajax();
        
        req.set_request('router.php','&series=a&action=pms_base__p_a&token=PRJT&param='+ele.value);
        
        req.send_get(request_response_project);
            
}

function request_response_project(response){
        
        var msg = 'message_'+element.id;
                
        if (response == 1) {
                
                element.value = '';
                
                document.getElementById(msg).innerHTML = "<span style='color: red;'>Already Exist</span>";
                      
        }
        
        else{
                document.getElementById(msg).innerHTML = "<span style='color: green;'>Added</span>";
                
        }
        
}
