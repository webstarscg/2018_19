<?PHP

        $LAYOUT='layout_full';
	
	use GuzzleHttp\Client;
				
	use GuzzleHttp\Query;
	
	include($LIB_PATH."def/cms/f.php");
	
	$F_SERIES['title']='News & Events';
	
  
      	
	$F_SERIES['data'][10]=array( 'field_name'=> 'Start Date',
                                                               
                                                              'field_id'=>'date',
                                                               
                                                               'type'=>'date',
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_200"' ,  
                                                               'set'=>array('min_date'=>'-5d', 'max_date'=>'+5d'),

                                                               );
	  
	
        $F_SERIES['data'][11]=array( 'field_name'=> 'End Date',
                                                               
                                                              'field_id'=>'date_II',
                                                               
                                                               'type'=>'date',
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_200"' ,  
                                                                'set'=>array('min_date'=>'-5d', 'max_date'=>'+5d'),
                                                               );   
      
      
      
        $F_SERIES['data'][8]=array( 'field_name'=> 'Event Title ',
                                                               
				'field_id'=>'sn',
				 
				 'type'=>'text',
				 
				 'is_mandatory'=>1,
				 
				 'input_html'=>'class="w_200" onkeypress  =   "return PR_All_Alpha_Numeric(event,\' -_.\');"'   
                                                               
                              );
	
	  
	  $F_SERIES['data'][6] = array('field_name'=>'Event Type',
                                                               
					'field_id'=>'parent_id',
					'type' => 'option',
                                                                					
					'option_data'=>$G->option_builder('entity_child','id,sn',' WHERE entity_code="ET" AND is_active="1"  ORDER BY sn ASC'),
					
					'is_mandatory'=>1,
					
					'input_html'=>'class="w_200"'                                                               
                                );
      
      
        $F_SERIES['data'][5] = array('field_name'=>'Event Description',
                                                               
					'field_id'=>'detail',
					
					'type'=>'textarea_editor',
					
					'is_mandatory'=>1,
					
					'input_html'=>'class="w_200"'                                                               
                                );
       
       $F_SERIES['back_to'] =  array( 'is_back_button' =>1, 'back_link'=>'?d=cms_news_events', 'BACK_NAME'=>'Back');
       
       $F_SERIES['deafult_value'] = array('entity_code' => "'NE'",
					   'addon'       => '\'[["","layout_right"]]\''
					  );
       
       $F_SERIES['page_code']  = 'FMNE';
       
       unset($F_SERIES['data'][1]);       
       unset($F_SERIES['data'][2]);       
       unset($F_SERIES['data'][14]);
       unset($F_SERIES['data'][4]);
       unset($F_SERIES['data'][9]); # addon
       
       
         # after add update
    
	function after_add_update($key_id){
	 
		  global $rdsql,$G,$LIB_PATH;
		  
		  $lv         = [];		  
		  $lv['temp'] = [];
		  
		  $lib = $LIB_PATH.'/comp/guzzle_rest/guzzle/vendor/autoload.php';			
		  
		  require_once $lib;
		  
		  
		$page_code = strtolower(preg_replace('/\s+/', '_', $_POST['X8']));
	    
		//update
	    
		$update = " UPDATE entity_child SET code = '$page_code' WHERE  id = $key_id";
		
		$execute_query = $rdsql->exec_query($update, 'update----');
		  
		$lv['temp'] = $G->get_key_value('code','entity_child'," AND id=$key_id");
        	
		$client = new Client([
		       // You can set any number of default request options.
		       'timeout'  => 2.0,
		]);
		  
		#print_r($lv['temp']);
		  		  
		$node_res = $client->GET($_SERVER["HTTP_HOST"].$_SERVER["SCRIPT_NAME"],['query'=>['t_series'=>'basic',
												    'key'     =>$lv['temp']['code']
												   ]
					]
		);
		  
		  
    } // end
?>