<?PHP

        $LAYOUT	    	= 'layout_full';
               
        $D_SERIES       =   array(
                                   'title'=>'Title',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
				    
				    'gx' => 1,
				    
                                    
                                    #table data
                                    
                                    'data'=> array( 
						        2=>array('th'=>'Name',
								
								'field' =>"sn",
								
								'td_attr' => ' class="label_father align_LM" width="30%"',
								
								'is_sort' => 0,	
								
								),
							
							3=>array('th'=>'Long Name ',
								
								'field' =>"ln",
								    
								'td_attr' => ' class="label_father align_LM" width="30%"',
								
								'is_sort' => 0,	
								
								),
							
							4=>array('th'=>'Date & Time ',
								
								'field'	=> "date_format(timestamp_punch,'%d-%b-%y   %h:%m:%s')",
                                                                   
								'td_attr' => ' class="label_father align_LM" width="50%"',
								
								'is_sort' => 0,	
								
								),
					
                                                    ),
				    
					
                                    #Table Info
                                    
                                    'table_name' =>'entity_child_base',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
				    
				    'key_filter'     =>	 " AND entity_code='PT'",
				    
                                    # Communication
                                
                                    'prime_index'   => 1,
                                
                                    # File Include
                                
                                   'js'            => array('is_top'=>1, 'top_js' => 'js/d_series/manage_cms'),
				   
				   # include
                                
				   'js'            => array('is_top'=>1, 'top_js' => $LIB_PATH.'def/project_type/d',
															   
							),
				  
						
				
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>1,'page_link'=>'f=project_type', 'b_name' => 'Add Project Type' ),
								
					'del_permission' => array('able_del'=>0,'user_flage'=>0), 
								
					'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  ''),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'PTYPE',
				
				'show_query'=>0,
                            
                            );
    
?>