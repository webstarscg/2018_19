<?PHP
    
    $LAYOUT	    = 'layout_full';
                            
    $F_SERIES	=	array(
				#Title
				
				'title'	=>'Status',
				
				#Table field
                    
				'data'	=>   array(
						   
						   
						   '3' =>array( 'field_name'=> 'Token ',
                                                               
								'field_id' => 'token',
                                                               
								'type' => 'text',
                                                               
                                                                'is_mandatory'=>0,
                                                               
                                                             ),
						   
						   '4' =>array( 'field_name'=> 'Short Name ',
                                                               
								'field_id' => 'sn',
                                                               
								'type' => 'text',
                                                               
                                                                'is_mandatory'=>1,
                                                               
                                                             ),
						   
						   '5' =>array( 'field_name'=> 'Long name ',
                                                               
								'field_id' => 'ln',
                                                               
								'type' => 'text',
                                                               
                                                                'is_mandatory'=>0,
                                                               
                                                             ),
						   
						   
					    ),
				
                                    
				#Table Name
				
				'table_name'    => 'entity_child_base',
				
				#Primary Key
                                
			        'key_id'        => 'id',
				
				'default_fields'    => array( 'entity_code' => "'PT'" ),
				
				# Default Additional Column
                                
				'is_user_id'       => 'user_id',
				
				'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?d=project_type', 'BACK_NAME'=>'Back'),
                                
				'show_query' => 0,
                               				
				# Communication
								
				'prime_index'   => 3,
                                
				# File Include
                                
				'page_code'	=> 'FMCG',
				
				
			);
?>