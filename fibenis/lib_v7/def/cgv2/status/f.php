<?PHP
	 
    $default_addon = $_GET['default_addon'];
    
    $temp = explode(':',$default_addon);
    
    $status_code = $temp[0];
    
    $entity_child_id=$temp[1];
    
    $entity_code = substr($temp[0],0,2);
    
    $LAYOUT	    = 'layout_full';
                            
    $F_SERIES	=	array(
				#Title
				
				'title'	=>'Status',
				
				#Table field
                    
				'data'	=>   array(
						   
						   
						   '1' =>array( 'field_name'=> 'Detail ',
                                                               
								'field_id' => 'note',
                                                               
								'type' => 'textarea',
                                                               
                                                                'is_mandatory'=>0,
                                                               
                                                             ),
						   
						   '2' =>array(     'field_name'=>'Status',
                                                               
								    'field_id'=>'status_code',
                                                               
								    'type' => 'option',
							       
								    'option_data'=>$G->option_builder('entity_child_base','token,sn',"WHERE token IN (SELECT status_code_end FROM status_map WHERE status_code_start='$status_code') ORDER BY sn ASC"),
								 
								    'is_mandatory'=>1,
                                                               
								    'input_html'=>'class="w_200"'
								    
                                                               ),
						   
					    ),
				
                                    
				#Table Name
				
				'table_name'    => 'status_info',
				
				#Primary Key
                                
			        'key_id'        => 'id',
				
				'default_fields'    => array(
							    'entity_code' => "'$entity_code'",
							    'entity_child_id' => "'$entity_child_id'",
							    ),
				
				# Default Additional Column
                                
				'is_user_id'       => 'user_id',
				
				'back_to'  => NULL,
				
				'show_query' => 1,
                               				
				# Communication
								
				'prime_index'   => 1,
                                
				# File Include
                                
				'page_code'	=> 'FMCG',
				
				'show_query'    =>0,
				
				'after_add_update'=>1,
                                
			);
    
     function after_add_update($key_id){
	 
		global $rdsql;
	
		$temp_result = $rdsql->exec_query("SELECT status_code,entity_code,entity_child_id FROM status_info WHERE id=$key_id","INS");
		
		$temp_row=$rdsql->data_fetch_row($temp_result);
		
		if($temp_row[1]=='TK'){
		    
		    $temp_row[1] = 'TS';
		}
		
		$update = $rdsql->exec_query("UPDATE exav_addon_varchar SET exa_value = '$temp_row[0]' WHERE exa_token = '$temp_row[1]'  AND parent_id = '$temp_row[2]'","Updation Failed");
		
		return null;	
		
        } 
?>