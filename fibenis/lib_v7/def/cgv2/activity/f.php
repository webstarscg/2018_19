<?PHP
    
    $date = date("d-m-Y");
    
    if(isset($_GET['default_addon'])){  

	$key = $_GET['default_addon'];
    
        $add = $_GET['add'];
      
        if($add=="add_activity"){
                        
                        $option_builder = $G->option_builder('exav_addon_varchar','parent_id,exa_value'," WHERE exa_token='TKNA' AND parent_id=$key ORDER BY exa_value ASC");
			$back	= array( 'is_back_button' =>0, 'back_link'=>'?d=activity', 'BACK_NAME'=>'Back');
			$division = $G->option_builder('entity_child_base','id,sn'," WHERE entity_code='PS' AND id=(SELECT exa_value FROM exav_addon_num WHERE parent_id=$key AND exa_token='PSDV') ORDER BY sn ASC");
			$avd=1;
	  }
    }
    else{
                       $option_builder = $G->option_builder('exav_addon_varchar','parent_id,exa_value'," WHERE exa_token='TKNA'  ORDER BY exa_value ASC");
		       $back	= array( 'is_back_button' =>1, 'back_link'=>'?d=activity', 'BACK_NAME'=>'Back');
		       $division = $G->option_builder('entity_child_base','id,sn'," WHERE entity_code='PS'  ORDER BY sn ASC");
		       $avd = 0;
    }
  
    $LAYOUT	    = 'layout_full';
                            
    $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Activity Details',
				
				#Table field
                    
				'data'	=>   array(
						    '1' =>array( 'field_name'=> 'Division ',
                                                               
                                                               'field_id' => 'exa_value',
                                                               
                                                               'type' => 'option',
							       
							       'option_data'=>$division,
                                
							       'child_table'         => 'exav_addon_num', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'exa_token',   // attribute code field
							       
							       'child_attr_code'     => 'ACDV',           // attribute code
							       
							       'input_html'=>'class="w_200"',
                                                               
							       //'input_html'=>'class="w_200" onchange="name_filter(this,2);"',
                                                               
							       'is_mandatory'=>1,
							       
							       'avoid_default_option'=>$avd,
							       
                                                               ),
						   
						   
						   '2' =>array( 'field_name'=> 'Name ',
                                                               
                                                               'field_id' => 'exa_value',
                                                               
                                                               'type' => 'option',
							       
							       'option_data'=>$option_builder,
                                
							       'child_table'         => 'exav_addon_num', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'exa_token',   // attribute code field
							       
							       'child_attr_code'     => 'ACTK',           // attribute code
							       
							       'input_html'=>'class="w_200"',
                                                               
							       'is_mandatory'=>1,
							       
							       'avoid_default_option'=>$avd,
                                                               
                                                               ),
						   
						   
						   '3' =>array( 'field_name'=> 'About ',
                                                               
                                                               'field_id' => 'exa_value',
                                                               
                                                               'type' => 'textarea',
							       
							       'child_table'         => 'exav_addon_varchar', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'exa_token',   // attribute code field
							       
							       'child_attr_code'     => 'ACAB',           // attribute code
							       
							       'input_html'=>'class="w_200"',
                                                               
							       'is_mandatory'=>1,
                                                               
                                                               ),
						   
				    /*
						    
						   '4' =>array( 'field_name'=> 'Start date',
                                                               
                                                               'field_id' => 'exa_value',
                                                               
                                                               'type' => 'date',
							       
                                                               'child_table'         => 'exav_addon_date', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'exa_token',   // attribute code field
							       
							       'child_attr_code'     => 'ACSD',           // attribute code
                                                               
							       'is_mandatory'=>0,
                                                               
                                                               'input_html'=>'class="w_200"' ,
							       
                                                               ),
						   
						    '5' =>array( 'field_name'=> 'End date',
                                                               
                                                               'field_id' => 'exa_value',
                                                               
                                                               'type' => 'date',
							       
							       'child_table'         => 'exav_addon_date', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'exa_token',   // attribute code field
							       
							       'child_attr_code'     => 'ACED',           // attribute code
                                                               
							       'is_mandatory'=>0,
                                                               
                                                               'input_html'=>'class="w_200"' ,
							       
                                                               ),
						  
						  
				 */
						
						   '6' =>array('field_name'=>'Reported Date',
                                                               
                                                               'field_id'=>'exa_value',
                                                               
                                                               'type' => 'date',
							       
							       'default_date' => $date,
							       
							       'set'          => array('min_date'=>'-3D','max_date'=>'0D'),
							       
                                                               'child_table'         => 'exav_addon_date', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
							       
							       'child_attr_field_id' => 'exa_token',
								 
							       'child_attr_code'     => 'ACRD',           // attribute code
                                                               
							       'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_200"'
                                                               
                                                               ),
						   
						   '7' =>array( 'field_name'=> 'Working Hours ',
                                                               
                                                               'field_id' => 'exa_value',
                                                               
                                                               'type' => 'text',
							       
							       'allow'        => 'd5[.]',
							       
							       'child_table'         => 'exav_addon_decimal', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'exa_token',   // attribute code field
							       
							       'child_attr_code'     => 'ACWH',           // attribute code
							       
							       'input_html'=>'class="w_100"',
                                                               
							       'is_mandatory'=>1,
                                                               
                                                               ),
						   
						   
					    ),
                                    
				#Table Name
				
				'table_name'    => 'entity_child',
				
				#Primary Key
                                
			        'key_id'        => 'id',
				
				'deafult_value'    => array('entity_code' => "'AC'", 'is_active' => "1"),
				
				# Default Additional Column
                                
				'is_user_id'       => 'created_by',
								
				# Communication
								
				'back_to'  => $back,
                                
				'prime_index'   => 1,
				
                                'page_code'	=> 'FMCG',
				
				'show_query'    =>0,
                                
			);
    
?>