<?PHP
	
	include($LIB_PATH."def/project_secondary/d.php");
	
	$D_SERIES['title']='Bug';
	
	$D_SERIES['key_filter']=" AND  entity_code='BG'  ";
	
	$D_SERIES['add_button']= array( 'is_add' =>1,'page_link'=>'f=bug', 'b_name' => 'Add Bug' );
	
	$D_SERIES['data'][9]['th'] = 'Severity ';
				     
        $D_SERIES['data'][9]['field']  = "(get_ecb_sn_by_id(get_exav_addon_num(entity_child.id,'BT')))";
	
	$D_SERIES['data'][10]['field'] = "concat(id,':',get_ecb_sn_by_token(get_exav_addon_varchar(entity_child.id,'BS')),':',get_exav_addon_varchar(entity_child.id,'BS'))"; 
	
	$D_SERIES['data'][13]['field'] = "concat(id,':',get_ecb_sn_by_token(get_exav_addon_varchar(entity_child.id,'BS')),':',get_exav_addon_varchar(entity_child.id,'BS'))"; 
	
	$D_SERIES['custom_filter'][1]['option_value']=$G->option_builder('entity_child_base','token,sn'," WHERE entity_code = 'BS'");
	
	$D_SERIES['custom_filter'][1]['filter_by'] = "get_exav_addon_varchar(id,'BS')";
	
	unset($D_SERIES['data'][5]);	//Unset Start date
	
	# default addon
	if(@$_GET['default_addon']){
		
	
		$D_SERIES['key_filter'].="  AND  get_exav_addon_num(id,'TKPR')=$default_addon";
	
		$D_SERIES['del_permission'] = array('able_del'=>0,'user_flage'=>0);
	
		$D_SERIES['add_button'] = array( 'is_add' =>0,'page_link'=>'f=bug', 'b_name' => 'Add Bug' );
	
		$D_SERIES['action'] = array('is_action'=>0, 'is_edit' =>0, 'is_view' =>0 );
	
		$D_SERIES['custom_filter'][0]=[];
	
		unset($D_SERIES['search']);
		
		unset($D_SERIES['data'][4]); //To remove project name

        }
                                                         
?>