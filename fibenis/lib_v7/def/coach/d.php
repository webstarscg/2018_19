<?PHP
        include($LIB_PATH."def/entity_child/d.php");
        
        $D_SERIES['key_filter']     = "  AND entity_code='CH'  ";
        
        $D_SERIES['custom_filter']  = [];
        
        $D_SERIES['search_filter_off'] =1;
        
        $D_SERIES['is_narrow_down'] = 1;
	
	$D_SERIES['show_query'] = 0;
	
	$D_SERIES['add_button']     = array( 'is_add' =>1,'page_link'=>'f=coach', 'b_name' => 'Add Coach' );
        
        $D_SERIES['del_permission'] = array(    'able_del'=>1,								  
                                                'avoid_del_field' => 'if(((SELECT count(*) FROM entity_child as ec WHERE ec.parent_id=entity_child.id) > 0),1,0)',								  
                                                'avoid_del_value' => 1								  
                                            );
	
	
        $D_SERIES['js']    	=	array('is_top' => 1,'top_js'=> $LIB_PATH.'/def/coach/d');
	
	
	$D_SERIES['before_delete'] = 1;
	
        // 2
        
        $D_SERIES['data'][1]['th'] = 'Coach Name';
        
        
        // code
        
        $D_SERIES['data'][6] = array(   'th'        =>  'Code',
									 
                                        'field'     =>  'code',
                                                 
                                        'td_attr'   =>  ' class="label_child align_LM" width="15%"',
                                        
                                        'is_sort'   =>  0
									 
				    );
       
	
	
	 $D_SERIES['data'][7] = array(   'th'       	=>  'Domain Name',
									 
                                        'field'     	=>  "get_eav_addon_varchar(id,'CHDN')",
                                                 
                                        'attr'  	=>  ['class'=>' txt_size_13 b clr_dark_blue'],
                                                                                                                                                               
                                        'is_sort'   	=>  0
									 
				    );
	 
	 
//	 $D_SERIES['data'][8] = array(   'th'       	=>  'Theme',
//									 
//                                        'field'     	=>  iSELECT sn FROM entity_child  as ec2 WHERE ec2.id=get_eav_ec_id(entity_child.id,'CHTH'))",
//                                                 
//                                        'attr'  	=>  ['class'=>' txt_size_13 b clr_dark_blue'],
//                                                                                                                                                               
//                                        'is_sort'   	=>  0
//									 
//				    );
	  
        $D_SERIES['data'][9] = array(   'th'        =>  'Actions',
									 
                                        'field'     =>  "concat(code,',',lower(get_eav_addon_varchar(id,'CHDN')))",
                                                 
                                        'td_attr'   =>  ' class="label_child align_LM" width="15%"',
                                                                                 
                                        'filter_out'=> function($data_in){
							
								list($coach_code,$domain_name) = explode(',',$data_in);
                                                                            
								return  "<a target='_blank' href='?d=one_page&default_addon=$domain_name'>Sections</a>&nbsp;|&nbsp;".
								        "<a href='JavaScript:coach_content(\"$coach_code\",\"$domain_name\");'>Publish</a>";
                        
                                        }, // end of function
                                                                              
                                        'is_sort'   =>  0
									 
				    );
                        
        unset($D_SERIES['data'][3]);    // 3
        unset($D_SERIES['data'][5]);    // 5
	
	
	// before delete
	
	function before_delete($param){
                
                // delete pages
		$param['rdsql']->exec_query("DELETE FROM entity_child WHERE get_eav_ec_id(id,'PGCH')=$param[key_id]",'');
		
		// get ones column		
		$temp_domain    	= 	$param['g']->get_one_column(['field'        => "get_eav_addon_varchar(id,'CHDN')",
									     'table'        => 'entity_child',
									     'manipulation' => "WHERE id=$param[key_id]"
									]);		
		
		$temp_domain_hash       =       md5($temp_domain);
		
		// delete entity key value
		$param['rdsql']->exec_query("DELETE FROM entity_key_value WHERE domain_hash='$temp_domain_hash'",'');
				
		$temp_coach_path        = get_config('coach_path');		    
				       
		$temp_coach_domain_path = $temp_coach_path."/$temp_domain";
		
						    
        } // end

?>