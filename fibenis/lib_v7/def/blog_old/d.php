<?PHP
	$default_addon = @$_GET['default_addon'];
	
	if(@$default_addon){		
	   $WHERE_FILTER = ' AND parent_id = '.$default_addon.' AND is_active=1' ;	
	}else{	   
	   //$WHERE_FILTER = ' AND  entity_code="BL" AND parent_id>0 AND is_active=1';
	   $WHERE_FILTER =  ' AND  entity_code="BL" AND parent_id IN (SELECT GROUP_CONCAT(id) FROM entity_child where entity_code="BL" AND parent_id=0 AND is_active=1 ) AND is_active=1';
	}
	
	
	
	
	$content ='concat_ws("[C]",id,concat("<i class=\"fa fa-clock-o\" aria-hidden=\"true\">&nbsp;</i>",date_format(date,"%d %b %Y")),
	                       ln,code,substring_index(detail,\'. \',3),(SELECT sn FROM entity_child as ec WHERE ec.id=entity_child.parent_id))';
			   
	
               
        $D_SERIES       =   array(
                                   'title'=>'',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
                                    
                                    #table data
                                    
					'data'=> array(
					                
							1 => array('th'         => 'User_name',
								   'field'      => "$content  as v1",
								   'js_call'    => 'blog_views',
								   'td_attr'    => ' class="txt_case_capital"',
								   'filter_out'	=> function($data_in){
													   return preg_replace(array('/\s{2,}/i'),
															       array(''),																															        
															       $data_in);
										       
								   }, // end
							 ),    
          		
                                        ),
				    
                                    
                                    #Sort Info
                                      
                                       'sort_field' =>array('code',
							    
							    'sn'),
                                        
                                       'action' => array('is_action'=>0, 'is_edit' =>0, 'is_view' =>0 ),
                                       
                                       'order_by'   =>'ORDER BY line_order ASC ' ,
				       		
                                
                                    #Table Info
                                    
                                    'table_name' =>'entity_child',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
				    'session_off'       =>  1,
				     
                                
                                    # Communication
                                
                                    'prime_index'   => 1,
                                
                                    # File Include
                                
                                  'js'            => array('is_top'=>1, 'top_js' => $LIB_PATH.'/def/blog/d'),
                                    
                                    #create search field
									
                                        'search_text' => array(
								
																						
								1=>array('get_search_text'  => get_search_array('entity_child','id as ST1,ln as ST2','Blog Title',1,1,' WHERE entity_code="BL" AND parent_id>0')),
								
								//2=>array('get_search_text'  => get_search_array('entity','id as ST1,code as ST2','Code',2,1)), 
								
                                                              ),
						
				
				#Search filter 
				'hide_sno'=>1,
        
				'hide_header'=>1,
				
				'search_field' => 'title',				
				'search_id' 	=> array('ln'),
				
				'is_narrow_down'=>1,
				
				'key_filter' => $WHERE_FILTER,
				
				'table_attr'=>' class="div_of_div div_of_div_box" align="center" style="width:100%;margin-top:10px;"',
				#summary:
				
				 'list_sort'=>array(
						        'set'=>array('label'=>'Select Sort Option '),
					
							1=>array('name'=>'Recently Published','query'=> ' date DESC '),
						
							2=>array('name'=>'Earlier Published','query'=> ' date ASC'),
					     ),
						    
				
				
				'custom_filter' => array(),
				
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>0,'page_link'=>'f_series=cms', 'b_name' => 'Add Blog' ),
								
					'del_permission' => array('able_del'=>0,'user_flage'=>1), 
								
					'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  ''),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'DVBL',
				
				'pager_option' => '<option value=5>5</option><option value=10>10</option>',
				
				'show_query'   =>0
                            
                            );
    
?>
