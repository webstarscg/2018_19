<?PHP

    //F_series definition:
                            
    $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Entity',
				
				#Table field
                    
				'data'	=>   array(
                                                    
                                                    '5' => array( 'field_name'=> 'Tab', 
                                                                'type' => 'heading',
                                                              
                                                                ),
                                                    
                                                   
                                                    '7' => array( 'field_name'=> 'Sub Heading', 
                                                                'type' => 'sub_heading'
                                                                ),
                                                       
                                                   
						   '1' => array( 'field_name'=> 'Code', 
                                                               
                                                                'field_id'   => 'code',
                                                               
                                                                'type' => 'text',
                                                                  'is_mandatory'=>1,
                                                                ),
                                                   
                                                   
                                                    
                                                    '2' => array( 'field_name'=> 'SN', 
                                                               
                                                                'field_id'   => 'sn',
                                                               
                                                                'type' => 'text'
                                                                ),
                                                    
                                                
                                                    
                                                    '6' => array( 'field_name'=> 'Tab Secondary', 
                                                                'type' => 'heading'
                                                                ),
                                                    
                                                    
                                                    '8' => array( 'field_name'=> 'Sub Heading Secondary', 
                                                               'type' => 'sub_heading'
                                                               ),
                                                    
                                                     
                                                    
                                                    '3' => array( 'field_name'=> 'LN', 
                                                               
                                                                'field_id'   => 'ln',
                                                               
                                                                'type' => 'text'
                                                                ),
                                                    
                                                       
                                                    '9' => array( 'field_name'=> 'Sub Heading II', 
                                                               'type' => 'sub_heading'
                                                               ),
                                                     
                                                    '4' => array( 'field_name'=> 'Detail', 
                                                               
                                                                'field_id'   => 'detail',
                                                               
                                                                'type' => 'textarea'
                                                                ),
                                                 
				    
                                ),
                                    
				#Table Name
				
				'table_name'    => 'entity_child',
				
				#Primary Key
                                
			        'key_id'        => 'id',
                                
				# Default Additional Column
                                
				'is_user_id'       => 'user_id',
                                
                               // 'js'=> ['is_top'=>1,'top_js'=>$LIB_PATH.'def/entity/f'],
								
				# Communication
								
				'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?d=test', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 1,
                                
                                'form_layout'   => 'form_100',
                                 
                                'default_fields' => ['entity_code'=>"'TT'"],
                                
                                'divider' => 'tab',
                                
				# File Include
                                
				//'js'            => 'q_details',
				
				#Page Code
				
				#'page_code'	=> 'FETY
				
                                
			);

?>
