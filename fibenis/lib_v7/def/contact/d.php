<?PHP
	#Page Permission:
	
	#define('PAGE_PERM',$_SESSION['VIEW_CONTACT']);    

	$user_role        = @$_SESSION['user_role'];

   	$communication_id = @$_SESSION['communication_id'];
        
        $LAYOUT='layout_full';
        
	#D Series definition:
	
        $D_SERIES = array(
    
		'title' => 'External Contacts',

		'page_code' => 'DMCP',

		#query display depend on the user

		'is_user_base_query' => 0,

		#table data

		'data' => array(
    
                        7 => array('th'      => 'Status',
                                   'field'   => "CONCAT(id,'[C]',is_active) as v7",
				   'filter_out'=>function($data_in){
										
											$temp     = explode('[C]',$data_in);
											
											$flag     = [1,0];
											
											$data_out = array(
													  'data'=>array('id'   => $temp[0],
															'key'  => md5($temp[0]),
															'label'=> 'Page',
															'cv'   => $temp[1],
															'fv'   => $flag[$temp[1]],
															'action'=>'contact',
															'series' => 'a',
															'token' =>'ECAI'
															)
													);
											
											return json_encode($data_out);
										},
                                   'td_attr' => ' align_CM',
                                   'js_call' => 'd_series.inline_on_off'
                        ),    
    
                        5 => array('th'      => 'Type',
                                   'field'   => '(SELECT sn FROM entity_child WHERE code=communication.comm_group_code) as v5',
                                   'td_attr' => " width='7%' class='label_grand_child'"
                        ),

                        
                
			1 => array('th' => 'Name', 'field' => 'sn as v1',
                                   'td_attr'=>' width="15%"',
                                   'html' => 'style = "cursor:pointer" onclick="JavaScript:E_V_PASS(\'sort_field\',1);E_V_PASS(\'sort_direction\',GET_E_VALUE(\'sort_col_1\'));filter_data();"', 'font' => 'class="sort"', 'span' => '<span id="sort_icon_1" name="sort_icon_1"></span>',
                                  // 'js_call' => 'show_contact'
                                   ),
                        
                        9 => array('th'         => 'Summary', 'field' => 'ln as v9',
                                   'td_attr'    => ' width="5%"',                                  
                                   'js_call'    => 'show_summary'
                                   ),
                        

			2 => array('th'      => 'Address',
                                   'field'   => 'add_i as v2 ',
                                   'td_attr' => " class='label_grand_child clr_gray_7'",
                                    'js_call'    => 'show_summary'
                                   
                                   ),

			3 => array('th' => 'Mobile / Phone', 'field' => 'concat(phone,\',\',mobile) as v3', 'js_call' => 'show_by_lines', 'td_attr' => ' class="label_father clr_gray_7"'),

			4 => array('th' => 'Website / Email', 'field' => 'concat(website,\',\',email) as v4', 'js_call' => 'show_by_lines','td_attr' => ' class="label_grand_father clr_gray_7"'),
                        
                        8 => array('th' => 'Map', 'field' => 'add_ii as v8',
                                   'js_call' => 'show_map',
                                   'td_attr' => ' width="5%"  '),
                        
                        6 => array('th' => 'Img', 'field' => 'is_img as v6',
                                   'filter_out'   =>	function($data_out){
									
                                                                        $temp = explode(',',$data_out);
                                                                        
									return '<img class="w_100" src="'. $temp[0].'">';
							},
                                                        
                                   'td_attr' => ' width="5%"  '),

			#5 => array('th' => 'Action', 'field' => 'concat(id,\'[S]\',IFNULL(parent_id,0),\'[S]\',is_org,\'[S]\',comm_group_code) as v5', 'js_call' => 'show_action_links', 'td_attr'=>' class="no_wrap"'),

		),

		#Table Info

		'table_name' => 'communication',

		'key_id' => 'id',

		'key_filter' => "  AND comm_group_code <> 'HOME' ",

		#Hidden Field:

		'hidden_data' => array('id','comm_group_code','sn'),

		#create search field

		'search_text' => array(
			
			1 => array('get_search_text'  => get_search_array("communication WHERE 1=1 AND comm_group_code <> 'HOME'",'id as ST1, sn as ST2','Name',1,0,'')
                                   ),

		),

		 //1=>array('get_search_text'  => get_search_array('communication WHERE 1=1 AND parent_id=0 '.$key_filter,'id as ST1,sn as ST2','Name',1,0)),
		 //
		 //2=>array('get_search_text'  => get_search_array('communication','(SELECT id FROM entity_attribute WHERE code=comm_group_code) as ST1,(SELECT sn FROM entity_attribute WHERE code=comm_group_code) as ST2','Group',2,0)),
		 //
		 //3=>array('get_search_text'  => get_search_array('communication','(SELECT id FROM entity_attribute WHERE code=address_type_code) as ST1,(SELECT sn FROM entity_attribute WHERE code=address_type_code) as ST2','Address Type',3,0)),
		 //
		 //4=>array('get_search_text'  => get_search_array('communication WHERE 1=1 AND parent_id <> 0 '.$key_filter,'id as ST1,concat(sn,\' (\',city_name,\')\') as ST2','Shipping Address',4,0)),
		 //),

		 #Search filter 

                'search_field' => 'sn',

		'search_id' 	=> array('id'),

		// #Custom Filter

		    'custom_filter' => array(
				
				  array(

					'field_name' => 'Status:',

					'field_id' => 'cf2', 

					'filter_type' =>'option_list', 
												
					'option_value'=> '<option value="true" class="clr_green">Active</option>'.
                                                         '<option value="false" class="clr_red">In-Active</option>',
							
                                        'cus_default_label' => 'Show All',
								
					'html' => " class='w_150'",
							
					'filter_by'  => "IF(is_active,'true','false')"
				
				) , 
                                array(

					'field_name' => 'Type :',

					'field_id' => 'cf1', 

					'filter_type' =>'option_list', 
												
					'option_value'=>$G->option_builder('entity_attribute','code,sn',"WHERE entity_code='CG' order by sn ASC "),
							
                                        'cus_default_label' => 'Show All',
								
					'html' => ' class="w_150"',
							
					'filter_by'  => 'comm_group_code'
				
				)                               
                    ),
                    
                    
                    

		#check_field

		'check_field' => array('id' => @$_GET['id']),								

		#Date

		'date_filter' => array('is_date_filter' => 0, 'date_field' => 'timestamp_punch'),

		// 'top_action' => array(array('label' => 'Create Label', 'style' => 'icon add', 'action' => "JavaScript:export_label('simple')")),

		#Summary:

		'summary_data' => array(array('name' => 'Total People:', 'field' => 'count(id)', 'html' => 'width="100%" class=summary')),


		#Sorting:

		'sort_field' => array('sn'),

		#Default Order:

		'order_by' => 'ORDER BY id ASC',

		#Narrow Down:

		'is_narrow_down' => 1,

		#Js File Include

		'js' => array('is_top' => 1, 'top_js' => $LIB_PATH.'def/contact/d', 'is_bottom' => 0),	

		#Action:

		'action' => array('is_action' => 0, 'is_edit' => 0, 'is_view' => 0, 'is_add_more_info' => 0),

                # bulk action
                
                'bulk_action'    => array(
                                            array('is_bulk_button' =>1, 'button_name' => 'Set Active Status', 'js_call' => 'set_contact_active'),
                                            
					),
                
		// #Custom Action:

		// 'custom_action' => array(
		// array('fun_name'=>'edit_contact','action_name'=>'', 'html'=>'class="icon edit hint--left" data-hint="Edit Contact Details" '),
		// ),

		#Add:

		'add_button' => array('is_add' => 1, 'page_link' => 'f=contact', 'b_name' => 'Add Contact info'),

		#Delete:

		'del_permission' => array('able_del' => 1, 'user_flage' => 1), 

		# Communication

		'prime_index' => 1,
                
		'show_query'=>0
                

		// #export data

		// 'export_csv'   => array('is_export_file' => 1, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
		// 'export_label' => array('simple'=>array('field'=>'sn as v1,
		// add_i as v2,
		// add_ii as v3,
		// add_iii as v4,
		// city_name as v5,
		// pincode as v6,
		// phone as v7,
		// mobile as v8,
		// email as v9',

		// 'style'=>'2x8',
		// 'file_name'=>'csv/customer_label.html',
		// 'per_page_data'=>'16')
		// )

);

  $D_SERIES['action'] = array('is_action' => 1, 'is_edit' => 1, 'is_view' => 0);

?>