<?PHP
        $LAYOUT	    = 'layout_full';
               
        $D_SERIES       =   array(
					'title'=>'',
                                    
					# query display depend on the user
                                    
					'is_user_base_query'=>0,
					
					# generation
				    
					'gx'	=>1,
                                    
					# table data
					
					'data'=> array(  
							
							1=>array(
									# column header
									'th'		=> 'Name',									 
									
									# table field
									'field'		=> 'sn',									 
									
									# column attribute
									'td_attr'	=> " class='label_father align_LM' width='75%'",
									
									# sort
									'is_sort' 	=> 1
							),				
							
							2=>array(
									'th'		=> 'Status',
									 
									'field'		=> "CONCAT(id,'[C]',is_active)",
								
									# filter out
									'filter_out'	=> function($data_in){
										
												$temp     = explode('[C]',$data_in);
											
												$flag     = [1,0];
											
												$data_out = array(
													  
														'data'=>array(	'id'   => $temp[0],
																'key'  => md5($temp[0]),
																'label'=> 'Page',
																'cv'   => $temp[1],
																'fv'   => $flag[$temp[1]],
																'action'=>'entity_child',
																'token' =>'ECAI',
																'series'=>'a'
															)
													);
											
												return json_encode($data_out);
											},                                                                   
                                                             
									 
									'td_attr' => " class='label_child align_LM  txt_case_upper b' style='padding-right:10px;' width='3%'",
								
									# call js
									'js_call'=> 'd_series.inline_on_off',									 
								),
					),					
					
					# table Info
                                    
					'table_name' 	=> 'entity_child',
					
					# primary key
					
					'key_id'    	=> 'id',                                    
					
					# default order
				       
					'order_by'   	=> 'ORDER BY id ASC ' ,
					
					# default actions
					
					'action'     	=> array('is_action'=>0, 'is_edit' =>1, 'is_view' =>0 ),
                                       					
					# default user column
                                
					'is_user_id'    => 'user_id',
                                
					# communication
                                
					'prime_index'   => 1,
                                
					# js additional include
                                
					'js'            => array('is_top'=>1, 'top_js' => ''),
                                    
					# search 
									
                                        'search_text' 	=> array(	1=>array('get_search_text'  => get_search_array('entity_child','id as ST1,sn as ST2','Category',1,0," WHERE entity_code='BL' AND parent_id=0 ")),),
								
					'search_field' 	=> 'sn',
				
					'search_id' 	=> array('id'),					
					
					# where filter default addon
					
					'key_filter'   => " AND entity_code = 'BL' AND parent_id=0", 
				
					# summary:
				
					'summary_data'	=> array(),				
				
					'custom_filter' => array(),
				
					# add control
								
					'add_button' 	=> array( 'is_add' =>1,'page_link'=>'f=blog_category', 'b_name' => 'Add Blog Category' ),
							
					# delete permission		
								
					'del_permission'=> array('able_del'=>1,'user_flage'=>1), 
							
					# date filter
								
					'date_filter'  	=> array( 'is_date_filter' =>0,'date_field' =>  ''),	
								
					# export data
				
					'export_csv'   	=> array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
					
					# page_code
								
					'page_code'    	=> 'DMBC'
                            
	); # end
    
?>