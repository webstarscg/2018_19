<?PHP

    $LAYOUT	    = 'layout_full';
                            
    $F_SERIES	    =	array(
	
				# title
				
				'title'	=>'Blog Category',
				
				# table fields
                    
				'data'	=>  array(
						    '1'	=>  array(
								    'field_name'	=> 'Category',
                                                               
								    'field_id' 		=> 'sn',
                                                               
								    'type'		=> 'text',
                                                               
								    'is_mandatory'	=> 1,
                                                               
								    'input_html'	=> 'class="w_200"'                                                               
							    ),
					    ),
                                    
				# tablename
				
				'table_name'	=> 'entity_child',
				
				# key
                                
			        'key_id' 	=> 'id',
				
				# default values
				
				'deafult_value' =>  array(  'entity_code' => "'BL'",
							    'is_active'   => "1"
						    ),
				
				# user id
                                
				'is_user_id'    => 'created_by',
								
				# communication
								
				'back_to'  	=>  array(  'is_back_button' => 1,
							    'back_link'	     => '?d=blog_category',
							    'back_name'	     => 'Back'
						    ),
                                
				# prime index
				
				'prime_index'   => 1,                                
				
				# page code								
				
				'page_code'	=> 'FMBC',
				
				# show query for debug				
				
				'show_query'    =>0,
                                
			);  # end   
    
?>