<?PHP
    $LAYOUT='layout_full';
 
    include_once($LIB_PATH."/inc/lib/f_addon.php");
 
   use GuzzleHttp\Client;
				
	use GuzzleHttp\Query;
  //  $lib = $LIB_PATH.'/comp/guzzle_rest/guzzle/vendor/autoload.php';
    

    //F_series definition:
                            
    $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Entity',
                                
                                'gx'    => 1,
				
				#Table field
                    
				'data'	=>   array(			
                                                    
                                                 
                                                            
                                                            1=>array(
								     
								'type'=>'text',
                                                                'field_id'=>'payment_type',
                                                                'field_name' =>'Payment Type',
                                                                'is_mandatory'        => 1,
                                                                
                                                                // 'remote_link' => 'router.php?series=a&action=test&token=FT_AUT', 
                                                                
                                                               
                                                            ),
							    
							    //amount
							   2=>array(
								     
								'type'=>'text',
                                                                'field_id'=>'payment_mode',
                                                                'field_name' =>'Payment Mode',
                                                                'is_mandatory'        => 1,
                                                                
                                                                // 'remote_link' => 'router.php?series=a&action=test&token=FT_AUT', 
                                                                
                                                               
                                                            ),
							   
							   3=>array(
								     
								'type'=>'text',
                                                                'field_id'=>'amount',
                                                                'field_name' =>'Amount',
                                                                'is_mandatory'        => 1,
                                                                
                                                                // 'remote_link' => 'router.php?series=a&action=test&token=FT_AUT', 
                                                                
                                                               
                                                            ), 
                                                            
                                                            
                                                        
                                                  
//                                                               
				    
                                ),
                                    
				#Table Name
				
				'table_name'    => 'payment_info',
				
				#Primary Key
                                
			        'key_id'        => 'id',
                                
				# Default Additional Column
                                
				'is_user_id'       => 'user_id',
                                
                                'js'=> ['is_top'=>1,'top_js'=>$LIB_PATH.'def/test/f'],
								
				# Communication
								
				'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?d=test', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 1,
                                
                                'form_layout'   => 'form_100',
                                 
                                //'default_fields' => ['entity_code'=>'TT'],
                                
				# File Include
                                
				//'js'            => 'q_details',
				
				#Page Code
				
                                'after_add_update'=>1,
                                
				#'page_code'	=> 'FETY
				
                                'show_query' => 0,
                                
                                'divider' => 'accordion',
				'avoid_trans_key_direct' => 1,

                                
			);
    
    
    $F_SERIES['after_add_update']=function($key_id){
	
	    global $LIB_PATH;
	    
	    #echo $LIB_PATH;
	
	    #$lib = $LIB_PATH.'/comp/guzzle_rest/guzzle/vendor/autoload.php';
	    
	    $lib = $LIB_PATH.'/comp/guzzle_rest/vendor/autoload.php';
	    
	    require_once $lib ;
	    
	    $pay_lib = $LIB_PATH.'/inc/lib/payment_gateway.php';
	    
	    require_once $pay_lib ;
	    
	    $client = new Client([
				    //'timeout'  => 5.0,
				]);
	     
	    
	    $param = array('client'=>$client);
	    
	    $P->set_payment($param);
	
	
	
	
	
    }
    
    
    
    
    
    
?>

