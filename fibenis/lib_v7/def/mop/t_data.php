<?php

    $T_SERIES['temp']['section']=[

						'detail'=>array('field' => 'detail'),
						
						
						
						'home_images'	=>	array(
										'is_child_addon'=> 1,
										
										'child_data'   	=> array(
														'1'=>array('key'=>'data_binder','field'=>"code"),
														'2'=>array('key'=>'content_src','field'=>"image")														
													),
										
										'table'		=> 	'entity_child',
										
										'key_filter'	=>	" AND entity_code='HI'
													  AND parent_id=get_entity_child_of_child_from_code('$coach_code','HI')
													  AND get_ec_status(parent_id)=1
													  ORDER BY line_order ",
										
										'show_query'	=> 0									
									),
						
						
						
						'home_banner'=>array(
									'is_child_addon' =>1,
									
									'child_data'   =>array(
													'1'=>array('key'=>'btn_title','field'=>'sn'),
													'2'=>array('key'=>'url','field'=>'ln' ),
													'3'=>array('key'=>'img_src','field'=>'image' ),
													
													'4'=>array(	'key'		=> 'heading','field'=>'detail',
														   
															'filter_out'	=> function($data_in){
																	
																			$lv = [];
																			
																			$lv['data']    = json_decode($data_in,true);
																			
																			$lv['content'] = [];																	
																			
																			foreach($lv['data'] as $data_key=>$data_info){
																				
																				if(@$data_info[0]){
																					array_push($lv['content'],['title'=>@$data_info[0]]);
																				} // end
																			}
																		
																			return $lv['content'];																	
																		} // end														   
														),
													
													
										),
									
									'table'=> ' entity_child',
									
									'key_filter'=>" AND entity_code='HB'
											
											AND parent_id=get_entity_child_of_child_from_code('$coach_code','HB')
											
											AND get_ec_status(parent_id)=1
											
											ORDER BY line_order",
									
									'show_query'=>0
									
						),
						
						
						'marquee'=>array(
									'is_child_addon' =>1,
									'child_data'   =>array(
													'1'=>array('key'=>'Title','field'=>'ln'),
													'2'=>array('key'=>'url','field'=>'sn' ),
													
													
										),
									
									'table'=> ' entity_child',
									
									'key_filter'=>" AND entity_code='MQ'
										
											AND parent_id=get_entity_child_of_child_from_code('$coach_code','MQ')
											
											AND get_ec_status(parent_id)=1
											
											ORDER BY line_order ",
									
									'show_query'=>0
									
						),
						
						# features_heading
						
						'feature_heading'=>array(
								       
									 'is_child_addon' =>1,
									'child_data'   =>array(
													'1'=>array('key'=>'heading',    'field' => 'ln'),
													'2'=>array('key'=>'sub_heading','field' => 'detail' )
										),
									
									'table'=> ' entity_child',
									
									//'key_filter'=>" AND entity_code='HF' ORDER BY line_order",
									
									'key_filter'=>" AND entity_code='HF'
									                AND parent_id=get_ec_id_by_codes('CH','$coach_code')",
									
									'show_query'=>0
								       
								       ),
						# features
						
						'features'=>array(
									'is_child_addon' =>1,
									'child_data'   =>array(
													'1'=>array('key'=>'fa-icon','field'=>'code'),
													'2'=>array('key'=>'title','field'=>'sn' ),
													'3'=>array('key'=>'hint','field'=>'ln' )
													
													
										),
									
									'table'=> ' entity_child',
									
									//'key_filter'=>" AND entity_code='HF' ORDER BY line_order",
									
									'key_filter'=>" AND entity_code='HF'
									                AND parent_id=get_entity_child_of_child_from_code('$coach_code','HF')
											AND get_ec_status(parent_id)=1
											ORDER BY line_order ",
									
									'show_query'=>0
									
						),
						
						# portfolio_heading
						
						'portfolio_heading'=>array(
								       
									 'is_child_addon' =>1,
									'child_data'   =>array(
													'1'=>array('key'=>'heading',    'field' => 'ln'),
													'2'=>array('key'=>'sub_heading','field' => 'detail' )
										),
									
									'table'=> ' entity_child',
									
									'key_filter'=>" AND entity_code='HG'
									                AND parent_id=get_ec_id_by_codes('CH','$coach_code')",
									
									'show_query'=>0
								       
								       ),
						
						# portfolio
						
						'portfolio'=>array(
								'is_child_addon' =>1,
								'child_data'   =>array(
												'1'=>array('key'=>'title','field'=>'sn'),
												'2'=>array('key'=>'thumb_image','field'=>'image' ),
												'3'=>array('key'=>'max_image','field'=>'image_a' ),
												
												
												
									),
								
								'table'=> ' entity_child',
								
								'key_filter'=>" AND entity_code='HG'
										AND parent_id=get_entity_child_of_child_from_code('$coach_code','HG')
										AND get_ec_status(parent_id)=1
										ORDER BY line_order
										",
								'show_query'=>0
								
						),
						
						# team_heading
						
						'team_heading'=>array(
								       
									 'is_child_addon' =>1,
									'child_data'   =>array(
													'1'=>array('key'=>'heading',    'field' => 'ln'),
													'2'=>array('key'=>'sub_heading','field' => 'detail' )
										),
									
									'table'=> ' entity_child',
									
									'key_filter'=>" AND entity_code='HT'
									                AND parent_id=get_ec_id_by_codes('CH','$coach_code')",
									
									'show_query'=>0
								       
								       ),
						
						# team
						
						'team'=>array(
								'is_child_addon' =>1,
								'child_data'   =>array(
												'1'=>array('key'=>'name','field'=>'sn'),
												'2'=>array('key'=>'image_path','field'=>'image' ),
												'3'=>array('key'=>'work','field'=>'ln' )
												
												
									),
								
								'table'=> ' entity_child',
								
								'key_filter'=>" AND entity_code='HT'
								                AND parent_id=get_entity_child_of_child_from_code('$coach_code','HT')
										AND get_ec_status(parent_id)=1
										ORDER BY line_order
										",
								
								'show_query'=>0
								
						),
						
						# counter_heading
						
						'counter_heading'=>array(
								       
									 'is_child_addon' =>1,
									'child_data'   => array(
													'1'=>array('key'=>'heading',    'field' => 'ln'),
													'2'=>array('key'=>'sub_heading','field' => 'detail'),
													'3'=>array('key'=>'bg_image',   'field' => 'image')
										    ),
									
									'table'=> ' entity_child',
									
									'key_filter'=>" AND entity_code='HC'
									                AND parent_id=get_ec_id_by_codes('CH','$coach_code')",
									
									'show_query'=>0
								       
								       ),
						
						# counter
						
						'counter'=>array(
									'is_child_addon' =>1,
									'child_data'   =>array(
													'1'=>array('key'=>'fa-icon','field'=>'code'),
													'2'=>array('key'=>'count','field'=>'sn' ),
													'3'=>array('key'=>'title','field'=>'ln' )
													
													
										),
									
									'table'=> ' entity_child',
									
									'key_filter'=>" AND entity_code='HC' 
											AND parent_id=get_entity_child_of_child_from_code('$coach_code','HC')
											AND get_ec_status(parent_id)=1
											ORDER BY line_order",
									
									'show_query'=>0
									
						),
				//		
				//		
				//		# testimonial_heading
				//		
				//		'testimonial_heading'=>array(
				//				       
				//					 'is_child_addon' =>1,
				//					'child_data'   =>array(
				//									'1'=>array('key'=>'heading','field'=>'sn'),
				//									//'2'=>array('key'=>'sub_heading','field'=>'ln' )
				//						),
				//					
				//					'table'=> ' entity_child',
				//					
				//					'key_filter'=>" AND entity_code='TM' AND parent_id=(SELECT id FROM entity_child WHERE entity_code='CH' AND code='$coach_code')",
				//					
				//					'show_query'=>0
				//				       
				//				       ),
				//		
				//		
				//		# testimonials
				//		
				//		'testimonials'=>array(
				//				'is_child_addon' =>1,
				//				'child_data'   =>array(
				//								'1'=>array('key'=>'title','field'=>'sn'),
				//								'2'=>array('key'=>'authour','field'=>'ln' ),
				//								'3'=>array('key'=>'content','field'=>'detail' )
				//								
				//								
				//					),
				//				
				//				'table'=> ' entity_child',
				//				
				//				'key_filter'=>" AND entity_code='TM'
				//						AND parent_id=get_entity_child_of_child_from_code('$coach_code','TM') ORDER BY line_order",
				//				
				//				'show_query'=>0
				//				
				//		),
				//		
				//		# log_heading
				//		//
				//		//'blog_heading'=>array(
				//		//	
				//		//					'key'=>'test',
				//		//					'field' => "'Blog'"),
				//				       
				//					// 'is_child_addon' =>1,
				//					//'child_data'   =>array(
				//					//				'1'=>array('key'=>'heading','field'=>'sn'),
				//					//				//'2'=>array('key'=>'sub_heading','field'=>'ln' )
				//					//	),
				//					//
				//					//'table'=> ' entity_child',
				//					//
				//					//'key_filter'=>" AND entity_code='BL' AND parent_id=(SELECT id FROM entity_child WHERE entity_code='CH' AND code='$coach_code')",
				//					//
				//					//'show_query'=>0
				//				       
				//				       //),
				//		
				//		# blog
				//		
				//		//'blog'=>array(
				//		//		'is_child_addon' =>1,
				//		//		'child_data'   =>array(
				//		//						'1'=>array('key'=>'title','field'=>'sn'),
				//		//						'2'=>array('key'=>'url','field'=>'ln' ),
				//		//						'3'=>array('key'=>'content','field'=>'detail' ),
				//		//						'4'=>array('key'=>'date','field'=>'date_format(date,"%d")'),
				//		//						'5'=>array('key'=>'month','field'=>'date_format(date,"%b")')
				//		//						
				//		//						
				//		//			),
				//		//		
				//		//		'table'=> ' entity_child',
				//		//		
				//		//		'key_filter'=>" AND entity_code='BL' AND parent_id <> 0",
				//		//		
				//		//		'show_query'=>0
				//		//		
				//		//),
				//		
						# aboutus_heading
						
						'home_aboutus_heading'=>array(
								       
									 'is_child_addon' =>1,
									'child_data'   =>array(
													'1'=>array('key'=>'heading',    'field' => 'ln'),
													'2'=>array('key'=>'sub_heading','field' => 'detail' )
										),
									
									'table'=> ' entity_child',
									
									'key_filter'=>" AND id={{SEC_ID}} AND entity_code='HA'
									                AND parent_id=get_ec_id_by_codes('CH','$coach_code')",
									
									'show_query'=>0
								       
								       ),
						
						
						# about us
						
						'home_aboutus'=>array(
								'is_child_addon' =>1,
								'child_data'   =>array(
												'1'=>array('key'=>'title','field'=>'sn'),
												'2'=>array('key'=>'content','field'=>'detail' ),
												'3'=>array('key'=>'image','field'=>'image' )
												
												
									),
								
								'table'=> ' entity_child',
								
								'key_filter'=>" AND parent_id={{SEC_ID}} AND entity_code='HA' 
										
										AND get_ec_status(parent_id)=1
										ORDER BY line_order
										",
								
								'show_query'=>0
								
						),
						
						
						# clients_heading
						
						'clients_heading'=>array(
								       
									 'is_child_addon' =>1,
									'child_data'   =>array(
													'1'=>array('key'=>'heading',    'field' => 'ln'),
													'2'=>array('key'=>'sub_heading','field' => 'detail' )
										),
									
									'table'=> ' entity_child',
									
									'key_filter'=>" AND entity_code='HL'
									                AND parent_id=get_ec_id_by_codes('CH','$coach_code')",
									
									'show_query'=>0
								       
								       ),
						
						# clients
						
						'clients'=>array(
								'is_child_addon' =>1,
								'child_data'   =>array(
												'1'=>array('key'=>'title','field'=>'sn'),
												'2'=>array('key'=>'url','field'=>'ln' ),
												'3'=>array('key'=>'image_path','field'=>'image' )
												
												
									),
								
								'table'=> ' entity_child',
								
								'key_filter'=>" AND entity_code='HL'
										AND parent_id=get_entity_child_of_child_from_code('$coach_code','HL')
										AND get_ec_status(parent_id)=1
										ORDER BY line_order",
								
								'show_query'=>0
								
						),
						
						# action_box_heading
						
						'action_box_heading'=>array(
								       
									 'is_child_addon' =>1,
									'child_data'   =>array(
													'1'=>array('key'=>'heading',    'field' => 'ln'),
													'2'=>array('key'=>'sub_heading','field' => 'detail' )
										),
									
									'table'=> ' entity_child',
									
									'key_filter'=>" AND entity_code='AB' AND parent_id=(SELECT id FROM entity_child WHERE entity_code='CH' AND code='$coach_code')",
									
									'show_query'=>0
								       
								       ),
						
						
						# action_box
						
						'action_box'=>array(
								'is_child_addon' =>1,
								'child_data'   =>array(
												'1'=>array('key'=>'button_name','field'=>'sn'),
												'2'=>array('key'=>'button_link','field'=>'ln' ),
												'3'=>array('key'=>'content','field'=>'detail' ),
												'4'=>array('key'=>'is_doc','field'=>'IFNULL(image,0)' )
												
												
									),
								
								'table'=> ' entity_child',
								
								'key_filter'=>" AND entity_code='AB'
										AND parent_id=get_entity_child_of_child_from_code('$coach_code','AB') ORDER BY line_order ",
								
								'show_query'=>0
								
						),
				//		
				//		//# product_price
				//		//
				//		//'product'=>array(
				//		//		'is_child_addon' =>1,
				//		//		'child_data'   =>array(
				//		//						'1'=>array('key'=>'name','field'=>'sn'),
				//		//						'2'=>array('key'=>'rate_per_month','field'=>'get_exav(entity_child.id,"HPRM")' ),
				//		//						'3'=>array('key'=>'rate_per_month_unit','field'=>'get_ecb_attr("HPRM","APUT")'),
				//		//						'4'=>array('key'=>'storage','field'=>'get_exav(entity_child.id,"HPST")' ),
				//		//						'5'=>array('key'=>'storage_unit','field'=>'get_ecb_attr("HPST","APUT")'),
				//		//						'6'=>array('key'=>'transfer_storage','field'=>'get_exav(entity_child.id,"HPTS")' ),
				//		//						'7'=>array('key'=>'transfer_storage_unit','field'=>'get_ecb_attr("HPTS","APUT")'),
				//		//						'8'=>array('key'=>'domains','field'=>'get_exav(entity_child.id,"HPDM")' ),
				//		//						'9'=>array('key'=>'projects','field'=>'get_exav(entity_child.id,"HPPR")' ),
				//		//						'10'=>array('key'=>'installation','field'=>'get_exav(entity_child.id,"HPIT")' ),
				//		//						'11'=>array('key'=>'sign_up','field'=>'get_exav(entity_child.id,"HPSU")' ),
				//		//						
				//		//						
				//		//						
				//		//			),
				//		//		
				//		//		'table'=> ' entity_child',
				//		//		
				//		//		'key_filter'=>" AND entity_code='PG' AND addon='pricing_tags' ",
				//		//		
				//		//		'show_query'=>0
				//		//		
				//		//),
				//		
				//		
						# accordion_heading
						
						'accordion_heading'=>array(
								       
									 'is_child_addon' =>1,
									'child_data'   =>array(
													'1'=>array('key'=>'heading',    'field' => 'ln'),
													'2'=>array('key'=>'sub_heading','field' => 'detail' )
										),
									
									'table'=> ' entity_child',
									
									'key_filter'=>" AND entity_code='AC'
									                AND parent_id=get_ec_id_by_codes('CH','$coach_code')",
									
									'show_query'=>0
								       
								       ),
						
						
						# accordion
						
						'accordion'=>array(
								'is_child_addon' =>1,
								'child_data'   =>array(
												'1'=>array('key'=>'question','field'=>'ln'),
												'2'=>array('key'=>'answer','field'=>'detail' ),
												
												
												
									),
								
								'table'=> ' entity_child',
								
								'key_filter'=>" AND entity_code='AC'
										AND parent_id=get_entity_child_of_child_from_code('$coach_code','AC')
										AND get_ec_status(parent_id)=1
										ORDER BY line_order ",
								
								'show_query'=>0
								
						),
				//		
				//		
						# tab_heading
						
						'tab_heading'=>array(
								       
									 'is_child_addon' =>1,
									'child_data'   =>array(
													'1'=>array('key'=>'heading',    'field' => 'ln'),
													'2'=>array('key'=>'sub_heading','field' => 'detail' )
										),
									
									'table'=> ' entity_child',
									
									'key_filter'=>" AND entity_code='TB'
											AND parent_id=get_ec_id_by_codes('CH','$coach_code')",
									
									'show_query'=>0
								       
								       ),
						
						
						# tab
						
						'home_tab_title'=>array(
								'is_child_addon' =>1,
								'child_data'   =>array(
												'1'=>array('key'=>'tab_title','field'=>'sn'),
												//'2'=>array('key'=>'tab_content','field'=>'detail' ),
												
												
												
									),
								
								'table'=> ' entity_child',
								
								'key_filter'=>" AND entity_code='TB'
										AND parent_id=get_entity_child_of_child_from_code('$coach_code','TB')
										AND get_ec_status(parent_id)=1
										ORDER BY line_order ",
								
								'show_query'=>0
								
						),
						
						
						# tab
						
						'home_tab_content'=>array(
								'is_child_addon' =>1,
								'child_data'   =>array(
												//'1'=>array('key'=>'tab_title','field'=>'sn'),
												'2'=>array('key'=>'tab_content','field'=>'detail' ),
												
												
												
									),
								
								'table'=> ' entity_child',
								
								'key_filter'=>" AND entity_code='TB'
										AND parent_id=get_entity_child_of_child_from_code('$coach_code','TB')
										AND get_ec_status(parent_id)=1
										ORDER BY line_order",
								
								'show_query'=>0
								
						),
				//		
				//		
				//		# timeline_heading
				//		
				//		'timeline_heading'=>array(
				//				       
				//					 'is_child_addon' =>1,
				//					'child_data'   =>array(
				//									'1'=>array('key'=>'heading','field'=>'sn'),
				//									'2'=>array('key'=>'sub_heading','field'=>'ln' )
				//						),
				//					
				//					'table'=> ' entity_child',
				//					
				//					'key_filter'=>" AND entity_code='TL' AND parent_id=(SELECT id FROM entity_child WHERE entity_code='CH' AND code='$coach_code')",
				//					
				//					'show_query'=>0
				//				       
				//				       ),
				//		
				//		
				//		# timeline
				//		
				//		'home_timeline'=>array(
				//				'is_child_addon' =>1,
				//				'child_data'   =>array(
				//								'1'=>array('key'=>'year','field'=>'sn'),
				//								'2'=>array('key'=>'title','field'=>'ln' ),
				//								'3'=>array('key'=>'content','field'=>'detail' ),
				//								
				//								
				//								
				//					),
				//				
				//				'table'=> ' entity_child',
				//				
				//				'key_filter'=>" AND entity_code='TL'
				//						AND parent_id=get_entity_child_of_child_from_code('$coach_code','TL') 
				//				                ORDER BY line_order ",
				//				
				//				'show_query'=>0
				//				
				//		),
				//		
				//		
						# enquiry & Contact
												
						'contact'	=>	array('field' => "(SELECT is_active FROM entity_child WHERE entity_code='CO' AND id=get_entity_child_of_child_from_code('$coach_code','CO'))"),
						
						
						
						'enquiry_items'=>array('field' => "get_eav_addon_varchar(id,'CHEI')", // table field name
                               
									// counter match to the index 
									// 1 will fetch the name from index 0       
								
								       'data'  => array( '1'=>array('key'=>'ITEM')
											 )
						 ),
						

						
						'contact_info'	=>	array(
										'is_child_addon'=> 1,
										
										'child_data'   	=> array(
														'1'=>array('key'=>'org_name','field'=>"get_eav_addon_varchar(id,'COFN')"),
														'2'=>array('key'=>'add_i',   'field'=>"get_eav_addon_varchar(id,'CORA')"),
														'3'=>array('key'=>'add_ii',  'field'=>"get_eav_addon_varchar(id,'CORB')"),
														'4'=>array('key'=>'phone',   'field'=>"get_eav_addon_varchar(id,'COLD')"),
														'5'=>array('key'=>'mobile',  'field'=>"get_eav_addon_varchar(id,'COMB')"),
														'6'=>array('key'=>'email',   'field'=>"get_eav_addon_varchar(id,'COEM')"),
													),
										
										'table'		=> 	' entity_child',
										
										'key_filter'	=>	" AND entity_code='CO'
													  AND parent_id=get_entity_child_of_child_from_code('$coach_code','CO')
													  AND get_ec_status(parent_id)=1
													  ORDER BY line_order",
										
										'show_query'	=> 0									
						),
						
						
						'current_year'=>array('field' => "'".date('Y')."'" ),
						
						
						'footer_info'=>array(
								       
									    'is_child_addon' =>1,
									    'child_data'   =>array(
													    '1'=>array('key'=>'title',    'field' => 'ln')
													    
										    ),
									    
									    'table'=> ' entity_child',
									    
									    'key_filter'	=>	" AND entity_code='HX'
													  AND parent_id=get_entity_child_of_child_from_code('$coach_code','HX')
													  AND get_ec_status(parent_id)=1
													  ORDER BY line_order",
									    
									    'show_query'=>0
								       
								       ),
	]

?>