<?php

    $T_SERIES['temp']['map']=[
			
			# Home Banner
			
			'AC' => ['accordion_heading','accordion'],
			
			'HB' => ['home_banner']	,
			
			'HA' => ['home_aboutus_heading','home_aboutus'],
			
			'HI' => ['home_images'],
			
			'HF' => ['feature_heading','features'],
			
			'CO' => ['enquiry_items','contact','contact_info'],
			
			'HT' => ['team_heading','team'],
			
			'MQ' => ['marquee'],
			
			'HG' => ['portfolio_heading','portfolio'],
			
			'HC' => ['counter_heading','counter'],
			
			'HL' => ['clients_heading','clients'],
			
			'AB' => ['action_box_heading','action_box'],
			
			'TB' => ['tab_heading','home_tab_title','home_tab_content'],
			
			'HX' => ['current_year','footer_info']	
	]

?>