<?PHP

    include("$LIB_PATH/def/ecb_parent_child/f.php");
    
    $F_SERIES['title'] = 'Definition';
    
    # option data
    
    $F_SERIES['data']['1']['option_data']=$G->option_builder('entity','code,sn'," WHERE code='DF' ORDER by sn ASC");
                                             
    $F_SERIES['data']['6']['field_name'] = 'Definition';
    
    $F_SERIES['back_to']['back_link']    = '?d=def';
    
    # customization
    
    if(@$_GET['key']){    
        $F_SERIES['temp']['option_data']      = "WHERE  entity_code='EG' AND id NOT IN (SELECT ecb_child_id FROM ecb_parent_child_matrix WHERE ecb_parent_id=$_GET[key])";            
        $F_SERIES['data']['5']['option_data'] = $G->option_builder('entity_child_base','id,sn',$F_SERIES['temp']['option_data'].' ORDER BY sn ASC');                
    }else{
        $F_SERIES['data']['5']['option_data'] = $G->option_builder('entity_child_base',"id,sn","  WHERE entity_code='EG' ");
    }
    
?>