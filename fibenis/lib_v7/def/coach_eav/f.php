<?PHP
                        
	     $F_SERIES	=	array(
				    
				    'title'	=>'Entity Child',
				    
				    # Table field
			
				    'data'	=>   array(
						       
						       
						       '1' =>array('field_name'=>'Code',
								   
								   'field_id'=>"ea_value",
								   
								   'type'=>'text',
								   
								   'is_mandatory'=>1,
								   
								   
								//child table
									
								'child_table'         => 'eav_addon_varchar', // child table 
								'parent_field_id'     => 'parent_id',         // parent field
											 
								'child_attr_field_id' => 'ea_code',   	      // attribute code field
								'child_attr_code'     => 'ECCD', 
								   
								  
								   
								   'attr'		      => ['class'=>'w_200',
												      'onkeyup'=>"(function(from,to) {
														
												    		    to.value=from.value;
												    		  })(G.$('X1'),G.$('X4'));"
												  
												  ],
								 
								   ),
						       
				       
						       '2' =>array('field_name'=>'Name',
								   
								   'field_id'=>"ea_value",
								   
								   'type'=>'text',
								   
								   'is_mandatory'=>1,
								   
								   
								//child table
									
								'child_table'         => 'eav_addon_varchar', // child table 
								'parent_field_id'     => 'parent_id',         // parent field
											 
								'child_attr_field_id' => 'ea_code',   	      // attribute code field
								'child_attr_code'     => 'ECSN', 
								   
								   'input_html'=>'class="w_150"'
								   
								   ),
							//      
							//'3' =>array('field_name'=>'Long Name',
							//	   
							//	   'field_id'=>'ln',
							//	   
							//	   'type'=>'textarea',
							//	   
							//	   'is_mandatory'=>0,
							//	   
							//	   ),
						    
							'4'=>[
								'field_name'          => 'Domain Name',                                                                
								'field_id'            => 'ea_value',				       
								'type' 	              => 'text',
								'is_mandatory'        => 1,
						
								//child table
									
								'child_table'         => 'eav_addon_varchar', // child table 
								'parent_field_id'     => 'parent_id',         // parent field
											 
								'child_attr_field_id' => 'ea_code',   	      // attribute code field
								'child_attr_code'     => 'CHDN',          // attribute code
								  'attr'		      => ['class'=>'w_200',
												      'onkeyup'=>"(function(from,to) {
														
												    		    to.value=from.value;
												    		  })(G.$('X4'),G.$('X1'));"
												  
												  ],
								 
							    ],	
						       
						    // EAV issue   
						//       '5'=> [
						//		    'field_name'          => 'Template',                                                                
						//		    'field_id'            => 'ec_id',				       
						//		    'type' 	              => 'option',
						//		    'is_mandatory'        => 0,
						//		    'option_data'         => $G->option_builder('entity_child',"id,get_eav_addon_varchar(id,'ECSN')"," WHERE entity_code='CH' and get_eav_addon_num(id,'ECPR')=0"),
						//		    'option_default'	  => array('label'=>'Select','value'=>'0'),
						//		    
						//		    'avoid_default_option'=>0,
						//		    
						//		    //child table
						//			    
						//		    'child_table'         => 'eav_addon_ec_id', // child table 
						//		    'parent_field_id'     => 'parent_id',         // parent field
						//					     
						//		    'child_attr_field_id' => 'ea_code',   	      // attribute code field
						//		    'child_attr_code'     => 'CHTL',          // attribute code
						//		    'attr'		      => ['class'=>'w_200',
						//						    //  'onchange'=>"(function(element) {
						//						    //		    element.value=0;
						//						    //		    element.disabled=true;
						//						    //		  })(G.$('X6'));"
						//						  
						//						  ],
						//		    'ro'		 =>0,
						//		    
						//		    'filter_out'=>function($data_in){ return ($data_in)?$data_in:0;} 
						//	],
						       
							//'6'=> [
							//	    'field_name'          => 'Create from Theme',                                                                
							//	    'field_id'            => 'ec_id',				       
							//	    'type' 	              => 'option',
							//	    'is_mandatory'        => 0,
							//	    'option_data'         => $G->option_builder('entity_child','id,sn'," WHERE entity_code='TH' and parent_id=0"),
							//	    'avoid_default_option'=>0,
							//	    
							//	    //child table
							//		    
							//	    'child_table'         => 'eav_addon_ec_id', // child table 
							//	    'parent_field_id'     => 'parent_id',         // parent field
							//				     
							//	    'child_attr_field_id' => 'ea_code',   	      // attribute code field
							//	    'child_attr_code'     => 'CHTH',          // attribute code
							//	    'attr'		      => ['class'=>'w_200']
							//],
							//      
						       
						       '7'=> [ 'field_name'          => 'Enquiry Items',                                                                
								'field_id'            => 'ea_value',				       
						
								// special config 
						                  'is_fibenistable'=>1,
								    'is_index' =>1,
								'type'                => 'fibenistable',
								
							       'child_table'         => 'eav_addon_varchar', // child table 
								'parent_field_id'     => 'parent_id',         // parent field
											 
								'child_attr_field_id' => 'ea_code',   	      // attribute code field
								'child_attr_code'     => 'CHEI', 
									
								//// Grid rows
								//'default_rows_prop'   => ['start_rows'    =>'4', // default number of rows
								//			  'min_spare_rows'=>'1', // empty rows setup
								//			  'max_rows'      =>'10' // maximum allowed rows
								//			 ],	
						
								// Array of Array Input
						 
						
								'colHeaders'          => [ ['column'=>'Items', // Column Header Name
											    'width' =>'500',      // Column Width                                               
											    // Type ( text,numeric,dropdown) 
											    'type'  =>'text']],  
											
				
				
							]
				    ),
					
				    #Table Name
				    
				    'table_name'    => 'entity_child',
				    
				    #Primary Key
				    
				    'key_id'        => 'id',
				  				    
				    # Communication
								    
				    'add_button' => array( 'is_add' =>1,'page_link'=>'f=entity_child', 'b_name' => 'Add Entity child' ),
			 
				    'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?d=coach_eav', 'BACK_NAME'=>'Back'),
				    
				    'prime_index'   => 2,
				    
				    'default_fields' => array(
							    "entity_code" => "'CH'",						    
							),
				    
				    # File Include
				    'after_add_update'	=>1,
				    
			            'is_user_id' 	=>1,
				     
				    'page_code'	=> 'FECA',  
                                
			);
	     
	     
	     if(@$_GET['key']){			  
			  $F_SERIES['data']['5']['ro']=1;			  
	     }
	     
	
	     
	     
	     # after add update
	     
	     function after_add_update($key_id){
		 
			  global $rdsql;
			  global $USER_ID;
			  global $G;
		      
			  $lv = [];
			  
			  $lv['theme_child_query_data'] = [];
			  $lv['coach_child_query_data'] = [];
			  
			  $lv['domain_name']            = $_POST['X4'];
			  
			  $lv['domain_name_hash']       = md5($lv['domain_name']);
			  
			  
			     
			  if(!$_POST['UPDATE']){
				       
				       $lv['coach_path']      =get_config('coach_path');		    
				       
				       $lv['coach_cache_path']=$lv['coach_path']."/$lv[domain_name]/cache/";
				       
				       mkdir($lv['coach_path']."/$lv[domain_name]",0755,true);
				       
				       mkdir($lv['coach_cache_path'],0755,true);
				       
				       mkdir($lv['coach_path']."/$lv[domain_name]/content",0755,true);
				       
				       mkdir($lv['coach_path']."/$lv[domain_name]/images",0755,true);
				       
				       $theme_content_file = fopen($lv['coach_cache_path']."/t.html","w") or die("Unable to open file!");		
				       
				       fclose($theme_content_file);
			      
			  } // end
			     
			     
			     
			  # coach clone			     
			  if((@$_POST['X5']) && (!$_POST['UPDATE'])){
				 
				 
				       $lv['template_id']            = @$_POST['X5'];
			     
				       $lv['template_coach_domain']       = $G->get_one_column(['field'	   => "get_eav_addon_varchar(id,'CHDN')",
											  'table'	   => 'entity_child',
											  'manipulation'=> " where id=$lv[template_id]"
											  ]);
				       
				       #echo "TCD".$lv['template_coach_domain'];
				       
				       $lv['template_coach_domain_hash']      = md5($lv['template_coach_domain']);
				 
				 
				       $lv['coach_id'] = $_POST['X5'];
						       
				       # child of coach	
				       $lv['coach_child_data'] = $rdsql->exec_query("SELECT
											   id,entity_code,sn,ln,line_order,is_active
										       FROM
											   entity_child
										       WHERE
											   parent_id=(SELECT
													       id
													   FROM
													       entity_child
													   WHERE
													       id=$lv[coach_id] AND entity_code='CH')","1");
				   
				       while($coach_child=$rdsql->data_fetch_assoc($lv['coach_child_data'])){
					   
					   
						    $lv['coach_child_query'] = "('$coach_child[entity_code]'".											      
											      ",'".stripcslashes($coach_child['sn'])."'".
											      ",'".stripcslashes($coach_child['ln'])."'".
											      ",$coach_child[line_order],$key_id,$coach_child[is_active])";
						    
						    // child query
						    
						    $rdsql->exec_query("INSERT INTO
										    entity_child (entity_code,sn,ln,line_order,parent_id,is_active)
										VALUES
										    $lv[coach_child_query]","1");
						    
						    
						    $lv['cloned_coach_id'] = $rdsql->last_insert_id('entity_child');
						    
						    // coach child of child						    
						    $lv['coach_child_of_child_data'] = $rdsql->exec_query("SELECT
														    entity_code,
														    sn,
														    ln,
														    detail,
														    line_order,
														    image,
														    image_a,
														    image_b,
														    is_active											
														FROM
														    entity_child
														WHERE
														    parent_id=$coach_child[id]",1);
								    
						    
						    while($coach_child_of_child=$rdsql->data_fetch_assoc($lv['coach_child_of_child_data'])){
						    
								 // coach_child_of_child
						    
								 $lv['coach_child_of_child_query_right'] = "('$coach_child_of_child[entity_code]'".
													      ",'".$rdsql->escape_string($coach_child_of_child['sn'])."'".
													     ",'".$rdsql->escape_string($coach_child_of_child['ln'])."'".
													     ",'".$rdsql->escape_string($coach_child_of_child['detail'])."'".									    
													     ",'".$rdsql->escape_string($coach_child_of_child['image'])."'".
													     ",'".$rdsql->escape_string($coach_child_of_child['image_a'])."'".
													     ",'".$rdsql->escape_string($coach_child_of_child['image_b'])."'".
													     ",$coach_child_of_child[line_order],$lv[cloned_coach_id],$coach_child_of_child[is_active])";
							 
								 // child of child query
								 $rdsql->exec_query("INSERT INTO
											   entity_child (entity_code,
													 sn,
													 ln,
													 detail,
													 image,
													 image_a,
													 image_b,
													 line_order,
													 parent_id,
													 is_active)
											   VALUES
													$lv[coach_child_of_child_query_right]","1");			
					   
						    } // end of child of child			
					   
				       } // end of child
				       
				       # domain hash key & values				       
				       $lv['coach_domain_key_value'] = $rdsql->exec_query("SELECT
											       entity_code,entity_key,entity_value
											   FROM
											       entity_key_value
											   WHERE
											       domain_hash='$lv[template_coach_domain_hash]'","1");
				     
				       
				       // each coach
				       while($coach_domain_key_value=$rdsql->data_fetch_assoc($lv['coach_domain_key_value'])){
					     
					     $lv['coach_domain_key_value_query'].= "('$coach_domain_key_value[entity_code]'".
										      ",'".stripcslashes($lv['domain_name_hash'])."'".
										      ",'".stripcslashes($coach_domain_key_value['entity_key'])."'".
										      ",'".stripcslashes($coach_domain_key_value['entity_value'])."'".
										      ",now(),$USER_ID),";
				       }
				       
				       # query remove extra ,
				       if($lv['coach_domain_key_value_query']){
				       
						    $lv['coach_domain_key_value_query']=substr($lv['coach_domain_key_value_query'],0,-1);
				       }
				       
				       # has query
				       if($lv['coach_domain_key_value_query']){
						    
						    // child of child query
						    $rdsql->exec_query("INSERT INTO
									      entity_key_value (
											    entity_code,
											    domain_hash,
											    entity_key,
											    entity_value,
											    creation,
											    user_id)
									      VALUES
											   $lv[coach_domain_key_value_query]","1");			
		 
				       }
			     
			  } // end of add circuit
		  
	     } // end of after add update
?>
    