<?PHP

    $LAYOUT	    = 'layout_full';
    
    $location =   "terminal/".@$_GET['domain'].'/'.$_GET['location'].'/';
    
    $image_name = $_GET['image_name'];
    
    $token      = @$_GET['token'];
    
    $image_size =  array('favicon' => array('w' =>32, 'h' =>32),
			 'logo' => array('w' =>270, 'h' =>99),
			 'bg' => array('w' =>335, 'h' =>338)
			 );
    
    
    $temp_image =  @$image_size[$token];
                            
    $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Image',
				
				#Table field
                    
				'data'	=>   array(
						   
						    
						   '1' =>array('field_name'=>'Image',
                                                               
                                                               'field_id'=>'image',
                                                               
                                                               'type'=>'file',
							       
							       'upload_type' => 'image',
							       
							       'location' => @$location,
							       'image_size'=>array($image_size[$token]['w'] =>$image_size[$token]['h']),
                                                               'image_name' =>$image_name,
                                                               'is_mandatory'=>0,
                                                               'save_ext_type'=>'png',
							       'save_file_name'=>$token,							       
                                                               'input_html'=>'class="w_400"',							       
							       'hint'=>'Image Size:'.$temp_image['w'].'x'.$temp_image['h']
                                                               
                                                               ),
						   
						   
						    '2' =>array( 'field_name'=> 'Domain',
                                                               
                                                               'field_id' => 'parent_id',
                                                               
                                                               'type' => 'option',
                                                               
                                                               'option_data'=>$G->option_builder('entity_child','id,sn'," WHERE entity_code='CH' ORDER BY sn,line_order ASC "),
                                                               
                                                               'is_mandatory'=>1,
                                                                           
							),
						   
						   
						  ),
                                    
				#Table Name
				
				'table_name'    => 'entity_child',
				
				#Primary Key
                                
			        'key_id'        => 'id',
				
				# Default Additional Column
                                'deafult_value'    => array('entity_code' => "'MP'", 'code' => "'PIMG'"   ),
				'is_user_id'       => 'created_by',
								
				# Communication
								
				'back_to'  => array( 'is_back_button' =>0, 'back_link'=>'?d=plug_slider_bg', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 1,
                                
				# File Include
                                //'after_add_update'	=>1,
				
				'page_code'	=> 'FMPS',
				
				'show_query'    =>0,
                                
			);
?>