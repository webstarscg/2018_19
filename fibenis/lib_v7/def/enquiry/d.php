<?PHP

               
        $D_SERIES       =   array(
                                   'title'=>'Enquiry',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
                                    
                                    #table data
                                    
                                    'data'=> array(
                                                        1=>array('th'=>'Name',
								 
								'field'=>'name as v1',
								     
								'html'      =>  'style	= "cursor:pointer" onclick="JavaScript:E_V_PASS(\'sort_field\',1);E_V_PASS(\'sort_direction\',GET_E_VALUE(\'sort_col_1\'));filter_data();"',
								  
								'font'      =>  'class="sort"',
																
								'span'      =>  '<span id="sort_icon_1" name="sort_icon_1"></span>',
									
								'td_attr' => ' width="15%" ',
                                                                            
								),
							
							
							2=>array('th'=>'Phone No.',
								 
								'field'=>'phone_no as v2',
									
								'td_attr' => ' width="12%" ',
                                                                            
								), 
													
                                                        3=>array('th'=>'Email Id',
									 
								'field'=>'email_id as v3',
                                                                   
								'html'      =>  'style	= "cursor:pointer" onclick="JavaScript:E_V_PASS(\'sort_field\',2);E_V_PASS(\'sort_direction\',GET_E_VALUE(\'sort_col_2\'));filter_data();"',
								  
								'font'      =>  'class="sort"',
																
								'span'      =>  '<span id="sort_icon_2" name="sort_icon_2"></span>',
									 
								'td_attr' => ' class="label_child align_LM" width="15%"',
									 
								),
								
							4=>array('th'	=> 'Enquiry',
									      
								'field'	=> 'enquiry_code as v4',
									
								'html'      =>  'style	= "cursor:pointer" onclick="JavaScript:E_V_PASS(\'sort_field\',3);E_V_PASS(\'sort_direction\',GET_E_VALUE(\'sort_col_3\'));filter_data();"',
								  
								'font'      =>  'class="sort"',
																
								'span'      =>  '<span id="sort_icon_3" name="sort_icon_3"></span>',
									      
								'td_attr' => ' class="align_LM" width="10%" '
									      
								),
							
							5=>array('th'=>'Enquiry On',
							                                       
								'field'=>'(SELECT sn FROM entity_attribute WHERE code=enquiry_code) as v5',
							                                       
								'input_html'  => ' class=" w_50" '
							                                       
								),
							
							6=>array('th'	=> 'Query',
									      
								'field'	=> 'query as v6',
									
								'html'      =>  'style	= "cursor:pointer" onclick="JavaScript:E_V_PASS(\'sort_field\',4);E_V_PASS(\'sort_direction\',GET_E_VALUE(\'sort_col_4\'));filter_data();"',
								  
								'font'      =>  'class="sort"',
																
								'span'      =>  '<span id="sort_icon_4" name="sort_icon_4"></span>',
									      
								'td_attr' => ' class="label_father" ',
									      
								'td_attr' => ' class="align_LM" width="240%" '
									      
								),
							
							
                                                    ),
				    
                                    
                                    #Sort Info
                                      
                                       'sort_field' =>array('name',
							    
							    'enquiry_code'),
                                        
                                       'action' => array('is_action'=>0, 'is_edit' =>1, 'is_view' =>0 ),
                                       
                                       'order_by'   =>'ORDER BY id ASC' ,
				       		
                                
                                    #Table Info
                                    
                                    'table_name' =>'enquiry',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    //'is_user_id'       => 'user_id',
                                
                                    # Communication
                                
                                    'prime_index'   => 1,
                                
                                    # File Include
                                
                                    //'js'            => 'm_code',
                                    
                                    #create search field
									
//                                        'search_text' => array(
//							       
//							       
//							       1=>array('get_search_text'  => get_search_array('enquiry',
//														'name as ST1,phone_no as ST2',
//														'email_id','enquiry_code','query'))
//								
//								//2=>array('get_search_text'  => get_search_array('entity_attribute','id as ST1,sn as ST2','First Name',2,1,''))								
//                                                        ),
//						
				
				#Search filter 
				
				//'search_field' => '(SELECT sn FROM entity WHERE code=entity_code)',
				//
				//'search_id' 	=> array('(SELECT sn FROM entity WHERE code=entity_code)'),
				//
				
				#check_field
								
					//'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>1,'page_link'=>'f=entity_attribute', 'b_name' => 'Add Enquiry' ),
								
					'del_permission' => array('able_del'=>1,'user_flage'=>1), 
								
					'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  'timestamp'),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'DEQY'
                            
                            );
    
?>