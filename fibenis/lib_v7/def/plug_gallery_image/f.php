<?PHP

    $LAYOUT	    = 'layout_full';
    
    $select_content_query = "SELECT sn FROM entity_child WHERE id=$_GET[gall_id] AND entity_code='GL'";
                                        
    $select_content_query_exec = $rdsql->exec_query($select_content_query,'error');
            
    $get_gallery = $rdsql->data_fetch_object($select_content_query_exec);
	 
                            
    $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Gallery Image',
				
				#Table field
                    
				'data'	=>   array(
				    
						 /*'4' =>array( 'field_name'=> 'Gallery',
                                                               
                                                               'field_id' => 'parent_id',
                                                               
                                                               'type' => 'text',
								
							       //'option_data'=>$G->option_builder('entity_child','id,sn',' WHERE entity_code="GL" AND parent_id=0 AND is_active=1  ORDER by sn ASC','show_query=1'),
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_200" value="'.$get_gallery->sn.'" disabled="disabled"',
							       	
                                                               
                                                               ),*/				
						   '1' =>array( 'field_name'=> 'Title',
                                                               
                                                               'field_id' => 'sn',
                                                               
                                                               'type' => 'text',
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_200"',
							       'filter_in'	=> function($data_in){
													   return preg_replace(array('/\s{2,}/i'),
															       array(''),																															        
															       $data_in);
										       }	
                                                               
                                                               ),
						   
						   '2' =>array('field_name'	=>'Image',
                                                               
                                                               'field_id'	=>'image',
                                                               
                                                               'type'		=>'file',

							       'upload_type'    => 'image',      

							       'location' 	=> 'images/gallery/',
							       
							       
							       'save_ext_type'   => 'png',
							       
							       'image_size'	=> array(280=>175,
											 500=>300,
											 840=>525),                                                              
                                                              
                                                               'is_mandatory'	=> 1,
                                                               
                                                               'input_html'=>'class="w_400"'                                                               
                                                        ),
						    
						    
				),
                                    
				#Table Name
				
				'table_name'    => 'entity_child',
				
				#Primary Key
                                
			        'key_id'        => 'id',
				
				'deafult_value'    => array('entity_code' => "'GL'", 'is_active' => "1", 'parent_id'=>@$_GET['gall_id']),
				
				# Default Additional Column
                                
				'is_user_id'       => 'created_by',
								
				# Communication
								
				'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?d=plug_gallery_image&menu=off&mode=simple&gall_id='.$_GET['gall_id'], 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 1,
                                
				# File Include
                                //'after_add_update'	=>1,
				
				'page_code'	=> 'FMET',
				'show_query'    =>0,
                                
			);
    
    
?>