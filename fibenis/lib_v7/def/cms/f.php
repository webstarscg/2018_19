<?PHP
   
   if(@$_GET['key']){
      $WHERE_FILTER = " WHERE entity_code='PG' AND is_active=1 AND id NOT IN($_GET[key])";
   }else{    
      $WHERE_FILTER = " WHERE entity_code='PG' AND is_active=1";
   }
   
    $status_option = '<option value="2">Active</option><option value="1">In-Active</option>';
   
   
    //F_series definition:
                            
    $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Page',
				
				#Table field
                    
				'data'	=>   array(
						    
						   '14' =>array( 'field_name'=> 'Root',
                                                               
                                                               'field_id' => 'parent_id',
                                                               
                                                               'type' => 'option',
                                                               
                                                               'option_data'=>$G->option_builder('entity_child','id,IF((parent_id>0),concat((SELECT sn FROM entity_child as ec WHERE ec.id=entity_child.parent_id),\' -> \',sn),sn ) as sn',$WHERE_FILTER.' ORDER BY sn,line_order ASC'),
                                                               
                                                               'is_mandatory'=>0,
                                                               
                                                               'input_html'=>'class="w_250"  onchange="JavaScript:root_count();"'
                                                               
                                                               ),
						     
				    
						   '1' =>array( 'field_name'=> 'Entity',
                                                               
                                                               'field_id' => 'entity_code',
                                                               
                                                               'type' => 'option',
                                                               
                                                               'option_data'=>$G->option_builder('entity','code,sn'," WHERE code='PG' OR code='BL'  ORDER by sn ASC"),
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_100"'
                                                               
                                                               ),
						   
						    '10' =>array('field_name'=>'Date',
                                                               
                                                               'field_id'=>'date',
                                                               
                                                               'type'=>'date',
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_200"'                                                               
                                                               ),
						    
						     '11' =>array('field_name'=>'Date',
                                                               
                                                               'field_id'=>'date_II',
                                                               
                                                               'type'=>'date',
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_200"'                                                               
                                                               ),
						   
						   '8' =>array('field_name'=>'Page Code',
                                                               
                                                               'field_id'=>'code',
                                                               
                                                               'type'=>'text',
                                                               
                                                               'is_mandatory'=>1,
                                                              
                                                               'input_html'=>'class="w_200" maxlength="75"  onkeypress ="return PR_All_Alpha_Numeric(event,\'-_.\');"'                                                               
                                                               ),
						   
						    '6' =>array('field_name'=>'Menu Name',
                                                               
                                                               'field_id'=>'sn',
                                                               
                                                               'type'=>'text',
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_200"  maxlength="64"  onkeypress="return PR_All_Alpha_Numeric(event,\' -_.\');"'
                                                               
                                                               ),
				   
						   '2' =>array('field_name'=>'Page Title',
                                                               
                                                               'field_id'=>'ln',
                                                               
                                                               'type'=>'text',
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_350" maxlength="120" onkeypress = "return PR_All_Alpha_Numeric(event,\' -_.\');"'
                                                               
                                                               ),
						   
						    '15' =>array('field_name'=>'Page KeyWord',
                                                               
                                                               'field_id'=>'ea_value',
                                                               
                                                               'type'=>'textarea',
							       
							       'child_table'         => 'eav_addon_varchar', // child table 
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       'child_attr_code'     => 'PGKW',           // attribute code
                                                               
                                                               'is_mandatory'=>0,
                                                               
                                                               'input_html'=>' class="w_350" maxlength="120" onkeypress = "return PR_All_Alpha_Numeric(event,\' -_.\');"'
                                                               
                                                               ),
						   '16' =>array('field_name'=>'Page Description',
                                                               
                                                               'field_id'=>'ea_value',
                                                               
                                                               'type'=>'textarea',
							       
							       'child_table'         => 'eav_addon_varchar', // child table 
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       'child_attr_code'     => 'PGPD',           // attribute code
                                                               
                                                               'is_mandatory'=>0,
                                                               
                                                               'input_html'=>' class="w_350" maxlength="120" onkeypress = "return PR_All_Alpha_Numeric(event,\' -_.\');"'
                                                               
                                                               ),
						   '5' =>array('field_name'=>'Page Content',
                                                               
                                                               'field_id'=>'detail',
                                                               
                                                               'type'=>'textarea_editor',
							       #'type'=>'textarea',
							       
							       'editor'=>array('height'=>300),
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               //'input_html'=>' style="width:600px;font-weight:normal !important;" rows=10 ',
							       
							       //'filter_out'=>function($data_out){ return htmlspecialchars($data_out);}
                                                               
                                                               ),
						   
						   
						   
						     
						   
						   
						   
												
						   '3' => array( 'field_name'=> 'Status', 
                                                               
                                                                'field_id' => 'is_active',
                                                               
                                                                'type' => 'option',
                                                                
                                                                'option_data'=> $status_option,//$G->option_builder('entity_attribute','code,sn',' WHERE entity_code="PP"  ORDER BY sn ASC'),
								
								'filter_in'=>function($data_in){
								  
									return ($data_in-1);
								  
								},
								
								
								'filter_out'=>function($data_in){
								  
									return ($data_in+1);
								  
								},
								
								'avoid_default_option'=>1,
                                                               
                                                                'is_mandatory'=>1,
                                                                
                                                                'input_html'=>' class="W_150"'
                                                                
                                                                ),
						   
                                                   
                                                   '4' => array( 'field_name'=> 'Line Order', 
                                                               
                                                                'field_id' => 'line_order',
                                                               
                                                                'type' => 'text',
                                                                
                                                                'is_mandatory'=>1,
                                                                
                                                                'input_html'=>' class="w_50" ',
								
								
							),
						   
						   
						   
						    '9' => array('is_plugin' =>1,
								     'type' => 'handsontable',
								     'field_name' => 'Addon',
								     'field_id' => 'addon',								     
								     'is_mandatory'=>0,
								     
								     'default_rows_prop'=>array('start_rows'=>'1',
											           'min_spare_rows'=>'1',
											           'max_rows'=>'1'),
												   
								     'colHeaders' =>array(
											   array('column'=>'CSS',   'width'=>'150', 'type'=>'dropDown' ,'source' => ' "glyphicon glyphicon-book", "glyphicon glyphicon-star", "glyphicon glyphicon-grain" ' ),
											   array('column'=>'Layout','width'=>'150','type'=>'dropDown', 'source' => ' "default", "layout_right"'),
										    ),
								     
								     		 	   
													   
								
								      ),
						    
						     '13' => array( 'field_name'=> 'Event Image', 
                                                               
                                                                'field_id' => 'image',
                                                               
                                                                'type' => 'file',
                                                                
                                                                'location' => 'images/gallery/',
                                                                'image_size'=>array(400 =>640, 300=> 480, 100 =>160),
                                                                'is_mandatory'=>0,
                                                                
                                                                'input_html'=>'class="W_150"'
                                                                
                                                                ),
						   
						   
				    
                                ),
                                    
				#Table Name
				
				'table_name'    => 'entity_child',
				
				#Primary Key
                                
			        'key_id'        => 'id',
				
				
				'after_add_update'=>1,
				
				
				
				
                                
				# Default Additional Column
                                
				'is_user_id'       => 'created_by',
				
				'deafult_value'    => array('entity_code' =>" 'PG' "),
								
				# Communication
								
				'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?d=cms_page', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 1,
                                
				# File Include
                                //'after_add_update'	=>1,
				
				'js'            => array('is_top'=>1,'top_js'=>'js/f_series/manage_cms'),
				
				
				'show_query'    =>0,
                                
			);
    
          
?>