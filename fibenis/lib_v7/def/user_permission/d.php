<?PHP
//            define("PAGE_CODE", "MANAGE_USER");
//		
//	    define('PAGE_PERM', $_SESSION['MANAGE_USER']);
    
            $D_SERIES       =   array(
			              #Title:
			              
                                      'title'=>'User Permission',

                                      # query display depend on the user

                                      'is_user_base_query'=>0,
                                    
                                      #table data
                                    
                                       'data'=> array(
                                                            1=>array('th'=>'Name', 'field'=>'sn as v1',
                                                                            
                                                                    'html' => 'style	= "cursor:pointer" onclick="JavaScript:E_V_PASS(\'sort_field\',1);E_V_PASS(\'sort_direction\',GET_E_VALUE(\'sort_col_1\'));filter_data();"',
								  
								    'font'=>  'class="sort"',
																
								    'span'=>'<span id="sort_icon_1" name="sort_icon_1"></span>'),
                                                            
                                                            2=>array('th'=>'Code','field'=>'code as v2',
								    
                                                                    'html' => 'style	= "cursor:pointer" onclick="JavaScript:E_V_PASS(\'sort_field\',2);E_V_PASS(\'sort_direction\',GET_E_VALUE(\'sort_col_2\'));filter_data();"',
								  
								    'font'=>  'class="sort"',
																
								    'span'=>'<span id="sort_icon_2" name="sort_icon_2"></span>',
								    
								    'td_attr'=>' class="b" '
								    
								    ),
							    
							    
						),
				       
				                                           
                                    #Table Info
                                    
                                    'table_name' =>'user_permission',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    //'is_user_id'       => 'user_id',
                                
                                    
                                    #Search:
                                    
                                    'search_text' => array(
								
								1=>array('get_search_text'  => get_search_array('user_permission','id as ST1,sn as ST2','User Role',1,0,'')),
                                                                
                                                              ),
						
				    #Search filter 
				
				    'search_field' => 'sn',
				
			            'search_id'    => array('id'),
				   
				    #check_field:
				   
				    'check_field'   =>  array('id' => @$_GET['id']),
				   
				    #date filter:
				   
				    'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>'timestamp_punch'),
				
				    #Sort Info
                                      
                                    'sort_field' =>array('sn','code'),
				    
				    #Default sorting:
				    
				    'order_by'   =>'ORDER BY sn ASC',
				    
				    #Action:
                                        
                                    'action' => array('is_action'=>1, 'is_edit' => 1, 'is_view' =>0),
                                       
                                    #Add:		
				    'add_button' => array( 'is_add' =>1,'page_link'=>'f=user_permission', 'b_name' => 'Add User Permission' ),
				    
				    #Del			
				    'del_permission' => array('able_del'=>1,'user_flage'=>1), 
								
			            # Communication   
                                
                                    'prime_index'   => 1,
                                
                                    # File Include
                                
								
				    #export data
				    'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				    'page_code'    => 'DUSP'
                            
                            );