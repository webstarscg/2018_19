<?PHP

    define('PAGE_PERM', $_SESSION['MANAGE_SETUP']);
    
    #F Series Definition:
    
    $F_SERIES=array(
		    #Title
		    
		    'title'=>'User Permission',
                    
		     #Table data:
		     
                     'data'=>array(

                                    '2' => array( 'field_name'=> 'Code',  'field_id' => 'code', 'type' => 'text','is_mandatory'=>1, 'allow' => 'w255',),

				    '1' => array( 
									  		
						'field_name'=> 'First Name',  'field_id' => 'sn', 'type' => 'text','is_mandatory'=>1, 'allow' => 'w255',
											 
						//'validate' => 'data_validate(\'user_role\',this)'
											    
						 ),
											
				    
				 ),
					
			
		     #Table name:
		     
                     'table_name'    => 'user_permission',
                      
		     #Primary Key:
		     
                     'key_id'        => 'id',
                                
                    # Default Additional Column
                                
                    'is_user_id'       => 'user_id',
								
                    # Communication
								
		    'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?d=user_permission', 'BACK_NAME'=>'Back'),
                    
		    #Message:
		    
                    'prime_index'   => 1,
                                
                    # File Include
                                
                    
		    #page_code
		    
		    'page_code'	=> 'FUSP'
                    
                    );
?>