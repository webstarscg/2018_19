<?PHP

    $F_SERIES=array( 'title'=>'Entity',
                    
                     'data'=>array('1' => array( 'field_name'=> 'Page Family',
						
						'field_id' => 'page_family',
						
						'type' => 'option',
						
						'option_data'=>$G->enum_option_builder('page_info','page_family'),
						
						'is_mandatory'=>1
						
						),
				   
				   '2' => array( 'field_name'=> 'Page Code',
						
						'field_id' => 'code',
						
						'type' => 'text',
						
						'is_mandatory'=>1
						
						),
				   
				   '3' => array( 'field_name'=> 'Page Name',
						
						'field_id' => 'sn',
						
						'type' => 'text',
						
						'is_mandatory'=>1
						
						),
				   
				   '4' => array( 'field_name'=> 'Path',
						
						'field_id' => 'path',
						
						'type' => 'text',
						
						'is_mandatory'=>0
						
						),
				    ),
		     
                    'table_name'    => 'page_info',
                                
                    'key_id'        => 'id',
                                
                    
		    # Default Additional Column
                                
                    //'is_user_id'       => 'user_id',
								
                    
		    # Communication			
		    
		    'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?d=page_info', 'BACK_NAME'=>'Back'),
                                
                    'prime_index'   => 1,
                                
                    
		    'page_code'	=> 'FPGI'
                                
                    
                    
                    );
?>