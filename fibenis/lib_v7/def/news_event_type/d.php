<?PHP
        $LAYOUT	    = 'layout_full';
               
        $D_SERIES       =   array(
                                   'title'=>'',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
                                    
                                    #table data
                                    
                                    'data'=> array(
					                
							3=>array('th'=>'Event Type',
									 
								'field'=>'sn as v3',
                                                                   
									 
								'td_attr' => " class='label_father align_LM' width='51%'",
									
									
								 
								),
							
							2=>array('th'=>'Status',
									 
								'field'=>"CONCAT(id,'[C]',is_active) as v2",
								
								'filter_out'=>function($data_in){
										
											$temp     = explode('[C]',$data_in);
											
											$flag     = [1,0];
											
											$data_out = array(
													  'data'=>array('id'   => $temp[0],
															'key'  => md5($temp[0]),
															'label'=> 'Page',
															'cv'   => $temp[1],
															'fv'   => $flag[$temp[1]],
															'action'=>'entity_child',
															'series' => 'a',
															'token' =>'ECAI'
															)
													);
											
											return json_encode($data_out);
										},
                                                                   
								/*'html'      	=>  'style	= "cursor:pointer" onclick="JavaScript:E_V_PASS(\'sort_field\',2);E_V_PASS(\'sort_direction\',GET_E_VALUE(\'sort_col_2\'));filter_data();"',
								  
								'font'      	=>  'class="sort"',*/
																
								'span'      	=>  '<span id="sort_icon_2" name="sort_icon_2"></span>',
									 
								'td_attr' 	=> ' class="label_child align_LM  txt_case_upper b" style="padding-right:10px;" width="3%"',
								
								'js_call'	=> 'd_series.inline_on_off',
									 
								),
					
								
                                                    ),
				    
                                    
                                    #Sort Info
                                      
                                       'sort_field' =>array('code',
							    
							    'sn'),
                                        
                                       'action' => array('is_action'=>0, 'is_edit' =>1, 'is_view' =>0 ),
                                       
                                       'order_by'   =>'ORDER BY id ASC ' ,
				       		
                                
                                    #Table Info
                                    
                                    'table_name' =>'entity_child',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
                                
                                    # Communication
                                
                                    'prime_index'   => 1,
                                
                                    # File Include
                                
                                  'js'            => array('is_top'=>1, 'top_js' => 'js/d_series/manage_cms'),
                                    
                                    #create search field
									
                                        'search_text' => array(
								
																						
								1=>array('get_search_text'  => get_search_array('entity_child','id as ST1,sn as ST2','Title',1,0,' WHERE entity_code="ET" AND parent_id=0')),
								
								//2=>array('get_search_text'  => get_search_array('entity','id as ST1,code as ST2','Code',2,1)), 
								
                                                              ),
						
				
				#Search filter 
				
				'search_field' => 'sn',
				//
				'search_id' 	=> array('id'),
				
				'is_narrow_down'=>1,
				'key_filter'   => " AND entity_code = 'ET' AND parent_id=0", 
				#summary:
				
				/*'summary_data'=>array(
							array(  'name'=>'No Data','field'=>'count(id)','html'=>'class=summary'),
				
				                   ),*/
				
				
				'custom_filter' => array(
							
						/*array(  'field_name' => 'Category',
						      
						      'field_id' => 'cat', 'filter_type' =>'option_list', 
											
							'option_value'=> $G->option_builder('entity','code,sn',' WHERE code="PG" OR code="BL"  ORDER by sn ASC'),
						
							//'default_option'=>$G->get_entity_value('MP','fy_id'),
							
							'input_html'=>' class="w_100" ',
							
							'cus_default_label'=>'Show All',
						
							'filter_by'  => 'entity_code'
						),*/
				),
				
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>1,'page_link'=>'f=news_event_type', 'b_name' => 'Add Event Type' ),
								
					'del_permission' => array('able_del'=>1,'user_flage'=>1), 
								
					'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  ''),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'DMET'
                            
                            );
    
?>