/////////////////////////// TIP ELEMENT ////////////////////////////////////////////

	var HELP_DOCUMENT=new Array();
    	
	HELP_DOCUMENT['help']='Check';

	var TIP_HIDE_ID;
	
	var TIP_SHOW_ID;
	
	var TOOL_TIP	= new Array();

	// pre_run();	
	
	var TIP_ELEMENT=document.getElementById('tool_tip');

	
	// pre run
	
	function pre_run(){
		
			var div_element=document.createElement("div");
			
			div_element.id="tool_tip";
			
			div_element.name="tool_tip";
			
			div_element.className="tool_tip";
						
			div_element.onmouseover=show_tip_element;
			
			div_element.onmouseout=hide_tip_element;
			
			document.body.appendChild(div_element);
			
	}
	
   
   // show Tip
   
   function show_tip(help_name,e,width,height){
	   
		document.getElementById('tool_tip').style.visibility='visible';
		
		div_tip_build(document.getElementById('tool_tip'),help_name,e,width,height);
   }
 
	
   // hide tip	
	
   function hide_tip_element(element){
	   
		 TIP_HIDE_ID=0;
		 
		 TIP_ELEMENT=element;
		 
		 TIP_SHOW_ID=setInterval('check_tip_status()',125);
	}
		
    
	// show tip	element
		
	function show_tip_element(element,e){
		
	   TIP_HIDE_ID=1;
	   
	   fix_position(element,e);
	   
	   TOOL_TIP['ELEMENT'].style.visibility='visible';
	}
	
	
	// checking tip status
	
	function check_tip_status(){
		
		 if(TIP_HIDE_ID==0){  		 
				document.getElementById('tool_tip').style.visibility='hidden';
				TIP_HIDE_ID=1;
				clearInterval(TIP_SHOW_ID);
		  } 
	}
	 
	 
	// hide tip
	 	 
	function hide_tip(){
		
			hide_tip_element(document.getElementById('tool_tip'));
	}   
	
	
    // tip off
	
	function tip_off(){
		
		  		document.getElementById('tool_tip').style.visibility='hidden';
				TIP_HIDE_ID=1;
		   		clearInterval(TIP_SHOW_ID);
	}

	
	// building the div	

	function div_tip_build(element,help,e,width,height)	{
       
		  // alert('...........'+e);
		  
		  
		  TOOL_TIP['ELEMENT']	= element; 
		  
		  element.innerHTML	= help;
		  
		  if(HELP_DOCUMENT[help]){
			  
			    element.innerHTML+='<br>'+help;
		  }
		  
		  if(width){ element.style.width=width+'px'; }
		  if(height){ element.style.height=height+'px';}
		  
	  	  fix_position(element,e);		  
	}	  
	
	
	// fixing the position
	
	function fix_position(element,e){
		
		//document.getElementById('temp').value=window.pageXOffset+'..'+document.documentElement.scrollLeft;
		
	        var x=((document.all)? event.clientX:e.clientX)+document.documentElement.scrollLeft;
	    
		var y=((document.all)? event.clientY:e.clientY)+document.documentElement.scrollTop;
		
		var temp_x=((x+element.offsetWidth)>document.body.offsetWidth)?(x-element.offsetWidth)+10:x+10;
		
		var temp_y=((y+element.offsetHeight)>document.body.offsetHeight)?(y-element.offsetHeight):y+10;
		
		element.style.left=temp_x+'px';
		
		element.style.top=temp_y+'px';
		
	}

