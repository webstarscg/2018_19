// JavaScript Document


	function WS_SCREEN(sub_content){						// Start the function.		
		
		this.sub_content	= sub_content;	
		this.screen_width	= 0;
		this.screen_height	= 0;
		
	
		this.calculate_screen_size= function(){	
		var viewportwidth;
		var viewportheight;
		 
		// the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
				 
		if (typeof window.innerWidth != 'undefined') {
			
			viewportwidth = window.innerWidth,
			viewportheight = window.innerHeight
		}
			 
		// IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
				
		else if (typeof document.documentElement != 'undefined'
				 && typeof document.documentElement.clientWidth !=
				 'undefined' && document.documentElement.clientWidth != 0) {
			
			viewportwidth = document.documentElement.clientWidth,
			viewportheight = document.documentElement.clientHeight
		}
			 
		// older versions of IE
				 
		else {
			
			viewportwidth = document.getElementsByTagName('body')[0].clientWidth,
			viewportheight = document.getElementsByTagName('body')[0].clientHeight
		}
			
			this.screen_width=viewportwidth;
			this.screen_height= viewportheight;				
				   
		}  // end of calculate 
			   
			this.calculate_screen_size();	

	}

	
	WS_SCREEN.prototype.create_visible = function() {		
	
		var wleft;	 	
		var wtop;		
	
		//var temp_height=(document.body.scrollHeight>this.screen_height)?document.body.scrollHeight:this.screen_height;
		//var temp_width=(document.body.scrollHeight>this.screen_width)?document.body.scrollHeight:this.screen_width;				
		
		var MW_Height =Number(this.screen_height); // screen.availHeight;
		
		var MW_Width =Number(this.screen_width);
		
		wtop = Math.floor((MW_Height-Number(document.getElementById(this.sub_content).clientHeight)) / 2);
		
		wleft =Math.floor((MW_Width-Number(document.getElementById(this.sub_content).clientWidth)) /2 ); 
		
	
	
	
		document.getElementById(this.sub_content).style.left = wleft+'px';
		//document.getElementById(this.sub_content).style.top =wtop+'px';	
		
		document.getElementById(this.sub_content).style.top =wtop+'px'; //'75px';						// 75px gave default have to change this.
		
		
		document.getElementById(this.sub_content).style.visibility='visible';

			
	}
	
	
