CREATE TABLE eav_addon_varchar

-----------------------------------------------------------------------------------------------------------------------------
--- eav_addon_vc128uniq

CREATE TABLE eav_addon_vc128uniq(
  id int(11) NOT NULL,
  parent_id int(11) NOT NULL,
  ea_code varchar(4) NOT NULL,
  ea_value varchar(128) NOT NULL,
  ea_value_hash varchar(32) NOT NULL,
  user_id int(11) NOT NULL,
  timestamp_punch timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER $$
CREATE TRIGGER trg_eav_addon_vc128uniq_before_ins BEFORE INSERT ON eav_addon_vc128uniq FOR EACH ROW BEGIN

    IF(LENGTH(new.ea_value)=0) THEN
        
            SET new.ea_value = new.id;
    END IF;        
            SET new.ea_value_hash=md5(concat(new.ea_code,'[C]',new.ea_value));
END
$$

CREATE TRIGGER trg_eav_addon_vc128uniq_before_upd BEFORE UPDATE ON eav_addon_vc128uniq FOR EACH ROW BEGIN

    IF(LENGTH(new.ea_value)=0) THEN
        
            SET new.ea_value = new.id;
    END IF;        
            SET new.ea_value_hash=md5(concat(new.ea_code,'[C]',new.ea_value));
    
END
$$


ALTER TABLE eav_addon_vc128uniq
ADD FOREIGN KEY (parent_id) REFERENCES entity_child(id) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE eav_addon_vc128uniq
ADD FOREIGN KEY (e_code) REFERENCES entity_attribute(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

CREATE FUNCTION get_eav_addon_vc128uniq(temp_ec_id INTEGER, temp_code CHAR(4)) RETURNS varchar(128) CHARSET utf8
BEGIN
    RETURN IFNULL((SELECT ea_value FROM eav_addon_vc128uniq WHERE  parent_id=temp_ec_id AND ea_code=temp_code),NULL);
END$$

--- eav_addon_num



ALTER TABLE eav_addon_num
ADD FOREIGN KEY (parent_id) REFERENCES entity_child(id) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE eav_addon_num
ADD FOREIGN KEY (ea_code) REFERENCES entity_attribute(code) ON DELETE RESTRICT ON UPDATE RESTRICT;


ALTER TABLE eav_addon_vc128uniq
ADD FOREIGN KEY (parent_id) REFERENCES entity_child(id) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE eav_addon_vc128uniq
ADD FOREIGN KEY (ea_code) REFERENCES entity_attribute(code) ON DELETE RESTRICT ON UPDATE RESTRICT;
-----------------------------------------------------------------------------------------------------------------------------

CREATE TRIGGER trg_entity_child_base_before_ins BEFORE INSERT ON entity_child_base FOR EACH ROW BEGIN

    IF(LENGTH(new.token)=0) THEN
        
            SET new.token = new.id;
    END IF; 
END
$$

CREATE TRIGGER trg_entity_child_base_before_upd BEFORE UPDATE ON entity_child_base FOR EACH ROW BEGIN

    IF(LENGTH(new.token)=0) THEN        
            SET new.token = new.id;
    END IF; 
    
END
$$

---- eav_addon_num

CREATE TABLE eav_addon_num (
  id int(11) NOT NULL,
  parent_id int(11) NOT NULL,
  ea_code varchar(32) NOT NULL,
  ea_value int(11) NOT NULL,
  user_id int(11) NOT NULL,
  timestamp_punch timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

---- Base Version
INSERT  INTO
                entity_attribute (id, entity_code, code, sn, ln, line_order, creation, user_id, timestamp_punch)
        VALUES
                (NULL, 'EC', 'ECSN', 'Short Name', 'Short Name', '0', CURRENT_TIMESTAMP, '8', CURRENT_TIMESTAMP),
                (NULL, 'EC', 'ECLN', 'Long Name', 'Long Name', '0', CURRENT_TIMESTAMP, '8', CURRENT_TIMESTAMP),
                (NULL, 'EC', 'ECDT', 'Detail', 'Detail', '0', CURRENT_TIMESTAMP, '8', CURRENT_TIMESTAMP),
                (NULL, 'EC', 'ECPR', 'Parent Id', 'Parent Id', '0', CURRENT_TIMESTAMP, '8', CURRENT_TIMESTAMP),
                (NULL, 'EC', 'ECCD', 'Code', 'Code', '0', CURRENT_TIMESTAMP, '8', CURRENT_TIMESTAMP),
                (NULL, 'EC', 'ECIA', 'Image A', 'Image A', '0', CURRENT_TIMESTAMP, '8', CURRENT_TIMESTAMP),
                (NULL, 'EC', 'ECIB', 'Image B', 'Image B', '0', CURRENT_TIMESTAMP, '8', CURRENT_TIMESTAMP),
                (NULL, 'EC', 'ECIC', 'Image C', 'Image C', '0', CURRENT_TIMESTAMP, '8', CURRENT_TIMESTAMP);
                

---- 30SEP2018
---- Mysql
---- Page Section
---- Inlcluded for Contact Map Attrobute
---- Contact map Entity 'CM'

INSERT INTO entity (code, sn, ln, creation, user_id, timestamp_punch)
        VALUES ('CM','Contact with Google Map','Contact with Google Map',CURRENT_TIMESTAMP,
               (SELECT user_info.id FROM user_info WHERE user_role_id=(SELECT user_role.id FROM user_role WHERE sn='SAD') LIMIT 0,1),
                CURRENT_TIMESTAMP);

---- 30SEP2018
---- Pgsql
---- Page Section
---- Inlcluded for Contact Map Attrobute
---- Contact map Entity 'CM'

INSERT INTO entity (code, sn, ln, creation, user_id, timestamp_punch)
        VALUES ('CM','Contact with Google Map','Contact with Google Map',CURRENT_TIMESTAMP,
               (SELECT user_info.id FROM user_info WHERE user_role_id=(SELECT user_role.id FROM user_role WHERE sn='SAD') LIMIT 0 OFFSET 1),
                CURRENT_TIMESTAMP);
                
                
                
---- 30SEP2018
---- Mysql
---- Page Section
---- Inlcluded for Contact Map Attrobute
INSERT  INTO entity_attribute (entity_code, code, sn, ln, line_order, creation, user_id, timestamp_punch)
        VALUES ('CO','COGM','Contact Google Map','Contact Google Map', '0', CURRENT_TIMESTAMP,
               (SELECT user_info.id FROM user_info WHERE user_role_id=(SELECT user_role.id FROM user_role WHERE sn='SAD') LIMIT 0,1),
                CURRENT_TIMESTAMP);
                

---- 30SEP2018
---- Pgsql
---- Page Section
---- Inlcluded for Contact Map Attrobute
INSERT  INTO entity_attribute (entity_code, code, sn, ln, line_order, creation, user_id, timestamp_punch)
        VALUES ('CO','COGM','Contact Google Map','Contact Google Map', '0', CURRENT_TIMESTAMP,
               (SELECT user_info.id FROM user_info WHERE user_role_id=(SELECT user_role.id FROM user_role WHERE sn='SAD') LIMIT 0 OFFSET 1),
                CURRENT_TIMESTAMP);


---- Mysql
---- Page Section
---- Inlcluded for CMS One Page, Page Section Pages
INSERT INTO entity_attribute (entity_code, code, sn, ln, line_order, creation, user_id, timestamp_punch) VALUES ('PG', 'PGHS', 'Page Home Section', 'Page Home Section', '0', CURRENT_TIMESTAMP,(SELECT user_info.id FROM user_info WHERE user_role_id=(SELECT user_role.id FROM user_role WHERE sn='SAD') LIMIT 0,1), CURRENT_TIMESTAMP);

---- pgsql
---- Page Section
---- Inlcluded for CMS One Page, Page Section Pages
INSERT INTO entity_attribute (entity_code, code, sn, ln, line_order, creation, user_id, timestamp_punch) VALUES ('PG', 'PGHS', 'Page Home Section', 'Page Home Section', '0', CURRENT_TIMESTAMP,(SELECT user_info.id FROM user_info WHERE user_role_id=(SELECT user_role.id FROM user_role WHERE sn='SAD') LIMIT 1 OFFSET 1), CURRENT_TIMESTAMP);

                             
 