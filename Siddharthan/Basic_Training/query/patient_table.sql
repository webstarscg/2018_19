
-- Table Creation

CREATE TABLE patient (
  id int(11) NOT NULL,
  timestamp_punch timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  user_id int(11) NOT NULL,
  name varchar(50) NOT NULL,
  age int(11) NOT NULL,
  gender varchar(15) NOT NULL,
  town varchar(40) NOT NULL,
  date date NOT NULL,
  visits varchar(100) NOT NULL,
  PRIMARY KEY (id)
);

-- Alter Table

ALTER TABLE patient MODIFY COLUMN id INT auto_increment;