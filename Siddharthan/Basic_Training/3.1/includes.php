<?php

function includes(){

    include_once 'config.php';
    include_once 'data_retrieval.php';
    include_once 'table_header.php';
    include_once 'table_builder.php';
    include_once 'insert.php';

    //get functions

    $connection     =   db_connection();
    $get_data       =   data_retrieve();
    $header         =   table_header();
    $table_build    =   table_view();
    $insert         =   insert_data();

    $content       =   array(
                            
                            "connection"    =>   $connection,
                            "get_data"      =>   $get_data,
                            "header"        =>   $header,
                            "table_build"   =>   $table_build,
                            "insert"        =>   $insert,

                        );

                return $content;

}

?>