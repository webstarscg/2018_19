<?php

    require_once    ("template.php");

    $options    =   array("filename" => "page.html" , "debug" => 0);

    $template   =&  new Template($options);

    $template->AddParam('title','IF Check');
    $template->AddParam('head','IF Condition checking');
    $template->AddParam('if_msg','IF Works');
    $template->AddParam('else_msg','ELSE Check');
    $template->AddParam('condition', '1');

    $template->EchoOutput();

?>