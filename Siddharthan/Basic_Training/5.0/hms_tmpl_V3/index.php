<?php 

require_once    ("lib/template.php");
require_once    ("data/data_retrieval.php");

$data_value   =   data_retrieve();

//print_r($data_value);

$options = array(
                "filename"=>"static/table.html", 
                "debug"=>0,
                "global_vars"=>0,
                "loop_context_vars"=>1
            );

$template   =&  new Template($options);

$template   ->  AddParam('title'    ,   'HMS Data');
$template   ->  AddParam('heading'  ,   'Patient Data Table');
$template   ->  AddParam('thead'    ,   array(

                                            array('head'    =>  'S.no'),
                                            array('head'    =>  'Name'),
                                            array('head'    =>  'Age'),
                                            array('head'    =>  'Gender'),
                                            array('head'    =>  'Town'),
                                            array('head'    =>  'Date'),
                                            array('head'    =>  'Visits'),
                                            array('head'    =>  'Action'),

                                        )//header array ends here...

                                    );//Parameter ends here..

$template   ->  AddParam('tbody',$data_value);//Data accomodate to Parameter in templating

$template   ->  AddParam('AddButton','+ Add Patient');


$template   -> EchoOutput();


?>