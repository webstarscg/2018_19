<?php

    
    function get_table_data($param){

        $lv                = [];
                
        $lv['fields']      = [];

        $lv['info']        = [];

        // table data

        foreach($param['table_data'] as $row_id=>$row_data){
            
            // field

            array_push($lv['fields'],$row_data['field']);
            
        } // end of table data

        $lv['temp_fields']  = implode(',',$lv['fields']);
        
        $lv['query'] = "SELECT  ".$lv['temp_fields']."
                            
                                FROM 

                                    $param[table_name]
                                ";
                                 
        $result =    mysqli_query($param['db_conn'],$lv['query']);
        
        mysqli_close($param['db_conn']);


//echo $lv['query'];

        //fetch data into array

        while($row_detail = mysqli_fetch_assoc($result)){

                $temp = [];

                foreach($row_detail as $row_key => $row_value){

                            array_push($temp,['COL_VALUE'=>$row_value]);
                }

                array_push($lv['info'],['ROW_INFO'=>$temp]);

        }// while ends...

        return  $lv['info'];

    }//function close here....

?>