<?php 

require_once    ("lib/template.php");
require_once    ("data/get_data.php");
require_once    ("data/table_data.php");
require_once    ("lib/config.php");

$conn       =   db_connection();

$param = array( 

                'table_data' => $table_data,
                'table_name' => 'patient',
                'db_conn'    => $conn
                
            );

$values = execute_query($param);

$options = array(
    
                "filename"=>"table.html", 
                "debug"=>0,
                "global_vars"=>0,
                "loop_context_vars"=>1
            );

            $lv = [];

            $lv['fields'] = [];

            print_r($table_data);

            //print_r($values);


            foreach($param['table_data'] as $row_id=>$row_data){
            
                // field
    
                array_push($lv['fields'],$row_data['field']);
                
            } // end of table data

            $data  = implode(',',$lv['fields']);

            print_r($data);

$template   =&  new Template($options);

$template   ->  AddParam('title'    ,   'HMS Data');

$template   ->  AddParam('heading'  ,   'Patient Data Table');

$template   ->  AddParam('thead'    ,    $header);//Header Data

$template   ->  AddParam('tbody'    ,    array( 

                                            array(

                                                'field'    => array(

                                                    $data
                                                    
                                                ),

                                                'content'   , $values,

                                        )

                                    )
                        );
 
//$template   ->  AddParam('content'  ,    $values);


$template   -> EchoOutput();


?>