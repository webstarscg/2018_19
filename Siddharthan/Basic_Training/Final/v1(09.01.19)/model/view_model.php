<?php

function patient_view($bind){

  include_once './view/table_content.php';

//**************************************************************************************************************//

//Data Presentation Goes here...

$table_header   = $bind['header'];

$details        = data_retrieve();//to fetch the values of $patient_detail to $cont

$id             = patient_id();

$table_content  =''; //Empty variable to concatena

$table_content .="<title>Patient Table</title>";//title of the page goes here

$table_content .='<center><h1 position="center">Patient Details</h1>'; //Heading

$table_content .='<table border="5" width="75%">'; //Table Declaration

$serial_number  = 0; //variable initialisation to auto generate serial number

//**************************************************************************************************************//

foreach ($table_header as  $head_value) {

          if(is_array($head_value)){
                  //foreach for no of visits and purpose
                  foreach ($head_value as $no_visit_key => $no_visit_value) {

                      $table_content.=  "<th>".$no_visit_value."</th>";

                  } //visits foreach ends here

          } //if ends here

              else{

                $table_content.=  "<th>".$head_value."</th>";

            } //else ends here

}//header foreach ends

 $table_content.=   "</thead>"; //table header initialise

//********************************************Header Ends**************************************************//

foreach ($details as $details_value) {

  $table_content.=  '<tr>'; //table row starts here

      if(is_array($details_value)){

            //  print_r($details_value);

              foreach ($details_value as $patient_key => $patient_value) {

                $table_content.=  "<td>";

                $table_content.=  $patient_value;

                $table_content.=  "</td>";

              } //foreach ends here...

              // Edit and Delete action

              $table_content.="<td>";

                  $table_content.=  '<a href = "./view/edit_view.php?id='.$id.'">
                                      <input type="button" value="Edit"> &nbsp &nbsp</a>';

                  $table_content.=  '<a href = "delete.php?id='.$id.'">
                                      <input type="button" value="Delete"></a>';

              $table_content.="</td>";

      } //overall  if ends here...

      else{

        $table_content.="<td>";

        $table_content.=$patient_data;

        $table_content.="</td>";

      }//else ends here...

      $table_content.="</tr>";//overall table row ends here...

} //overall table value foreach value ends


$table_content.='</table></center>';

  return "$table_content";

} //function ends here

?>
