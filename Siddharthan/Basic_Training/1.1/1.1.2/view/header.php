<?php

$table_header = array(

                  "s.no"        =>  "S.No",
                  "name"        =>  "Name",
                  "gender"      =>  "Gender",
                  "age"         =>  "Age",
                  "town"        =>  "Town",
                  "visit"       =>  "Visits",
                  "edit"        =>  "Edit",
                  "delete"      =>  "Delete",

              ); //header array ends here...

              //variable for store Edit and Delete Button

              $edit_button = '<a href = "./view/edit_view.php">
                              <input type="button" name="edit" value = "Edit"></a>';
              $delete_button = '<a href = "./model/delete.php">
                                <input type="button" name="delete" value = "Delete"></a>';

 $button = array(

              "edit"      =>  $edit_button,
              "delete"    =>  $delete_button,

 );//button array ends...

 $content = array(

                "header"  =>  $table_header,
                "button"  =>  $button,

 );//content array ends...

?>
