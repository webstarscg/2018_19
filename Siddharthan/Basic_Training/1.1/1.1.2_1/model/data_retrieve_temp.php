<?php

    function data_retrieve(){

    include_once 'config.php';

    //Patient Details Array Delaration

    $query = "SELECT * FROM patient"; //Query for fetch data from DB

    $conn = connect_db(); //Function to call the DB connection

    $result = mysqli_query($conn, $query); //variable to execute our Query

    $patient_data = array(); //Empty Array to store our DB

  //  if(mysqli_num_rows($result) > 0){

      while($details = mysqli_fetch_array($result)){

        $id[]       =   array('id'      =>  $details['patient_id']);
        $name[]     =   array('name'    =>  $details['name']);
        $gender[]   =   array('gender'  =>  $details['gender']);
        $age[]      =   array('age'     =>  $details['age']);
        $town[]     =   array('town'    =>  $details['town']);
        $visits[]   =   array('visits'  =>  $details['visits']);

        } //while ends here..

        $patient_data   =   array(

                              "id"    =>  $id,
                              "name"  =>  $name,
                              "gender"  =>  $gender,
                              "age "    =>  $age,
                              "town"    =>  $town,
                              "Visits"  =>  $visits,
        );

        return $patient_data;

    $conn->close(); //DB Connection closed

    }
?>
