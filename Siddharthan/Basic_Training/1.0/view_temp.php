<?php

function patient_view($cont){

//Data Presentation Goes here...
$table_header =$cont['header'];//to fetch the values of $table_header to $cont

$patient_detail=$cont['data'];//to fetch the values of $patient_detail to $cont

$table_content='';

$table_content.="<title>Patient Table</title>";//title of the page goes here

$table_content.='<center><h1 position="center">Patient Details</h1>'; //Heading

$table_content.='<table border="5">'; //Table Declaration

$no = 0;

//foreach for header
foreach ($table_header as  $head_value) {

          if(is_array($head_value)){
                  //foreach for no of visits and purpose
                  foreach ($head_value as $no_visit_key => $no_visit_value) {

                    $table_content.="<th>$no_visit_value</th>";

                  } //visits foreach ends here

          }    //if ends here

          else{

            $table_content.="<th>$head_value</th>";

          } //else ends here

}//header foreach ends

$table_content.='<tr>'; //table row starts here

//$table_content.="<td>"; //Patient details column starts here

//foreach for data
foreach ($patient_detail as $patient_data) {

  $table_content.="<td>".++$no."</td>";

      foreach ($patient_data as $data_key => $data_value) {

            if(is_array($data_value)){

                  // for visiting

                  $table_content.="<td>"; //visits column starts here

                  foreach ($data_value as $visit_key => $visit_value) {

                      if(is_array($visit_value)){

                        //purpose of Visits

                        foreach ($visit_value as $purpose_key => $purpose_value) {

                                if(is_array($purpose_value)){

                                  //prescription foreach

                                  foreach ($purpose_value as $prescription_key => $prescription_value) {

                                      $table_content.="<br>&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp $prescription_value";

                                  } //prescription_value foreach ends...

                                }//if ends here...
                                else{

                                    $table_content.="&nbsp &nbsp &nbsp".$purpose_value;

                                } //else ends here....

                        } //purpose_value foreach ends...

                      } //if ends here...
                      else{

                        $table_content.="<br><li>$visit_value<br></li>";

                      } //else ends here...

                  } // visit value foreach ends...

            } // if ends...

            else{

                $table_content.="<td>$data_value</td>";


           } //else ends here..

           $table_content.="</td>"; //visit column ends here...

    }  //data value foreach ends

  $table_content.="</td>"; //patient details column ends here...

  $table_content.='</tr>'; //table row ends here...

} //data foreach ends

  $table_content.='</table></center>';

  return "$table_content";

} //function ends here
?>
