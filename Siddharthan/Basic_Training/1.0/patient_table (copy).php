<?php

  // function created for patient table
  function patient_table(){

    $patient_detail=array(

                          array(
                              "name"        => "Siddharthan",
                              "gender"      => "Male",
                              "age"         => "23",
                              "address"     => "Pattukkottai",
                              "visit_time"  =>  array(
                                                    "visit1" =>  "12.10.2016",
                                                                            "purpose" =>  array(
                                                                                              1 => "Checkup",
                                                                                                        "prescription"  =>  array(

                                                                                                            "tablet"    =>  "Calbol",

                                                                                                            "syrup"     =>  "cough syrup"
                                                                                                            
                                                                                                        ),
                                                                                            ),
                                                    "visit2" =>  "13.07.2015",
                                                                            "purpose1" =>  array(
                                                                                                  2 => "Cold",
                                                                                                ),
                                                    "visit3" =>  "08.05.2018",
                                                                            "purpose2" =>  array(
                                                                                                3 => "Headache",
                                                                                              ),
                                                  ),  //visit time ends
                            ),

                          array(
                              "name"    =>"Dharmalingam",
                              "gender"  =>"Male",
                              "age"     =>"59",
                              "address" =>"Peravoorani",
                              "visit_time"  =>  array(
                                                    "visit1"  =>  "02.08.2014",
                                                                            "purpose" =>  array(
                                                                                              1 => "Fever",
                                                                                            ),
                                                    "visit2" =>  "19.05.2016",
                                                                            "purpose1" =>  array(
                                                                                                2 => "Checkup",
                                                                                              ),
                                                    "visit3" =>  "01.07.2017",
                                                                            "purpose2" =>  array(
                                                                                                3 => "Heard attack",
                                                                                              ),
                                                  ), //visit time ends
                            ),

                          array(
                              "name"    =>"Ananthanayaki",
                              "gender"  =>"Female",
                              "age"     =>"46",
                              "address" =>"Anavayal",
                              "visit_time"  =>  array(
                                                    "visit1" =>  "11.03.2010",
                                                                            "purpose" =>  array(
                                                                                              1 => "Dengue Fever",
                                                                                            ),
                                                    "visit2" =>  "15.05.2013",
                                                                            "purpose1" =>  array(
                                                                                                2 => "Cold",
                                                    ),
                                                    "visit3" =>  "01.01.2016",
                                                                            "purpose2" =>  array(
                                                                                                3 => "Checkup",
                                                                                              ),
                                                  ), //visit time ends
                            ),

                          array(
                              "name"    =>"Vishnu",
                              "gender"  =>"Female",
                              "age"     =>"22",
                              "address" =>"Mettupalayam",
                              "visit_time"  =>  array(
                                                    "visit1"  =>  "11.07.2001",
                                                                            "purpose" =>  array(
                                                                                              1 => "Cold",
                                                                                            ),
                                                    "visit2" =>  "12.07.2006",
                                                                            "purpose1" =>  array(
                                                                                                2 => "Fever",
                                                                                              ),
                                                    "visit3" =>  "08.12.2008",
                                                                            "purpose2" =>  array(
                                                                                                3 => "Checkup",
                                                                                              ),
                                                  ), //visit time ends
                            ),

                          array(
                              "name"    =>"Sathish",
                              "gender"  =>"Male",
                              "age"     =>"23",
                              "address" =>"Sivakasi",
                              "visit_time"  =>  array(
                                                    "visit1" =>  "10.01.2006",
                                                                          "purpose" =>  array(
                                                                                            1 => "Back Pain",
                                                                                          ),
                                                    "visit2" =>  "03.09.2015",
                                                                          "purpose1" =>  array(
                                                                                            2 => "Cold",
                                                                                          ),
                                                    "visit3" =>  "08.05.2018",
                                                                          "purpose2" =>  array(
                                                                                            3 => "Back Pain",
                                                                                          ),
                                                  ),  //visit time ends
                            ),

                          array(
                              "name"    =>"Sivarathy",
                              "gender"  =>"Female",
                              "age"     =>"25",
                              "address" =>"Coimbatore",
                              "visit_time"  =>  array(
                                                    "visit1" =>   "12.10.2016",

                                                                            "purpose" =>  array(
                                                                                              1 => "Headache",
                                                                                              ),
                                                    "visit2" =>  "23.08.2017",

                                                                            "purpose1" =>  array(
                                                                                                2 => "Cold",
                                                                                              ),
                                                    "visit3" =>  "08.05.2018",
                                                                            "purpose2" =>  array(
                                                                                                3 => "Stomach Pain",
                                                                                              ),
                                                ),  //visit time ends

                            ),//data about user ends

    );  //Main array Ends


    //Data Presentation Goes here...

    $table_content='';
    //foreach for data
    foreach ($patient_detail as $patient_data) {

      #$table_content.='<tr>';

          foreach ($patient_data as $data_key => $data_value) {

                if(is_array($data_value)){

                      // for visting
                      foreach ($data_value as $visit_key => $visit_value) {

                          if(is_array($visit_value)){

                            //purpose of Visits

                            foreach ($visit_value as $purpose_key => $purpose_value) {

                              if(is_array($purpose_value)){

                                    foreach ($purpose_value as $prescription_key => $prescription_value) {

                                      $table_content.="<dd> &nbsp &nbsp <br> &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp $prescription_key : $prescription_value</dd>";

                                    }

                              }

                              else {

                                $table_content.="<dd> &nbsp &nbsp <br> &nbsp &nbsp &nbsp &nbsp $purpose_value</dd>";
                              }

                            }

                          }else{

                            $table_content.="<dt>&nbsp &nbsp<li>&nbsp &nbsp $visit_key : $visit_value</li></dt>";

                          }

                      }

                } // if ends
                else{

                  $table_content.="<dt>$data_key : $data_value</dt>";

               }
        }  //inner foreach ends

        $table_content.="<hr>";


    } //data foreach ends

      echo "$table_content";

  }

  patient_table();  //call the function


?>
