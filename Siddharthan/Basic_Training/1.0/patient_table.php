<?php

  // function created for patient table
  function patient_table(){

    //Header Array Declaration

    $table_header=array(

                    "name"        =>  "Name",
                    "gender"      =>  "Gender",
                    "age"         =>  "Age",
                    "town"        =>  "Town",
                    "visit_time"  =>  "Visit1",
                                              "no_of_visits"  =>  array(
                                                                      1 => "Visit2",
                                                                      2 => "Visit3",
                                                                    ),
                    "purpose"     =>  "Purpose1",
                                              "purpose_of"  =>  array(
                                                                      1 => "Purpose2",
                                                                      2 => "Purpose3",
                                                                    ),

                  );

    //Patient Details Array Delaration

    $patient_detail=array(

                          array(
                            "name"        => "Siddharthan",
                            "gender"      => "Male",
                            "age"         => "23",
                            "address"     => "Pattukkottai",
                            "visit_time"  =>  array(
                                                  1         =>  "12.10.2016",
                                                            "purpose" =>  array(
                                                                    1 => "Checkup",
                                                  ),
                                                  2 =>  "13.07.2015",
                                                  "purpose1" =>  array(
                                                                    2 => "Cold",
                                                  ),
                                                  3 =>  "08.05.2018",
                                                  "purpose2" =>  array(
                                                                    3 => "Headache",
                                                  ),
                                                ),  //visit time ends
                          ),

                          array(
                            "name"    =>"Dharmalingam",
                            "gender"  =>"Male",
                            "age"     =>"59",
                            "address" =>"Peravoorani",
                            "visit_time"  =>  array(
                                                  1         =>  "02.08.2014",
                                                  "purpose" =>  array(
                                                                    1 => "Fever",
                                                  ),
                                                  2 =>  "19.05.2016",
                                                  "purpose1" =>  array(
                                                                    2 => "Checkup",
                                                  ),
                                                  3 =>  "01.07.2017",
                                                  "purpose2" =>  array(
                                                                    3 => "Heard attack",
                                                  ),
                                                ), //visit time ends
                          ),

                          array(
                            "name"    =>"Ananthanayaki",
                            "gender"  =>"Female",
                            "age"     =>"46",
                            "address" =>"Anavayal",
                            "visit_time"  =>  array(
                                                  1         =>  "11.03.2010",
                                                  "purpose" =>  array(
                                                                    1 => "Dengue Fever",
                                                  ),
                                                  2 =>  "15.05.2013",
                                                  "purpose1" =>  array(
                                                                    2 => "Cold",
                                                  ),
                                                  3 =>  "01.01.2016",
                                                  "purpose2" =>  array(
                                                                    3 => "Checkup",
                                                  ),
                                                ), //visit time ends
                          ),

                          array(
                            "name"    =>"Vishnu",
                            "gender"  =>"Female",
                            "age"     =>"22",
                            "address" =>"Mettupalayam",
                            "visit_time"  =>  array(
                                                  1         =>  "11.07.2001",
                                                  "purpose" =>  array(
                                                                    1 => "Cold",
                                                  ),
                                                  2 =>  "12.07.2006",
                                                  "purpose1" =>  array(
                                                                    2 => "Fever",
                                                  ),
                                                  3 =>  "08.12.2008",
                                                  "purpose2" =>  array(
                                                                    3 => "Checkup",
                                                  ),
                                                ), //visit time ends
                          ),

                          array(
                            "name"    =>"Sathish",
                            "gender"  =>"Male",
                            "age"     =>"23",
                            "address" =>"Sivakasi",
                            "visit_time"  =>  array(
                                                  1         =>  "10.01.2006",
                                                  "purpose" =>  array(
                                                                    1 => "Back Pain",
                                                  ),
                                                  2 =>  "03.09.2015",
                                                  "purpose1" =>  array(
                                                                    2 => "Cold",
                                                  ),
                                                  3 =>  "08.05.2018",
                                                  "purpose2" =>  array(
                                                                    3 => "Back Pain",
                                                  ),
                                                ),  //visit time ends
                          ),

                          array(
                            "name"    =>"Sivarathy",
                            "gender"  =>"Female",
                            "age"     =>"25",
                            "address" =>"Coimbatore",
                            "visit_time"  =>  array(
                                                  1         =>  "12.10.2016",
                                                  "purpose" =>  array(
                                                                    1 => "Headache",
                                                  ),
                                                  2 =>  "23.08.2017",
                                                  "purpose1" =>  array(
                                                                    2 => "Cold",
                                                  ),
                                                  3 =>  "08.05.2018",
                                                  "purpose2" =>  array(
                                                                    3 => "Stomach Pain",
                                                  ),
                                                ),  //visit time ends
                          ),

    );  //Main array Ends


    //Data Presentation Goes here...

    $table_content='';

    $table_content.='<center><h1 position="center">Patient Details</h1>'; //Heading

    $table_content.='<table border="5">'; //Table Declaration


    //foreach for header
    foreach ($table_header as  $head_value) {

              if(is_array($head_value)){
                      //foreach for no of visits and purpose
                      foreach ($head_value as $no_visit_key => $no_visit_value) {

                        $table_content.="<th>$no_visit_value</th>";

                      } //visits foreach ends here

              }    //if ends here

              else{

                $table_content.="<th>$head_value</th>";

              } //else ends here

    }//header foreach ends

    //foreach for data
    foreach ($patient_detail as $patient_data) {

      $table_content.='<tr>';

          foreach ($patient_data as $data_key => $data_value) {

                if(is_array($data_value)){

                      // for visting
                      foreach ($data_value as $visit_key => $visit_value) {

                          if(is_array($visit_value)){

                            //purpose of Visits

                            foreach ($visit_value as $purpose_key => $purpose_value) {

                              $table_content.="<td>".$purpose_value."</td>";

                            }

                          }else{

                            $table_content.="<td><li>$visit_value</li></td>";

                          }

                      }

                } // if ends
                else{

                  $table_content.="<td>$data_value</td>";

               }


        }  //inner foreach ends

      $table_content.='</tr>';//table structure ends


    } //data foreach ends


      $table_content.='</table></center>';

      echo "$table_content";

      #print_r($patient_detail);

  }

  patient_table();  //call the function


?>
