<?php

    // Name of the file
    $filename = 'database.sql';
    // MySQL host
    $host = 'localhost';
    // MySQL username
    $username = 'root';
    // MySQL password
    $password = '';
    // Database name
    $database = 'import_check';
    
    // Connect to MySQL server
    $link = mysqli_connect($host, $username, $password) or die('Error connecting to MySQL server: ' . mysqli_error($link));
    // Select database
    mysqli_select_db($link,$database) or die('Error selecting MySQL database: ' . mysqli_error($link));
    
    // Temporary variable, used to store current query
    $templine = '';
    // Read in entire file
    $lines = file($filename);
    // Loop through each line
    foreach ($lines as $line)
    {
    // Skip it if it's a comment
    if (substr($line, 0, 2) == '--' || $line == '')
        continue;
    
    // Add this line to the current segment
    $templine .= $line;
    // If it has a semicolon at the end, it's the end of the query
    if (substr(trim($line), -1, 1) == ';')
    {
        // Perform the query
        mysqli_query($link,$templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysqli_error($link) . '<br /><br />');
        // Reset temp variable to empty
        $templine = '';
    }
    }
     echo "Tables imported successfully";
?>