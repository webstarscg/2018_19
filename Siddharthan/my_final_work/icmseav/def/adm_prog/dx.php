<?PHP

	
	
	$LAYOUT	    	= 'layout_full';
               
        $D_SERIES       =   array(
                                   'title'=>'Programme',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
				    
				    'gx' => 1,
				    
                                    
                                    #table data
                                    
                                    'data'=> array(
					       
						      1=>array('th'=>'Programme/Ref No.',
								
								'field' =>"concat_ws(':',(SELECT sn FROM entity WHERE code = entity_child.entity_code),
								                          get_exav_addon_varchar(id,'REFN'))",
								
								'td_attr' => ' class=" align_LM no_wrap" width="18%"',
								
								'is_sort' => 0,
								
								'filter_out' => function($data_in){
									
											$temp = explode(':',$data_in);
											
											return "<div class='block normal txt_case_uppercase txt_size_11 clr_gray_a'>$temp[0]</div>".
										               "<div class='block b txt_size_10 clr_gray_4'>$temp[1]</div>";
											       
									
								                }
								
								),
						       
						//       2=>array('th'=>'Reference No',
						//		
						//		'field' =>"get_exav_addon_varchar(id,'REFN')",
						//		
						//		'td_attr' => ' class="label_father clr_dark_blue align_LM txt_size_10" width="17%"',
						//		
						//		'is_sort' => 0,
						//		
						//		),
							
							
							3=>array('th'=>'Applicant Detail',
								
								'field' 	=>"concat_ws(':',get_eav_addon_varchar((SELECT is_internal FROM user_info WHERE id = entity_child.user_id),'COFN'),
											get_eav_addon_varchar((SELECT is_internal FROM user_info WHERE id = entity_child.user_id),'COEM'),
											get_eav_addon_varchar((SELECT is_internal FROM user_info WHERE id = entity_child.user_id),'COMB'))",
										
								
								'td_attr' 	=> ' class=" align_LM" width="20%"',
								
								'is_sort' 	=> 1,
								
								'filter_out' => function($data_in){
									
											$temp = explode(':',$data_in);
											
											//<i class='fa fa-send-o clr_dark_blue txt_size_13' aria-hidden='true'></i>&nbsp;
											
											return "<div class='block b txt_case_capitall'>$temp[0]</div>".
										               "<div class='block normal clr_gray_7 txt_size_11'>$temp[1]</div>".
											       "<div class='block normal clr_gray_5 txt_size_11'> $temp[2]</div>";
									
								                }
								
								),
							//
							//4=>array('th'=>'Email',
							//	
							//	'field' =>"get_eav_addon_varchar((SELECT is_internal FROM user_info WHERE id = entity_child.user_id),'COEM')",
							//	
							//	'td_attr' => ' class="label_father align_LM" width="10%"',
							//	
							//	'is_sort' => 1,	
							//	
							//	),
							//
							//12=>array('th'=>'Mobile',
							//	
							//	'field' =>"get_eav_addon_varchar((SELECT is_internal FROM user_info WHERE id = entity_child.user_id),'COMB')",
							//	
							//	'td_attr' => ' class="label_father align_LM" width="10%"',
							//	
							//	'is_sort' => 1,	
							//	
							//	),
							//
							5=>array('th'=>'Title',
								
								'field'     => "concat_ws(':',substring_index(get_exav_addon_varchar(id,'TITLE'),' ',7),get_exav_addon_varchar(id,'TITLE'))",
								
								'td_attr'   => 'class="label_father align_LM" width="30%"',
								
								'is_sort'   => 0,
								
								'filter_out'=>function($data_in){
									
									$temp = explode(':',$data_in);
									
									return "<a class='tip clr_gray_5'>$temp[0]...</a><span class='tooltiptext'>$temp[1]</span>";
										
								}
							),
							
							6=>array('th'   =>  'Status',
								
								'field' =>  "upper(get_ecb_sn_by_token(get_exav_addon_exa_token(id,'STAT')))",
								
								//'field' =>"get_ecb_sn_by_token(get_exav_addon_exa_token(id,entity_child.entity_code+'ST'))",
								
								'td_attr'  	=>  ' class="label_grand_child align_LM" width="10%"',
								
								'is_sort'	=>  0,
								
								'filter_out'    => function($data_in){
									
											$temp=['NEW'=>'file-o clr_orange','SUBMITTED'=>'file-text-o clr_dark_blue'];
									
											return "<span><i class='fa fa-$temp[$data_in] txt_size_15' aria-hidden='true'>&nbsp;&nbsp;</i>".
											       "$data_in</span>";
										}
								
								),
							
							7=>array('th'=>'Updated On',
								
								'field' =>"date_format(updated_on,'%d-%b-%Y %H:%I') ",
								
								'td_attr' => ' class="no_wrap clr_gray_a align_LM txt_size_11" width="10%"',
								
								'is_sort' => 1,
								
								),
							
							8=>array(       'th'=>'View ',
								 
									'td_attr' => ' class="align_LM no_wrap " width="10%"',
                                
									'field'	=> "concat(entity_code,':',id)",
									
									//'field'	=> "id",
								
									'filter_out'=>function($data_in){
                                              
										return '<a class="pointer clr_gray_b txt_size_11" onclick="JavaScript:adm_view('."'".$data_in."'".');">'.
											'<i class="fa fa-file-pdf-o clr_red txt_size_15" aria-hidden="true"></i>&nbsp;'.
											'View</a>';
									}
                                                                
								),
							
							9=>array(	'th'=>'Reviewer ',
								 
									'th_attr'=>' colspan=2 ',
								 
									'field'	=> "concat(id,':',get_exav_addon_exa_token(id,'STAT'))",
									
									'filter_out'=>function($data_in){
                                                                            
													$temp = explode(':',$data_in);
													
													
													if($temp[1]=='SASU'){
															
															$data_out = array(	'id'   => $temp[0],
																		'link_title'=>'Add Reviewer',
																		'is_fa'=>'fa-plus-square txt_size_24 clr_green ',
																		'title'=>'Add Reviewer',
																		'src'=>"?fx=set_reviewer&menu_off=1&mode=simple&default_addon=$temp[0]",
																		'style'=>"border:none;width:100%;height:600px;",
																		'refresh_on_close'=>1
																	);
															return json_encode($data_out);
														
													
													}
													
													
													else{
															$data_out = array(	'id'   => $temp[0],
																		'link_title'=>'Not Applicable',
																		'is_fa'=>'fa-ban txt_size_12 clr_orange ',
																		'title'=>'Not Applicable',
																		'src'=>"",
																		'style'=>"border:none;width:100%;height:600px;",
																		'refresh_on_close'=>0
																	);
															return json_encode($data_out);
														
													}
												 },
												 
									'js_call'=>'d_series.set_nd'
                                                                
								),
							
							10=>array(       'th'=>'',
								 
									//'field'	=> "concat(id,':',get_exav_addon_ec_id(id,'REVW'))",
									
									'field'	=> "concat(id,':',(SELECT ec_id FROM exav_addon_ec_id WHERE exa_token = 'REVW' AND parent_id = entity_child.id LIMIT 1   ))",
									
									'filter_out'=>function($data_in){
                                                                            
													$temp = explode(':',$data_in);
													
													
													if($temp[1]!=NULL){
															
															$data_out = array(	'id'   => $temp[0],
																		'link_title'=>'Manage Reviewer',
																		'is_fa'=>'fa-user txt_size_24 clr_green ',
																		'title'=>'Manage Reviewer',
																		'src'=>"?dx=set_reviewer&menu_off=1&mode=simple&default_addon=$temp[0]",
																		'style'=>"border:none;width:100%;height:600px;",
																		'refresh_on_close'=>1
																	);
															return json_encode($data_out);
														
													
													}
													
													else{
															$data_out = array(	'id'   => $temp[0],
																		'link_title'=>'Not Applicable',
																		'is_fa'=>'fa-ban txt_size_12 clr_orange ',
																		'title'=>'Not Applicable',
																		'src'=>"",
																		'style'=>"border:none;width:100%;height:600px;",
																		'refresh_on_close'=>0
																	);
															return json_encode($data_out);
														
													}
												 },
									 
                                                                        'js_call'=>'d_series.set_nd'
                                                                
								),
							
							11=>array(	'th'=>'Comments ',
								 
									'th_attr'=>' colspan=2 ',
								 
									'field'	=> "concat(id,':',get_exav_addon_exa_token(id,'STAT'))",
									
									'filter_out'=>function($data_in){
										
													$temp = explode(':',$data_in);
													
													if($temp[1]=='SASU'){
                                                                            
														$data_out = array(	'id'   => $temp[0],
																	'link_title'=>'Add Comments',
																	'is_fa'=>'fa-plus-square txt_size_24 clr_orange ',
																	'title'=>'Add Comments',
																	'src'=>"?fx=review_comments&menu_off=1&mode=simple&default_addon=$temp[0]",
																	'style'=>"border:none;width:100%;height:600px;",
																	'refresh_on_close'=>0
																);
																return json_encode($data_out);
													}
													
													else{
															$data_out = array(	'id'   => $temp[0],
																		'link_title'=>'Not Applicable',
																		'is_fa'=>'fa-ban txt_size_12 clr_orange ',
																		'title'=>'Not Applicable',
																		'src'=>"",
																		'style'=>"border:none;width:100%;height:600px;",
																		'refresh_on_close'=>0
																	);
															return json_encode($data_out);
														
													}
												 },
												 
									'js_call'=>'d_series.set_nd'
                                                                
								),
							
							12=>array(       'th'=>'',
								 
									'field'	=> "concat(id,':',get_exav_addon_exa_token(id,'STAT'))",
									
									'filter_out'=>function($data_in){
                                                                            
													$temp = explode(':',$data_in);
													
													if($temp[1]=='SASU'){
														
														$data_out = array(	'id'   => $temp[0],
																	'link_title'=>'View comments',
																	'is_fa'=>'fa-comments-o txt_size_24 clr_orange ',
																	'title'=>'View comments',
																	'src'=>"?dx=review_comments&menu_off=1&mode=simple&default_addon=$temp[0]",
																	'style'=>"border:none;width:100%;height:600px;",
																	'refresh_on_close'=>0
																);
																return json_encode($data_out);
													}
													
													else{
															$data_out = array(	'id'   => $temp[0],
																		'link_title'=>'Not Applicable',
																		'is_fa'=>'fa-ban txt_size_12 clr_orange ',
																		'title'=>'Not Applicable',
																		'src'=>"",
																		'style'=>"border:none;width:100%;height:600px;",
																		'refresh_on_close'=>0
																	);
															return json_encode($data_out);
														
													}
												 },
									 
                                                                        'js_call'=>'d_series.set_nd'
                                                                
								),

							
					),
				    
					
                                    #Table Info
                                    
                                    'table_name' =>'entity_child',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
				    
				    'key_filter'     =>	 " AND (entity_code='NP' OR entity_code='EI' OR entity_code='FA' OR entity_code='NA')",
				    
				    'is_narrow_down'=>1,
				    
				    'js' => array('is_top' => 1, 'top_js' => "def/adm_prog/dx"),
				    
                                    # Communication
                                
                                    'prime_index'   => 1,
				    
				    'summary_data'=>array(
								array(  'name'=>'No. of Application','field'=>'count(id)','html'=>'class=summary'),
								
								array(  'name'=>'Submitted',
									 
									'field'=>'count((SELECT id
											 FROM
												exav_addon_exa_token as exavt
											WHERE
												exavt.parent_id=entity_child.id  AND
												exa_token="STAT" AND
												exa_value_token="SASU"))',
									'html'=>'class=summary'
								),
						),
				    
				    'custom_filter' => array(  			     						   
							      
									array(  'field_name' => 'Programme:',
									      
										'field_id' => 'cf1', // 
										
										'filter_type' =>'option_list', 
												    
										'option_value'=> $G->option_builder('entity','code,sn'," WHERE code = 'EI' OR code = 'NP' OR code = 'NA'"),
							     
										'html'=>'  title="Select"   data-width="160px"  class="w_60"',
								    
										'cus_default_label'=>'Show All',
							    
										'filter_by'  => "entity_code" // main table value
									),
									
									array(  'field_name' => 'Status:',
									      
										'field_id' => 'cf2', // 
										
										'filter_type' =>'option_list', 
												    
										'option_value'=> $G->option_builder('entity_child_base','token,sn'," WHERE entity_code = 'SA' AND (token='SANW' OR token='SASU')"),
							    
										'html'=>'  title="Select"   data-width="160px"  ',
								    
										'cus_default_label'=>'Show All',
							    
										'filter_by'  => "get_exav_addon_exa_token(id,'STAT')" // main table value
									),
									
									array(  'field_name' => 'Batch:',
									      
										'field_id' => 'cf3', // 
										
										'filter_type' =>'option_list', 
												    
										'option_value'=> $G->option_builder('entity_child_base','token,sn'," WHERE entity_code = 'BA' AND dna_code='EBMS'"),
							    
										'html'=>'  title="Select Type"   data-width="160px" class="w_60"  ',
								    
										'cus_default_label'=>'Show All',
							    
										'filter_by'  => "get_exav_addon_varchar(id,'BATCH')" // main table value
									),
							),
				//    
				//    'search'=> 	array(
				//			  
				//			array(  'data'  =>array('table_name' 	=> 'entity_child',
				//						'field_id'	=> 'id',
				//						'field_name' 	=> 'get_contact_name(id)',
				//						'filter'	=> "AND entity_code='CO'",
				//						//'filter'	=> "AND entity_code='CO' AND id IN (SELECT is_internal FROM user_info WHERE user_role_id=7)" 
				//					 ),
				//			      
				//				'title' 		=> 'Name',										
				//				'search_key' 		=> 'get_contact_name(id)',													       
				//				'is_search_by_text' 	=> 1, //( For Text search case)	      
				//			),
				//	),
				//    
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>0,'page_link'=>'', 'b_name' => '' ),
								
					'del_permission' => array('able_del'=>1,'user_flage'=>0), 
								
					'date_filter'  => array( 'is_date_filter' =>1,'date_field' =>  "updated_on"),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'ADMIN',
				
				'show_query'=>0,
				
				'hide_show_all' => 0,
				
				'hide_pager'     =>0,
				
				'filter_off' =>0,
				 
				'search_filter_off'	=>1,
				
                            
			    
                            );
	
	
	
	if($USER_ROLE == 'REW'){
		
		$D_SERIES['del_permission']['able_del'] = 0 ;
		
		$D_SERIES['key_filter'] =  " AND (entity_code='NP' OR entity_code='EI' OR entity_code='FA' OR entity_code='NA')
					     AND id IN (SELECT parent_id FROM exav_addon_ec_id WHERE exa_token = 'REVW'
					     AND ec_id = (SELECT is_internal FROM user_info WHERE id = $USER_ID))";
		
		unset($D_SERIES['data'][9]);
		
		unset($D_SERIES['data'][10]);
				    
		unset($D_SERIES['custom_filter'][1]);
		
		unset($D_SERIES['summary_data'][1]);
	}
    
?>