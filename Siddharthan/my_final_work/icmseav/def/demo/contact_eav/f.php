<?PHP

    $LAYOUT	    = 'layout_full';
    
    $new_option = '<option value=-1 class="clr_green">+ Add New</option>';
                            
    $F_SERIES	=	array(
				#Form Title
				
				'title'	=>'Contact',
				
				#Table field
                    
				'data'	=>   array(
						   
						   '0'  => ['field_name'=>'Basic',
								 'type'=>'heading'					 
								],
						   
						   
						   '1' =>array( 'field_name'=> 'Type ',
                                                               
                                                               'field_id' => 'exa_value_token',
                                                               
                                                               'type' => 'option',
							       
							       'option_data' => $G->option_builder('entity_child_base','token,sn'," WHERE entity_code='CG'  ORDER BY sn ASC"),
                                                               
							       'child_table'         => 'eav_addon_exa_token', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => 'COCG',           // attribute code
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_100"'
                                                               
                                                               ),
						   
						   '2' =>array( 'field_name'=> 'Title ',
                                                               
                                                               'field_id' => 'exa_value_token',
                                                               
                                                               'type' => 'option',
							       
							       'option_data' => $G->option_builder('entity_child_base','token,sn'," WHERE entity_code='HN'  ORDER BY sn ASC"),
                                                               
							       'child_table'         => 'eav_addon_exa_token', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => 'COHN',           // attribute code
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_50"'
                                                               
                                                               ),
						   
						   
						   
						   '3' =>array( 'field_name'=> 'First Name ',
                                                               
                                                               'field_id' => 'ea_value',
                                                               
                                                               'type' => 'text',
							       
							       'child_table'         => 'eav_addon_varchar', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => 'COFN',           // attribute code
							       
							       'input_html'=>'class="w_200"',
							       
							       'allow'		=> 'w128[ .]',
                                                               
							       'is_mandatory'=>1,
                                                               
                                                               ),
						   
						   
						   '4' =>array( 'field_name'=> 'Last Name ',
                                                               
                                                               'field_id' => 'ea_value',
                                                               
                                                               'type' => 'text',
							       
							       'child_table'         => 'eav_addon_varchar', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => 'COLN',           // attribute code
							       
							       'input_html'=>'class="w_200"',
							       
							       'allow'		=> 'w128[ .]',
                                                               
							       'is_mandatory'=>1,
                                                               
                                                               ),
						   
						    '5' =>array(   'field_name'=> 'Image ',
                                                               
                                                            'field_id' => 'image',
                                                               
                                                            'type' => 'file',
                                                               
							    'upload_type' => 'image',
							    
							    //'save_file_name_suffix'=> '_outer',
      
							    'allow_ext'   => array('jpg','jpeg','png'),
							    
							    'max_size'    => 100,

							    'location'    => 'images/entity_child/',          // attribute code
                                                               
                                                            'is_mandatory'=>0,
                                                               
                                                            'input_html'=>'class="w_200"',
							    
							    //'image_size_auto' => json_decode($page_img_size_auto,TRUE),
                                                               
                                                        ),
						   
						   
//						   '6' =>array( 'field_name'=> 'Gender ',
//                                                               
//                                                               'field_id' => 'exa_value_token',
//                                                               
//                                                               'type' => 'option',
//							       
//							       'option_data' => $G->option_builder('entity_child_base','token,sn'," WHERE entity_code='GE'  ORDER BY sn ASC"),
//							       
//							       'child_table'         => 'eav_addon_exa_token', // child table
//							       
//							       'parent_field_id'     => 'parent_id',    // parent field
//										       
//							       'child_attr_field_id' => 'ea_code',   // attribute code field
//							       
//							       'child_attr_code'     => 'COGE',           // attribute code
//                                                               
//                                                               'is_mandatory'=>1,
//                                                               
//                                                               'input_html'=>'class="w_100"'
//                                                               
//                                                               ),
//						   
//						   
//						   '7' =>array( 'field_name'=> 'Date Of Birth',
//                                                               
//                                                               'field_id' => 'ea_value',
//                                                               
//                                                               'type' => 'date',
//							       
//							       'set'  => array('min_date'=>'-100Y','max_date'=>'0D'),
//							       
//                                                               'child_table'         => 'eav_addon_date', // child table
//							       
//							       'parent_field_id'     => 'parent_id',    // parent field
//										       
//							       'child_attr_field_id' => 'ea_code',   // attribute code field
//							       
//							       'child_attr_code'     => 'CODB',           // attribute code
//                                                      
//							       'is_mandatory'=>0,
//							       
//							       'default_date'=>date("d-m-Y"),
//                                                               
//                                                               'input_html'=>'class="w_100"' ,
//							       
//                                                               ),
//						   
						    '8'  => ['field_name'=>'Communication',
								 'type'=>'heading'					 
								],
						   
						    '9' =>array( 'field_name'=> 'Landline ',
                                                               
                                                               'field_id' => 'ea_value',
                                                               
                                                               'type' => 'text',
							       
							       'child_table'         => 'eav_addon_varchar', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => 'COLD',           // attribute code
							       
							       'input_html'=>'class="w_100"',
                                                               
							       'is_mandatory'=>0,
							       
							       'allow' => 'd10',
                                                               
                                                               ),
						    
						    '10' =>array( 'field_name'=> 'Mobile ',
                                                               
                                                               'field_id' => 'ea_value',
                                                               
                                                               'type' => 'text',
							       
							       'child_table'         => 'eav_addon_varchar', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => 'COMB',           // attribute code
							       
							       //'input_html'=>'class="w_100"',
							       
							       'input_html'=> "class='w_100'  onchange='check_mobile(this);'",
                                                               
							       'is_mandatory'=>1,
							       
							       'allow' => 'd10',
                                                               
                                                               ),
						    
						    '11' =>array( 'field_name'=> 'Email ',
                                                               
                                                               'field_id' => 'ea_value',
                                                               
                                                               'type' => 'text',
							       
							       'child_table'         => 'eav_addon_varchar', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => 'COEM',           // attribute code
							       
							       'input_html'=>"class='w_200' onchange='check_email(this);'",
							       
							       'allow'		=> 'x128',
                                                               
							       'is_mandatory'=>0,	
                                                               
                                                               ),
						    
						    '12' =>array( 'field_name'=> 'Residential Address Line 1 ',
                                                               
                                                               'field_id' => 'ea_value',
                                                               
                                                               'type' => 'textarea',
							       
							       'child_table'         => 'eav_addon_varchar', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => 'CORA',           // attribute code
							       
							       'input_html'=>'class="w_200"',
                                                               
							       'is_mandatory'=>1,
							       
							       'allow'		=> 'x1028',
                                                               
                                                               ),
						    
						    '13' =>array( 'field_name'=> 'Residential Address Line 2 ',
                                                               
                                                               'field_id' => 'ea_value',
                                                               
                                                               'type' => 'textarea',
							       
							       'child_table'         => 'eav_addon_varchar', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => 'CORB',           // attribute code
							       
							       'input_html'=>'class="w_200"',
                                                               
							       'is_mandatory'=>0,
							       
							       'allow'		=> 'x1028',
                                                               
                                                               ),
						   
						   '14' =>array(
                                    
								'field_name'=> 'City ',
											   
								'field_id' => 'exa_value_token',
											   
								'type' => 'option',
							       
								'option_data' => $G->option_builder('entity_child_base','token,sn'," WHERE entity_code='CT'  ORDER BY sn ASC").$new_option,
								
								'child_table'         => 'eav_addon_exa_token', // child table
								
								'parent_field_id'     => 'parent_id',    // parent field
											
								'child_attr_field_id' => 'ea_code',   // attribute code field
								
								'child_attr_code'     => 'CORC',           // attribute code
								
								'input_html'=>"class='w_150' onchange='add_new_master(this,15)'",
								
								'is_mandatory'=>1,
							       
                                                               ),
						   
						   
						   '15'=>array( 'field_name' => 'New City',
							      
								'field_id'=>'sn',
                                                               
								'type'=>'text',
								
		                        'is_hide'=>1,
	               
                                'is_mandatory'=>0,
								
								'ro'	=>1,
								
								'allow'		=> 'w128[ ]',
								
								'input_html'=> "  onchange='check_city(this);'",
								
							),
						   
						    '16' =>array( 'field_name'=> 'Pincode ',
                                                               
                                                               'field_id' => 'ea_value',
                                                               
                                                               'type' => 'text',
							       
							       'child_table'         => 'eav_addon_varchar', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => 'CORP',           // attribute code
							       
							       'input_html'=>'class="w_100"',
                                                               
							       'is_mandatory'=>1,
							       
							       'allow' => 'd6',
                                                               
                                                               ),
						   
					    
					    ),
                                    
				#Table Name
				
				'table_name'    => 'entity_child',
				
				#Primary Key
                                
			        'key_id'        => 'id',
				
				//'deafult_value'    => array('entity_code' => "'CO'", 'is_active' => "1"),
				
				'deafult_value'    => array('entity_code' => "'CO'"),
				
				'js'=> ['is_top'=>1,'top_js'=>$LIB_PATH.'def/contact_eav/f'],
				
				# Default Additional Column
                                
				'is_user_id'       => 'created_by',
								
				# Communication
								
				'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?d=contact_eav', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 3,
                                
				//'flat_message'	=> 'Successfully Added',
				
				# File Include
				
				'before_add_update' => 1,
				
				'show_query'    =>0,
				
				'divider'       => 'tab'
				
                                
			);
    
        function before_add_update(){
	
	    global $G,$rdsql,$USER_ID;
	    
	    if($_POST['X14']==-1){
		
		$value = $_POST['X15'];
		
		$insert = $rdsql->exec_query("INSERT INTO entity_child_base (entity_code,token,sn,user_id) VALUES ('CT',md5('$value'),'$value',$USER_ID)","Insertion Failed");
		 
		$temp = $rdsql->last_insert_id('entity_child_base');
		
		$get = $rdsql->exec_query("SELECT token FROM entity_child_base WHERE id = $temp","Selection Failed");
		
		$get_val = $rdsql->data_fetch_object($get);
		
		$_POST['X14'] = $get_val->token;
		 
	    }
	    
	    return null;
	}
	
	$name_box = $SG->get_cookie("name_box");
	
	$pos = strpos($name_box, 'single');
	
	if($pos !== FALSE){
	    
	    $F_SERIES['data'][3]['field_name'] = 'Name';
	    unset($F_SERIES['data'][4]);
	    
	}
	
?>