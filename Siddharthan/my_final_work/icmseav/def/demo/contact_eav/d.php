<?PHP
		
	$default_addon = @$_GET['default_addon'];
	
        $LAYOUT	    	= 'layout_full';
               
        $D_SERIES       =   array(
                                   'title'=>'Contact Details',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
				    
				    'gx' => 1,
				    
                                    #table data
                                    
                                    'data'=> array(
					                20=>array('th'=>'ID ',
								
                                                                'field' => "id",
								
								'td_attr' => ' class="label_father align_RM" width="7%"',
								
								'is_sort' => 1,
								
								'filter_out'=>$FILTER['square_secondary']
								
								),
							
							24=>array('th'=>'Image',
									 
								'field'=>'image',
									 
								'td_attr' => ' class="label_child align_CM" width="5%"',
								
								'filter_out'   =>	function($data_out){
									
                                                                        $temp = explode(',',$data_out);
                                                                        
									return '<img class="w_25" src="'. $temp[0].'">';
									
									},
									 
								),
					
							1=>array('th'=>'Name ',
								
                                                                'field' => "get_contact_name(id)",
								  
								'td_attr' => ' class="clr_pri b align_LM no_wrap" width="20%"',
								
								'is_sort' => 1,
								
								),
					
							2=>array('th'=>'Gender ',
								 
								'field' => "(get_ecb_sn_by_token(get_eav_addon_exa_token(id,'COGE')))",
								   
								'td_attr' => ' class="align_LM label_grand_child" width="10%"',
								
								'is_sort' => 0,
								
								'js_ca;'
									 
								),
							
							3=>array('th'=>'DOB ',
								
								'field' => "date_format((SELECT ea_value FROM eav_addon_date WHERE parent_id = entity_child.id AND ea_code = 'CODB'),'%d-%b-%Y')",
							
								//'field' => "(get_eav_addon_date(id,'CODB'))",
								
								'td_attr' => ' class="align_LM" width="10%"',
								
								'is_sort' => 0,
									 
								),
							
							
							
							
							4=>array('th'=>'Mobile ',
								
								'field' => "(get_eav_addon_varchar(id,'COMB'))",
                                                                   
								'td_attr' => ' class="align_LM b" width="10%"',
								
								'is_sort' => 0,
								
								),
							
							//5=>array('th'=>'Address 1 ',
							//	
							//	'field' => "(get_eav_addon_varchar(id,'CORA'))",
							//          
							//	'td_attr' => ' class="align_LM" width="10%"',
							//	
							//	'is_sort' => 0,
							//	
							//	),
							//
							//6=>array('th'=>'Address 2 ',
							//	
							//	'field' => "(get_eav_addon_varchar(id,'CORB'))",
							//                                                            
							//	'td_attr' => ' class="align_LM" width="10%"',
							//	
							//	'is_sort' => 0,
							//	
							//	),
							
							7=>array('th'=>'City ',
								
								'field' => "(get_ecb_sn_by_token(get_eav_addon_exa_token(id,'CORC')))",
                                                        
								'td_attr' => ' class="align_LM clr_gray_9" width="10%"',
								
								'is_sort' => 0,
								
								),
							
							//8=>array('th'=>'Pincode ',
							//	
							//	'field' => "(get_eav_addon_varchar(id,'CORP'))",
							//                                                         
							//	'td_attr' => ' class="align_LM" width="10%"',
							//	
							//	'is_sort' => 0,
							//	
							//	),
							//
							9=>array('th'=>'Type ',
								 
								'field' => "(get_ecb_sn_by_token(get_eav_addon_exa_token(id,'COCG')))",
                                                                
								'td_attr' => ' class="align_LM" width="10%"',
								
								'is_sort' => 1,
								
								),
					
								
                                                    ),
				    
					
                                        
                                       'action' => array('is_action'=>0, 'is_edit' =>1, 'is_view' =>0 ),
                                       
                                       'order_by'   =>'ORDER BY id ASC ' ,
				       
				       
				    #Filter Info
				    
					'custom_filter' => array(  			     						   
							      
									array(  'field_name' => 'Gender:',
									      
										'field_id' => 'cf1', // 
										
										'filter_type' =>'option_list', 
												    
										'option_value'=> $G->option_builder('entity_child_base','token,sn'," WHERE entity_code = 'GE'"),
							    
										'html'=>'  title="Select Type"   data-width="160px"  ',
								    
										'cus_default_label'=>'Show All',
							    
										'filter_by'  => "get_eav_addon_exa_token(id,'COGE')" // main table value
									),
									
									array(  'field_name' => 'Type:',
									      
										'field_id' => 'cf2', // 
										
										'filter_type' =>'option_list', 
												    
										'option_value'=> $G->option_builder('entity_child_base','token,sn'," WHERE entity_code = 'CG'"),
							    
										'html'=>'  title="Select Type"   data-width="160px"  ',
								    
										'cus_default_label'=>'Show All',
							    
										'filter_by'  => "get_eav_addon_exa_token(id,'COCG')" // main table value
									),
									
									
								),   
				       		
                                
				
                                    #Table Info
                                    
                                    'table_name' =>'entity_child',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
				    
									'key_filter'     =>	 " AND  entity_code='CO'  ",
                                
                                    # Communication
                                
                                    'prime_index'   => 1,
                                
                                    # File Include
                                
                                   'js'            => array('is_top'=>1, 'top_js' => 'js/d_series/manage_cms'),
				  
								   
				 #Search_info
				 
				 'search'=> 	array(
							  
							array(  'data'  =>array('table_name' 	=> 'entity_child',
										'field_id'	=> 'id',
										'field_name' 	=> 'get_contact_name(id)',
										'filter'	=> " AND entity_code='CO'" 
									 ),
							      
								'title' 		=> 'Name',										
								'search_key' 		=> "get_contact_name(id)",													       
								'is_search_by_text' 	=> 1, //( For Text search case)	      
							),
							
							array(  'data'  =>array('table_name' 	=> 'entity_child',
										'field_id'	=> 'id',
										'field_name' 	=> "concat(get_contact_name_mobile(id),' (',get_contact_name(id),')')",
										'filter'	=> " AND entity_code='CO'"  
									 ),
							      
								'title' 		=> 'Mobile',										
								'search_key' 		=> 'id',													       
								'is_search_by_text' 	=> 0, //( For Text search case)	      
							),
							
							array(  'data'  =>array('table_name' 	=> 'entity_child',
										'field_id'	=> 'id',
										'field_name' 	=> "concat(id,' (',get_contact_name(id),')')",
										'filter'	=> " AND entity_code='CO'"  
									 ),
							      
								'title' 		=> 'ID',										
								'search_key' 		=> 'id',													       
								'is_search_by_text' 	=> 0, //( For Text search case)	      
							),	
						),
	      
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>1,'page_link'=>'f=contact_eav', 'b_name' => 'Add Contact' ),
								
					'del_permission' => array('able_del'=>1,'user_flage'=>1), 
								
					'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  ''),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'DMPG',
				
				'show_query'=>0,
                            
                            );
	
?>