<?PHP
        	
	$key        	= @$_GET['key'];
	
	$LAYOUT = "layout_full";
				
	$T_SERIES       =   array(
		
                                'table' => 'entity_child',
				 
				'data'  => array(
						 
						//'key_id'=>array('field'=>'id'),
						
						'domain'=> array('field'=>"'http://psgstep.fibenis.org/'"),
						
						//Name_of_the_applicant
						'name_of_the_applicant_title'=>array('field'=>"get_ecb_sn_by_token('NAFN')"),
						'name_of_the_applicant'=>array('field'=>"get_exav_addon_varchar(id,'NAFN')"),


						//Email Address
						'email_address_title'=>array('field'=>"get_ecb_sn_by_token('NAEM')"),
						'email_address'	 =>array('field'=>"get_exav_addon_varchar(id,'NAEM')"),
						
						//Mobile Number
						'mobile_number_title'=>array('field'=>"get_ecb_sn_by_token('NAMN')"),
						'mobile_number'	     =>['field'=>"get_exav_addon_varchar(id,'NAMN')"],
						
						//date Of Birth 
						'dob_title'=>array('field'=>"get_ecb_sn_by_token('NADOB')"),
						'dob'	   =>array('field'=>"date_format(get_exav_addon_date(id,'NADOB'),'%d-%b-%y')"),
						
						// Company / Institution 
						'company_institution_title'=>array('field'=>"get_ecb_sn_by_token('NACI')"),
						'company_institution'=>array('field'=>"get_exav_addon_varchar(id,'NACI')"),
						
						// Highest Education Qualification 
						'high_edu_qualification_title'=>array('field'=>"get_ecb_sn_by_token('NAEQ')"),
						//'high_edu_qualification'	 =>array('field'=>"get_exav_addon_varchar(id,'NAEQ')"),
						'high_edu_qualification'	 =>array('field'=>"get_ecb_sn_by_token(get_exav_addon_varchar(id,'NAEQ'))"),
						
						
						// Current Employer 
						'curr_emplo_title'=>array('field'=>"get_ecb_sn_by_token('NACE')"),
						'curr_emplo'	 =>array('field'=>"get_ecb_sn_by_token(get_exav_addon_varchar(id,'NACE'))"),
						
						//// Current Employer - others
						//'curr_emplo_other_title'=>array('field'=>"get_ecb_sn_by_token('NACEOT')"),
						//'curr_emplo_other'	 =>array('field'=>"get_exav_addon_varchar(id,'NACEOT')"),
						
						//Designation/Role in the Startup/Corporate/Institution
						'des_role_title'=>array('field'=>"get_ecb_sn_by_token('NADRCI')"),
						'des_role'	 =>array('field'=>"get_exav_addon_varchar(id,'NADRCI')"),
						
						
						// Are you currently working full-time
						'are_you_work_full_time_title'=>array('field'=>"get_ecb_sn_by_token('NACW')"),
						'are_you_work_full_time'	 =>array('field'=>"get_exav_addon_num(id,'NACW')"),
						
						//  What is your challenge area?  
						'what_is_challenge_area_title'=>array('field'=>"get_ecb_sn_by_token('NACA')"),
						'what_is_challenge_area'	 =>array('field'=>"get_ecb_sn_by_token(get_exav_addon_varchar(id,'NACA'))"),
						
						//Current Status of your innovation
						'curr_stat_of_inno_title'=>array('field'=>"get_ecb_sn_by_token('NACSOI')"),
						'curr_stat_of_inno'	 =>array('field'=>"get_ecb_sn_by_token(get_exav_addon_varchar(id,'NACSOI'))"),
						
						////Provide the [tentative] title of your innovation
						'pro_the_title_of_inno_title'=>array('field'=>"(SELECT sn FROM entity_child_base WHERE entity_code ='NA' AND token='TITLE')"),
						'pro_the_title_of_inno'	 =>array('field'=>"get_exav_addon_varchar(id,'TITLE')"),
						
						// Provide a summary of your solution to the problem
						'prov_summ_of_your_sol_title'=>array('field'=>"get_ecb_sn_by_token('NAPS')"),
						'prov_summ_of_your_sol'	 =>array('field'=>"get_exav_addon_text(id,'NAPS')"),
						
						// Kindly upload your 2 page idea summary
						'upload_idea_summ_title'=>array('field'=>"get_ecb_sn_by_token('NAKUY')"),
						'upload_idea_summ'=>array('field'=>"get_exav_addon_varchar(id,'NAKUY')"),
						
						//Have you participated in PSG Nanochallenge before?
						'part_in_psg_nano_before_title'=>array('field'=>"get_ecb_sn_by_token('NAHYP')"),
						'part_in_psg_nano_before'	 =>array('field'=>"get_exav_addon_num(id,'NAHYP')"),
						
						//Place
						'place_title'=>array('field'=>"get_ecb_sn_by_token('NAPLA')"),
						'place'=>array('field'=>"get_exav_addon_varchar(id,'NAPLA')"),
						
						
						
						
						
						
						
						
						
						
						//toogle _switch
						//'appli_is_plan_to_pur_title'=>array('field'=>"get_ecb_sn_by_token('NP9ES2')"),
						//'appli_is_plan_to_pur'	 =>array('field'=>"get_exav_addon_num(id,'NP9ES2')"),
						
						//simple_list
						//'marking_a_model_title'=>array('field'=>"get_ecb_sn_by_token('NP3WCO2')"),
						//'marking_a_model'	 =>array('field'=>"get_exav_addon_num(id,'NP3WCO2')"),
						
						//upload_document
						//'att_copy_of_itr_title'=>array('field'=>"get_ecb_sn_by_token('NP1IT')"),
						//'att_copy_of_itr'=>array('field'=>"get_exav_addon_varchar(id,'NP1IT')"),
						
						
						),	
				
				'image_path' => '../../../images/logo/nano_logo.png',
				'key_id' => 'id',
				
				'key_filter'=>' ',
				
				'show_query'=>0,
				
				# if data stored in array way like [ ['ins','1'],['ins B',2]]
  
                                'template'       => 'def/nano/tx.html',
				
				// save data 
				'save_as'=> array(
						
						array('type'=>'pdf',
						      'file_name'=>'home',
						      'path'=>'def/nano/')
						
					)

                            );
	
	
    
?>