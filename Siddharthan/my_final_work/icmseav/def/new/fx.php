<?PHP 

    $F_SERIES   =   array(
                    
                        #form title
                        'title'                     =>  'New Form',

                        #table name
                        'table _name'               =>  'entity_child',

                        #Unique Key ID
                        'key_id'                    =>  'id',

                        #To show query
                        'show_query'                =>  1,

                        #Tab view of multiple forms
                        'divider'                   =>  'tab',

                        #message to display
                        'flat_message'              =>  "New Data Successfully Added!!!",
                        
                        #To display particluar Data added...
                        #'prime_index'              =>  1,

                        #To avoid transcation key on url bar
                        'avoid_trans_key_direct'    =>  1,

                        #back button

                        'back_to'                   =>  array(

                                                            'is_back_button'    =>  1,
                                                            'back_link'         =>  '?dx=new',
                                                            'BACK_NAME'         =>  'Back',

                                                        ),


                        #user id
                        'is_user_id'                =>  'user_id',

                        #Form Fields...

                        'data'                      =>  array(

                                                            '1' =>  array(

                                                                        'field_name'            =>  'Name',
                                                                        'field_id'              =>  'exa_value',
                                                                        'type'                  =>  'text',
                                                                        'is_mandatory'          =>  1,
                                                                        'hint'                  =>  'Enter Name',
                                                                        'parent_field_id'       =>  'parent_id',
                                                                        'child_table'           =>  'exav_addon_varchar',
                                                                        'child_attr_field_id'   => 'exa_token',
                                                                        'child_attr_code'       =>  'NWNA',

                                                            ),

                                                            '2' =>  array(

                                                                        'field_name'            =>  'Gender',
                                                                        'field_id'              =>  'exa_value',
                                                                        'type'                  =>  'option',
                                                                        'option_data'           =>  '<option value = Male>Male</option>
                                                                                                    <option value  = Female>Female</option>',
                                                                        'avoid_default_option'  =>  1,
                                                                        'is_mandatory'          =>  1,
                                                                        'parent_field_id'       =>  'parent_id',
                                                                        'child_table'           =>  'exav_addon_varchar',
                                                                        'child_attr_field_id'   => 'exa_token',
                                                                        'child_attr_code'       =>  'NWGN',

                                                            ),


                                                            '3' =>  array(

                                                                        'field_name'            =>  'Age',
                                                                        'field_id'              =>  'exa_value',
                                                                        'type'                  =>  'text',
                                                                        'allow'                 =>  'd2',
                                                                        'avoid_default_option'  =>  1,
                                                                        'is_mandatory'          =>  1,
                                                                        'parent_field_id'       =>  'parent_id',
                                                                        'child_table'           =>  'exav_addon_varchar',
                                                                        'child_attr_field_id'   => 'exa_token',
                                                                        'child_attr_code'       =>  'NWAG',


                                                            ),

                                                            '4' =>  array(

                                                                        'field_name'            =>  'Date',
                                                                        'field_id'              =>  'exa_value',
                                                                        'type'                  =>  'date',
                                                                        'is_mandatory'          =>  1,
                                                                        'parent_field_id'       =>  'parent_id',
                                                                        'child_table'           =>  'exav_addon_date',
                                                                        'child_attr_field_id'   => 'exa_token',
                                                                        'child_attr_code'       =>  'NWDA',


                                                            ),

                                                            '5' =>  array(

                                                                        'field_name'            =>  'Purpose of Visit',
                                                                        'field_id'              =>  'exa_value',
                                                                        'type'                  =>  'textarea',
                                                                        'parent_field_id'       =>  'parent_id',
                                                                        'child_table'           =>  'exav_addon_varchar',
                                                                        'child_attr_field_id'   => 'exa_token',
                                                                        'child_attr_code'       =>  'NWVI',


                                                            ),


                                                            '6' =>  array(

                                                                        'field_name'            =>  'Email',
                                                                        'field_id'              =>  'exa_value',
                                                                        'type'                  =>  'w30[@.]',
                                                                        'parent_field_id'       =>  'parent_id',
                                                                        'child_table'           =>  'exav_addon_varchar',
                                                                        'child_attr_field_id'   => 'exa_token',
                                                                        'child_attr_code'       =>  'NWEM',


                                                            ),

                          ),

                          'default_fields'    =>  array(

                            'entity_code'   =>  "'NW'",

                    ),




    );

?>