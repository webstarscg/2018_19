<?PHP


$D_SERIES = array(
	
	'title' => 'Innovation',

	#query display depend on the user

	'is_user_base_query' => 0,
        
        'gx'=>1,

	#table data

	'data' => array(
	    
//		0 => array(
//                                'th' => 'ID',
//                                
//                                'field' => "id",			
//                                
//                                'attr' =>['class'=>'label_grand_father align_LM',
//                                                    'width'=> '20%'
//                                        ] 
//		),

		1 => array(
                                'th' => 'Innovation Title',
                                
                                'field' => "get_exav_addon_varchar(id,'FAINOTIT')",			
                                
                                'attr' =>['class'=>'label_grand_father align_LM',
                                                    'width'=> '20%'
                                        ] 
		),
                
                3 => array(
                                'th' => 'Applicant',
                                
                                'field' => "concat('<b>',get_user_internal_name(user_id),'</b><br><span class=\"clr_gray_6 txt_size_11\" >',get_user_internal_email(user_id),'</span>')",			
                                
                                'attr' =>['class'=>' align_LM clr_gray_4',
                                                    'width'=> '10%'
                                        ] 
		),
                
                2 => array(
                                'th' => 'App. Type',
                                
                                'field' => "get_ecb_sn_by_token(get_exav_addon_varchar(id,'FACT'))",			
                                
                                'attr' =>['class'=>' align_LM clr_gray_9',
                                                    'width'=> '5%'
                                        ] 
		),
                
                
              
                
                20 => array(
                                'th' => 'Stage',
                                
                                'field' => "get_ecb_sn_by_token(get_exav_addon_varchar(id,'FAPROSTA'))",			
                                
                                'attr' =>['class'=>' align_LM txt_size_11 clr_gray_a',
                                                    'width'=> '10%'
                                        ] 
		),
		
                5 => array(
                                'th' => 'Is Startup',
                                
                                'field' => "concat(get_exav_addon_varchar(id,'FAIS'),'[S]',IFNULL(get_exav_addon_varchar(id,'FASN'),''))",			
                                
                                'attr' =>['class'=>' align_CM',
                                                    'width'=> '8%'
                                        ],
                                
                                'filter_out' => function($data_in){
                                    
                                    $temp = explode('[S]',$data_in);
                                    
                                    $result = ($temp[0]=='TRUE')?'<i class="fa fa-circle clr_green txt_size_2em" aria-hidden="true"></i>
':'<i class="fa fa-circle-o clr_red txt_size_2em" aria-hidden="true"></i>';
                                    
                                    return $result;
                                }
		),
                
                
                6=>array(       'th'=>'Status ',
								 
				'td_attr' => ' class="align_LM clr_gray_a" width="5%"',
                                
                                'field'	=> "concat(id,':',get_ecb_sn_by_token(get_exav_addon_exa_token(entity_child.id,'FASA')),':',
						   get_exav_addon_exa_token(entity_child.id,'FASA'),':',get_ec_parent_id_ifnull(id,'PA'))",
								
                                'filter_out'=>function($data_in){
                                                                            
					$temp = explode(':',$data_in);
				
					if(($temp[3]!=0)&&($temp[2]!='SASU')&&($temp[2]!='SAVF')){
						
						return '<span class=txt_size_11 clr_gray_b txt_upper>'.$temp[1].'</span><br />'.
                                                       '<a class="pointer" onclick="JavaScript:change_status('.$temp[0].');">'.
                                                       '<i class="fa fa-chevron-circle-right clr_green txt_size_14" aria-hidden="true">&nbsp;</i>Submit'.
                                                       '</a>';
					}
                                        elseif($temp[2]==NULL){
					        return '<span class="clr_gray_a">NA</span>';
					}
                                        else{
						return '<span class=txt_size_11 clr_gray_c brdr_bottom >'.$temp[1].'</span>';
					}
				},
                                                                 
			),
		
		 8=>array(       'th'=>'View ',
								 
				'td_attr' => ' class="align_LM no_wrap" width="5%"',
                                
                                'field'	=> "concat(id,':',get_exav_addon_exa_token(entity_child.id,'FASA'))",
								
                                'filter_out'=>function($data_in){
                                                                            
					$temp = explode(':',$data_in);
				
				if($temp[1]==NULL){
                                        
                                            return '<span class="clr_gray_a">NA</span>';
                                }
				else{
						return '<a class="pointer clr_gray_b txt_size_11" onclick="JavaScript:view_project('.$temp[0].');">'.
                                                '<i class="fa fa-file-o clr_dark_blue txt_size_13" aria-hidden="true"></i>&nbsp;'.
                                                'View</a>';
                                }
					
                            },
                                                                
								           
			),
		
		7=>array(       'th'=>'Review ',
			 
				'th_attr'=>' colspan=2 ',
								 
				'td_attr' => ' class="align_CM" width="5%"',
                                
                                'field'	=> "id",
								
                                'filter_out'=>function($data_in){
						$data_out = array('id'   => $data_in,
						'link_title'=>'Add Review',
						'title'=>'Add Review',
						'is_fa'=>' fa fa-plus-square-o clr_red fa-lg',
						'src'=>"?fx=review&menu_off=1&mode=simple&default_addon=$data_in",
						'style'=>"border:none;width:100%;height:600px;",
						'refresh_on_close'=>0);
						return json_encode($data_out);
					    },
                                                                        
                                'js_call'=>'d_series.set_nd'
								           
			),
		
		
		
		12=>array(	'th'	=> '',
								     
				'field'	=> "id",
								
				'filter_out'=>function($data_in){
                                                $data_out = array('id'   => $data_in,
						'link_title'=>'View Review',
						'is_fa'=>'fa fa-folder-o clr_red fa-lg',
						'title'=>'View Review',
						'src'=>"?dx=review&menu_off=1&mode=simple&default_addon=$data_in",
						'style'=>"border:none;width:100%;height:600px;",
						'refresh_on_close'=>0);
						return json_encode($data_out);
					    },
												 
				    'attr' => ['class'=>'align_CM'],
                                                                        
				    'js_call'=>'d_series.set_nd'
			 ),
		
		
		
	),

	#Sort Info

	'action' => array('is_action' => 1, 'is_edit' => 0, 'is_view' => 0),

	'order_by' => 'ORDER BY id ASC',

	#Table Info

	'table_name' => 'entity_child',

	'key_id' => 'id',

	'key_filter' => " AND entity_code='FA'",

	# Default Additional Column

	'is_user_id' => 'user_id',

	# Communication

	'prime_index' => 1,
        
        'is_narrow_down' => 1,
        
        'custom_filter' => array(  			     						   
							      
									array(  'field_name' => 'Status:',
									      
										'field_id' => 'cf1', // 
										
										'filter_type' =>'option_list', 
												    
										'option_value'=> $G->option_builder('entity_child_base','token,sn'," WHERE entity_code = 'SA'"),
							    
										'html'=>'  title="Select Type"   data-width="100px"  ',
								    
										'cus_default_label'=>'Show All',
							    
										'filter_by'  => "get_exav_addon_exa_token(id,'FASA')" // main table value
									),
									
									array(  'field_name' => 'Stage:',
									      
										'field_id' => 'cf2', // 
										
										'filter_type' =>'option_list', 
												    
										'option_value'=> $G->option_builder('entity_child_base','token,sn'," WHERE entity_code = 'PS' ORDER BY id"),
							    
										'html'=>'  title="Select Type"   data-width="100px"  ',
								    
										'cus_default_label'=>'Show All',
							    
										'filter_by'  => "get_exav_addon_varchar(id,'FAPROSTA')" // main table value
									),
									
									array(  'field_name' => 'Application Type:',
									      
										'field_id' => 'cf3', // 
										
										'filter_type' =>'option_list', 
												    
										'option_value'=> $G->option_builder('entity_child_base','token,sn'," WHERE entity_code = 'I1' ORDER BY sn"),
							    
										'html'=>'  title="Select Type"   data-width="100px"  ',
								    
										'cus_default_label'=>'Show All',
							    
										'filter_by'  => "get_exav_addon_varchar(id,'FACT')" // main table value
									),
									
								
									
								),      
				       		
//        'search'=> 	array(
//							  
//							array(  'data'  =>array('table_name' 	=> 'entity_child',
//										'field_id'	=> 'id',
//										'field_name' 	=> "get_name(id)",
//                                                                                'filter'	=> "AND entity_code ='CO'"  
//									 ),
//							      
//								'title' 		=> 'Name',										
//								'search_key' 		=> "user_id",													       
//								'is_search_by_text' 	=> 0, //( For Text search case)	      
//							),
//								
//						),
	'custom_action' => array(	
		
		//array('fun_name'=>'send_user_mail','action_name'=>'Reset Password','html'=>'class=" hint--left" data-hint="Reset Password"')

	),


	# File Include

	'js' => array('is_top' => 1, 'top_js' => "def/innovation/dx"),

	
        # user ineternal
        
	#check_field

	'check_field' => array('id' => @$_GET['id']),								

	'add_button' => array('is_add' => 0, 'page_link' => 'f=user_neutral', 'b_name' => '' ),

	'del_permission' => array('able_del' => 1,'user_flage' => 1), 



	'date_filter' => array('is_date_filter' => 0, 'date_field' => 'timestamp_punch'),	

	#export data
	
	'export_csv' => array('is_export_file' => 0, 'button_name' => 'Create CSV', 'csv_file_name' => 'csv/log_'.time().'.csv'),

	'page_code' => 'DUSI',
        
        'search_filter_off' => 1,
        
        'show_query' => 0

);

?>