<?PHP

//if(isset($_GET['default_addon'])){
//    
//    $find_id = $rdsql->exec_query("SELECT id FROM entity_child WHERE entity_code = 'NP' AND user_id = $USER_ID","Selection Fails");
//	    
//    $value = $rdsql->data_fetch_row($find_id);
//    
//    if($value[0]==NULL){
//        
//        $set_query  = "INSERT INTO entity_child (entity_code,user_id) VALUES ('NP',$USER_ID)";
//       
//        $exe_query= $rdsql->exec_query($set_query,'new_entry');
//       
//        $get_last_id = $rdsql->last_insert_id('entity_child');
//	
//	 header('location:?fx=prayas&menu_off=1&key='.$get_last_id);
//
//    }
//}										

$D_SERIES = array(
	
	'title' => 'NIDHI PRAYAS',

	'is_user_base_query' => 0,
        
        'gx'=>1,

	#table data

	'data' => array(
	    
//		0 => array(
//                                'th' => 'ID',
//                                
//                                'field' => "id",			
//                                
//                                'attr' =>['class'=>'label_grand_father align_LM',
//                                                    'width'=> '5%'
//                                        ] 
//		),

//		1 => array(
//                                'th' => 'Reference Number',
//                                
//                                'field' => "get_exav_addon_varchar(id,'NPRF')",			
//                                
//                                'attr' =>['class'=>'label_grand_father align_LM',
//                                                    'width'=> '10%'
//                                        ] 
//		),
		
		10=>array('th'=>'Booked On',
								
			//'field' => "date_format(updated_on,'%d-%b-%Y /  %T')",
			
			'field' => "date_format(updated_on,'%d-%b-%Y')",
								
			'td_attr' => ' class="align_LM no_wrap" width="5%"',
								
			'is_sort' => 1,
									 
			),
		
		
		3=>array('th'=>'Image',
									 
								'field'=>"get_exav_addon_varchar(id,'NP1POA')",
									 
								'td_attr' => 'class="label_child align_CM" width="5%"',
								
								'filter_out'   =>	function($data_out){
									
                                                                        $temp = explode(',',$data_out);
                                                                        
									return '<img class="w_25" src="'. $temp[0].'">';
									
									},
									 
								),
		2 => array(
                                'th' => 'Applicant',
                                
                                'field' => "concat('<b>',get_exav_addon_varchar(id,'NP1NOA'),'</b><br><span class=\"clr_gray_6 txt_size_11\" >',get_exav_addon_varchar(id,'NP1EM'),'</span>')",			
                                
                                'attr' =>['class'=>' align_LM clr_gray_4',
                                                    'width'=> '10%'
                                        ] 
		),
		
		
                
                
//		3 => array(	'th' => 'Email',
//                                
//                                'field' => "get_exav_addon_varchar(id,'EI1EA')",			
//                                
//                                'attr' =>['class'=>'label_grand_father align_LM',
//                                                    'width'=> '10%'
//                                        ] 
//		),
//		
		4 => array(	'th' => 'Mobile',
                                
                                'field' => "get_exav_addon_varchar(id,'NP1MB')",			
                                
                                'attr' =>['class'=>'label_grand_father align_LM',
                                                    'width'=> '10%'
                                        ] 
		),
		
//		5 => array(	'th' => 'Aadhar No',
//                                
//                                'field' => "get_exav_addon_varchar(id,'EI1AN')",			
//                                
//                                'attr' =>['class'=>'label_grand_father align_LM',
//                                                    'width'=> '10%'
//                                        ] 
//		),
//		
//		6 => array(	'th' => 'Education',
//                                
//                                'field' => "get_exav_addon_varchar(id,'EI1ED')",			
//                                
//                                'attr' =>['class'=>'label_grand_father align_LM',
//                                                    'width'=> '10%'
//                                        ] 
//		),
//		
		
		
		7 => array(	'th' => 'Place',
                                
                                'field' => "get_exav_addon_varchar(id,'NP10PL')",			
                                
                                'attr' =>['class'=>'label_grand_father align_LM',
                                                    'width'=> '10%'
                                        ] 
		),
		
		10 => array(	'th' => 'Status',
                                
                                'field' => "get_ecb_sn_by_token(get_exav_addon_exa_token(entity_child.id,'NPST'))",			
                                
                                'attr' =>['class'=>'label_grand_father align_LM',
                                                    'width'=> '10%'
                                        ] 
		),
		
		
				
		8=>array(       'th'=>'View ',
								 
				'td_attr' => ' class="align_LM no_wrap" width="5%"',
                                
                                'field'	=> "id",
								
                                'filter_out'=>function($data_in){
                                              
					      return '<a class="pointer clr_gray_b txt_size_11" onclick="JavaScript:view_prayas('.$data_in.');">'.
                                                '<i class="fa fa-file-o clr_dark_blue txt_size_13" aria-hidden="true"></i>&nbsp;'.
                                                'View</a>';
                            },
                                                                
								           
			),
		
		9=>array(       'th'=>'Edit ',
								 
				'td_attr' => ' class="align_LM no_wrap" width="5%"',
                                
                                'field'	=> "concat(id,':',get_exav_addon_exa_token(entity_child.id,'NPST'))",
								
                                'filter_out'=>function($data_in){
                                            
					    $temp = explode(':',$data_in);  
					     
					     if($temp[1]=='SANW'){ 
					      return '<a class="pointer clr_gray_b txt_size_11" onclick="JavaScript:edit_prayas('.$data_in.');">'.
                                                '<i class="fa fa-edit clr_red txt_size_13" aria-hidden="true"></i>&nbsp;'.
                                                'Edit</a>';
						
					     }
					     
					     else{
							return '<span class="clr_gray_a">NA</span>';
						
						
						}
					     
                            },	
                                                                
								           
			),
		
	),

	#Sort Info

	'action' => array('is_action' => 0, 'is_edit' => 0, 'is_view' => 0),

	'order_by' => 'ORDER BY id ASC',

	#Table Info

	'table_name' => 'entity_child',

	'key_id' => 'id',

	'key_filter' => " AND entity_code='NP' ",

	# Default Additional Column

	'is_user_id' => 'user_id',

	# Communication

	'prime_index' => 1,
        
        'is_narrow_down' => 0,
    
	# File Include

	'js' => array('is_top' => 1, 'top_js' => "def/prayas/dx"),

	#check_field

	'check_field' => array('id' => @$_GET['id']),								

	'add_button' => array('is_add' => 0, 'page_link' => 'f=user_neutral', 'b_name' => '' ),

	'del_permission' => array('able_del' => 0,'user_flage' => 0), 
	
	'date_filter' => array('is_date_filter' => 1, 'date_field' => 'updated_on'),	

	#export data
	
	'export_csv' => array('is_export_file' => 0, 'button_name' => 'Create CSV', 'csv_file_name' => 'csv/log_'.time().'.csv'),

	'page_code' => 'NPRA',
        
        'search_filter_off' => 1,
        
        'show_query' => 0

);

?>