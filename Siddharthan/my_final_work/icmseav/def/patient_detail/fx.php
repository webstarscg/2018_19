<?PHP 

    $F_SERIES   =   array(

                        #Desk title

                        'title'         => 'Patient',

                        #table Name

                        'table_name'    =>  'patient',

                        #Unique Key ID

                        'key_id'        =>  'id',

                        #'session_off'   =>  1,

                        #'show_query'     =>  1,

                        'divider'   =>  'tab',

                        #'flat_message'  =>  'Patient Added Successfully',

                        'prime_index'   =>  1,

                        #'avoid_trans_key_direct'    =>  1,

                        'back_to'  => array( 
                                             'is_back_button' =>1, 
                                             'back_link'=>'?dx=patient_detail',
                                             'BACK_NAME'=>'Back'
                                         ),

                        'default_fields'    =>  array(

                                                    'user_id'   =>  '1',

                                                ),

                        #Table Field

                        'data'          =>  array(

                                                '10' =>  array(

                                                            'field_name'    => 'Patient Data Heading',
                                                            'type'          => 'heading',
                                                ),

                                                '9' =>  array(

                                                            'field_name'    => 'Patient Data Sub Heading',
                                                            'type'          => 'sub_heading',
                                                ),

                                                '1' =>  array(

                                                            'field_name'    => 'Patient Name',
                                                            'field_id'      => 'name',
                                                            'type'          => 'text',
                                                            'is_mandatory'  => 1,
                                                            'hint'          => 'Enter Name',
                                                            #'is_hide'       =>  1,
                                                ),

                                                '2' =>  array(

                                                            'field_name'    => 'Age',
                                                            'field_id'      => 'age',
                                                            'type'          => 'text',
                                                            'is_mandatory'  => 1,
                                                            'allow'         =>  'd2'

                                                    
                                                ),

                                                '3' =>  array(

                                                            'field_name'    => 'Gender',
                                                            'field_id'      => 'gender',
                                                            'type'          => 'text',
                                                            'is_mandatory'  => 1,
                                                            'allow'         => 'w10',

                                                    
                                                ),

                                                '4' =>  array(

                                                            'field_name'    => 'Town',
                                                            'field_id'      => 'town',
                                                            'type'          => 'textarea'
                                                    
                                                ),

                                                '5' =>  array(

                                                            'field_name'    =>  'Date',
                                                            'field_id'      =>  'date',
                                                            'type'          =>  'date',
                                                            'default_date'  =>  '25.12.1996',
                                                            'year_range'    =>  '1996 : 2019',
                                                            'is_mandatory'  => 1,
                                                    
                                                ),

                                                '6' =>  array(

                                                            'field_name'    => 'Visits',
                                                            'field_id'      => 'visits',
                                                            'type'   => 'file',

                                                ),

                                                '7' =>  array(

                                                            'field_name'    =>  'Email',
                                                            'field_id'      =>  'email',
                                                            'type'          =>  'text',
                                                            'allow'         =>  'w20[@.]'
                                            
                                                ),

                                                '8' =>  array(

                                                            'field_name'    =>  'Option',
                                                            'type'          =>  'option',
                                                            'option_data'   =>  $G->option_builder('entity_patient','code,sn','WHERE 1=1'),
                                                ),

                                                '9' =>  array(

                                                            'field_name'    =>  'Text',
                                                            'type'          =>  'textarea_editor',
                                                           
                                                ),

                                                '10' => array(

                                                            'field_name'    =>  'Code',
                                                            'type'          =>  'code_editor',

                                                ),

                                                '11' => array(

                                                            'field_name'    =>  'Toggle Switch',
                                                            'type'          =>  'toggle',
                                                            'is_round'      =>  1,
                                                            'on_label'      =>  'On',
                                                            'off_label'     =>  'Off',
                                                            'is_default_on' =>  1,  

                                                ),

                                                '12' => array(

                                                            'field_name'    =>  'Table',
                                                            'type'          =>  'fibenistable',
                                                            'is_fibenistable'=> 1,
                                                            'colHeaders'=> array(array(
                                                                'column'    => 'Name',
                                                                'width'     => '50',
                                                                'type'      => 'text',
                                                                                                    ),
                                                            array(
                                                                'column'    => 'Purpose',
                                                                'width'     => '50',
                                                                'type'      => 'text',
                                                                                                    ),
                                                        )
                                                ),


                        ),

    );

?>