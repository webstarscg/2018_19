<?PHP


$D_SERIES = array(
	
	'title' => 'Innovation',

	#query display depend on the user

	'is_user_base_query' => 0,
        
        'gx'=>1,

	#table data

	'data' => array(

		1 => array(
                                'th' => 'Innovation Title',
                                
                                'field' => "get_exav_addon_varchar(id,'FAINOTIT')",			
                                
                                'attr' =>['class'=>'label_grand_father align_LM',
                                                    'width'=> '18%'
                                        ] 
		),
                
                
                2 => array(
                                'th' => 'App. Type',
                                
                                'field' => "get_ecb_sn_by_token(get_exav_addon_varchar(id,'FACT'))",			
                                
                                'attr' =>['class'=>' align_LM',
                                                    'width'=> '8%'
                                        ] 
		),
                
                
                3 => array(
                                'th' => 'Name',
                                
                                'field' => "get_user_internal_name(user_id)",			
                                
                                'attr' =>['class'=>' align_LM',
                                                    'width'=> '8%'
                                        ] 
		),
                
                4 => array(
                                'th' => 'Email',
                                
                                'field' => "get_user_internal_email(user_id)",			
                                
                                'attr' =>['class'=>' align_LM',
                                                    'width'=> '10%'
                                        ] 
		),
                
                
                5 => array(
                                'th' => 'Startup',
                                
                                'field' => "concat(get_exav_addon_varchar(id,'FAIS'),'-',IFNULL(get_exav_addon_varchar(id,'FASN'),''))",			
                                
                                'attr' =>['class'=>' align_LM',
                                                    'width'=> '8%'
                                        ] 
		),
                
                
                6 => array(
                                'th' => 'Status',
                                
                                'field' => "'--'",			
                                
                                'attr' =>['class'=>' align_LM',
                                                    'width'=> '8%'
                                        ] 
		),
                

	),

	#Sort Info

	

	'action' => array('is_action' => 1, 'is_edit' => 1, 'is_view' => 0),

	'order_by' => 'ORDER BY id ASC',

	#Table Info

	'table_name' => 'entity_child',

	'key_id' => 'id',

	'key_filter' => " AND entity_code='FA' ",

	# Default Additional Column

	'is_user_id' => 'user_id',

	# Communication

	'prime_index' => 1,

	'custom_action' => array(	
		
		//array('fun_name'=>'send_user_mail','action_name'=>'Reset Password','html'=>'class=" hint--left" data-hint="Reset Password"')

	),


	# File Include

	#'js' => array('is_top' => 1, 'top_js' => "def/innovation/dx"),

	
        # user ineternal
        
        'custom_filter' => array(
            


	),
        
	#check_field

	'check_field' => array('id' => @$_GET['id']),								

	'add_button' => array('is_add' => 0, 'page_link' => 'f=user_neutral', 'b_name' => '' ),

	'del_permission' => array('able_del' => 1,'user_flage' => 1), 



	'date_filter' => array('is_date_filter' => 0, 'date_field' => 'timestamp_punch'),	

	#export data
	
	'export_csv' => array('is_export_file' => 0, 'button_name' => 'Create CSV', 'csv_file_name' => 'csv/log_'.time().'.csv'),

	'page_code' => 'DUSI'

);

?>