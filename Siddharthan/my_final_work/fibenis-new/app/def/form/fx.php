<?PHP

	if(get_config('db_name')){

		header('Location: index.php');

	}

	$F_SERIES	=   array(
						
				'title'	     => 'Configuraion',
						
				# columns
				'data'	=>  array(

							'0'	=>	array(
									'field_name'			=>	'Fibenis Configuration',
									'type'					=>	'heading',		
								),

							'1' =>	array(
									'field_name'			=> 	'Host',
									'type'                  =>  'text',
									'hint'                  =>  'Enter Your Host Name',
									'is_mandatory'          =>  1,
								),
						
							'2' =>	array(
									'field_name'			=> 	'Database Name',
									'type'                  =>  'text',
									'hint'                  =>  'Enter Your DB Name',
									'is_mandatory'          =>  1,                        
								),

							'3'	=>	array(
									'field_name'			=> 	'User Name',
									'type'                  =>  'text',
									'hint'                  =>  'Enter Your User Name',
									'is_mandatory'          =>  1,
								),
								
							'4'	=>	array(
									'field_name'			=> 	'Password',
									'type'                  =>  'password',
									'hint'                  =>  'Enter Your Password',
									'is_mandatory'          =>  0,
								),

					),//data array ends here...
				
				'key_id'           	=> 'id',    				
				'is_user_id'   		=> 'user_id	',
				'session_off'       =>  1,
				
				# communication		    
				'back_to'  			=> array( 
										'is_back_button' => 1,
										'back_link'=>'#'),       				
				'show_query'  		=> 0 , //for debugging	
				
				'flat_message'      => 'Configured Successfully !!!',

				'before_add_update'	=>1
				
			);

	function before_add_update(){
		
		global $G,$PASS_ID,$LIB_PATH;

			$value  = [];

			$value['host'] 		= $_POST['X1'];
			$value['db_name'] 	= $_POST['X2'];
			$value['user'] 		= $_POST['X3'];
			$value['password'] 	= $_POST['X4'];

			$link = mysqli_connect($value['host'], $value['db_name'], $value['user'], $value['password']);

			if (!$link) {

				header('Location: ?fx=form');

				exit;
			}
			
			$dir = __DIR__;

			$options = array(
				"filename"=>"$dir/tx.html", 
				"debug"=>0,
				"global_vars"=>0,
				"loop_context_vars"=>1
			);

			$T   =  new Template($options);

			$T   ->  AddParam('host', $value['host']);
			$T   ->  AddParam('db_name', $value['db_name']);
			$T   ->  AddParam('user', $value['user']);
			$T   ->  AddParam('password', $value['password']);
			
			$content = $T->Output();

			$file = "fE7zRhHqYfSLT9CRm55cBPGHjAGuhqhhjKGSZrB.php";

			$fp = fopen($file, 'w');

			fwrite($fp,$content);	

			fclose($fp);
		
			header('Location: index.php');

	}//before add update ends...

?>