<?PHP
                        
    $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Time Range',
				
				#Table field
                    
				'data'	=>   array(
						   
						    '10' =>array('field_name'=>'Parent',
							     
							     'field_id'=>'parent_id',
							     
							     'type'=>'option',
							     
							     'is_mandatory'=>0,
							     
							     'option_data'=>$G->option_builder('entity_child','id,code',"WHERE  entity_code='TS' AND parent_id=0 "),
							     
							     'avoid_default_option'=>1,
							     
							     'input_html'=>'class="w_200"',                                                               
							     
							     ),
						   
						   
						   
						    '1' =>array( 'field_name'=> 'Entity',
                                                               
                                                               'field_id' => 'entity_code',
                                                               
                                                               'type' => 'option',
                                                               
                                                               'option_data'=>$G->option_builder('entity','code,sn'," WHERE code='TS' "),
							       
							       'avoid_default_option'=>1,
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_200"',
                                                               
                                                               
                                                               
                                                               ),
                                                   
                                                  
						   
						   
//						   '8' =>array('field_name'=>'Child Code',
//                                                               
//                                                               'field_id'=>'code',
//                                                               
//                                                               'type'=>'text',
//                                                               
//                                                               'is_mandatory'=>0,
//                                                               
//                                                               'input_html'=>'class="w_100"',                                                               
//                                                               
//                                                               ),
						   
				   
						   '2' =>array('field_name'=>'Short Name',
                                                               
                                                               'field_id'=>'ea_value',
                                                               
                                                               'type'=>'text',
							       
							        'child_table'         => 'eav_addon_varchar', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => 'TSNA',           // attribute code
							       
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_150"'
                                                               
                                                               ),                                               
						   
						    '3' =>array('field_name'=>'Start Time',
                                                               
                                                               'field_id'=>'ea_value',
                                                               
                                                               'type'=>'hms',
							       
							        'child_table'         => 'eav_addon_time', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => 'TSLL',           // attribute code
							       
                                                               'is_mandatory'=>1
                                                               
                                                              
                                                               
                                                               ),
						    
						    '4' =>array('field_name'=>'End Time',
                                                               
                                                               'field_id'=>'ea_value',
                                                               
                                                               'type'=>'hms',
							       
							        'child_table'         => 'eav_addon_time', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => 'TSUL',           // attribute code
							       
                                                               'is_mandatory'=>1
                                                               
                                                              
                                                               
                                                               ), 
                                                   
                                                   '5' => array( 'field_name'=> 'Line Order', 
                                                               
                                                                'field_id' => 'line_order',
                                                               
                                                                'type' => 'text',
                                                                
                                                                'is_mandatory'=>0,
                                                                
                                                                'allow'        => 'd3',
                                                                
                                                                'input_html'=>' class="w_50"  '
                                                                
                                                                ),
				    
                                ),
                                    
				#Table Name
				
				'table_name'    => 'entity_child',
				
				#Primary Key
                                
			        'key_id'        => 'id',
                                
				# Default Additional Column
                                
				'is_user_id'       => 'created_by',
								
				# Communication
								
				'add_button' => array( 'is_add' =>1,'page_link'=>'f=entity_child', 'b_name' => 'Add Entity child' ),
                     
                                'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?d=time_range', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 2,
                                
				# File Include
                                'after_add_update'	=>0,
				
				'page_code'	=> 'FECA',                                
				
                                
			);

     
?>