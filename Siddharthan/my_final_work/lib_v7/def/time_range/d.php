<?PHP
	
        $D_SERIES       =   array(
                                   'title'=>'Time Range',
                                    
                                    #query display depend on the user
                                    
                                    'gx'	=> 1,
				    
				    #table data
                                    
                                    'data'=> array(
					                
							
							 5=>array('th'=>'Entity_ID',
									 
								'field'=>'id',
									 
								'td_attr' => ' class="label_child align_LM" width="5%"',
								
								'is_sort' => 0
									 
								),
							
							1=>array('th'=>'Lower Limit',
									 
								'field'=>"(SELECT ea_value FROM eav_addon_time WHERE parent_id=entity_child.id AND ea_code = 'TSLL')",
                                                                   
								'td_attr' => ' class="label_father align_LM" width="25%"',
								
								'is_sort' =>0
									 
								),
							
							2=>array('th'=>'Upper Limit',
									 
								'field'=>"(SELECT ea_value FROM eav_addon_time WHERE parent_id=entity_child.id AND ea_code = 'TSUL')",
                                                                   
								'td_attr' => ' class="label_father align_LM" width="25%"',
								
								'is_sort' =>0
									 
								),
					
                                                        3=>array('th'=>'Value',
								 
								'field'=>"(get_eav_addon_varchar(id,'TSNA'))",
								
								'is_sort' => 1,	
								
								'td_attr' => ' width="25%" ',
                                                                            
								),
										
							 6=>array('th'=>'Line Order',
									 
								'field'=>'line_order',
									 
								'td_attr' => ' class="label_child align_LM" width="10%"',
								
								'is_sort' => 1
									 
								),
							
							 
							),
				    
                                    
				    'action' => array('is_action'=>0, 'is_edit' =>1, 'is_view' =>0 ),
                                       
				    'order_by'   =>'ORDER BY id ASC ' ,
				    
				    'custom_filter' => array(  			     						   
							      
									array(  'field_name' => 'Entity:',
									      
										'field_id' => 'cf1', // 
										
										'filter_type' =>'option_list', 
												    
										'option_value'=> $G->option_builder('entity','code,sn'," ORDER BY sn ASC"),
							    
										'html'=>'  title="Select Client"   data-width="160px"  ',
								    
										'cus_default_label'=>'Show All',
							    
										'filter_by'  => "entity_code"  // main table value
									),
							),
				    
				    #Search
				
				'search'=> 	array(
							  
							array(  'data'  =>array('table_name' 	=> 'entity_child',
										'field_id'	=> 'sn',
										'field_name' 	=> 'sn',
										 
									 ),
							      
								'title' 		=> 'Name',										
								'search_key' 		=> 'sn',													       
								'is_search_by_text' 	=> 1, //( For Text search case)	      
							),
								
						),
				
				       		
                                
                                    #Table Info
                                    
                                    'table_name' =>'entity_child',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
                                
                                    # Communication
                                
                                    'prime_index'   => 1,
                                
				
				    'key_filter'	=>" AND entity_code = 'TS' AND parent_id <> 0",
				
				#summary:
				
				'summary_data'=>array(
							array(  'name'=>'No.','field'=>'count(id)','html'=>'class=summary'),
				
				                   ),
				
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>1,'page_link'=>'f=time_range', 'b_name' => 'Add Time Range' ),
								
					'del_permission' => array('able_del'=>1,'user_flage'=>1), 
								
					'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  ''),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 1, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'DECA'
                            
                            );
	
    
?>