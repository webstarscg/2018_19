<?PHP

    //F_series definition:
                            
    $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Entity',
				
				#Table field
                    
				'data'	=>   array(			
						   
                                                   '1' => array( 'field_name'=> 'Code', 
                                                               
                                                                'field_id' => 'code',
                                                               
                                                                'type' => 'text'
                                                                
                                                                
                                                                
                                                                ),
                                                   
                                                   
                                                     
                                                   
                                                    //
                                                    //'2' => array(
                                                    //    
                                                    //    
                                                    //    'field_name'          => 'Handson Table',                                                                
                                                    //    'field_id'            => 'ea_value',				       
                                                    //   
                                                    //
                                                    //    //child table
                                                    //            
                                                    //    'child_table'         => 'eav_addon_varchar', // child table 
                                                    //    'parent_field_id'     => 'parent_id',    // parent field
                                                    //                            
                                                    //    'child_attr_field_id' => 'ea_code',   // attribute code field
                                                    //    'child_attr_code'     => 'TTHA',           // attribute code
                                                    //             
                                                    //    // special config 
                                                    //
                                                    //    'is_plugin'           => 1,                                                                
                                                    //    'type'                => 'handsontable',				       
                                                    //            
                                                    //    // Grid rows
                                                    //    'default_rows_prop'   => ['start_rows'    =>'5', // default number of rows
                                                    //                              'min_spare_rows'=>'2', // empty rows setup
                                                    //                              'max_rows'      =>'10' // maximum allowed rows
                                                    //                             ],	
                                                    //
                                                    //    // Array of Array Input
                                                    //
                                                    //
                                                    //    'colHeaders'          => [ ['column'=>'Property', // Column Header Name
                                                    //                                'width' =>'300',      // Column Width                                               
                                                    //                                // Type ( text,numeric,dropdown) 
                                                    //                                'type'  =>'text'],  
                                                    //                               
                                                    //                               ['column'=>'Value',
                                                    //                                'width' =>'200',      
                                                    //                                'type'  =>'numeric'], 
                                                    //
                                                    //                               ['column'=>'Status',
                                                    //                                'width' =>'100',      
                                                    //                                'type'  =>'select',
                                                    //                                'source'=> ' "ACTIVE", "INACTIVE" ' 
                                                    //                               ], 
                                                    //                                
                                                    //                                  ['column'=>'Status',
                                                    //                                'width' =>'100',      
                                                    //                                'type'  =>'autocomplete',
                                                    //                                'source'=> ' "ACTIVE", "INACTIVE" ' 
                                                    //                               ], 
                                                    //                                
                                                    //                               ['column'=>'Option Data',
                                                    //                                'width' =>'300',      
                                                    //                                'type'  =>'autocomplete',
                                                    //                                //'source'=> ' "ACTIVE", "INACTIVE" ' 
                                                    //                                //'table'=>'communication',
                                                    //                                'source_url'=>'router.php?series=a&action=test&token=ENLI', 
                                                    //                                //'set_new_entry' => 'inc/handson_data/set_data.php'  
                                                    //                               ] 
                                                    //                        ]
                                                    //             
                                                    //            ),
                                                    //
                                                    
                                                    '3' =>array(
                                                                    'field_name'          => 'Fibenis Data',                                                                
                                                                    
                                                                    'field_id'            => 'detail',				       
                                                                    
                                                                    'is_fibenistable'=>1,
                                                                    
                                                                    'type' => 'fibenistable',
                                                               
                                                                    'is_mandatory'        => 0,
                                                                                                                        
                                                                    'default_rows_prop'=>array('start_rows'=>'2','max_row'=>4),
								   
                                                                    'is_index' =>1,
                                                                  
                                                                    'default_data'  => '[[12345,"abcdef",22,{"id":313,"name":"Unit"}]]',
								    
								    'update_route_point'=>[ 'is_update_route_point' => 1,
											    'method' => 'post',
											    'url'    => 'router.php?series=a&action=test&token=FT_TEST&FTUPDATE=1',	
											  ],
								 
								     
                                                                    'colHeaders'=> array(

											 
                                                                                            array(
                                                                                                    'column'    => 'Num 5',
                                                                                                    'width'     => '50',
                                                                                                    'type'      => 'text',
                                                                                                    'attr'      => ' class="align_RM"',
                                                                                                    'allow'     => 'd5',
                                                                                                    
                                                                                            ),
                                                                                          
                                                                                          array(   'column'     => 'Alpha. Num. 10',
                                                                                                    'width'     => '100',
                                                                                                    'type'      => 'text',
												    'allow'      => 'w10',

                                                                                                ),
											  
											  
											   array(   'column'    => 'Option',
                                                                                                    'width'     => '200',
                                                                                                    'type'      => 'date', 
                                                                                                    'data'      => $G->ft_option_builder('entity_attribute','id,entity_code'," ORDER BY id ASC"),
												    'is_default_value' =>0
                                                                                                ),
											   
											   array(   'column'=>'Auto Complete Dynamic',                                                                                             
                                                                                                    'width'=>'150',
                                                                                                    'type'=>'autocomplete',
                                                                                                    'get_data_url'=>'router.php?series=a&action=test&token=FT_TEST',
												   
												    
                                                                                                ),
											   
                                                                                           array(   'column'     => 'Auto Comp. Static',
                                                                                                    'width'      => '150',
                                                                                                    'type'       => 'autocomplete',                                                                                                   
                                                                                                    'get_data_url'=>'router.php?series=a&action=test&token=FT_TEST',
                                                                                                    'restrict_new_entry'=>1,
                                                                                                ),
											   
											     array(   'column'=>'Select Mutliple',
                                                                                                    'width'=>'125',
                                                                                                    'type'=>'multiple_select',                                                                                                   
                                                                                                    'data'=>$G->ft_option_builder('entity','id,sn',""),
												    'input_html' =>'data-container="body" '
                                                                                                ),
											     
											     
											     array(  'column'=>'Date',
                                                                                                     'width'=>'125',
                                                                                                     'type'=>'date',                                                                                                   
                                                                                                   ),
                                                                                                    
                                                                                         ),
								     
								     
                                                                     
                                                                     'is_hide' => 0, 
                                                                     
                                                               ),
                                                    
                                                    
                                                    
                                                     
						   '4' =>array( 'field_name'=> 'Auto Complete', 
                                                               
								'field_id' => 'sn',
                                                               
                                                               'type' => 'auto_complete',
							       
							       'remote_link'=>'router.php?series=a&action=test&token=TTAC',
							   							       
							       'input_html'=>'class="w_200"',
                                                               
							       'is_mandatory'=>1,
							       
							       'input_html'=>'class="w_150""',
								 
                                                               ),
						   
				    
                                ),
                                    
				#Table Name
				
				'table_name'    => 'entity_child',
				
				#Primary Key
                                
			        'key_id'        => 'id',
                                
				# Default Additional Column
                                
				'is_user_id'       => 'user_id',
                                
                               // 'js'=> ['is_top'=>1,'top_js'=>$LIB_PATH.'def/entity/f'],
								
				# Communication
								
				'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?d=test', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 1,
                                
                                'form_layout'   => 'form_100',
                                 
                                'default_fields' => ['entity_code'=>"'TT'"],
                                
				# File Include
                                
				//'js'            => 'q_details',
				
				#Page Code
				
				#'page_code'	=> 'FETY
				
                                'show_query' => 1
                                
			);

?>
