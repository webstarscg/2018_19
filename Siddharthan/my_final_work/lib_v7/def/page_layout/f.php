<?PHP

     $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Page Layout',
				
				#Table field
                    
				'data'	=>   array(
						   
						   '6' =>array( 'field_name'=> 'Basic',
							        'type'=>'heading'),
						   
						   
						   '1' =>array( 'field_name'=> 'Entity',
                                                               
                                                               'field_id' => 'entity_code',
                                                               
                                                               'type' => 'option',
                                                               
                                                               'option_data'=>$G->option_builder('entity','code,sn'," WHERE code='PL' "),
                                                               
                                                               'is_mandatory'=>1,
							       
							       'avoid_default_option'=>1,
                                                               
                                                               'input_html'=>'class="w_100"'
                                                            
                                                               ),
                                                   
                                                  
						   '2' =>array('field_name'=>'Token',
                                                               
                                                               'field_id'=>'token',
                                                               
                                                               'type'=>'text',
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_350"'
                                                               
                                                               ),
                                                   
						   '3' =>array('field_name'=>'Name',
                                                               
                                                               'field_id'=>'sn',
                                                               
                                                               'type'=>'text',
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_350"'
                                                               
                                                               ),
						
						   
						   '4' =>array('field_name'=>'Note',
                                                               
                                                               'field_id'=>'ln',
                                                               
                                                               'type'=>'text',
							       #'type'=>'textarea',
                                                               
                                                               'is_mandatory'=>0,
                                                               
                                                               'input_html'=>'class="4_400"'
                                                               
                                                               ),
						   
						   
						    '7' =>array( 'field_name'=> 'Content',
							         'type'=>'heading'),
							   												
						   '5' =>array(  'field_name'          => 'Content',                                                                
								'field_id'            => 'note',				       
								'type' 	              => 'textarea',
								'is_mandatory'        => 1,
								'input_html'          => ' rows=15 class="col-md-12" ',
                
						   
                                                               ),
						   
						   
                                                   
                                    
                                ),
                                    
				#Table Name
				
				'table_name'    => 'entity_child_base',
				
				#Primary Key
                                
			        'key_id'        => 'id',
				
				'divider'       => 'tab',
                                
				# Default Additional Column
                                
				'is_user_id'       => 'user_id',
								
				# Communication
								
				'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?d=page_layout', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 1,
                                
				# File Include
                                'after_add_update'	=>0,
				
				'page_code'	=> 'FECB',
				
                                
			);
    
    
    
  
   				
    # after add update
    
    function after_add_update($key_id){
	 
		  global $rdsql,$G,$LIB_PATH;
		  
		  $lv         = [];		  
		  $lv['temp'] = [];
		  
		  $lib = $LIB_PATH.'/comp/guzzle_rest/guzzle/vendor/autoload.php';			
		  
		  require_once $lib ;
		  
		  $lv['temp'] = $G->get_key_value('token','entity_child_base'," AND id=$key_id");
        	
		  $client = new Client([
		       // You can set any number of default request options.
		       'timeout'  => 2.0,
		  ]);
		  
		  #print_r($lv['temp']);
		  		  
		  $node_res = $client->GET($_SERVER["HTTP_HOST"].$_SERVER["SCRIPT_NAME"],['query'=>['t_series'=>$lv['temp']['addon'],
												    'key'     =>$lv['temp']['code']
												   ]
											  ]
					   );
		  
		  
    } // end

  
?>