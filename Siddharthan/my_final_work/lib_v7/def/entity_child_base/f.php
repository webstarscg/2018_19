<?PHP

    //F_series definition:
                            
    $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Entity Child Base',
				
				#Table field
                    
				'data'	=>   array('1' =>array( 'field_name'=> 'Entity',
                                                               
                                                               'field_id' => 'entity_code',
                                                               
                                                               'type' => 'option',
                                                               
                                                               'option_data'=>$G->option_builder('entity','code,sn',' ORDER by sn ASC'),
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_100"',
                                                               
                                                               'avoid_default_option' => 0,
                                                            
                                                               ),
                                                   
                                                  
						   '2' =>array('field_name'=>'Token',
                                                               
                                                               'field_id'=>'token',
                                                               
                                                               'type'=>'text',
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_60" onchange="check_token(this);"',
                                                               
                                                               ),
                                                   
						   '3' =>array('field_name'=>'Short Name',
                                                               
                                                               'field_id'=>'sn',
                                                               
                                                               'type'=>'text',
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_350"'
                                                               
                                                               ),
						   
//						   '8' =>array('field_name'=>'Page Code',
//                                                               
//                                                               'field_id'=>'code',
//                                                               
//                                                               'type'=>'text',
//                                                               
//                                                               'is_mandatory'=>1,
//                                                               
//                                                               'input_html'=>'class="w_200"'                                                               
//                                                               ),
						   
						   
						   '4' =>array('field_name'=>'Long Name',
                                                               
                                                               'field_id'=>'ln',
                                                               
                                                               'type'=>'textarea',
                                                               
                                                               'is_mandatory'=>0,
                                                               
                                                               'input_html'=>'class="W_150"'
                                                               
                                                               ),
						   
						    '5' =>array('field_name'=>'DNA',
                                                               
                                                               'field_id'=>'dna_code',
                                                               
                                                               'type'=>'option',
                                                       
                                                               'option_data'=>$G->option_builder('entity_attribute',
                                                                                                 'code,sn',
                                                                                                 " WHERE entity_code='EB' ORDER by sn ASC"
                                                                                            ),                                                               
                                                               
							       'is_mandatory'=>0,
                                                               
                                                               'input_html'=>' class="w_350"'
						   
                                                               ),
                                                     
                                                     
						   '6' =>array('field_name'=>'Note',
                                                               
                                                               'field_id'=>'note',
                                                               
                                                               'type'=>'textarea',
							       
							       'is_mandatory'=>0,
                                                               
                                                               'input_html'=>' class="w_350"'
						   
                                                               ),
                                                   
                                                   '7' =>array( 'field_name'=> 'Priority ( ea_code Issue)',
                                                               
                                                               'field_id' => 'ea_value',
                                                               
                                                               'type' => 'option',
                                                               
                                                               'option_data'         => '<option value=1>Yes</option><option value=0>No</option>',
							       
							       'child_table'         => 'ecb_av_addon_varchar', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'ea_code',   // attribute code field
							       
							       'child_attr_code'     => '     ',           // attribute code
                                                               
                                                               'is_mandatory'=>0,
							       
							       //'option_default'=> array('label'=>'Yes','value'=>1),
							       
							       'avoid_default_option' => 1,
                                                               
                                                               'ro'=>1,
                                                               
                                                               'input_html'=>'class="w_60"'
                                                               
                                                               ),
                                                   
                                                   '9' =>array('field_name'=>'Line order',
                                                               
                                                               'field_id'=>'line_order',
                                                               
                                                               'type'=>'text',
							       
							       'allow'=>'d6[.]',
                                                               
                                                               'input_html'=>' class="w_50"'
						   
                                                               ),
                                                   
                                                   
                                                   
                                    
                                ),
                                    
				#Table Name
				
				'table_name'    => 'entity_child_base',
				
				#Primary Key
                                
			        'key_id'        => 'id',
                                
				# Default Additional Column
                                
				'is_user_id'       => 'user_id',
								
				# Communication
				
                                'add_button' => array( 'is_add' =>1,'page_link'=>'f=entity_child_base', 'b_name' => 'Add Entity child' ),
								
				'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?d=entity_child_base', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 1,
                                
				# File Include
                                'after_add_update'	=>0,
                                
                                'js'=> ['is_top'=>1,'top_js'=>$LIB_PATH.'def/entity_child_base/f'],
				
				'page_code'	=> 'FECB',
				
                                
			);
    
    
    if(isset($_GET['default_addon'])){  
	
		$default_addon = $_GET['default_addon'];
		$F_SERIES['data'][1]['option_data'] = $G->option_builder('entity','code,sn',"WHERE code = (SELECT code FROM entity WHERE id = $default_addon)");
                $F_SERIES['data'][1]['avoid_default_option'] = 1;
                $F_SERIES['back_to']['is_back_button'] = 0;
                $F_SERIES['add_button']['is_add'] = 0;
	}
     
?>