<?PHP

    $LAYOUT	    = 'layout_full';
    
     $status_option = '<option value="2">Active</option><option value="1">In-Active</option>';
                            
    $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Status',
				
				#Table field
                    
				'data'	=>   array('1' =>array( 'field_name'=> 'Status',
                                                               
                                                               'field_id' => 'is_active',
                                                               
                                                               'type' => 'option',
                                                               'avoid_default_option'=>1,
                                                               'option_data'=> $status_option,//$G->option_builder('entity_attribute','code,sn',' WHERE entity_code="PP"  ORDER BY sn ASC'),
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_100"',
							       
							        'filter_in'	=> function($data_in){
													   return ($data_in-1);
										       },
										       
								'filter_out'	=> function($data_out){
													   return ($data_out+1);
										       }
                                                               
                                                               
                                                               ),
						        ),
                                    
				#Table Name
				
				'table_name'    => 'communication',
				
				#Primary Key
                                
			        'key_id'        => 'id',
				
				# Default Additional Column
                                
				'is_user_id'       => 'user_id',
								
				# Communication
								
				'back_to'  => array( 'is_back_button' =>0, 'back_link'=>'?d_series=contact&menu=off&layout=full', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 1,
                                
				# File Include
                                //'after_add_update'	=>1,
				
				'page_code'	=> 'FMPS',
				'show_query'    =>0,
				'show_update_query'=>0
                                
			);
?>