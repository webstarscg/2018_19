<?PHP

    $LAYOUT	    = 'layout_full';
                            
    $F_SERIES	=	array(
				#Title
				
				'title'	=>'Contact',
				
				#Table field
                    
				'data'	=>   array(
						   
						   
						   '1' =>array( 'field_name'=> 'Name ',
                                                               
								'field_id' => 'sn',
                                                               
								'type' => 'text',
                                                               
                                                                'is_mandatory'=>1,
                                                               
                                                             ),
						   
						   '2' =>array( 'field_name'=> 'Addr 1 ',
                                                               
								'field_id' => 'ln',
                                                               
								'type' => 'text',
                                                               
                                                                'is_mandatory'=>0,
                                                               
                                                             ),
						   
						   '3' =>array( 'field_name'=> 'Addr 2 ',
                                                               
								'field_id' => 'detail',
                                                               
								'type' => 'textarea',
                                                               
                                                                'is_mandatory'=>0,
                                                               
							      ),
						   
						   '4' =>array( 'field_name'=> 'Code ',
                                                               
								'field_id' => 'code',
                                                               
								'type' => 'text',
                                                               
                                                                'is_mandatory'=>0,
								
                                                               
							      ),
						   
						   '5' =>array(
								    'field_name'=>'Entity Code',
                                                               
								    'field_id'=>'entity_code',
                                                               
								    'type' => 'option',
							       
								    'option_data'=>$G->option_builder('entity','code,sn',"WHERE code='CO' ORDER BY sn ASC"),
								 
								    'is_mandatory'=>1,
								    
								    //'hint'	=>'Default value',
								    
								    'avoid_default_option'=>1,
                                                               
								    'input_html'=>'class="w_200"'
                                                               
							      ),
						   
					    ),
				
                                    
				#Table Name
				
				'table_name'    => 'entity_child',
				
				#Primary Key
                                
			        'key_id'        => 'id',
				
				//'default_fields'    => array(
				//			    'entity_code' => "'$entity_code'",
				//			    'entity_child_id' => "'$entity_child_id'",
				//			    ),
				//
				# Default Additional Column
                                
				'is_user_id'       => 'user_id',
				
				'back_to'  => NULL,
				
				'show_query' => 0,
                               				
				# Communication
				
				'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?d=contact_enh', 'BACK_NAME'=>'Back'),
								
				'prime_index'   => 1,
                                
				# File Include
                                
				'page_code'	=> 'FMCG',
				
				'show_query'    =>0,
				
				'after_add_update'=>0,
                                
			);
    
       if(isset($_GET['default_addon'])){
	
	$F_SERIES['back_to']['is_back_button'] = 0;
	
	}
    
?>