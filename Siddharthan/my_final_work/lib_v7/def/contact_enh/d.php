<?PHP
		
	
        $LAYOUT	    	= 'layout_full';
               
        $D_SERIES       =   array(
                                   'title'=>'Contact',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
				    
				    'gx' => 1,
				    
                                    
                                    #table data
                                    
                                    'data'=> array(
					                
							3=>array('th'=>'Name ',
								
                                                                'field' => "sn",
								   
								'td_attr' => ' class="label_father align_LM" width="20%"',
								
								'is_sort' => 1,
								
								),
							
							4=>array('th'=>'Addr 1 ',
								
                                                                'field' => "ln",
								   
								'td_attr' => ' class="align_LM" width="40%"',
								
								'is_sort' => 1,
								
								),
					
							
							5=>array('th'=>'Addr 2 ',
								
								'field' =>" detail",
                                                                   
								'td_attr' => ' class="align_LM" width="40%"',
								
								'is_sort' => 1,
									 
								),
							
					
								
                                                    ),
				    
					
                                    #Sort Info
                                      
                                       'sort_field' =>array('code', 'sn'),
                                        
                                       'action' => array('is_action'=>0, 'is_edit' =>1, 'is_view' =>0 ),
                                       
                                       'order_by'   =>'ORDER BY id ASC ' ,
				  
                                    #Table Info
                                    
                                    'table_name' =>'entity_child',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
				    
				    'key_filter'     =>	 " AND  entity_code='CO'  ",
                                
                                    # Communication
                                
                                    'prime_index'   => 1,
                                
                                    # File Include
                                
                                   'js'            => array('is_top'=>1, 'top_js' => 'js/d_series/manage_cms'),
				  
								   
				 #Search_info
				 
				 'search'=> 	array(
							  
							array(  'data'  =>array('table_name' 	=> 'entity_child',
										'field_id'	=> 'id',
										'field_name' 	=> 'sn',
										'filter'	=> " AND entity_code='CO' " 
									 ),
							      
								'title' 		=> 'Name',										
								'search_key' 		=> 'id',													       
								'is_search_by_text' 	=> 0, //( For Text search case)	      
							),
							
							array(  'data'  =>array('table_name' 	=> 'entity_child',
										'field_id'	=> 'id',
										'field_name' 	=> 'ln',
										'filter'	=> " AND entity_code='CO' " 
									 ),
							      
								'title' 		=> 'Long Name',										
								'search_key' 		=> 'id',													       
								'is_search_by_text' 	=> 0, //( For Text search case)	      
							),
							
							array(  'data'  =>array('table_name' 	=> 'entity_child',
										'field_id'	=> 'id',
										'field_name' 	=> 'detail',
										'filter'	=> " AND entity_code='CO' " 
									 ),
							      
								'title' 		=> 'Detail',										
								'search_key' 		=> 'id',													       
								'is_search_by_text' 	=> 0, //( For Text search case)	      
							),
								
						),
	      
				'is_narrow_down'=>1,
				
				'before_delete'=>0,
				
				
				# include
                                
				'js'            => array('is_top'=>1, 'top_js' => $LIB_PATH.'def/task_enh/d',
															   
							),
			   
				
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>1,'page_link'=>'f=contact_enh', 'b_name' => 'Add Contact' ),
								
					'del_permission' => array('able_del'=>1,'user_flage'=>1), 
								
					'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  ''),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'DMPG',
				
				'show_query'=>0,
                            
                            );
	
	if(@$_GET['default_addon']){

		$D_SERIES['hide_show_all'] = 1;
		unset($D_SERIES['export_csv']);
		$D_SERIES['add_button']['is_add'] = 0;
		$D_SERIES['action']['is_edit']=0;
		$D_SERIES['del_permission']['able_del']=0;
	}
    
    
?>