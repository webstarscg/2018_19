<?PHP
        $LAYOUT	    = 'layout_full';
               
        $D_SERIES       =   array(
                                   'title'=>'Project Details',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
				    
				    'gx' => 1,
				    
                                    #table data
                                    
                                    'data'=> array(
					                
							3=>array('th'=>'Name ',
								
								'field'		=>"get_exav_addon_varchar(id,'PRNA')",
                                                                   
								'td_attr' 	=> ' class="label_father align_LM" width="20%"',
								
								'is_sort'	=> 1,	
								
								),
					
							
							4=>array('th'=>'Client ',
								
								//'field' => "(get_ecb_sn_by_id(get_exav_addon_num(entity_child.id,'PRCL')))",
                                                                
								'field' => "( SELECT ec.sn FROM entity_child as ec WHERE ec.id=get_exav_addon_num(entity_child.id,'PRCL'))",
								   
								'td_attr' => ' class="align_LM" width="20%"',
								
								'is_sort'	=> 1,
									 
								),
							5=>array('th'=>'Start date ',
								
								'field' => "ifnull(date_format((get_exav_addon_date(id,'PRSD')),'%d-%b-%y'),'--NA--')",
								
								'td_attr' => ' class="align_RM" width="20%"',
								
								'is_sort'	=> 1,
									 
								),
							6=>array('th'=>'End date ',
								
								'field' => "ifnull(date_format((get_exav_addon_date(id,'PRED')),'%d-%b-%y'),'--NA--')",
								   
								'td_attr' => ' class="align_RM" width="20%"',
								
								'is_sort'	=> 1,
									 
								),
							7=>array('th'=>'Project Type ',
								
								
								'field' => "(get_ecb_sn_by_id(get_exav_addon_num(entity_child.id,'PRTY')))",
                                                                
								'td_attr' => ' class="label_father align_LM" width="25%"',
									 
								'is_sort'	=> 1,
								
								),
							
							8=>array('th'	=> 'Task', 'th_attr'=>' colspan=2 ',
								     
								'field'	=> 'id',
								
								'attr' =>  [ 'class'=>""],
									
                                                                'filter_out'=>function($data_in){
                                                                            
													$data_out = array('id'   => $data_in,
												        'link_title'=>'View',
												         'title'=>'Task View',
												         'src'=>"?d=task&menu_off=1&mode=simple&default_addon=$data_in",
												         'style'=>"border:none;width:100%;height:600px;");
													 return json_encode($data_out);
													 
													 
												 },
                                                                        
                                                                        'js_call'=>'d_series.set_nd'
									
									
                                                                        
								    ),
							
							
							12=>array('th'	=> '',
								     
								  'field'	=> 'id',
								  
								  'attr' =>  [ 'class'=>"brdr_right"],
									
								  'filter_out'=>function($data_in){
                                                                            
													$data_out = array('id'   => $data_in,
													'link_title'=>'Add',
													'title'=>'Add Task',
													'src'=>"?f=task&id=$data_in&menu_off=1&mode=simple&default_addon=add",
													'style'=>"border:none;width:100%;height:600px;"
												  );
														
													return json_encode($data_out);
													 
												 },
                                                                        
                                                                        'js_call'=>'d_series.set_nd'
                                                                        
								    ),
							
							9=>array('th'	=> 'Bug', 'th_attr'=>' colspan=2 ',
								     
								'field'	=> 'id',
								
								'attr' =>  [ 'class'=>""],
								
								'filter_out'=>function($data_in){
                                                                            
													$data_out = array('id'   => $data_in,
							  					        'link_title'=>'View',
							        					'title'=>'Bug View',
													'src'=>"?d=bug&menu_off=1&mode=simple&default_addon=$data_in",
													'style'=>"border:none;width:100%;height:600px;"
												  );
													return json_encode($data_out);
													 
												 },
                                                                        
                                                                        'js_call'=>'d_series.set_nd'
                                                                        
								    ),
							
							
							13=>array('th'	=> '',
								     
								  'field'	=> 'id',
								  
								  'attr' =>  [ 'class'=>"brdr_right"],
								
								  'filter_out'=>function($data_in){
                                                                            
													$data_out = array('id'   => $data_in,
													'link_title'=>'Add',
													'title'=>'Add Bug',
													'src'=>"?f=bug&id=$data_in&menu_off=1&mode=simple&default_addon=add",
													'style'=>"border:none;width:100%;height:600px;"
												  );
														
													return json_encode($data_out);
													 
												 },
                                                                        
                                                                        'js_call'=>'d_series.set_nd'
                                                                        
								    ),
							
							10=>array('th'	=> 'Enhancement', 'th_attr'=>' colspan=2 ',
								     
								'field'	=> 'id',
								
								'attr' =>  [ 'class'=>""],
												  'filter_out'=>function($data_in){
                                                                            
													$data_out = array('id'   => $data_in,
													'link_title'=>'View',
													'title'=>'Enhancement View',
													'src'=>"?d=enhancement&menu_off=1&mode=simple&default_addon=$data_in",
													'style'=>"border:none;width:100%;height:600px;"
												  );
													return json_encode($data_out);
													 
												 },
                                                                        
                                                                        'js_call'=>'d_series.set_nd'
                                                                        
								    ),
							
							14=>array('th'	=> '',
								     
								  'field'	=> 'id',
								  
								  'attr' =>  [ 'class'=>"brdr_right"],
									
								  'filter_out'=>function($data_in){
                                                                            
													$data_out = array('id'   => $data_in,
													  'link_title'=>'Add',
													  'title'=>'Add Enhancement',
													  'src'=>"?f=enhancement&id=$data_in&menu_off=1&mode=simple&default_addon=add",
													  'style'=>"border:none;width:100%;height:600px;"
												  );
														
													return json_encode($data_out);
													 
												 },
                                                                        
                                                                        'js_call'=>'d_series.set_nd'
                                                                        
								    ),
							
							11=>array('th'	=> 'Issue', 'th_attr'=>' colspan=2 ',
								     
								'field'	=> 'id',
								
								'attr' =>  [ 'class'=>""],
									
								 'filter_out'=>function($data_in){
                                                                            
													$data_out = array('id'   => $data_in,
													'link_title'=>'View',
													'title'=>'Issue View',
													'src'=>"?d=issue&menu_off=1&mode=simple&default_addon=$data_in",
													'style'=>"border:none;width:100%;height:600px;"
												  );
													return json_encode($data_out);
													 
												 },
                                                                        
                                                                        'js_call'=>'d_series.set_nd'
                                                                        
								    ),
							
								
							15=>array('th'	=> '',
								     
								  'field'	=> 'id',
								  
								  'attr' =>  [ 'class'=>"brdr_right"],
								
								'filter_out'=>function($data_in){
                                                                            
												   	  $data_out = array('id'   => $data_in,
													  'link_title'=>'Add',
													  'title'=>'Add Issue',
													  'src'=>"?f=issue&id=$data_in&menu_off=1&mode=simple&default_addon=add",
													  'style'=>"border:none;width:100%;height:600px;"
											  );
														
													return json_encode($data_out);
									 },
                                                                        
									'js_call'=>'d_series.set_nd'
                                                                        
								    ),	
                                                    ),
				    
					
                                       'action' => array('is_action'=>1, 'is_edit' =>1, 'is_view' =>0 ),
                                       
                                       'order_by'   =>'ORDER BY id ASC ' ,
				       
				    #Filter Info
				    
					'custom_filter' => array(  			     						   
							      
									array(  'field_name' => 'Client:',
									      
										'field_id' => 'cf1', // 
										
										'filter_type' =>'multi_select', 
												    
										'option_value'=> $G->option_builder('entity_child','id,sn'," WHERE entity_code = 'CO'"),
							    
										'html'=>'  title="Select Client"   data-width="160px"  ',
								    
										'cus_default_label'=>'Show All',
							    
										'filter_by'  => "( SELECT exa_value FROM exav_addon_num WHERE parent_id=entity_child.id AND exa_token='PRCL' )"  // main table value
									),
									
									
									array(  'field_name' => 'Project Type:',
									      
										'field_id' => 'cf2', // 
										
										'filter_type' =>'option_list', 
												    
										'option_value'=> $G->option_builder('entity_child_base','id,sn'," WHERE entity_code = 'PT'"),
							    
										'html'=>'  title="Select Project Type"   data-width="160px"  ',
								    
										'cus_default_label'=>'Show All',
							    
										'filter_by'  => "( SELECT exa_value FROM exav_addon_num WHERE parent_id=entity_child.id AND exa_token='PRTY' )" // main table value
										
										//'filter_by'	=> "get_exav_addon_num('entity_child.id','PRTY')",
									),
								
								),
                                
                                    #Table Info
                                    
                                    'table_name' =>'entity_child',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
				    
				    'key_filter'     =>	 " AND  entity_code='PR'  ",
                                
                                    # Communication
                                
                                    'prime_index'   => 1,
                                
                                    # File Include
                                
                                  'js'            => array('is_top'=>1, 'top_js' => 'js/d_series/manage_cms'),
				  
				
				
				#Search
				
				'search'=> 	array(
							  
							array(  'data'  =>array('table_name' 	=> 'exav_addon_varchar',
										'field_id'	=> 'exa_value',
										'field_name' 	=> 'exa_value',
										'filter'	=> " AND exa_token='PRNA' " 
									 ),
							      
								'title' 		=> 'Name',										
								'search_key' 		=> ' get_exav_addon_varchar(id,"PRNA") ',													       
								'is_search_by_text' 	=> 1, //( For Text search case)	      
							),
							
								
						),
								
							
				'is_narrow_down'=>1,
				
				'before_delete'=>1,
				
				
				# include
                                
				'js'            => array('is_top'=>1, 'top_js' => $LIB_PATH.'def/project/d',
															   
							),
			   
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>1,'page_link'=>'f=project', 'b_name' => 'Add Project' ),
								
					'del_permission' => array('able_del'=>1,'user_flage'=>1), 
								
					'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  ''),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'DMPG',
				
				'show_query'=>0,
                            
                            );
	
	
	// after delete
	function before_delete($param){
		
		$lv 	      = array();
		
		
		$lv['result'] = $param['g']->get_one_columm(['table'=>'entity_child',
						    'field'=>'code',
						    'manipulation'=>" WHERE id=$param[key_id]" 
						    ]);
		
		$content_page = 'template/page_content/'.$lv['result'].".html";		     
 
		if(is_file($content_page)){
		   
			unlink($content_page);
		}  		
		
	} // end
	
	
	    # addon
	if(@$_GET['default_addon']){
		
		global $G;
                global $D_SERIES;
		
		$temp['addon'] = json_decode($_GET['default_addon']);
                
                if($temp['addon']->at){
                    
                        $D_SERIES['key_filter'].=" AND addon='".$temp['addon']->at."'";
                    
                } // end
                
        }
	
	//if($USER_ID !=2){
	//	
	//	$D_SERIES['data'] = null;
	//	$D_SERIES['key_filter'] = null;
	//}
    
?>