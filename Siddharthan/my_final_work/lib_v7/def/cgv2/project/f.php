<?PHP

    $LAYOUT	= 'layout_full';
    
    $new_option = '<option value=-1 class="clr_green">+ Add New</option>';
                            
    $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Project Details',
				
				#Table field
                    
				'data'	=>   array(
						   
						   
						   '1' =>array( 'field_name'=> 'Project Name ',
                                                               
                                                               'field_id' => 'exa_value',
                                                               
                                                               'type' => 'text',
                                                               
							       'child_table'         => 'exav_addon_varchar', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'exa_token',   // attribute code field
							       
							       'child_attr_code'     => 'PRNA',           // attribute code
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_150"'
                                                               
                                                               ),
						   
						   '2' =>array( 'field_name'=> 'Client ',
                                                               
                                                               'field_id' => 'exa_value',
                                                               
                                                               'type' => 'option',
							       
							       'option_data'=>$G->option_builder('entity_child','id,sn'," WHERE entity_code='CO'  ORDER BY sn ASC"),
                                
							       'child_table'         => 'exav_addon_num', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'exa_token',   // attribute code field
							       
							       'child_attr_code'     => 'PRCL',           // attribute code
							       
							       'input_html'=>'class="w_150"',
                                                               
							       'is_mandatory'=>1,
                                                               
                                                               ),
						   
						   '3' =>array( 'field_name'=> 'Start date',
                                                               
                                                               'field_id' => 'exa_value',
                                                               
                                                               'type' => 'date',
							       
                                                               'child_table'         => 'exav_addon_date', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'exa_token',   // attribute code field
							       
							       'child_attr_code'     => 'PRSD',           // attribute code
							       
							       'set'		     => array('min_date' => '-1D',
											      'max_date' => '10D'),
                                                               
							       'is_mandatory'=>0,
                                                               
                                                               'input_html'=>'class="w_100"' ,
							       
                                                               ),
						   
						    '4' =>array( 'field_name'=> 'End date',
                                                               
                                                               'field_id' => 'exa_value',
                                                               
                                                               'type' => 'date',
							       
							       'child_table'         => 'exav_addon_date', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'exa_token',   // attribute code field
							       
							       'child_attr_code'     => 'PRED',           // attribute code
							       
							       'set'		     => array('min_date' => '0D'),
                                                               
							       'is_mandatory'=>0,
                                                               
                                                               'input_html'=>'class="w_100"' ,
							       
                                                               ),
						  
						   '5' =>array('field_name'=>'Project Type',
                                                               
                                                               'field_id'=>'exa_value',
                                                               
                                                               'type' => 'option',
							       
							       'option_data'=>$G->option_builder('entity_child_base','id,sn'," WHERE entity_code='PT'  ORDER BY sn ASC").$new_option,
                                
                                                               'child_table'         => 'exav_addon_num', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
							       
							       'child_attr_field_id' => 'exa_token',
								 
							       'child_attr_code'     => 'PRTY',      // attribute code
                                                               
							       'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_150" onchange="add_new_master(this,6)"'
                                                               
                                                               ),
						   
						   '6'=>array( 'field_name' => 'New Project Type',
							      
								'field_id'=>'sn',
                                                               
								'type'=>'text',
								
		                                                'is_hide'=>1,
	               
	                                                        'is_mandatory'=>0,
								
								'ro'	=>1,
								
								'input_html'=> '  onchange="check_project(this);"  ',
								
							),
					   ),
                                    
				#Table Name
				
				'table_name'    => 'entity_child',
				
				#Primary Key
                                
			        'key_id'        => 'id',
				
				 'js'=> ['is_top'=>1,'top_js'=>$LIB_PATH.'def/project/f'],
				
				'deafult_value'    => array('entity_code' => "'PR'", 'is_active' => "1"),
				
				# Default Additional Column
                                
				'is_user_id'       => 'created_by',
				
				'before_add_update'=>1,
								
				# Communication
								
				'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?d=project', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 1,
                                
				'page_code'	=> 'FMCG', //irrelevant information
				
				'show_query'    =>0,
                                
			);
    
    function before_add_update(){
	
	global $G,$F_SERIES,$rdsql;
	
	if($_POST['X5']==-1){
	    
	    $value = $_POST['X6'];
	    
	    $insert = $rdsql->exec_query("INSERT INTO entity_child_base (entity_code,sn) VALUES ('PT','$value')","Insertion Failed");
	     
	    $temp = $rdsql->last_insert_id('entity_child_base');
	     
	    $_POST['X5'] = $temp;
	     
	}
	
	return null;
    }
    
    
?>