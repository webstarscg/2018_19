<?PHP
	
	$default_addon = @$_GET['default_addon'];
	
        $LAYOUT	    = 'layout_full';
               
        $D_SERIES       =   array(
                                   'title'=>'Activity Details',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
				    
				    'gx' => 1,
				    
                                    #table data
                                    
                                    'data'=> array(	
					                
							
							2=>array('th'=>'About Activity ',
								
								'field'	=> "get_exav_addon_varchar(id,'ACAB')",
                                                                   
								'td_attr' => ' class="label_father align_LM" width="50%"',
								
								'is_sort' => 1,	
								
								),
							3=>array('th'=>'Task Name ',
								
								'field' =>" get_exav_addon_varchar(get_exav_addon_num(id,'ACTK'),'TKNA')",
								
								'td_attr' => ' class=" align_LM" width="30%"',
								
								'is_sort' => 1,	
								
								),
							
					/*		
							5=>array('th'=>'Start date ',
								
								'field' => "ifnull(date_format((get_exav_addon_date(id,'ACSD')),'%d-%b-%y'),'--NA--')",
								
								'td_attr' => ' class="label_father align_LM" width="15%"',
								
								'is_sort' => 1,
									 
								),
							6=>array('th'=>'End date ',
								
								'field' => "ifnull(date_format((get_exav_addon_date(id,'ACED')),'%d-%b-%y'),'--NA--')",
								
								'td_attr' => ' class="label_father align_LM" width="15%"',
								
								'is_sort' => 1,
									 
								),
					*/			
							7=>array('th'=>'Reported Date ',
								
								'field' => "date_format((get_exav_addon_date(entity_child.id,'ACRD')),'%d-%b-%y')",
                                                        
								'td_attr' => ' class="align_LM" width="10%"',
									 
								),
							
							8=> array('th'=>'Updation ',
								    
								    'field' => "concat(get_userinfo(user_id),' | ',date_format(updated_on,'%d-%b-%y %T'))",
								    
								    'td_attr' => ' class="align_LM" width="25%"',
								    
								    'is_sort'	=>1,
								 ),
							
							9=>array('th'=>'Working Hours ',
								
								'field' => "(get_exav_addon_decimal_2(entity_child.id,'ACWH'))",
                                                        
								'td_attr' => ' class="align_LM" width="10%"',
									 
								),
								
                                                    ),
				    
				    
				    #filter info
				    
				    'custom_filter' => array(  			     						   
									
									array(  'field_name' => 'Task name:',
									      
										'field_id' => 'cf1', // 
										
										'filter_type' =>'multi_select', 
												    
										'option_value'=> $G->option_builder('exav_addon_varchar','parent_id,exa_value'," WHERE exa_token = 'TKNA'"),
							    
										'html'=>'  title="Select Task"   data-width="160px"  ',
								    
										'cus_default_label'=>'Show All',
							    
										'filter_by'  => "( SELECT exa_value FROM exav_addon_num WHERE parent_id=entity_child.id AND exa_token='ACTK' )" // main table value
										
										//'filter_by'	=> "get_exav_addon_num('entity_child.id','PRTY')",
									),
									
									array(  'field_name' => 'Division:',
									      
										'field_id' => 'cf2', // 
										
										'filter_type' =>'option_list', 
												    
										'option_value'=> $G->option_builder('entity_child_base','id,sn'," WHERE entity_code = 'PS'"),
							    
										'html'=>'  title="Select Task"   data-width="160px"  ',
								    
										'cus_default_label'=>'Show All',
							    
										'filter_by'  => "( SELECT exa_value FROM exav_addon_num WHERE parent_id=entity_child.id AND exa_token = 'ACDV' )", // main table value
										
										//'filter_by'	=> "get_exav_addon_num('entity_child.id','PRTY')",
									),
								
								),
				    
					
                                    #Sort Info
                                      
                                       'sort_field' =>array('code','sn'),
                                        
                                       'action' => array('is_action'=>0, 'is_edit' =>1, 'is_view' =>0 ),
                                       
                                       'order_by'   =>'ORDER BY id ASC ' ,
				       		
                                
                                    #Table Info
                                    
                                    'table_name' =>'entity_child',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
				    
				    'key_filter'     =>	 " AND entity_code='AC' ",
                                
                                    # Communication
                                
                                    'prime_index'   => 1,
                                
                                    # File Include
                                
                                  'js'            => array('is_top'=>1, 'top_js' => 'js/d_series/manage_cms'),
				  
								
				'is_narrow_down'=>1,
								
				
				# include
                                
				'js'            => array('is_top'=>1, 'top_js' => $LIB_PATH.'def/activity/d',
															   
							),
			   
				
				'search'=> 	array(
							  
							array(  'data'  =>array('table_name' 	=> 'exav_addon_varchar',
										'field_id'	=> 'parent_id',
										'field_name' 	=> 'exa_value',
										'filter'	=> " AND exa_token='ACAB' " 
									 ),
							      
								'title' 		=> 'About activity',										
								'search_key' 		=> 'get_exav_addon_varchar(id,"ACAB")',													       
								'is_search_by_text' 	=> 1, //( For Text search case)	      
							),
								
						),
	
				
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>1,'page_link'=>'f=activity', 'b_name' => 'Add Activity' ),
								
					'del_permission' => array('able_del'=>0,'user_flage'=>0), 
								
					'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  ''),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'DMPG',
				
				'show_query'=>0,
                            
                            );
	
	
	# default addon
	if(@$_GET['default_addon']){
		
		$D_SERIES['key_filter'].=" AND get_exav_addon_num(id,'ACTK')=$default_addon";
		
		$D_SERIES['del_permission'] = array('able_del'=>0,'user_flage'=>0);
		
		$D_SERIES['add_button'] = array( 'is_add' =>0,'page_link'=>'f=activity', 'b_name' => 'Add Activity' );
		
		$D_SERIES['action'] = array('is_action'=>0, 'is_edit' =>0, 'is_view' =>0 );			
		
		unset($D_SERIES['custom_filter'][0]);
		unset($D_SERIES['search']);
		unset($D_SERIES['data'][3]); //To remove task name
		
                
        }
	
	if($USER_ID!=2){
					
		$D_SERIES['key_filter'].=" AND user_id IN ($USER_ID)";
	}
	   
?>