<?PHP
		
	$default_addon = @$_GET['default_addon'];
	
        $LAYOUT	    	= 'layout_full';
	
	$temp_user_role=$SG->get_session('user_role');
	
	//echo $USER_ID;
               
        $D_SERIES       =   array(
                                   'title'=>'Title',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
				    
				    'gx' => 1,
				    
                                    
                                    #table data
                                    
                                    'data'=> array(
					                
							2=>array('th'=>'Project ',
								
								'field' =>" get_exav_addon_varchar(get_exav_addon_num(id,'TKPR'),'PRNA')",
                                                                   
								'td_attr' => ' class="label_father align_LM" width="20%"',
								
								'is_sort' => 1,
									 
								),
							
							
							3 => array('th'=>'Division ',
								    
								    'field' => "(SELECT sn FROM entity WHERE code = entity_code )",
								    
								    'td_attr' => ' class="align_LM" width="15%"',
								    
								    'is_sort' =>1,
								 ),
							
							4=>array('th'=>'Name ',
								
                                                                'field' => "get_exav_addon_varchar(id,'TKNA')",
								   
								'td_attr' => ' class="align_LM" width="20%"',
								
								'is_sort' => 1,
								
								),
					
							
							5=>array('th'=>'Start date ',
								
								'field' => "ifnull(date_format((get_exav_addon_date(id,'TKSD')),'%d-%b-%y'),'--NA--')",
								
								'td_attr' => ' class="align_LM" width="12.5%"',
								
								'is_sort' => 1,
									 
								),
							6=>array('th'=>'End date ',
								
								'field' => "ifnull(date_format((get_exav_addon_date(id,'TKED')),'%d-%b-%y'),'--NA--')",
                                                                   
								'td_attr' => ' class="align_LM" width="12.5%"',
								
								'is_sort' => 1,
								
								),
							8=>array('th'=>'Assigned ',
								
								'field'   =>"(SELECT GROUP_CONCAT(user_name) FROM user_info WHERE id IN (SELECT exa_value FROM exav_addon_num WHERE exa_token='TKAS' AND parent_id=entity_child.id))", 
                                                                   
								'td_attr' => ' class="align_LM" width="10%"',
								
								),
							
							9=>array('th'=>'Status ',
								
								'field' => "(SELECT sn FROM entity_child_base WHERE token = (SELECT exa_value FROM exav_addon_varchar WHERE parent_id=entity_child.id AND (exa_token='TS' OR exa_token='ES' OR exa_token='IU' OR exa_token='BS')))",
								
								'td_attr' => ' class="align_LM" width="10%"',
								
								),
							
							
							11=>array('th'	=> 'Activity', 'th_attr'=>' colspan=2 ',
								     
								'field'	=> 'id',
									
                                                                'filter_out'=>function($data_in){
                                                                            
													$data_out = array('id'   => $data_in,
												        'link_title'=>'View',
												         'title'=>'Activity View',
												         'src'=>"?d=activity&menu_off=1&mode=simple&default_addon=$data_in",
												         'style'=>"border:none;width:100%;height:600px;");
													 return json_encode($data_out);
												 },
                                                                        
                                                                        'js_call'=>'d_series.set_nd'
                                                                        
								    ),
							
							12=>array('th'	=> '',
								     
								  'field'	=> 'id',
								  
								  'attr' =>  [ 'class'=>"brdr_right"],
								
								  'filter_out'=>function($data_in){
                                                                            
													$data_out = array('id'   => $data_in,
												        'link_title'=>'Add',
												         'title'=>'Add Activity',
												         'src'=>"?f=activity&add=add_activity&menu_off=1&mode=simple&default_addon=$data_in",
												         'style'=>"border:none;width:100%;height:600px;");
													 return json_encode($data_out);
													 
												 },
                                                                        
                                                                        'js_call'=>'d_series.set_nd'
                                                                        
								    ),
							
							14 => array('th'=>'Updation ',
								    
								    'field' => "concat(get_userinfo(user_id),' | ',date_format(updated_on,'%d-%b-%y %T'))",
								    
								    'td_attr' => ' class="align_LM" width="25%"',
								    
								    'is_sort'	=>1,
								 ),
							
							
							
								
                                                    ),
				    
					
                                    #Sort Info
                                      
                                       'sort_field' =>array('code', 'sn'),
                                        
                                       'action' => array('is_action'=>0, 'is_edit' =>0, 'is_view' =>0 ),
                                       
                                       'order_by'   =>'ORDER BY id ASC ' ,
				       
				       
				    #Filter Info
				    
					'custom_filter' => array(  			     						   
							      
									array(  'field_name' => 'Project Name:',
									      
										'field_id' => 'cf1', // 
										
										'filter_type' =>'option_list', 
												    
										'option_value'=> $G->option_builder('exav_addon_varchar','parent_id,exa_value'," WHERE exa_token = 'PRNA'"),
							    
										'html'=>'  title="Select Project Name"   data-width="160px"  ',
								    
										'cus_default_label'=>'Show All',
							    
										'filter_by'  => "get_exav_addon_num(id,'TKPR')" // main table value
									),
									
									array(  'field_name' => 'Division:',
									      
										'field_id' => 'cf2', // 
										
										'filter_type' =>'option_list', 
												    
										'option_value'=> $G->option_builder('entity_child_base','id,sn'," WHERE entity_code = 'PS'"),
									  
										'html'=>'  title="Select Task"   data-width="160px"  ',
									   
										'cus_default_label'=>'Show All',
									  
										'filter_by'	=> "get_exav_addon_num(id,'PSDV')",
										
									),
									
									
									array(  'field_name' => 'Assigned:',
									      
										'field_id' => 'cf3', // 
										
										'filter_type' =>'option_list', 
												    
										'option_value'=> $G->option_builder('user_info','id,user_name'," WHERE user_role_id = 2"),
									  
										'html'=>'  title="Select Task"   data-width="160px"  ',
									   
										'cus_default_label'=>'Show All',
									  
										'filter_by'	=> "get_exav_addon_num(id,'TKAS')",
										
									),

								),   
				       		
                                
				
                                    #Table Info
                                    
                                    'table_name' =>'entity_child',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
				    
				    
				    'key_filter'     =>	 "  AND (entity_code='BG' OR  entity_code='TK' OR  entity_code='IS' OR  entity_code='EN')  ",
				    
				    
                                    # Communication
                                
                                    'prime_index'   => 1,
                                
                                    # File Include
                                
                                   'js'            => array('is_top'=>1, 'top_js' => 'js/d_series/manage_cms'),
				  
								   
				 #Search_info
				 
				 'search'=> 	array(
							  
							array(  'data'  =>array('table_name' 	=> 'exav_addon_varchar',
										'field_id'	=> 'parent_id',
										'field_name' 	=> 'exa_value',
										'filter'	=> " AND exa_token='TKNA' " 
									 ),
							      
								'title' 		=> 'Name',										
								'search_key' 		=> ' get_exav_addon_varchar(id,"TKNA") ',													       
								'is_search_by_text' 	=> 1, //( For Text search case)	      
							),
							
								
						),
	      
				'is_narrow_down'=>1,
				
				'before_delete'=>0,
				
				
				# include
                                
				'js'            => array('is_top'=>1, 'top_js' => $LIB_PATH.'def/secondary/d',
															   
							),
			   
				
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>0,'page_link'=>'f=task', 'b_name' => 'Add Task' ),
								
					'del_permission' => array('able_del'=>1,'user_flage'=>1), 
								
					'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  ''),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'DMPG',
				
				'show_query'=>0,
				
                            
                            );
	
	
				if(@$_GET['cf1']>0){
					
					unset($D_SERIES['key_filter']);
				}
				
				if($USER_ID!=2){
					
					$D_SERIES['key_filter'].="AND id IN (SELECT parent_id FROM exav_addon_num WHERE exa_token='TKAS' AND exa_value IN ($USER_ID,0))";
				}
    
?>