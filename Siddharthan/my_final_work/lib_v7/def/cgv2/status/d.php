<?PHP
			
    $default_addon = $_GET['default_addon'];
    
    $temp = explode(':',$default_addon);
    
    $status_code = $temp[0];
    
    $entity_child_id=$temp[1];
    
    $entity_code = substr($temp[0],0,2);
    
	
        $LAYOUT	    	= 'layout_full';
               
        $D_SERIES       =   array(
                                   'title'=>'Title',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
				    
				    'gx' => 1,
				    
                                    
                                    #table data
                                    
                                    'data'=> array( 
						        2=>array('th'=>'Status ',
								
								'field' =>"(SELECT sn FROM entity_child_base WHERE token = status_code)",
								
								'td_attr' => ' class="label_father align_LM" width="30%"',
								
								'is_sort' => 0,	
								
								),
							
							3=>array('th'=>'Note ',
								
								'field' =>"note",
								    
								'td_attr' => ' class="label_father align_LM" width="30%"',
								
								'is_sort' => 0,	
								
								),
							
							4=>array('th'=>'Date & Time ',
								
								'field'	=> "date_format(timestamp_punch,'%d-%b-%y   %h:%m:%s')",
                                                                   
								'td_attr' => ' class="label_father align_LM" width="50%"',
								
								'is_sort' => 0,	
								
								),
					
                                                    ),
				    
					
                                    #Table Info
                                    
                                    'table_name' =>'status_info',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
				    
				    'key_filter'     =>	 " AND entity_code='$entity_code' AND entity_child_id=$entity_child_id",
				    
                                    # Communication
                                
                                    'prime_index'   => 1,
                                
                                    # File Include
                                
                                   'js'            => array('is_top'=>1, 'top_js' => 'js/d_series/manage_cms'),
				   
				   # include
                                
				   'js'            => array('is_top'=>1, 'top_js' => $LIB_PATH.'def/status/d',
															   
							),
				  
						
				
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>0,'page_link'=>'f=task_enh', 'b_name' => 'Add Task' ),
								
					'del_permission' => array('able_del'=>0,'user_flage'=>0), 
								
					'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  ''),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'STATUS',
				
				'show_query'=>0,
                            
                            );
    
?>