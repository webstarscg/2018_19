<?PHP
        $LAYOUT	    = 'layout_full';
               
        $D_SERIES       =   array(
                                   'title'=>'',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
				    
				    'gx'=>1,
                                    
                                    #table data
                                    
                                    'data'=> array(
					                
							7=>array('th'=>'Coach',
									 
								'field'=>" get_page_coach(id)",
                                                                   
									 
								'td_attr' => ' class="label_grand_child align_LM" width="15%"',
									 
								),
							
							1=>array('th'=>'Menu Name',
								 
								'field'=>"get_eav_addon_varchar(id,'ECSN')",
								     
								/*'html'      =>  'style	= "cursor:pointer" onclick="JavaScript:E_V_PASS(\'sort_field\',1);E_V_PASS(\'sort_direction\',GET_E_VALUE(\'sort_col_1\'));filter_data();"',
								  
								'font'      =>  'class="sort"',*/
																
								'span'      =>  '<span id="sort_icon_1" name="sort_icon_1"></span>',
									
								'td_attr' => ' width="15%" class="clr_dark_blue"',
                                                                            
								),
							  
							
							3=>array('th'=>'Page Title ',
									 
								'field'=>"get_eav_addon_varchar(id,'ECLN')",
                                                                   
									 
								'attr' => [ 'class'=>'label_father align_LM', 'width'=>'30%'],
									 
								),
							
							
							
							6=>array('th'=>'Parent',
									 
								'field'=>" ifnull((get_eav_addon_varchar(get_eav_addon_num(id,'ECPR'),'ECSN')),' NA ')",                                                                   
									 
								'td_attr' => ' class="label_grand_child align_LM" width="15%"',
									 
								),
							
					
                                                        8=>array('th'=>'Addon',
									 
								'field'=>" get_eav_addon_varchar(id,'PGAT')",
                                                                   
									 
								'td_attr' => ' class="align_LM" width="10%" ',
									 
								),
													
                                                        2=>array('th'=>'Status',
									 
								'field'=>"CONCAT(id,'[C]',is_active)",
								
								'filter_out'=>function($data_in){
										
											$temp     = explode('[C]',$data_in);
											
											$flag     = [1,0];
											
											$data_out = array(
													  'data'=>array('id'   => $temp[0],
															'key'  => md5($temp[0]),
															'label'=> 'Page',
															'cv'   => $temp[1],
															'fv'   => $flag[$temp[1]],
															'action'=>'entity_child',
															'series'=>'a',
															'token' =>'ECAI'
															)
													);
											
											return json_encode($data_out);
										},
                                                                   
									 
								'td_attr' => ' class="label_child align_LM  txt_case_upper b" style="padding-right:10px;" width="3%"',
								
								'js_call'=> 'd_series.inline_on_off',
									 
								),
							
							
							5=>array('th'=>'Updation',
									 
								'field'=>"concat(get_user_internal_name(user_id),',',date_format(timestamp_punch,'%d-%b-%y %T'))",
							        									 
								'td_attr' => 'width="10%"',
								
								'js_call'=> 'show_user_info',
									 
								),
						
						
							9=>array('th'=>'Action',
									 
								'field'=>"concat(id,':',IFNULL(get_eav_addon_varchar(id,'PGAT'),0))",
							        									 
								'td_attr' => 'width="10%"',
								
								'filter_out' => function($data_in){
									
									$temp = explode(':',$data_in);
									
									return '<a class="icon edit" href=?f=cms_page_eav&key='.$temp[0].(($temp[1])?"&default_addon=$temp[1]":'').'>Edit</a>';
									 
								}
							),
							 
							//  5=>array('th'=>'Content',
							//		 
							//	'field'=>'detail as v5',
							//                                                            
							//	//'html'      =>  'style	= "cursor:pointer" onclick="JavaScript:E_V_PASS(\'sort_field\',2);E_V_PASS(\'sort_direction\',GET_E_VALUE(\'sort_col_2\'));filter_data();"',
							//	  
							//	//'font'      =>  'class="sort"',
							//									
							//	
							//		 
							//	'td_attr' => ' class="label_child align_LM" width="50%"',
							//		 
							//	),
							//
							
								
								
							
								
                                                    ),
				    
                                    
                                    #Sort Info
                                      
                                       'sort_field' =>array("get_eav_addon_varchar(id,'ECCD')",
							    
							    "get_eav_addon_varchar(id,'ECSN')"),
                                        
                                       'action' => array('is_action'=>0, 'is_edit' =>0, 'is_view' =>0 ),
                                       
                                       'order_by'   =>'ORDER BY id ASC ' ,
				       		
                                
                                    #Table Info
                                    
                                    'table_name' =>'entity_child',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
				    
				    'key_filter'     =>	 " AND  entity_code='PG' ",
                                
                                    # Communication
                                
                                    'prime_index'   => 1,
                                
                                    # File Include
                                
                                  'js'            => array('is_top'=>1, 'top_js' => 'js/d_series/manage_cms'),
				  
				  
				  
				   
				 'search' => array(
							  
							       array(  'data' 	=> array(      'table_name' => 'entity_child',
											       'field_id'   => 'id',
											       'field_name' => "get_eav_addon_varchar(id,'ECLN')",
											       'filter'	    => " AND entity_code='PG' " 
											),
								     
								        'title' =>'Title',
									
									'search_key' => 'id',
												       
								        //l'is_search_by_text' =>0, //( For Text search case)
							                
																		      
								),
							       
							//        array(
							//		'data' => array('table_name' 	=> 'entity_child',
							//			        'field_id'	=> 'code',
							//				'field_name'	=> 'code',											
							//				'filter'	=> " AND entity_code='PG' " 
							//			),
							//	        
							//		'title'=>'Code',
							//		
							//		'search_key' => 'code',
							//					       
							//	        'is_search_by_text' 	=> 1, //( For Text search case)
							//                
							//		
							//	),
							//	
							//	 array(
							//		'data' => array('table_name' =>'entity_child',
							//			        'field_name'=>'id,id',
							//			),
							//	        
							//		'title' 		=> 'id',
							//					       
							//	        'is_search_by_text' 	=> 0, //( For Text search case)
							//                
							//		'search_key' 		=> 'id'		
							//	),
						    ), 
                                    
//                                    #create search field
//									
//                                        'search_text' => array(
//								
//																						
//								1=>array('get_search_text'  => get_search_array('entity_child','id as ST1,ln as ST2','Title',1,0," WHERE entity_code='PG'")),
//								
//								//2=>array('get_search_text'  => get_search_array('entity','id as ST1,code as ST2','Code',2,1)), 
//								
//                                                              ),
//						
//				
//				#Search filter 
//				
//				'search_field' => 'sn',
//				
//				'search_id' 	=> array('id'),
				
				'is_narrow_down'=>1,
				
				'before_delete'=>1,
				
				
				# include
                                
				'js'            => array('is_top'=>1, 'top_js' => $LIB_PATH.'def/cms_page/d',
															   
							),
			   
				
				'bulk_action' => array(
							array('is_bulk_button' =>1,
							      'button_name'    => ' Update Content ',
							      'js_call'        => 'recreate_page'
							      )							
							
						),
				
				
				
				# summary:
				
				/*'summary_data'=>array(
							array(  'name'=>'No Data','field'=>'count(id)','html'=>'class=summary'),
				
				                   ),*/
				
				'custom_filter' => array(
							
							array(
								'field_name' 	=> 'Coach',
								
								'field_id' 	=> 'cf_ii',
								
								'filter_type' 	=> 'option_list', 
								    
								'option_value'	=> $G->option_builder('entity_child',"id,get_eav_addon_varchar(id,'ECSN')"," WHERE entity_code='CH'  ORDER BY get_eav_addon_varchar(id,'ECSN'),line_order ASC"),							   
								
								'attr'		=> ['class'=>'w_150'],
								
								'cus_default_label'=>'Show All',
							
								'filter_by'  	=> "get_eav_ec_id(id,'PGCH')"
							),
							
							
							array(
								'field_name' 	=> 'Parent',
								
								'field_id' 	=> 'cf_i',
								
								'filter_type' 	=> 'option_list', 
								    
								'option_value'	=> $G->option_builder('entity_child',"id,IF((get_eav_addon_num(id,'ECPR')>0),concat(get_eav_addon_varchar(get_eav_addon_num(id,'ECPR'),'ECSN'),' -> ',get_eav_addon_varchar(id,'ECSN')),get_eav_addon_varchar(id,'ECSN')) as sn"," WHERE entity_code='PG' AND is_active=1 ORDER BY sn,line_order ASC"),							   
								
								'attr'		=> ['class'=>'w_150'],
								
								'cus_default_label'=>'Show All',
							
								'filter_by'  	=> "get_eav_addon_num(id,'ECPR')"
							),
							
							
							array(
								'field_name' 	=> 'Addon Type',
								
								'field_id' 	=> 'cf_iii',
								
								'filter_type' 	=> 'option_list', 
								    
								'option_value'	=> $G->option_builder('entity_child_base','token,sn'," WHERE entity_code='AT'  ORDER BY sn,line_order ASC"),							   
								
								'attr'		=> ['class'=>'w_150'],
								
								'cus_default_label'=>'Show All',
							
								'filter_by'  	=> "get_eav_addon_varchar(id,'PGAT')"
							),
							    
				),
				
				
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>1,'page_link'=>'f=cms_page_eav', 'b_name' => 'Add' ),
								
					'del_permission' => array('able_del'=>1,'user_flage'=>1), 
								
					'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  ''),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'DMPG',
				
				'show_query'=>0,
                            
                            );
	
	
	// after delete
	function before_delete($param){
		
		$lv 	      = array();
		
		
		$lv['result'] = $param['g']->get_one_columm(['table'=>'entity_child',
						    'field'=>'code',
						    'manipulation'=>" WHERE id=$param[key_id]" 
						    ]);
		
		$content_page = 'template/page_content/'.$lv['result'].".html";		     
 
		if(is_file($content_page)){
		   
			unlink($content_page);
		}  		
		
	} // end
	
	
	$temp_addon = (@$_COOKIE['cms_pageget_field_id2'])?$_COOKIE['cms_pageget_field_id2']:@$_GET['cf_iii'];
	
	# addon
	if( ($temp_addon) && ($temp_addon!=-1)){
		
		$D_SERIES['add_button']['page_link'] = "f=cms_page_eav&default_addon=$temp_addon";
                
        }
    
?>