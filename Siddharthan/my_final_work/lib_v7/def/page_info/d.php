<?PHP

               
        $D_SERIES       =   array(
                                   'title'=>'Page Info',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
                                    
                                    #table data
                                    
                                    'data'=> array(
                                                        1=>array('th'=>'Page Family',
									 
								'field'=>'page_family as v1',
                                                                   
								'html'      =>  'style	= "cursor:pointer" onclick="JavaScript:E_V_PASS(\'sort_field\',1);E_V_PASS(\'sort_direction\',GET_E_VALUE(\'sort_col_1\'));filter_data();"',
								  
								'font'      =>  'class="sort"',
																
								'span'      =>  '<span id="sort_icon_1" name="sort_icon_1"></span>',
									 
								'td_attr' => ' class="label_grand_father align_LM" width="22%"',
									 
								),
							2=>array('th'=>'Page Code',
									 
								'field'=>'code as v2',
                                                                   
								'html'      =>  'style	= "cursor:pointer" onclick="JavaScript:E_V_PASS(\'sort_field\',1);E_V_PASS(\'sort_direction\',GET_E_VALUE(\'sort_col_1\'));filter_data();"',
								  
								'font'      =>  'class="sort"',
																
								'span'      =>  '<span id="sort_icon_1" name="sort_icon_1"></span>',
									 
								'td_attr' => ' class="label_grand_father align_LM" width="22%"',
									 
								),	
							3=>array('th'	=> 'Page Name',
									      
								'field'	=> 'sn as v3',
									
								'html'      =>  'style	= "cursor:pointer" onclick="JavaScript:E_V_PASS(\'sort_field\',2);E_V_PASS(\'sort_direction\',GET_E_VALUE(\'sort_col_2\'));filter_data();"',
								  
								'font'      =>  'class="sort"',
																
								'span'      =>  '<span id="sort_icon_2" name="sort_icon_2"></span>',
									      
								'td_attr' => ' class="label_father" ',
									      
								'td_attr' => ' class="align_LM" width="27%" '
									      
								),
							
							4=>array('th'	=> 'Path',
									      
								'field'	=> 'path as v4',
									
								'td_attr' => ' class="label_father" ',
									      
								'td_attr' => ' class="align_LM" width="27%" '
									      
								),	
                                                    ),
				    
                                    
                                    #Sort Info
                                      
                                       'sort_field' =>array('code',
							    
							    'sn',
							    
							    ),
                                        
                                       'action' => array('is_action'=>0, 'is_edit' =>1, 'is_view' =>0 ),
                                       
                                       'order_by'   =>'ORDER BY id ASC ' ,
				       		
                                
                                    #Table Info
                                    
                                    'table_name' =>'page_info',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
                                
                                    # Communication
                                
                                    'prime_index'   => 1,
                                
                                    # File Include
                                
                                    'js'            => 'm_code',
                                    
                                    #create search field
									
                                        'search_text' => array(
								
																						
								1=>array('get_search_text'  => get_search_array('page_info','id as ST1,sn as ST2','Page Name',1,0,'')),
								
								2=>array('get_search_text'  => get_search_array('page_info','id as ST1,code as ST2','Code',2,1,'')), 
								
                                                              ),
						
				
				#Search filter 
				
				'search_field' => 'sn,code',
				
				'search_id' 	=> array('id','code'),
				
				'custom_filter' => array(
							
							    array(  'field_name' => ' Page Family :',
								    'field_id' => 'pf', 'filter_type' =>'option_list', 
												    
								    'option_value'=> $G->enum_option_builder('page_info','page_family'),
							    
								    //'default_option'=>$G->get_entity_value('MP','fy_id'),
								    
								    'input_html'=>' class="w_200" ',
								    
								    'cus_default_label'=>'Show All',
							    
								    'filter_by'  => 'page_family'
							    ),
							), 
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>1,'page_link'=>'f=page_info', 'b_name' => 'Add info' ),
								
					'del_permission' => array('able_del'=>1,'user_flage'=>1), 
								
					'date_filter'  => array( 'is_date_filter' =>1,'date_field' =>  'timestamp'),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 1, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'DPGI'
                            
                            );
    
?>