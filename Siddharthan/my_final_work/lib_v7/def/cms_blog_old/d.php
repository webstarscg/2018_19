<?PHP
       
	$LAYOUT='layout_full';
	    
	include($LIB_PATH."def/cms_page/d.php");
	    
	$D_SERIES['key_filter']  = 	 " AND  entity_code='BL' AND parent_id>0 ";
	
	
	$D_SERIES['data'][3]=array('th'=>'Blog title ',
									 
								'field'=>'ln as v3',
                                                                   
									 
								'td_attr' => ' class="label_father align_LM" width="30%"',
									 
								);
	
	$D_SERIES['data'][6]=array('th'=>'Category',
									 
				'field'   => '(SELECT sn as e_child FROM entity_child as e_child WHERE e_child.id=entity_child.parent_id) as v6',
				 
				'td_attr' => ' class="label_grand_child align_LM" width="15%"',
					 
				);
	
	// search
	
	$D_SERIES['search_text'] = array(																       
						1=>array('get_search_text'  => get_search_array('entity_child','id as ST1,ln as ST2','Blog Title',1,0," WHERE entity_code='BL' AND parent_id > 0 AND is_active=1")),
				);
	
	$D_SERIES['search_field'] = 'sn';
				
	$D_SERIES['search_id'] 	= array('id');
	
	$D_SERIES['add_button']= array( 'is_add' =>1,'page_link'=>'f=cms_blog', 'b_name' => 'Add Blog' );
	
	$D_SERIES['page_code']  = 'DMBL';
	
	$D_SERIES['show_query'] = 0;
	
	$D_SERIES['del_permission'] = array('able_del'=>1,'user_flage'=>1);
      
     
?>