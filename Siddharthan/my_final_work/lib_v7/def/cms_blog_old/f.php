<?PHP

        $LAYOUT='layout_full';	
	
	use GuzzleHttp\Client;
				
	use GuzzleHttp\Query;
		
	include($LIB_PATH."def/cms/f.php");
	
	$F_SERIES['title']='Blog';
	
	$no_row = $G->table_no_rows( array('table_name'=>'entity_child','WHERE_FILTER'=>" AND entity_code='BL' AND parent_id=0"));
	
	$line_order= $no_row[0]+1;
	
	
	
	$F_SERIES['data'][14]=array( 'field_name'=> 'Category',
                                                               
                                                               'field_id' => 'parent_id',
                                                               
                                                               'type' => 'option',
                                                               
                                                               'option_data'=>$G->option_builder('entity_child','id,sn'," WHERE entity_code='BL' AND parent_id=0 AND is_active=1  ORDER by sn ASC"),
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_100" onchange="JavaScript:blog_root_count();"'
                                                               
                                                               );
	
	$F_SERIES['data']['4'] = array(
				       'field_name'=> 'Line Order', 
                                                               
					'field_id' => 'line_order',
				       
					'type' => 'text',
					
					'is_mandatory'=>1,
					
					'input_html'=>'class="w_75"  onkeypress = "return PR_All_Numeric(event);" maxlength="4"   value='.$line_order,
					
						
				);
	
    
       $F_SERIES['deafult_value'] = array('entity_code' => "'BL'",
					  'addon'       => '\'[["","layout_right"]]\''
					);
       
       $F_SERIES['back_to'] =  array( 'is_back_button' =>1, 'back_link'=>'?d=cms_blog', 'BACK_NAME'=>'Back');
       
       $F_SERIES['page_code']  = 'FMBL';
       
       $F_SERIES['show_query']  = 0;
       
       unset($F_SERIES['data'][11]);
       unset($F_SERIES['data'][1]);
       unset($F_SERIES['data'][9]); # addon
       
       
        # after add update
    
	function after_add_update($key_id){
	 
		  global $rdsql,$G,$LIB_PATH;
		  
		  $lv         = [];		  
		  $lv['temp'] = [];		
		  
		  $lib = $LIB_PATH.'/comp/guzzle_rest/guzzle/vendor/autoload.php';			
		  
		  require_once $lib;
		  
		  $lv['temp'] = $G->get_key_value('code','entity_child'," AND id=$key_id");
	       	
		  $client = new Client([
		       // You can set any number of default request options.
		       'timeout'  => 2.0,
		  ]);
		  
		  #print_r($lv['temp']);
		  		  
		  $node_res = $client->GET($_SERVER["HTTP_HOST"].$_SERVER["SCRIPT_NAME"],['query'=>['t_series'=>'basic',
												    'key'     =>$lv['temp']['code']
												   ]
											  ]
					   );
		  
		  
	} // end
       
       
       
?>