<?PHP
	# key

	
	
	if(@$key){
		$where = "WHERE entity_code='CO' ";		
	}
	else{
		$where = "WHERE   entity_code='CO'";
	}

	$F_SERIES = array(
	
		'title' => 'User Information',
	
		'table_name' => 'user_info',
	
		'key_id' => 'id',
	
		'data' => array( 
	
			'1' => array( 
				'field_name' => 'User Name',
				'field_id' => 'is_internal',
				'type' => 'option',
				'option_data' => $G->option_builder('entity_child',"id,get_eav_addon_varchar(id,'COFN' )", " $where "),
				'is_mandatory' => 1,
				//'avoid_default_option' => $default,
				'input_html'=>' class="w_200" onclick="JavaScript:change_roles(this);"'
			),
			
			'2' => array( 
				'field_name'=> 'User Role',
				'field_id' => 'user_role_id',
				'type' => 'option',
				'option_data' => $G->option_builder('user_role', 'id,ln', ''),
				'is_mandatory' => 1,
				'input_html'=>' class="w_200"'
			),
	
			'3' => array('field_name' => 'Password', 'field_id' => 'password', 'type' => 'password', 'is_mandatory' => 1,
				     
				     'filter_in'   => function($data_in){ 
                                
					             return md5($data_in);
                                                }
				),
	
			
	
		),
	
		#'no_edit' => array('password'),
	
		'is_user_id' => 'user_id',
		
		"default_fields"=>array('is_active'            => 1,
					'is_mail_check'        => 0,
					'is_send_mail'         => 0,
					'is_send_welcome_mail' => 0
				),

		'js' => array('is_top' => 1, 'top_js' => 'js/f_series/user_internal'),
	
		'back_to' => array('is_back_button' => 1, 'back_link' => '?d_series=user_internal', 'BACK_NAME' => 'Back'),
	
		'prime_index' => 1,
	
		'cascade_action' => 0,
	
		'before_add_update' => 0,
	
		'page_code'	=> 'FUSI'
	
	);

?>
