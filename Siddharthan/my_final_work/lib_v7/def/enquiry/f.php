<?PHP

   // define('PAGE_PERM', $_SESSION['MANAGE_SETUP']);
   
    $LAYOUT='layout_full';

    $F_SERIES =array(
                     
                     'layout' => 'layout_full',
                     
                     'title'=>'Enquiry',
                     
                     'session_off' => 1,
                     
                     'data'=>array('1'=>array('field_name'=>'Name',
                                              
                                              'field_id'=>'name',
                                              
                                              'type'=>'text',
                                              
                                              'allow'=>'x50',
                                              
                                              'is_mandatory'=>1
                                              
                                              ),
                                   
                                     '2'=>array('field_name'=>'Phone No.',
                                              
                                              'field_id'=>'phone_no',
                                              
                                              'type'=>'text',
                                              
                                              'allow'=>'d25[,- ]',
                                              
                                              'is_mandatory'=>0
                                              
                                              ),
                                   
                                     '3'=>array('field_name'=>'Email Id.',
                                              
                                              'field_id'=>'email_id',
                                              
                                              'type'=>'text',
                                              
                                              'allow'=>'x75',
                                              
                                              'is_mandatory'=>1
                                              
                                              ),
                                                                                  
                                   
                                     '4'=>array('field_name'=>'Enquiry On',
                                              
                                              'field_id'=>'enquiry_code',
                                              
                                              'type'=>'option',
                                              
                                              'option_data'=>$G->option_builder('entity_child_base',
                                                                                'token,sn',
                                                                                'WHERE entity_code = "EQ" order by sn'),
                                              
                                              //'option_data'=>"<option id=EQAC value='EQAC'>Automotive Components</option><option id=EQPC value='EQPC'>Pump Components</option><option id=EQTM value='EQTM'>Textile Machinery Parts and Assemblies</option><option id=EQOT value='EQOT'>Others</option>",
                                              
                                              'allow'=>'w140',
                                              
                                              'is_mandatory'=>1,
                                              
                                              'input_html'  => ' class=" w_250" '
                                              
                                              
                                              ),
                                   
                                
                                   
                                   '5'=>array('field_name'=>'Detail',
                                              
                                              'field_id'=>'query',
                                              
                                              'type'=>'textarea',
                                              
                                              'is_mandatory'=>1,
                                              
                                              'input_html'  => ' class=" w_250" '
                                              
                                              )
                                   
                                   //'4'=>array('field_name'=>'Line Order',
                                   //           
                                   //           'field_id'=>'line_order',
                                   //           
                                   //           'type'=>'text',
                                   //           
                                   //           'is_mandatory'=>1,
                                   //           
                                   //           'input_html'=>' maxlength=4 class=w_50 '
                                   //           
                                   //           ),
                                   //
                                   //'5'=>array('field_name'=>'Attribute Code',
                                   //           
                                   //           'field_id'=>'code',
                                   //           
                                   //           'type'=>'text',
                                   //           
                                   //           'is_mandatory'=>0,
                                   //           
                                   //           'input_html'=>' maxlength=4 class=w_50 '
                                   //           
                                   //           )
                                  ),
                     
                     'table_name'=>'enquiry',
                     
                     'key_id'    =>'id',
                     
                     'is_user_id' =>'user_id',
                     
                     'back_to'  =>array('is_back_button'=>0,'back_link'=>'?d=enquiry', 'BACK_NAME'=>'Back'),
                     
                     'button_name'   =>  ' Send Enquiry ',
                     
                     'prime_index'=>1,
                    
                    'alert'=>array(
					       'is_after_add' => 1,
					        
						'mail' => array(
								    'data' => array('Name'=>'name','phone_no'=>'phone_no','email_id'=>'Email','query'=>'Enquiry'),
								),
						
						'to'=> 'ratbew@gmail.com',
						'cc'=> 'raja@webstarscg.com',
						
                                                
						'subject' =>'Query Alert',
						'message' =>''
					      ),
                     
                     'flat_message'=> 'Thanks for your query. We will get back to you soon.',
                     
                     'page_code'=>'FENQ'
                                   
                    )
     
     
?>

<style type="text/css">
        footer{
            display: none !important;
        }
	
	fieldset{	
		background-color	        : #fcf9f7 !important;
		border				: 1px solid #d6c1ad !important;                
                padding-top                     : 25px !important;
                box-shadow                      : none !important;       
	}
        
        fieldset legend{
            
                display: none !important;
        }
        
        fieldset .label{
                color:  #985051 !important;
        }
        
        fieldset .button {
                /*border: 1px solid #985051 !important;*/
                background-color: #985051 !important;
                color: #985051 !important;
                box-shadow: 0px 0px 3px #d6c1ad;
                font-weight:bold !important;
                font-size: 18px !important;                
                text-transform: capitalize !important;
        }
        
        fieldset .button:hover{
            
                box-shadow: 0px 0px 0px #d6c1ad !important;
        }
	
        fieldset input,
	fieldset select,
        fieldset textarea{
		border: 1px solid #d6c1ad !important;
               	
	}
        
        fieldset input:focus,
	fieldset select:focus,
        fieldset textarea:focus{
		border: 1px solid #985051 !important;
               	
	}
        
        fieldset select option{
                background-color: #fff !important;
        }
        
        legend{
            border:none !important;
        }
	
</style>
    