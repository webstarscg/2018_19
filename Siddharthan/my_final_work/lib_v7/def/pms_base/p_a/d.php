<?PHP
        $LAYOUT	    = 'layout_full';
	
	$parent_id = @$_GET['default_addon'];
               
        $D_SERIES       =   array(
                                   'title'=>'Activity Details',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
				    
				    'gx' => 1,
				    
                                    #table data
                                    
                                    'data'=> array(
					                5=>array('th'=>'Project ',
								
								'field'		=>"get_exav_addon_varchar(parent_id,'PBNA')",
							                                                            
								'td_attr' 	=> ' class="label_father align_LM" width="20%"',
								
								'is_sort'	=> 1,	
								
								),
							
							1=>array('th'=>'Task ',
								
								'field'		=>"get_exav_addon_varchar(get_exav_addon_ec_id(id,'A1TS'),'TSNA')",
							                                                            
								'td_attr' 	=> ' class="label_father align_LM" width="20%"',
								
								'is_sort'	=> 1,	
								
								),
							
							2=>array('th'=>'Description ',
								
								'field'		=>"get_exav_addon_text(id,'A1DS')",
                                                                   
								'td_attr' 	=> ' class="label_father align_LM" width="20%"',
								
								'is_sort'	=> 1,	
								
								),
							
							7=>array('th'=>'Reported Date ',
								
								'field' => "date_format((get_exav_addon_date(entity_child.id,'A1DT')),'%d-%b-%y')",
                                                        
								'td_attr' => ' class="align_LM" width="10%"',
									 
								),
							
							3=>array('th'=>'Working hours ',
								
								'field'		=>"get_exav_addon_decimal_2(id,'A1WH')",
                                                                   
								'td_attr' 	=> ' class="label_father align_LM" width="20%"',
								
								'is_sort'	=> 1,	
								
								),
							
							4=>array('th'=>'Updation',
									 
								'field'=>"concat(get_user_internal_name(user_id),',',date_format(updated_on,'%d-%b-%y %T'))",
							        									 
								'td_attr' => 'width="10%"',
								
								'js_call'=> 'show_user_info',
									 
								),
							
                                                    ),
				    
					
                                       'action' => array('is_action'=>0, 'is_edit' =>0, 'is_view' =>0 ),
                                       
                                       'order_by'   =>'ORDER BY id ASC ' ,
				    
				       'table_name' =>'entity_child',
                                    
					'key_id'    =>'id',
					
					'is_user_id'       => 'user_id',
					
					'key_filter'     =>	 " AND  entity_code='A1'  AND parent_id = $parent_id",
					
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>1,'page_link'=>'f=pms_base__p_a', 'b_name' => 'Add Activity' ),
								
					'del_permission' => array('able_del'=>1,'user_flage'=>0), 
								
					'date_filter'  => array( 'is_date_filter' =>1,'date_field' =>  'updated_on'),	
					
					'custom_filter' => array(  			     						   
							      
									array(  'field_name' => 'User:',
									      
										'field_id' => 'cf1', // 
										
										'filter_type' =>'option_list', 
												    
										'option_value'=> $G->option_builder('user_info',"id,get_eav_addon_varchar(is_internal,'COFN')","
														    WHERE user_role_id = (SELECT id FROM user_role WHERE sn='SAD')
														    ORDER BY get_eav_addon_varchar(is_internal,'COFN')"),
							     
										'html'=>'  title="Select"   data-width="160px"  class="w_60"',
								    
										'cus_default_label'=>'Show All',
							    
										'filter_by'  => "user_id" // main table value
									),
									
									array(  'field_name' => 'Project:',
									      
										'field_id' => 'cf2', // 
										
										'filter_type' =>'option_list', 
												    
										'option_value'=> $G->option_builder('entity_child',"id,get_exav_addon_varchar(id,'PBNA')","
														    WHERE entity_code='PB'
														    ORDER BY get_exav_addon_varchar(id,'PBNA')"),
							     
										'html'=>'  title="Select"   data-width="160px"  class="w_60"',
								    
										'cus_default_label'=>'Show All',
							    
										'filter_by'  => "parent_id" // main table value
									),
							),
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'ACTY',
				
				'show_query'=>0,
				
                            
                            );
	
	if(@$_GET['default_addon']){
		
		
		$LAYOUT	    = 'layout_narrow_down';
		$D_SERIES['key_filter'] = " AND  entity_code='A1'";
		unset($D_SERIES['export_csv']);
		unset($D_SERIES['data'][5]);
		$D_SERIES['action']['is_edit']=0;
		$D_SERIES['add_button']['is_add']=0;
		$D_SERIES['del_permission']['able_del']=1;
		$D_SERIES['summary_data'] = [];
		$D_SERIES['hide_show_all'] = 1;
		$D_SERIES['search_filter_off']	=1;
		//$D_SERIES['hide_show_all'] = 1;
		$D_SERIES['hide_pager'] = 1;
		$D_SERIES['show_all_rows']=1;
		$D_SERIES['filter_off'] = 1;
	}else{
		
		$D_SERIES['key_filter'] = " AND  entity_code='A1'";
		
	}
	
	
    
?>