<?PHP

    $LAYOUT	= 'layout_full';
    
    $new_option = '<option value=-1 class="clr_green">+ Add New</option>';
    
    if(isset($_GET['default_addon'])){
	$parent_id = @$_GET['default_addon'];
    }else{
	$parent_id = 0;
    }
    $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Activity Details',
				
				#Table field
                    
				'data'	=>   array('1' =>array( 'field_name'=> 'Project',
                                                               
                                                               'field_id' => 'parent_id',
                                                               
                                                               'type' => 'option',
							       
							       'option_data' => $G->option_builder("entity_child","id,get_exav_addon_varchar(id,'PBNA')",
												   " WHERE entity_code='PB'
												   ORDER BY id ASC").$new_option,
							       
							       'input_html'=>"class='w_150' onchange='add_new_master(this,2)'",
                                                               
							       'is_mandatory'=>1,
							       
							       'is_hide'=>0,
							       
                                                               ),
						  
						   '2'=>array( 'field_name' => 'New Project',
							      
								'field_id'=>'entity_code',
                                                               
								'type'=>'text',
								
		                                                'is_hide'=>1,
	               
	                                                        'is_mandatory'=>0,
								
								'ro'	=>1,
								
								'allow'		=> 'w128[ ]',
								
								'input_html'=> "  onchange='check_project(this);'",
								
							),
						  
						   '3' =>array( 'field_name'=> 'Task',
                                                               
                                                               'field_id' => 'ec_id',
                                                               
                                                               'type' => 'option',
							       
							       'option_data' => $G->option_builder("entity_child","id,get_exav_addon_varchar(id,'TSNA')",
												   " WHERE entity_code='TS' AND parent_id=$parent_id ORDER BY sn ASC").$new_option,
							       
							       'child_table'         => 'exav_addon_ec_id', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'exa_token',   // attribute code field
							       
							       'child_attr_code'     => 'A1TS',           // attribute code
							       
							       'input_html'=>"class='w_150' onchange='add_new_master(this,4)'",
                                                               
							       'is_mandatory'=>1,
							       
							       'is_hide'=>0,
							       
                                                               ),
						  
						   '4'=>array( 'field_name' => 'New Task',
							      
								'field_id'=>'entity_code',
                                                               
								'type'=>'text',
								
		                                                'is_hide'=>1,
	               
	                                                        'is_mandatory'=>0,
								
								'ro'	=>1,
								
								'allow'		=> 'w128[ ]',
								
								'input_html'=> "  onchange='check_task(this,$parent_id);'",
								
							),
						   
//						   '5' =>array( 'field_name'=> 'Activity',
//                                                               
//                                                               'field_id' => 'exa_value',
//                                                               
//                                                               'type' => 'text',
//							       
//							       'child_table'         => 'exav_addon_varchar', // child table
//							       
//							       'parent_field_id'     => 'parent_id',    // parent field
//										       
//							       'child_attr_field_id' => 'exa_token',   // attribute code field
//							       
//							       'child_attr_code'     => 'A1AC',           // attribute code
//							       
//							       'input_html'=>"class='w_150'",
//                                                               
//							       'is_mandatory'=>1,
//							       
//                                                               ),
						  
						   
						   '5' =>array( 'field_name'=> 'Description',
                                                               
                                                               'field_id' => 'exa_value',
                                                               
                                                               'type' => 'textarea',
							       
							       'child_table'         => 'exav_addon_text', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'exa_token',   // attribute code field
							       
							       'child_attr_code'     => 'A1DS',           // attribute code
							       
							       'input_html'=>"class='w_250'",
                                                               
							       'is_mandatory'=>1,
							       
                                                               ),
						   
						   '6' =>array('field_name'=>'Reported Date',
                                                               
                                                               'field_id'=>'exa_value',
                                                               
                                                               'type' => 'date',
							       
							       'default_date' => $date,
							       
							       'set'          => array('min_date'=>'-3D','max_date'=>'0D'),
							       
                                                               'child_table'         => 'exav_addon_date', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
							       
							       'child_attr_field_id' => 'exa_token',
								 
							       'child_attr_code'     => 'A1DT',           // attribute code
                                                               
							       'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_200"'
                                                               
                                                               ),
						   
						   '7' =>array( 'field_name'=> 'Working hours',
                                                               
                                                               'field_id' => 'exa_value',
                                                               
                                                               'type' => 'text',
							       
							       'child_table'         => 'exav_addon_decimal', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'exa_token',   // attribute code field
							       
							       'child_attr_code'     => 'A1WH',           // attribute code
							       
							       'input_html'=>"class='w_100'",
                                                               
							       'is_mandatory'=>1,
							       
                                                               ),
						   
						   
					   ),
                                
				'table_name'    => 'entity_child',
				
				'before_add_update'=> 1,
				
				'key_id'        => 'id',
				
				'js'=> ['is_top'=>1,'top_js'=>$LIB_PATH.'def/pms_base/p_a/f'],
				
				'deafult_value'    => array('entity_code' => "'A1'",
							    'parent_id'=>''.$parent_id.''),
				
				
				
				'is_user_id'       => 'created_by',
				
				'back_to'  => array( 'is_back_button' =>0, 'back_link'=>'?d=pms_base', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 1,
                                
				'page_code'	=> 'ACTY', //irrelevant information
				
				'show_query'    =>0,
                                
			);
    
    if(isset($_GET['default_addon'])){
	
	unset($F_SERIES['data'][1]);
	unset($F_SERIES['data'][2]);
    
    }else{
	
	$F_SERIES['deafult_value']    = array('entity_code' => "'A1'");
				
	
	$F_SERIES['back_to']['is_back_button'] = 1;	
	
	$F_SERIES['data'][1]['option_data']=$new_option;
	$F_SERIES['data'][1]['avoid_default_option']=1;
	$F_SERIES['data'][3]['avoid_default_option']=1;
	
	$F_SERIES['data'][2]['is_hide']=0;
	$F_SERIES['data'][4]['is_hide']=0;
	
	$F_SERIES['data'][2]['is_mandatory']=1;
	$F_SERIES['data'][4]['is_mandatory']=1;
	$F_SERIES['data'][7]['is_mandatory']=0;
	
	echo  "<style>
		    #X1_panel { display: none;}
		    #X3_panel { display: none;} 
	      </style>";
	
    }
    
     function before_add_update(){
	
	global $G,$F_SERIES,$rdsql,$USER_ID;
	
	$parent_id = $_GET['default_addon'];
	
	if($_POST['X1']==-1){
	    
	    $value = $_POST['X2'];
	    
	    $insert_child = $rdsql->exec_query("INSERT INTO entity_child (entity_code,user_id) VALUES ('PB',$USER_ID)","Insertion of PB entity child Failed");
	     
	    $temp = $rdsql->last_insert_id('entity_child');
	    
	    $_POST['X1'] = $temp;
	    
	    $parent_id  = $temp;
	    
	    $insert_varchar = $rdsql->exec_query("INSERT INTO exav_addon_varchar (parent_id,exa_token,exa_value,user_id) VALUES ($parent_id,'PBNA','$value',$USER_ID)",
						 "INSERT INTO exav_addon_varchar failed");
	    
	}
	
	if($_POST['X3']==-1){
	    
	    $value = $_POST['X4'];
	    
	    $insert_child = $rdsql->exec_query("INSERT INTO entity_child (entity_code,parent_id,user_id) VALUES ('TS',$parent_id,$USER_ID)","Insertion of entity child Failed");
	     
	    $temp = $rdsql->last_insert_id('entity_child');
	    
	    $_POST['X3'] = $temp;
	    
	    $parent_id  = $temp;
	    
	    $insert_varchar = $rdsql->exec_query("INSERT INTO exav_addon_varchar (parent_id,exa_token,exa_value,user_id) VALUES ($parent_id,'TSNA','$value',$USER_ID)",
						 "INSERT INTO exav_addon_varchar failed");
	    
	}
	
	return null;
    }

?>
