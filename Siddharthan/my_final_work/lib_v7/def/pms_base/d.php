<?PHP
        $LAYOUT	    = 'layout_full';
               
        $D_SERIES       =   array(
                                   'title'=>'Project Details',
                                    
                                    'is_user_base_query'=>0,
				    
				    'gx' => 1,
				    
                                    'data'=> array(
					                
							1=>array('th'=>'Project Name ',
								
								'field'		=>"get_exav_addon_varchar(id,'PBNA')",
                                                                   
								'td_attr' 	=> ' class="label_father align_LM" width="10%"',
								
								'is_sort'	=> 1,	
								
								),
							
							4=>array('th'=>'Description ',
								
								'field'     => "concat_ws(':',substring_index(get_exav_addon_text(id,'PBDS'),' ',7),get_exav_addon_text(id,'PBDS'))",
								
								'td_attr' => ' class="label_father align_LM" width="30%"',
								
								'is_sort' => 0,
								
								'filter_out'=>function($data_in){
									
									$temp = explode(':',$data_in);
									
									return "<a class='tip clr_gray_5'>$temp[0]...</a><span class='tooltiptext'>$temp[1]</span>";
										
								}
								
								),
					
							2=>array('th'=>'Start date ',
								
								'field' => "ifnull(date_format((get_exav_addon_date(id,'PBSD')),'%d-%b-%y'),'--NA--')",
								
								'td_attr' => ' class="align_RM" width="10%"',
								
								'is_sort'	=> 1,
									 
								),
							3=>array('th'=>'End date ',
								
								'field' => "ifnull(date_format((get_exav_addon_date(id,'PBED')),'%d-%b-%y'),'--NA--')",
								   
								'td_attr' => ' class="align_RM" width="10%"',
								
								'is_sort'	=> 1,
									 
								),
							
							9=>array('th'=>'Updation',
									 
								'field'=>"concat(get_user_internal_name(user_id),',',date_format(updated_on,'%d-%b-%y %T'))",
							        									 
								'td_attr' => 'width="10%"',
								
								'js_call'=> 'show_user_info',
									 
								),
							
							
							
							5=>array('th'	=> 'Task',
								 
								'th_attr'=>' colspan=2 ',
								
								'field'	=> "concat(id,':',(SELECT COUNT(*) FROM entity_child WHERE entity_code='TS' AND parent_id = key_id))",
								
								'attr' =>  [ 'class'=>""],
									
                                                                'filter_out'=>function($data_in){
													$temp = explode(':',$data_in);
						
													$data_out = array('id'   => $temp[0],
												         
													 'link_title'=>''.$temp[1],
													 'is_fa'=>' fa-chevron-circle-right clr_dark_orange fa-lg ',
													 'is_fa_btn'=>' btn-default btn-sm ',
													 'title'=>'Task View',
												         'src'=>"?d=pms_base__p_t&menu_off=1&mode=simple&default_addon=$temp[0]",
												         'style'=>"border:none;width:100%;height:600px;");
													 return json_encode($data_out);
													 
													 
												 },
                                                                        
                                                                        'js_call'=>'d_series.set_nd'
									
									
                                                                        
							),
							
							6=>array('th'	=> '',
								     
								  'field'	=> 'id',
								  
								  'attr' =>  [ 'class'=>"brdr_right"],
									
								  'filter_out'=>function($data_in){
                                                                            
													$data_out = array('id'   => $data_in,
													'link_title'=>'Add task',
													'is_fa'=>' fa fa-plus-square-o clr_dark_orange fa-lg',
													'title'=>'Add task',
													'src'=>"?f=pms_base__p_t&menu_off=1&mode=simple&default_addon=$data_in",
													'style'=>"border:none;width:100%;height:600px;"
												  );
														
													return json_encode($data_out);
													 
												 },
                                                                        
                                                                        'js_call'=>'d_series.set_nd'
                                                                        
								    ),
							
							7=>array('th'	=> 'Activity', 'th_attr'=>' colspan=2 ',
								     
								'field'	=> "concat(id,':',(SELECT COUNT(*) FROM entity_child WHERE entity_code='A1' AND parent_id = key_id))",
								
								'attr' =>  [ 'class'=>""],
									
                                                                'filter_out'=>function($data_in){
													$temp = explode(':',$data_in);
						
													$data_out = array('id'   => $temp[0],
												         
													 'link_title'=>''.$temp[1],
													 'is_fa'=>' fa-chevron-circle-right clr_dark_blue fa-lg ',
													 'is_fa_btn'=>' btn-default btn-sm ',
													 'title'=>'Activity View',
												         'src'=>"?d=pms_base__p_a&menu_off=1&mode=simple&default_addon=$temp[0]",
												         'style'=>"border:none;width:100%;height:600px;");
													 return json_encode($data_out);
													 
													 
												 },
                                                                        
                                                                        'js_call'=>'d_series.set_nd'
									
									
                                                                        
								    ),
							
							8=>array('th'	=> '',
								     
								  'field'	=> 'id',
								  
								  'attr' =>  [ 'class'=>"brdr_right"],
									
								  'filter_out'=>function($data_in){
                                                                            
													$data_out = array('id'   => $data_in,
													'link_title'=>'Add activity',
													'is_fa'=>' fa fa-plus-square-o clr_dark_blue fa-lg',
													'title'=>'Add activity',
													'src'=>"?f=pms_base__p_a&menu_off=1&mode=simple&default_addon=$data_in",
													'style'=>"border:none;width:100%;height:600px;"
												  );
														
													return json_encode($data_out);
													 
												 },
                                                                        
                                                                        'js_call'=>'d_series.set_nd'
                                                                        
								    ),
							
//							10=>array('th'	=> 'Issue',
//								 
//								'td_attr' => 'width="10%"',
//								
//								'field'	=> "id",
//								
//								'attr' =>  [ 'class'=>""],
//									
//                                                                'filter_out'=>function($data_in){
//													$temp = explode(':',$data_in);
//						
//													$data_out = array('id'   => $temp[0],
//												         
//													 'link_title'=>''.$temp[1],
//													 'is_fa'=>' fa-chevron-circle-right clr_dark_orange fa-lg ',
//													 'is_fa_btn'=>' btn-default btn-sm ',
//													 'title'=>'Task View',
//												         'src'=>"?d=pms_base__p_t&menu_off=1&mode=simple&default_addon=$temp[0]",
//												         'style'=>"border:none;width:100%;height:600px;");
//													 return json_encode($data_out);
//													 
//													 
//												 },
//                                                                        
//                                                                        'js_call'=>'d_series.set_nd'
//									
//									
//                                                                        
//							),
							
							
							
                                                    ),
				    
					
                                       'action' => array('is_action'=>0, 'is_edit' =>1, 'is_view' =>0 ),
                                       
                                       'order_by'   =>'ORDER BY id ASC ' ,
				       
					'table_name' =>'entity_child',
                                    
					'key_id'    =>'id',
					
					# Default Additional Column
				    
					'is_user_id'       => 'user_id',
					
					'key_filter'     =>	 " AND  entity_code='PB'  ",
				    
					# Communication
				    
					'prime_index'   => 1,
				    
					'is_narrow_down'=>1,
					
					'before_delete'=>1,
					
					'js'            => array('is_top'=>1, 'top_js' => $LIB_PATH.'def/pms_base/d',),
			   
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>1,'page_link'=>'f=pms_base__p_a', 'b_name' => 'Add New' ),
								
					'del_permission' => array('able_del'=>0,'user_flage'=>1), 
								
					'date_filter'  => array( 'is_date_filter' =>1,'date_field' =>  'updated_on'),	
					
					
					 'custom_filter' => array(  			     						   
							      
									array(  'field_name' => 'User:',
									      
										'field_id' => 'cf1', // 
										
										'filter_type' =>'option_list', 
												    
										'option_value'=> $G->option_builder('user_info',"id,get_eav_addon_varchar(is_internal,'COFN')","
														    WHERE user_role_id = (SELECT id FROM user_role WHERE sn='SAD')
														    ORDER BY get_eav_addon_varchar(is_internal,'COFN')"),
							     
										'html'=>'  title="Select"   data-width="160px"  class="w_60"',
								    
										'cus_default_label'=>'Show All',
							    
										'filter_by'  => "user_id" // main table value
									),
					),
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'DMPG',
				
				'show_query'=>0,
                            
                            );
	
	
    
?>