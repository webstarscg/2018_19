<?PHP

    $LAYOUT	= 'layout_full';
    
    if(isset($_GET['default_addon'])){
	$parent_id = @$_GET['default_addon'];
    }else{
	$parent_id = 0;
    }
    $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Activity Details',
				
				#Table field
                    
				'data'	=>   array('1' =>array( 'field_name'=> 'Project',
                                                               
                                                               'field_id' => 'parent_id',
                                                               
                                                               'type' => 'option',
							       
							       'option_data' => $G->option_builder("entity_child","id,get_exav_addon_varchar(id,'PBNA')",
												   " WHERE entity_code='PB'
												   ORDER BY id ASC"),
							       
							       'input_html'=>"class='w_150'",
                                                               
							       'is_mandatory'=>1,
							       
							       'is_hide'=>0,
							       
                                                               ),
						  
						   
						   '4'=>array( 'field_name' => 'New Task',
							      
								'field_id' => 'exa_value',
                                                               
                                                               'type' => 'text',
							       
							       'child_table'         => 'exav_addon_varchar', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'exa_token',   // attribute code field
							       
							       'child_attr_code'     => 'TSNA',           // attribute code
							       
							       'input_html'=>"class='w_250'",
	               
	                                                        'is_mandatory'=>1,
								
								'allow'		=> 'w256[ ]',
								
								'input_html'=> "  onchange='check_task(this,$parent_id);'",
								
							),
						  
						   '6' =>array( 'field_name'=> 'Description',
                                                               
                                                               'field_id' => 'exa_value',
                                                               
                                                               'type' => 'textarea',
							       
							       'child_table'         => 'exav_addon_text', // child table
							       
							       'parent_field_id'     => 'parent_id',    // parent field
										       
							       'child_attr_field_id' => 'exa_token',   // attribute code field
							       
							       'child_attr_code'     => 'TSDS',           // attribute code
							       
							       'input_html'=>"class='w_250'",
                                                               
							       'is_mandatory'=>0,
							       
                                                               ),
						   
					   ),
                                
				'table_name'    => 'entity_child',
				
				'before_add_update'=> 0,
				
				'key_id'        => 'id',
				
				'js'=> ['is_top'=>1,'top_js'=>$LIB_PATH.'def/pms_base/p_t/f'],
				
				'deafult_value'    => array('entity_code' => "'TS'",
							    'parent_id'=>''.$parent_id.''),
				
				
				
				'is_user_id'       => 'created_by',
				
				'back_to'  => array( 'is_back_button' =>0, 'back_link'=>'?d=pms_base__p_t', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 1,
                                
				'page_code'	=> 'ACTY', //irrelevant information
				
				'show_query'    =>1,
                                
			);
    
    if(isset($_GET['default_addon'])){
	
	unset($F_SERIES['data'][1]);
	
    }else{
	
	$F_SERIES['deafult_value']    = array('entity_code' => "'TS'");
	$F_SERIES['back_to']['is_back_button'] = 1;	
	
    }
    
    

?>
