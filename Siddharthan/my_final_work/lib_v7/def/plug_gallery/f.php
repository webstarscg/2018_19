<?PHP

    $LAYOUT	    = 'layout_full';
                            
    $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Gallery',
				
				#Table field
                    
				'data'	=>   array(
						   '1' =>array( 'field_name'=> 'Title',
                                                               
                                                               'field_id' => 'sn',
                                                               
                                                               'type' => 'text',
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_200"',
							       'filter_in'	=> function($data_in){
													   return preg_replace(array('/\s{2,}/i'),
															       array(''),																															        
															       $data_in);
										       }	
                                                               
                                                               ),
						   
						   '2' =>array('field_name'	=> 'Image',
                                                               
                                                               'field_id'	=> 'image',
                                                               
                                                               'type'		=> 'file',
							       
							       'upload_type'    => 'image',
							       
							       'location' 	=> 'images/gallery/',
							       
							       'image_size'	=> array(280=>175,
											 840=>525),                                                              
                                                               'is_mandatory'	=> 1,
                                                               
							       'input_html'	=> 'class="w_400"'
                                                               
                                                               ),
						    
						    
						        ),
                                    
				#Table Name
				
				'table_name'    => 'entity_child',
				
				#Primary Key
                                
			        'key_id'        => 'id',
				
				'deafult_value'    => array('entity_code' => "'GL'", 'is_active' => "1"),
				
				# Default Additional Column
                                
				'is_user_id'       => 'created_by',
								
				# Communication
								
				'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?d=plug_gallery', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 1,
				
				#'after_add_update'=>1, 
                                
				# File Include
                                //'after_add_update'	=>1,
				
				'page_code'	=> 'FMET',
				'show_query'    =>0,
                                
			);
    
    
?>