<?PHP

  $mode =(@$_GET['mode']);

  $key = (@$_GET['key']);

  $is_org = @$_GET['is_org'];

  $parent_id = @$_GET['parent_id'];

  $type = @$_GET['type'];
  
  $org_type = (!@$_GET['cg_code'])?' entity_code=\'OR\' AND code <> \'HOME\' ':' entity_code=\'CG\' AND code=\''.@$_GET['cg_code'].'\' ';

  # layout full
  
  $LAYOUT='layout_full';

  #F Series Definition:

  $F_SERIES = array(

  'title' => 'Contact Information',

  'data' => array(
            
    '1' => array('field_name'   => 'Contact Type', 'field_id' => 'comm_group_code', 'type' => 'option',
                 'option_data'  => $G->option_builder('entity_child','code,sn',"WHERE entity_code='CG' AND is_active=1 order by sn ASC "), 'avoid_default_option' => 1,
                 'is_mandatory' => 0,
                 'input_html'   => ' class="w_200" ' 
                 
                 ),

    '2' => array('field_name' => 'Short Name', 'field_id' => 'sn', 'type' => 'text', 'is_mandatory' => 1),

    '3' => array('field_name' => 'Summary', 'field_id' => 'ln', 'type' => 'textarea', 'is_mandatory' => 1,'input_html'=>' class="w_300"  maxlength="200" rows=3'),

    '4' => array('field_name' => 'Address 1', 'field_id' => 'add_i', 'type' => 'textarea', 'is_mandatory' => 1,'input_html'=>' class="w_300"  maxlength="300" rows=4'),

    

    '6' => array('field_name' => 'Pincode', 'field_id' => 'pincode', 'type' => 'text', 'is_mandatory' => 0),

    '7' => array('field_name' => 'Phone', 'field_id' => 'phone', 'type' => 'text', 'is_mandatory' => 0),

    '8' => array('field_name' => 'Mobile', 'field_id' => 'mobile', 'type' => 'text', 'is_mandatory' => 0),

    '9' => array('field_name' => 'Email', 'field_id' => 'email', 'type' => 'text', 'is_mandatory' => 0,
		 
		 //'validate' => 'data_validate(\'email\',this)'
		 
		 ),
    
    '10' => array('field_name' => 'Website', 'field_id' => 'website', 'type' => 'text', 'is_mandatory' => 0),

    '5' => array('field_name' => 'Map Link', 'field_id' => 'add_ii', 'type' => 'textarea', 'is_mandatory' => 0,'input_html'=>' class="w_300"  maxlength="500" rows=2',
		 'hint'=>'<span class="clr_green">Give the Google Map URL</span>'
		 
		 ),
    
    '11' => array('field_name' => 'Fax No.', 'field_id' => 'fax_no', 'type' => 'hidden', 'is_mandatory' => 0),

    '13' => array('field_name' => 'Type', 'field_id' => 'is_org', 'type' => 'hidden',  'avoid_default_option' => 1, 'is_mandatory' => 0,'input_html'=> ' value=1' ),
    
    '14' => array('field_name' 	  => 'Type',
		  'field_id'      => 'is_img',
		  'type'          => 'file',                
                  'location' 	  => 'images/contact/',
		  'upload_type'   => 'image', 
		  'image_size'	  => array(120 =>200,500 =>500),
		  'hint'	  => '<span class="clr_green">Scaled to 400x400px</span>'
            ),
    
    '15' => array('field_name' => 'Type', 'field_id' => 'is_active', 'type' => 'option',                
		'option_data' => '<option value="1" selected class="clr_green">Active</option>'.
		                 '<option value="0" class="clr_red">In-Active</option>',
		'avoid_default_option'=>1,
		
	  ),

  ),

  # Table Info

  'table_name' => 'communication',

  'key_id' => 'id',

  # Default Additional Column

  'is_user_id' => 'user_id',

  

  #Action:

  'cascade_action' => 0,

  'after_add_update' => 0,

  # Communication
                        
  'back_to'	=> array('is_back_button' => 1, 'back_link' => '?d=contact', 'BACK_NAME' => 'Back'),
    
  'prime_index' => 3,
    
  # File Include
    
  // 'js' => array('is_top' => 1, 'top_js' => 'js/f_series/contact'),
  
  # Custom Message

  // 'message' => array('sn'),

  # Page Code

  'page_code' => 'FMCP',
  
  # Form Layout
  
  #'form_layout' => 'form_100'
  
);
  
  

  
  
?>