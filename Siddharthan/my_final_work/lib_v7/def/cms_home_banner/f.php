<?PHP
   
    $LAYOUT='layout_full';
    
    if(@$_GET['key']){
       $WHERE_FILTER = " WHERE entity_code='PG' AND parent_id=0 AND id NOT IN($_GET[key])";
					
    }else{    
       $WHERE_FILTER = " WHERE entity_code='PG' AND parent_id=0";
    }
   
  
   
   
    //F_series definition:
                            
    $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Home banner',
				
				#Table field
                    
				'data'	=>   array(
						   '1' =>array('field_name'=>'Title',
                                                               
                                                               'field_id'=>'sn',
                                                               
                                                               'type'=>'text',
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'attr'=> ['class' => "w_100"]
                                                               
                                                               ),
						   
						   '2' =>array('field_name'=>'Url',
                                                               
                                                               'field_id'=>'ln',
                                                               
                                                               'type'=>'text',
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>' class="W_150" ' 
                                                               
                                                               ),
						   
						   '3' =>array('field_name'=>'Image',
                                                                
								'field_id'=>'image',
                                                                
								'type'=>'file',
								
								'upload_type'=>'image',
								
								'save_ext_type'=>'jpg',
							        
								'location' => 'images/home_banner/',
							        
								'image_size'=>array(1600=>450),
                                                                
								'is_mandatory'=>0,
                                                                
								'input_html'=>' class="W_150" ',
								
								/*'filter_in' => function($data_in){
											//json_encode(@$image_data),
											$temp     = explode('[C]',$data_in);
											$flag     = [1,0];
											$data_out = array(
													  'data'=>array(
															//'id'   => $temp[0],
															'key'  => md5($temp[0]),
															//'key'   => md5($_GET['key']),
															'label' => 'Page',
															'action'=>'general',
															'series'=>'a',
															'token' =>'ECAI',
															'table' => 'entity_child',
															'field' => 'image',
															'key_filter' =>array(
																	      'entity_code' =>'HB',
																	      'id' => $temp[0]
																	      )
															)
													);
											return json_encode($data_out);
										    },*/
								
								//'save_file_name_prefix'=> 'prefix_',
								//
								//'save_file_name_suffix'=> '_suffix',
								// 
								//'image_size_auto'    => ['landscape' => [
								//					['w'=>800,'h'=>400],
								//					['w'=>400,'h'=>200],
								//					['w'=>200,'h'=>100],
								//			    ],
								//	     
								//	     'portrait' => [
								//			     ['w'=>400,'h'=>800],
								//			     ['w'=>200,'h'=>400],
								//			     ['w'=>100,'h'=>200],
								//			     ],
								//	     
								//	     
								//	     'square' => [
								//			    ['w'=>400,'h'=>400],
								//			     ['w'=>200,'h'=>200],
								//			     ['w'=>100,'h'=>100],
								//			 ]
								//	     ],
																
                                                               ),
				    
                                ),
                                    
				#Table Name
				
				'table_name'    => 'entity_child',
				
				#Primary Key
                                
			        'key_id'        => 'id',
				
				
				//'after_add_update'=>1,
				
                                
				# Default Additional Column
                                
				'is_user_id'       => 'created_by',
				
				'deafult_value'    => array('entity_code' => "'HB'", 'is_active'=>1),
								
				# Communication
								
				'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?d=cms_home_banner', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 1,
                                
				# File Include
                                //'after_add_update'	=>1,
				
				'js'            => array('is_top'=>1,'top_js'=>'js/f_series/manage_cms'),
				
				'page_code'	=> 'FCHB',
				'show_query'    =>0,
                                
			);
    
    
       if(isset($_GET['default_addon'])){
	
	$F_SERIES['back_to']['is_back_button'] = 0;
	
	}
          
?>