<?PHP

    //F_series definition:
                            
    $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Entity',
                                
                                'gx'    => 1,
				
				#Table field
                    
				'data'	=>   array(			
                                                    
                                                  4=>array(
                                                                    'field_name'          => 'Fibenis Data',                                                                
                                                                    
                                                                    'field_id'            => 'detail',				       
                                                                    
                                                                    'is_fibenistable'     => 1,
                                                                    
                                                                    'type'                => 'fibenistable',
                                                               
                                                                    'is_mandatory'        => 1,
                                                                    
                                                                    'min_rows_to_fill'    => 1, 
                                                                                                                        
                                                                    'default_rows_prop'   => array('start_rows'=>'3','max_row'=>3),
								   
                                                                    'is_index'            =>1,
                                                                  
                                                                    'default_data'  => '[[12345,"abcdef",22,{"id":313,"name":"Unit"}]]',
								    
								    'update_route_point'=>[ 'is_update_route_point' => 1,
											    'method' => 'post',
											    'url'    => 'router.php?series=a&action=test&token=FT_TEST&FTUPDATE=1',	
											  ],
								    
								    'after_datapreparation' =>'router.php?series=a&action=test&token&TEST_UPDATE&key=1',
								    
								     /* update_route_point=>[
									'series'=>'a',
									'action'=>'test',
									'token'=>'TEST_UPDATE',
									 method => 'post' ]
						  
									After datapreparation-> router.php?series=a&action=test&token&TEST_UPDATE&key=1
									
									     content=>'[[]]*/
								     
                                                                    'colHeaders'=> array(

											 
                                                                                            array(
                                                                                                    'column'    => 'A',
                                                                                                    'width'     => '50',
                                                                                                    'type'      => 'text',
                                                                                                    //'input_html' => '  onkeyup=cal_total(this);',
                                                                                                    'key_up_addon'=> 'cal_total(this)',
                                                                                                    'allow'     => 'd5',
                                                                                                    
                                                                                            ),
                                                                                          
  
                                                                                            array(
                                                                                                    'column'    => 'B',
                                                                                                    'width'     => '50',
                                                                                                    'type'      => 'text',
                                                                                                    //'input_html' => '  onkeyup=cal_total(this);',
                                                                                                    'key_up_addon'=> 'cal_total(this)',
                                                                                                    'allow'     => 'd5',
                                                                                                    
                                                                                            ),
                                                                                            
                                                                                            
                                                                                             array(
                                                                                                    'column'    => 'C',
                                                                                                    'width'     => '50',
                                                                                                    'type'      => 'text',
                                                                                                    'input_html' => '  readonly=true onchange="cal_total(this)"',
                                                                                                   // 'onchange'=> 'cal_total(this)',
                                                                                                    'allow'     => 'x5',
                                                                                                    
                                                                                            ),
                                                                                          
                                                                                          
  
//											     array( 'column'    =>'Select Mutliple',
//                                                                                                    'width'     =>'125',
//                                                                                                    'type'      =>'dropDown',                                                                                                   
//                                                                                                    'data'=>$G->ft_option_builder('entity','id,sn',''),
//												    'input_html' =>''
//                                                                                                ),
											     

                                                                                                    
                                                                                         ),
								     
								    
                                                                     
                                                                     'is_hide'=>0,
                                                                     
                                                               ),
                                                  
                                                  
                                                            //2=>['type'=>'autocomplete',
                                                            //    'field_id'=>'ec_id',
                                                            //    'field_name' => 'AC',
                                                            //    
                                                            //    'child_table'         => 'exav_addon_ec_id', // child table 
                                                            //    'parent_field_id'     => 'parent_id',    // parent field
                                                            //                               
                                                            //    'child_attr_field_id' => 'exa_token',   // attribute code field
                                                            //    'child_attr_code'     => 'TTIX',           // attribute code
                                                            //    
                                                            //     'remote_link' => 'router.php?series=a&action=test&token=FT_AUT', 
                                                            //    
                                                            //],
                                                            
                                                            //3=>array('type'=>'autocomplete',
                                                            //    'field_id'=>'parent_id',
                                                            //    'field_name' => 'AC Flat',
                                                            //    'is_mandatory'        => 1,
                                                            //    
                                                            //     'remote_link' => 'router.php?series=a&action=test&token=FT_AUT', 
                                                            //    
                                                            //   
                                                            //),
                                                            //
                                                            
                                                         1=>array(
                                                                     
                                                                'type'=>'option',
                                                                
                                                                'field_id'=>'parent_id',
                                                                'field_name' => 'Option',
                                                                'is_mandatory'        => 1,
                                                                
                                                                'attr' => ['onchange'=>"JavaScript:(function(ele){ var temp=(ele.value==1)?1:0; element_show_hide(4,temp); })(this);checkIsSelect(this);",
                                                                           
                                                                           'data-update-prefill-off' => 1
                                                                           ],
                                                                
                                                                'option_data'=>'<option value="1">Show</option><option value=2>Hide</option>'
                                                                
                                                            )
                                                  
//                                                               
				    
                                ),
                                    
				#Table Name
				
				'table_name'    => 'entity_child_old',
				
				#Primary Key
                                
			        'key_id'        => 'id',
                                
				# Default Additional Column
                                
				'is_user_id'       => 'user_id',
                                
                                'js'=> ['is_top'=>1,'top_js'=>$LIB_PATH.'def/test/f'],
								
				# Communication
								
				'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?d=test', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 1,
                                
                                'form_layout'   => 'form_100',
                                 
                                'default_fields' => ['entity_code'=>'TT'],
                                
				# File Include
                                
				//'js'            => 'q_details',
				
				#Page Code
				
                                
                                
				#'page_code'	=> 'FETY
				
                                'show_query' => 0,
                                
                                'divider' => 'accordion'
                                
			);
    
    
    
    
?>

