<?PHP

    //F_series definition:
                            
    $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Entity',
                                
                                'gx'    => 1,
				
				#Table field
                    
				'data'	=>   array(			
						   
                                                    1 => array( 'field_name'=> 'Heading I', 
                                                               
                                                           
                                                               
                                                                'type'     => 'heading',
                                                                
                                                            
                                                                
                                                                #'hint'        =>'Give 5 digits'
                                                                
                                                                ),
                                                    
                                                    20=>array(
                                                                    'field_name'  => 'Photo', # table display column head                      
                                                                    'field_id'    => 'image', # db table field id
                                                                    'type'        => 'file',  #
                                                             
                                                                    # docs or image, 
                                                                    # docs for (other than image formats)
                                                                    # image for jpeg,jpg,png formats
                                                              
                                                                    'upload_type' => 'docs',   
                                                                   
                                                                   
                                                                    'location'    => 'images/entity_child/', # file save location    
                                                             
                                                                    // file name to save. (optional)
                                                                    // By default, it will save by primary id           
                                                                    
                                                                    'save_file_name' => '',          # filename used to save
                                                             
                                                                    'save_file_name_prefix'=> 'prefix_',     # results prefix_<id>.<file_format>'
                                                                                                             
                                                                    'save_file_name_suffix'=> '_suffix',     # results <id>_suffix.<file_format>'
                                                                    
                                                                    ),
                                                    
                                                    
                                                    21=>array(
                                                                    'field_name'  => 'Photo', # table display column head                      
                                                                    'field_id'    => 'image_a', # db table field id
                                                                    'type'        => 'file',  #
                                                             
                                                                    # docs or image, 
                                                                    # docs for (other than image formats)
                                                                    # image for jpeg,jpg,png formats
                                                              
                                                                    'upload_type' => 'image',   
                                                                   
                                                                   
                                                                    'location'    => 'images/entity_child/', # file save location    
                                                             
                                                                    // file name to save. (optional)
                                                                    // By default, it will save by primary id           
                                                                    
                                                                    #'save_file_name' => '',          # filename used to save
                                                             
                                                                    'save_file_name_prefix'=> 'img_a_',     # results prefix_<id>.<file_format>'
                                                                                                             
                                                                   # 'save_file_name_suffix'=> '_suffix',     # results <id>_suffix.<file_format>'
                                                                    
                                                                    ),
                                                    
                                                    
                                                     22=>array(
                                                                    'field_name'  => 'Photo', # table display column head                      
                                                                    'field_id'    => 'image_b', # db table field id
                                                                    'type'        => 'file',  #
                                                             
                                                                    # docs or image, 
                                                                    # docs for (other than image formats)
                                                                    # image for jpeg,jpg,png formats
                                                              
                                                                    'upload_type' => 'image',   
                                                                    'image_size'     =>  array(400 => 640,       # different size of images to create 
                                                                                                    300 => 480,       # width => height 
                                                                                                    100 => 160),
                                                                                                                                     
                                                                    'location'    => 'images/entity_child/', # file save location    
                                                             
                                                                    // file name to save. (optional)
                                                                    // By default, it will save by primary id           
                                                                    
                                                                    #'save_file_name' => '',          # filename used to save
                                                             
                                                                    'save_file_name_prefix'=> 'img_b_',     # results prefix_<id>.<file_format>'
                                                                                                             
                                                                   # 'save_file_name_suffix'=> '_suffix',     # results <id>_suffix.<file_format>'
                                                                    
                                                                    ),
                                                    
//						    2 => array( 'field_name'=> 'Simple List', 
//                                                               
//                                                                 'field_id' => 'is_active',
//                                                               
//                                                                'type'     => 'option',
//                                                                
//                                                                'option_data'=>'<option value="1">OK</option>',
//                                                                
//                                                                'is_mandatory'=>1,
//                                                              
//                                                                
//                                                                ),
//                                                    
                                                     3 => array( 'field_name'=> 'Date', 
                                                               
                                                                'field_id' => 'created_on',
                                                                 
                                                                'type'     => 'date',
                                                                 
                                                                'year_range'   =>'1950:2000',
                                                                
                                                                'default_date' => "01-01-2000",
                                                                 
                                                                'is_mandatory'=>1,
                                                              
                                                                
                                                                ),
                                                   
//						      4 => array( 'field_name'=> 'Text', 
//                                                               
//                                                                'field_id' => 'sn',
//                                                               
//                                                                'type'     => 'text',
//                                                                
//                                                                'allow'    =>'v1-50',
//                                                                
//                                                                'is_mandatory'=>1,
//                                                                
//                                                                #'hint'        =>'Give 5 digits'
//                                                                
//                                                                'input_html'  => ''
//                                                                
//                                                                ),
                                                      
                                                      
                                                       5 => array( 'field_name'=> 'Heading II', 
                                                               
                                                           
                                                               
                                                                'type'     => 'heading',
                                                                
                                                            
                                                                
                                                                #'hint'        =>'Give 5 digits'
                                                                
                                                                ),
                                                       
                                                         
                                                           
						   
                                                    //6 => array( 'field_name'=> 'Text Area', 
                                                    //           
                                                    //            'field_id' => 'sn',
                                                    //           
                                                    //            'type'     => 'textarea',
                                                    //            
                                                    //            //'allow'    =>'x10-100',
                                                    //            
                                                    //            'is_mandatory'=>1,
                                                    //            
                                                    //            'allow'    =>'v1-10',
                                                    //            
                                                    //            'hint'        =>'<span></span>'
                                                    //            
                                                    //            ),
                                                    //
                                                    
                                                    //14 => array( 'field_name'=> 'Text Area 2<br>
                                                    //                            <div class="pad_5_tb clr_gray_a txt_size_12"><input type=checkbox onchange=JavaScript:copy_address(this,"X6","X14");>&nbsp;Check, if the address is same as permananet</div>', 
                                                    //           
                                                    //            'field_id' => 'sn',
                                                    //           
                                                    //            'type'     => 'textarea',
                                                    //            
                                                    //            //'allow'    =>'x10-100',
                                                    //            
                                                    //            'is_mandatory'=>1,
                                                    //            
                                                    //            'allow'    =>'v1-10',
                                                    //            
                                                    //            #'hint'        =>'Give 5 digits'
                                                    //            
                                                    //            ),
                                                     
                                                     
                                                    7 => array(
                                                          'field_name'=> 'Check Yes or No.',                                                                
                                                          'field_id' => 'is_active',                                                               
                                                          'type'      => 'toggle',
                                                          'is_round'  => 0,
                                                          'show_status_label'=>1,
                                                          'is_default_on'=>1,
                                                          'on_label'   =>'Show',
                                                          'off_label'  =>'Hide',
                                                         ),
                                                   
						   
						   8 => array(
								 'field_name'=> 'Auto complete',                                                                
                                                                 'field_id' => 'code',                                                               
                                                                 'type'      => 'autocomplete',
								 'remote_link' => 'router.php?series=a&action=test&token=FT_AUT', 
								 
								 'is_mandatory'        => 0,
								// 'source_url'=>'router.php?series=a&action=test&token=ENLI', 

                                                                ),
						   
						   
						   9 => array(
								 'field_name'=> 'Auto complete restrict mode',                                                                
                                                                 'field_id' => 'ln',                                                               
                                                                 'type'      => 'autocomplete',
								 'remote_link' => 'router.php?series=a&action=test&token=FT_AUT', 								 
								 'is_mandatory'        => 0,								 
								 'restrict_new_entry'=> 1,
								 
								// 'source_url'=>'router.php?series=a&action=test&token=ENLI', 

                                                                ),
                                                   
                                                   
						     10=> array(
                                                        
                                                        
                                                        'field_name'          => 'Toggle EAV',                                                                
                                                        'field_id'            => 'ea_value',				       
                                                       
                                                    
                                                        //child table
                                                                
                                                        'child_table'         => 'eav_addon_varchar', // child table 
                                                        'parent_field_id'     => 'parent_id',    // parent field
                                                                                
                                                        'child_attr_field_id' => 'ea_code',   // attribute code field
                                                        'child_attr_code'     => 'TTAC',           // attribute code
                                                                 

                                                        'type'                => 'toggle',
                                                        'is_round'=>1,
                                                          'show_status_label'=>1
                                                        ),
						   
                                                   
                                                  
                                                    
                                                      11 => array( 'field_name'=> 'Check with Grid', 
                                                               
                                                                'field_id' => 'sn',
                                                               
                                                                'type'     => 'text',
                                                                
                                                                'allow'    =>'d5',
                                                                
                                                                'is_mandatory'=>1,
                                                                
                                                                #'hint'        =>'Give 5 digits'
                                                                
                                                                ),
                                                    
                                                  12 =>array(
                                                                    'field_name'          => 'Fibenis Data',                                                                
                                                                    
                                                                    'field_id'            => 'detail',				       
                                                                    
                                                                    'is_fibenistable'     => 1,
                                                                    
                                                                    'type'                => 'fibenistable',
                                                               
                                                                    'is_mandatory'        => 1,
                                                                    
                                                                    'min_rows_to_fill'    => 1, 
                                                                                                                        
                                                                    'default_rows_prop'   => array('start_rows'=>'2','max_row'=>3),
								   
                                                                    'is_index'            =>1,
                                                                  
                                                                    'default_data'  => '[[12345,"abcdef",22,{"id":313,"name":"Unit"}]]',
								    
								    'update_route_point'=>[ 'is_update_route_point' => 1,
											    'method' => 'post',
											    'url'    => 'router.php?series=a&action=test&token=FT_TEST&FTUPDATE=1',	
											  ],
								    
								    'after_datapreparation' =>'router.php?series=a&action=test&token&TEST_UPDATE&key=1',
								    
								     /* update_route_point=>[
									'series'=>'a',
									'action'=>'test',
									'token'=>'TEST_UPDATE',
									 method => 'post' ]
						  
									After datapreparation-> router.php?series=a&action=test&token&TEST_UPDATE&key=1
									
									     content=>'[[]]*/
								     
                                                                    'colHeaders'=> array(

											 
                                                                                            array(
                                                                                                    'column'    => 'Num 5 (Unique)',
                                                                                                    'width'     => '50',
                                                                                                    'type'      => 'text',
                                                                                                    'input_html' => '  onchange=cal_total(this);',
                                                                                                    'allow'     => 'd5',
                                                                                                    
                                                                                            ),
                                                                                          
                                                                                          array(   'column'     => 'Alpha. Num. 10',
                                                                                                    'width'     => '100',
                                                                                                    'type'      => 'text',
												    'allow'      => 'w10',
                                                                                                    'input_html' => ' readonly'

                                                                                                ),
											  
											  
											   array(   'column'    => 'Option',
                                                                                                    'width'     => '200',
                                                                                                    'type'      => 'date', 
                                                                                                    'data'      => $G->ft_option_builder('entity_attribute','id,entity_code'," ORDER BY id ASC"),
												    'is_default_value' =>0,
                                                                                                    'input_html' => 'maxlength=8'
                                                                                                ),
											   
//											   array(   'column'=>'Auto Complete Dynamic',                                                                                             
//                                                                                                    'width'=>'150',
//                                                                                                    'type'=>'autocomplete',
//                                                                                                    'get_data_url'=>'router.php?series=a&action=test&token=FT_TEST',
//												   
//												    
//                                                                                                ),
//											   
//                                                                                           array(   'column'     => 'Auto Comp. Static',
//                                                                                                    'width'      => '150',
//                                                                                                    'type'       => 'autocomplete',                                                                                                   
//                                                                                                    'get_data_url'=>'router.php?series=a&action=test&token=FT_TEST',
//                                                                                                    'restrict_new_entry'=>1,
//                                                                                                ),
											   
//											     array(   'column'=>'Select Mutliple',
//                                                                                                    'width'=>'125',
//                                                                                                    'type'=>'multiple_select',                                                                                                   
//                                                                                                    'data'=>$G->ft_option_builder('entity','id,sn',""),
//												    'input_html' =>'data-container="body" '
//                                                                                                ),
											     
											     
											     array(  'column'=>'Date',
                                                                                                     'width'=>'125',
                                                                                                     'type'=>'date',                                                                                                   
                                                                                                   ),
                                                                                                    
                                                                                         ),
								     
								    
                                                                     
                                                                     'is_hide' => 0, 
                                                                     
                                                               ),
                                                  
//                                                     13 =>array(
//                                                                    'field_name'          => 'Fibenis Table Calculation',                                                                
//                                                                    
//                                                                    'field_id'            => 'detail',				       
//                                                                    
//                                                                    'is_fibenistable'     => 1,
//                                                                    
//                                                                    'type'                => 'fibenistable',
//                                                               
//                                                                    'is_mandatory'        => 1,
//                                                                    
//                                                                    'min_rows_to_fill'    => 1, 
//                                                                                                                        
//                                                                    'default_rows_prop'   => array('start_rows'=>'2','max_row'=>3),
//								   
//                                                                    'is_index'            =>1,
//                                                                  
////                                                                    'default_data'  => '[[12345,"abcdef",22,{"id":313,"name":"Unit"}]]',
////								    
////								    'update_route_point'=>[ 'is_update_route_point' => 1,
////											    'method' => 'post',
////											    'url'    => 'router.php?series=a&action=test&token=FT_TEST&FTUPDATE=1',	
////											  ],
//								    
//								    #'after_datapreparation' =>'router.php?series=a&action=test&token&TEST_UPDATE&key=1',
//								    
//								     /* update_route_point=>[
//									'series'=>'a',
//									'action'=>'test',
//									'token'=>'TEST_UPDATE',
//									 method => 'post' ]
//						  
//									After datapreparation-> router.php?series=a&action=test&token&TEST_UPDATE&key=1
//									
//									     content=>'[[]]*/
//								     
//                                                                    'colHeaders'=> array(
//
//											 
//                                                                                            array(
//                                                                                                    'column'    => 'Field 1',
//                                                                                                    'width'     => '50',
//                                                                                                    'type'      => 'text',                                                                                                 
//                                                                                                    'allow'     => 'd5',
//                                                                                                    'key_up_addon'=>'cal_total(this)'
//                                                                                            ),
//                                                                                          
//                                                                                          array(   'column'     => 'Field 2',
//                                                                                                    'width'     => '100',
//                                                                                                    'type'      => 'text',
//												    'allow'      => 'd5',
//                                                                                                    'key_up_addon'=>'cal_total(this)'
//
//                                                                                                ),
//											
//                                                                                            array(   'column'     => 'Field 3',
//                                                                                                    'width'     => '100',
//                                                                                                    'type'      => 'text',
//												   
//                                                                                                    'input_html' => ' readonly '
//
//                                                                                                ),
//											  
//                                                                                         ),
//								     
//								    
//                                                                     
//                                                                     'is_hide' => 0, 
//                                                                     
//                                                               ),
				    
                                ),
                                    
				#Table Name
				
				'table_name'    => 'entity_child',
				
				#Primary Key
                                
			        'key_id'        => 'id',
                                
				# Default Additional Column
                                
				'is_user_id'       => 'user_id',
                                
                                'js'=> ['is_top'=>1,'top_js'=>$LIB_PATH.'def/test/f'],
								
				# Communication
								
				'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?d=test', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 1,
                                
                                'form_layout'   => 'form_100',
                                 
                                'default_fields' => ['entity_code'=>'TT'],
                                
				# File Include
                                
				//'js'            => 'q_details',
				
				#Page Code
				
                                'after_add_update' => 1,
                                
				#'page_code'	=> 'FETY
				
                                'show_query' => 0,
                                
                                'divider' => 'accordion'
                                
			);
    
    function after_add_update(){
       
       //echo "test";
        header('Location:?d=test');
		 
		return null;
    }
    
    
?>
<script>
    
    function copy_address(element,from,to){
        
        if (element.checked==true){        
            ELEMENT(to).value=ELEMENT(from).value;        
        }else{            
            ELEMENT(to).value='';        
        }
    
    } // end    
    
</script>
