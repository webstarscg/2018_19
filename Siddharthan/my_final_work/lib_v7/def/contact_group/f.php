<?PHP

    $LAYOUT	    = 'layout_full';
                            
    $F_SERIES	=	array(
				#Desk Title
				
				'title'	=>'Contact Group',
				
				#Table field
                    
				'data'	=>   array(
						   
						   
						   '1' =>array( 'field_name'=> 'Group Name ',
                                                               
                                                               'field_id' => 'sn',
                                                               
                                                               'type' => 'text',
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_200"'
                                                               
                                                               ),
						   
						   
						   '2' =>array( 'field_name'=> 'code ',
                                                               
                                                               'field_id' => 'code',
                                                               
                                                               'type' => 'text',
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_50" maxlength=4 value="CG" onkeypress  = "return PR_All_Alpha_Numeric(event);" ',
							       
							       'hint'=> 'Kindly give four letter code starts with CG'
                                                               
                                                               ),
						   
						   
						   
						   '3' =>array( 'field_name'=> 'Group Image ',
                                                               
                                                               'field_id' => 'image',
							       
							       
                                                               
                                                               'type' => 'file',
							       
							       'upload_type'   => 'image', 
                                                               
							       'location' => 'images/gallery/',
							       
                                                               'is_mandatory'=>0,
                                                               
                                                               'image_size'=>array(200=>200)
                                                               
                                                               ),
					    ),
                                    
				#Table Name
				
				'table_name'    => 'entity_child',
				
				#Primary Key
                                
			        'key_id'        => 'id',
				
				'deafult_value'    => array('entity_code' => "'CG'", 'is_active' => "1"),
				
				# Default Additional Column
                                
				'is_user_id'       => 'created_by',
								
				# Communication
								
				'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?d=contact_group', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 1,
                                
				# File Include
                                //'after_add_update'	=>1,
				
				'page_code'	=> 'FMCG',
				'show_query'    =>0,
                                
			);
    
   
    
    
?>