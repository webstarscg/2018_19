<?PHP
        $LAYOUT	    = 'layout_full';
               
        $D_SERIES       =   array(
                                   'title'=>'Contact Group',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
                                    
                                    #table data
                                    
                                    'data'=> array(					                
							1 => array('th' => 'Img', 'field' => 'image as v1',
								     
								     'td_attr' => ' width="5%"  ',
								     
								     'filter_out'   =>	function($data_out){
									
                                                                        $temp = explode(',',$data_out);
                                                                        
									return '<img class="w_100" src="'. $temp[0].'">';
								     }
							
							),
					
							2 => array('th' => 'Code', 'field' => 'code as v2',								     
								     'td_attr' => ' width="7%" class="label_grand_child"  '
							),
					
							3=>array(
									'th'      => 'Contact Group',									 
									'field'   => 'sn as v3',
									'td_attr' => ' class="label_father align_LM" width="61%"',									 
								),
							4=>array('th'=>'Status',
									 
								'field'=>'CONCAT(id,"[C]",is_active) as v4',
								
								'filter_out'=>function($data_in){
										
											$temp     = explode('[C]',$data_in);
											
											$flag     = [1,0];
											
											$data_out = array(
													  'data'=>array('id'   => $temp[0],
															'key'  => md5($temp[0]),
															'label'=> 'Page',
															'cv'   => $temp[1],
															'fv'   => $flag[$temp[1]],
															'series'=>'a',
															'action'=>'entity_child',
															'token' =>'ECAI'
															)
													);
											
											return json_encode($data_out);
										},
                                                                   
								/*'html'      =>  'style	= "cursor:pointer" onclick="JavaScript:E_V_PASS(\'sort_field\',2);E_V_PASS(\'sort_direction\',GET_E_VALUE(\'sort_col_2\'));filter_data();"',
								  
								'font'      =>  'class="sort"',*/
																
								'span'      =>  '<span id="sort_icon_2" name="sort_icon_2"></span>',
									 
								'td_attr' => ' class="label_child align_LM  txt_case_upper b" style="padding-right:10px;" width="3%"',
								
								'js_call'=> 'd_series.inline_on_off',
									 
								),
					),
				    
                                    
                                    #Sort Info
                                      
                                       'sort_field' =>array(),
                                        
                                       'action' => array('is_action'=>0, 'is_edit' =>1, 'is_view' =>0 ),
                                       
                                       'order_by'   =>'ORDER BY id ASC ' ,
				       		
                                
                                    #Table Info
                                    
                                    'table_name' =>'entity_child',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
                                
                                    # Communication
                                
                                    'prime_index'   => 1,
                                
                                    # File Include
                                
                                  'js'            => array('is_top'=>1, 'top_js' => 'js/d_series/manage_cms'),
                                    
                                    #create search field
									
                                        'search_text' => array(
								
																						
								1=>array('get_search_text'  => get_search_array('entity_child','id as ST1,sn as ST2','Title',1,0,' WHERE entity_code="CG" AND parent_id=0')),
								
								//2=>array('get_search_text'  => get_search_array('entity','id as ST1,code as ST2','Code',2,1)), 
								
                                                              ),
						
				
				#Search filter 
				
				'search_field' => 'sn',
				
				'search_id' 	=> array('id'),
				
				'key_filter'   => ' AND entity_code = "CG" AND parent_id=0', 
				
				'is_narrow_down'=>1,
				
				'custom_filter' => array(),
				
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>1,'page_link'=>'f=contact_group', 'b_name' => 'Add Contact Group' ),
								
					'del_permission' => array('able_del'=>1,'user_flage'=>1), 
								
					'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  ''),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'DMCG'
                            
                            );
    
?>