<?PHP
        $LAYOUT	    = 'layout_full';
               
        $D_SERIES       =   array(
                                   'title'=>'',
                                    
                                    #query display depend on the user
                                    
                                    'is_user_base_query'=>0,
                                    
                                    #table data
                                    
                                    'data'=> array(
					
							
							4=>array('th'=>'Coach',
									 
								'field'=>" get_eav_addon_varchar(parent_id,'CHDN') as v4",
                                                                   
									 
								'td_attr' => ' class="label_father align_LM" width="30%"',
									 
								),
							 
					              
						      1=>array('th'=>'Name',
									 
								'field'=>'ln as v1',
                                                                   
									 
								'td_attr' => ' class="label_father align_LM" width="30%"',
									 
								),
						      
						      
							2=>array('th'=>'Logo',
									 
								'field'=>'image as v2',
                                                                   
									 
								'td_attr' => ' class="label_father align_LM" width="30%"',
								
								'filter_out'   =>	function($data_out){
									
                                                                        $temp = explode(',',$data_out);
                                                                        
									return '<img class="w_25" src="'. $temp[0].'">';
									
									},	 
								),
							
						     3=>array('th'=>'&nbsp;',
									 
								'field'=>'CONCAT_WS(",",id,detail,image,sn,get_eav_addon_varchar(parent_id,\'CHDN\')) as v3',
								'td_attr' => ' class="label_father align_LM" width="5%"',
								'js_call'=> 'image_edit',	 
								),
					
								
                                                    ),
				    
                                    
                                    #Sort Info
                                      
                                       'sort_field' =>array('code','sn'),
                                        
                                       'action' => array('is_action'=>1, 'is_edit' =>0, 'is_view' =>0 ),
                                       
                                       'order_by'   =>'ORDER BY id ASC ' ,
				       		
                                
                                    #Table Info
                                    
                                    'table_name' =>'entity_child',
                                    
                                    'key_id'    =>'id',
                                    
                                    # Default Additional Column
                                
                                    'is_user_id'       => 'user_id',
                                
                                    # Communication
                                
                                    'prime_index'   => 1,
                                
                                    # File Include
                                
					'js'            => array('is_top'=>1, 'top_js' => $LIB_PATH.'def/plug_image/d'),
                                    
                                    #create search field
									
                                        'search_text' => array(
								
																						
								1=>array('get_search_text'  => get_search_array('entity_child','id as ST1,sn as ST2','Title',1,0,' WHERE entity_code="MQ" ')),
								
								//2=>array('get_search_text'  => get_search_array('entity','id as ST1,code as ST2','Code',2,1)), 
								
                                                              ),
						
				
				#Search filter 
				
				'search_field' => 'sn',
				
				'search_id' 	=> array('id'),
				
				'search_filter_off' => 1,
				
				'hide_pager'    => 1,
				
				'hide_show_all'    => 1,
				
				'is_narrow_down'=>1,
				
				'key_filter'   => " AND entity_code='MP' AND code='PIMG' ",
				
				#summary:
				
				/*'summary_data'=>array(
							array(  'name'=>'No Data','field'=>'count(id)','html'=>'class=summary'),
				
				                   ),*/
				
				
				'custom_filter' => array(
					
				),
				
				#check_field
								
					'check_field'   =>  array('user_id' => @$_GET['user_id'],'page_code' => @$_GET['page_code']),								
								
					'add_button' => array( 'is_add' =>0,'page_link'=>'f=plug_logo', 'b_name' => 'Add Master Pannel' ),
								
					'del_permission' => array('able_del'=>0,'user_flage'=>1), 
								
					'date_filter'  => array( 'is_date_filter' =>0,'date_field' =>  ''),	
								
				#export data
				
				'export_csv'   => array('is_export_file' => 0, 'button_name'=>'Create CSV','csv_file_name' => 'csv/log_'.time().'.csv'  ),
								
				'page_code'    => 'DPIM',
				'show_query'   => 0
                            
                            );
    
?>