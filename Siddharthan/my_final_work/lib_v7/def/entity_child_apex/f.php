<?PHP

    //F_series definition:
                            
    $F_SERIES	=	array(
				# Desk Title
				
				'title'	=>'Entity Attribute',
				
				# Table field
                    
				'data'	=>   array('1' =>array( 'field_name'=> 'Entity',
                                                               
                                                               'field_id' => 'entity_code',
                                                               
                                                               'type' => 'option',
                                                               
                                                               'option_data'=>$G->option_builder('entity','code,sn'," WHERE code='MP'  ORDER by sn ASC"),
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'avoid_default_option' => 1,
                                                               
                                                               'input_html'=>'class="w_200"'
                                                               
                                                               ),
				   
						   '2' =>array('field_name'=>'Name',
                                                               
                                                               'field_id'=>'sn',
                                                               
                                                               'type'=>'text',
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_200"'
                                                               
                                                               ),
						   
						   '8' =>array('field_name'=>'Page Code',
                                                               
                                                               'field_id'=>'code',
                                                               
                                                               'type'=>'text',
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_200"'                                                               
                                                               ),
						   
						   
						   '5' =>array('field_name'=>'Label',
                                                               
                                                               'field_id'=>'ln',
                                                               
                                                               'type'=>'textarea',
							       #'type'=>'textarea',
                                                               
                                                               'is_mandatory'=>1,
                                                               
                                                               'input_html'=>'class="w_200"'
                                                               
                                                               ),
						   
												
						   '3' => array( 'field_name'=> 'Status', 
                                                               
                                                                'field_id' => 'status_code',
                                                               
                                                                'type' => 'option',
                                                                
                                                                'option_data'=>$G->option_builder('entity_attribute','code,sn'," WHERE entity_code='PP'  ORDER BY sn ASC"),
                                                               
                                                                'is_mandatory'=>1,
                                                                
                                                                'input_html'=>'class="W_150"'
                                                                
                                                                ),
						   
                                                   '9' => array(    'field_name'=> 'Line Order', 
                                                               
                                                                    'field_id' => 'line_order',
                                                               
                                                                    'type' => 'text',
                                                                    
                                                                    'allow' => 'd03',
                                                                                                                                
                                                                    'input_html'=>'class="w_50" value="0"'
                                                                
                                                                )
                                                 
				    
                                ),
                                    
				#Table Name
				
				'table_name'    => 'entity_child',
				
				#Primary Key
                                
			        'key_id'        => 'id',
                                                          
								
				# Communication
								
				'back_to'  => array( 'is_back_button' =>1, 'back_link'=>'?d=entity_child_apex', 'BACK_NAME'=>'Back'),
                                
				'prime_index'   => 1,
                                
				# File Include
                                'after_add_update'	=>0,
				
				'page_code'	=> 'FENC',
				'show_query'    =>0,
                                
			);
?>