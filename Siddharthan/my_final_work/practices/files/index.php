<?php
$filename = 'config.php';

if (file_exists($filename)) {
    echo "The file $filename exists";
    header('Location: http://www.google.com/');
} else {
    echo "The file $filename does not exist";
    header('Location: http://www.duckduckgo.com/');
}
?>