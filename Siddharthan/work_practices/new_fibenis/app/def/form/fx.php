<?PHP

	$F_SERIES	=   array(
					
			'title'	     => 'Configuraion',
					
			# columns
			'data'	=>  array(

						'0'	=>	array(
								'field_name'			=>	'Fibenis Configuration',
								'type'					=>	'heading',		
							),

						'1' =>	array(
								'field_name'			=> 	'Host',
								'type'                  =>  'text',
								'hint'                  =>  'Enter Your Host Name',
								'is_mandatory'          =>  1,
							),
					
						'2' =>	array(
								'field_name'			=> 	'Database Name',
								'type'                  =>  'text',
								'hint'                  =>  'Enter Your DB Name',
								'is_mandatory'          =>  1,                        
							),

						'3'	=>	array(
								'field_name'			=> 	'User Name',
								'type'                  =>  'text',
								'hint'                  =>  'Enter Your User Name',
								'is_mandatory'          =>  1,
							),
							
						'4'	=>	array(
								'field_name'			=> 	'Password',
								'type'                  =>  'password',
								'hint'                  =>  'Enter Your Password',
								'is_mandatory'          =>  0,
							),

				),//data array ends here...
			
			'key_id'           	=> 'id',    				
			'is_user_id'   		=> 'user_id	',
			'session_off'       =>  1,
			
			# communication		    
			'back_to'  			=> array( 
									'is_back_button' =>1,
									'back_link'=>'#'),       				
			'show_query'  		=> 1 , //for debugging	
			
			'flat_message'      => 'Configured Successfully !!!',

			'before_add_update'	=>1
			
		);

	use GuzzleHttp\Client;
     
	use GuzzleHttp\Query;

	function before_add_update(){
		global $G,$PASS_ID,$LIB_PATH;

			$value  = [];
			$lv 	= [];

			$value['host'] 		= $_POST['X1'];
			$value['db_name'] 	= $_POST['X2'];
			$value['user'] 		= $_POST['X3'];
			$value['password'] 	= $_POST['X4'];

			//$d_a = $_GET['default_addon'];
		  
		  	$lib = $LIB_PATH.'/comp/guzzle_rest/vendor/autoload.php';				
		  
		  	require_once $lib ;
		  
		  	$client = new Client(['timeout'  => 2.0,]);
		   

			$lv['req']  = json_encode(
								['db_name' 	=>  $value['db_name'],
								 'host'		=>	$value['host'],
								 'user'		=>	$value['user'],
								 'password'	=>	$value['password'],
								]
								);
				
			$lv['trans_key']	=	time().rand().$PASS_ID;//  

			$lv['temp_req']		=	$G->encrypt($lv['req'],$lv['trans_key']);

			//print_r($lv);					
								
			$node_res = $client->GET($_SERVER["HTTP_HOST"].$_SERVER["SCRIPT_NAME"],
						['query'		=>	  ['tx'=> $_GET['fx'],
												'req'       	=> $lv['temp_req'],
												'trans_key' 	=> $lv['trans_key']
												]
						]);


			header('Location: ../app/index.php');
	}

?>