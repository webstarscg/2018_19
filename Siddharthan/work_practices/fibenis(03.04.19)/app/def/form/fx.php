<?PHP

	$F_SERIES	=   array(
					
			'title'	     => 'Configuraion',
					
			# columns
			'data'	=>  array(

						'0'	=>	array(
								'field_name'			=>	'Fibenis Configuration',
								'type'					=>	'heading',		
							),

						'1' =>	array(
								'field_name'			=> 	'Host',
								'type'                  =>  'text',
								'hint'                  =>  'Enter Your Host Name',
								'is_mandatory'          =>  1,
							),
					
						'2' =>	array(
								'field_name'			=> 	'Database Name',
								'type'                  =>  'text',
								'hint'                  =>  'Enter Your DB Name',
								'is_mandatory'          =>  1,                        
							),

						'3'	=>	array(
								'field_name'			=> 	'User Name',
								'type'                  =>  'text',
								'hint'                  =>  'Enter Your User Name',
								'is_mandatory'          =>  1,
							),
							
						'4'	=>	array(
								'field_name'			=> 	'Password',
								'type'                  =>  'password',
								'hint'                  =>  'Enter Your Password',
								'is_mandatory'          =>  0,
							),

				),//data array ends here...
			
			'key_id'           	=> 'id',    				
			'is_user_id'   		=> 'user_id	',
			'session_off'       =>  1,
			
			# communication		    
			'back_to'  			=> array( 
									'is_back_button' => 1,
									'back_link'=>'#'),       				
			'show_query'  		=> 0 , //for debugging	
			
			'flat_message'      => 'Configured Successfully !!!',

			'before_add_update'	=>1
			
		);

	function before_add_update(){
		
		global $G,$PASS_ID,$LIB_PATH;

			$value  = [];

			$value['host'] 		= $_POST['X1'];
			$value['db_name'] 	= $_POST['X2'];
			$value['user'] 		= $_POST['X3'];
			$value['password'] 	= $_POST['X4'];

			/*print_r($value);
			echo '<hr>';

			$options = array(
				"filename"=>"../app/def/form/tx.html", 
				"debug"=>0,
				"global_vars"=>0,
				"loop_context_vars"=>1
			);

			$template   =  new Template($options);

			$template   ->  AddParam('host', $value['host']);
			$template   ->  AddParam('host', $value['db_name']);
			$template   ->  AddParam('host', $value['user']);
			$template   ->  AddParam('host', $value['password']);*/

			$content = $value;


			$file = 'config.php';	
			fopen($file,"w") or die("Unable to open file!");
			chmod($file,0777);	
			file_put_contents($file,$content);
			
			/*//The name of the file that we want to create if it doesn't exist.
			$file = 'config.php';
			
			//Use the function is_file to check if the file already exists or not.
			if(!is_file($file)){
				//Some simple example content.
				$contents = 'This is a test!';
								echo "1";
				//Save our content to the file.
				file_put_contents($file, $contents);
			}*/

	}//before add update ends...

?>