<?php

#--------------------------------------Header Data-----------------------------------------------------#
//Header Array Delaration
    $table_header = array(
                        "s.no"        =>  "S.No",
                        "name"        =>  "Name",
                        "gender"      =>  "Gender",
                        "dob"         =>  "Date of Birth",
                        "town"        =>  "Address",
                      ); //header array ends here...

#-----------------------------------Table Column Data---------------------------------------------------#
//Column data Array Delaration

    $table_column = array(

                    '1' =>  array(

                            "name"        =>  "Siddharthan",
                            "gender"      =>  "Male",
                            "dob"         =>  "18-07-1996",
                            "address"     =>  "Pattukkottai",
                        ), //student1 data ends

                    '2' => array(

                            "name"        =>  "Sathish Kumar",
                            "gender"      =>  "Male",
                            "dob"         =>  "21-08-1995",
                            "address"     =>  "Sivakasi",

                        ), //student2 data ends

    );  //Overall Patient data ends here...

#-----------------------------------Fetch Data to Table------------------------------------------------#
  #data retrieval by "for each loop"

    $table_content = "";

    $no = 1;

    $table_content.="<table border='2'>"; //table definition

      #foreach for header
      foreach ($table_header as  $head_value) {
                  
        $table_content.="<th>$head_value</th>";

      }//header foreach ends

          #foreach for data
          foreach ($table_column as $column_data) {

              $table_content.="<tr>"; //Table Row Starts

              $table_content.="<td>".$no++."</td>"; //column for autoincrement serial number

                foreach($column_data as $value){

                    $table_content.="<td>$value</th>"; //$value holds the data
                  
                }//data foreach ends here...

          }//student_data foreach ends here...

  $table_content.="</tr>";

  $table_content.="</table>";
?>

<!--------------------------------------HTML Content---------------------------------------->
<!DOCTYPE html>
<html>
      <head>
          <title>Welcome Fibenis</title>
      </head>
      <body><?php echo $table_content; ?></body>
</html>