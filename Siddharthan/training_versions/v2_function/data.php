<?php

$s_no = 0;

    $table_header=array(
      
                    "s.no"        =>  "S.No",
                    "name"        =>  "Name",
                    "gender"      =>  "Gender",
                    "dob"         =>  "Date of Birth",
                    "town"        =>  "Town"

                  ); //header array ends here...

    //Patient Details Array Delaration

    $table_column=array(

                    '1' =>  array(

                            "name"        =>  "Siddharthan",
                            "gender"      =>  "Male",
                            "dob"         =>  "18-07-1996",
                            "address"     =>  "Pattukkottai",

                          ), //student1 data ends

                    '2' => array(

                            "name"        =>  "Sathish Kumar",
                            "gender"      =>  "Male",
                            "dob"         =>  "21-08-1995",
                            "address"     =>  "Sivakasi",

                          ), //student2 data ends

                  '3' =>  array(

                            "name"        =>  "Aakash",
                            "gender"      =>  "Male",
                            "dob"         =>  "15-02-1995",
                            "address"     =>  "Ooty",
                        
                          ), //student3 data ends

                    '4' => array(

                            "name"        =>  "Ramya",
                            "gender"      =>  "Female",
                            "dob"         =>  "29-09-1996",
                            "address"     =>  "Sankagiri",
          
                          ), //student3 data ends

    );  //Overall Patient data ends here...

    //single array to merge two arrays

    $content = array(

                  "header" => $table_header,
                  "data"   => $table_column

    );

?>
