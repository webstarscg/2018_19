<?php

  require_once 'config.php';

    function data_retrieve(){

          //query for fetch data from db
          $query          =   "SELECT id, name, gender, dob, town, cgpa 
                                      FROM student";

          //variable to call the db connection
          $conn           =   db_connection();
          
          //variable to execute our Query
          $result         =   mysqli_query($conn, $query);

          //Empty Array to store our db
          $data   =   array(); 

          //fetch the data from db and push it into an array
            while($details = mysqli_fetch_array($result,MYSQLI_ASSOC)){

                  array_push($data, $details);

              } //while ends here..

          // close db Connection
          $conn->close();

          //returns the data
          return $data;

  }//function close here....

?>