<?php 

            require_once    ("lib/template.php");
            require_once    ("data/data_retrieval.php");
            
            $data_value   =   data_retrieve();

            $options = array(
                            "filename"=>"static/table.html", 
                            "debug"=>0,
                            "global_vars"=>0,
                            "loop_context_vars"=>1
                        );
            
            $template   =  new Template($options);
            
            $template   ->  AddParam('title'    ,   'Student Data');
            $template   ->  AddParam('heading'  ,   'Student Data Table');
            $template   ->  AddParam('thead'    ,   array(
            
                                                        array('head'    =>  'S.no'),
                                                        array('head'    =>  'Name'),
                                                        array('head'    =>  'Date of Birth'),
                                                        array('head'    =>  'Gender'),
                                                        array('head'    =>  'Town'),
                                                        array('head'    =>  'CGPA'),
                                                        array('head'    =>  'Action'),
            
                                                    )//header array ends here...
            
                                                );//header parameter ends here..
            
            //data_value variable accomodate to parameter in templating
            $template   ->  AddParam('tbody',$data_value);
            
            $template   -> EchoOutput();
            
?>