function generate_table() {
			
			
	var image_array = new Array(
									/*['Images/8.jpg',  'Image1'],
									['Images/7.jpg',  'Image2'],
									['Images/2.jpg',  'Image3'],
									['Images/5.jpg',  'Image4'],
									['Images/4.jpg',  'Image5']*/
									
									['Name', 'College /Institute','Degree',  'Percentage','Year of Passing'],
									['Aravind', 'PSG College of Technology','MCA',  '76%','2017'],
									['Bala','Coimbatore Institute of Technologyges','M.Sc',  '85%','2012'],
									['Chandru',  'Kumaraguru College of Tech','M.Tech',  '63%','2007'],
									['Hari',  'PSG College of Technology','MCA',  '74%','2019'],
									['Vinoth',  'PSG Arts and Science','B.Tech',  '85%','2015']
										
								);
								
	//empty the div using innerHTML
	
	document.getElementById("stage").innerHTML = "";
		
	// get the reference for the body
	
	var division = document.getElementById('stage');
	

	// creates a <table> element and a <tbody> element
	
	var tbl = document.createElement("table");
	
	var tblBody = document.createElement("tbody");

    // creating all cells
	
	for (var rows = 0; rows < image_array.length; rows++)
	{
  
		// creates a table row
		
		var row = document.createElement("tr");

		for (var cols = 0; cols < image_array[rows].length; cols++)
		{

			// Create a <td> element and a text node, make the text
			// node the contents of the <td>, and put the <td> at
			// the end of the table row
			
			var cell = document.createElement("td");
		 
			var heading = document.createElement("th");
		 
			var label = document.createElement('div');
			
			var img = document.createElement('img');
				
			if(image_array[rows][cols].includes('.jpg'))
			{
							
				img.src = image_array[rows][cols];
									
				cell.appendChild(img);
			
			}
				
			else
			{
				label.innerHTML=image_array[rows][cols];
				
				if(rows==0)
				{
					label.setAttribute("font-size", "40");
					heading.appendChild(label);
					
				}
				else
				{
						
					cell.appendChild(label);
				}	
			}
			if(rows==0)
				{
					row.appendChild(heading);
					
				}
				else
				{
						
					row.appendChild(cell);
				}		
			   
		
		} //for loop for column ends

    // add the row to the end of the table body
	
    tblBody.appendChild(row);
	
	} // fro loop for row ends
  
    // put the <tbody> in the <table>
	
	tbl.appendChild(tblBody);
  
	// appends <table> into <body>
	
	division.appendChild(tbl);
  
	tbl.setAttribute("border", "2");
	
	tbl.setAttribute("cellpadding", "10");
	
	tblBody.setAttribute("align", "center");
	
	
  
}