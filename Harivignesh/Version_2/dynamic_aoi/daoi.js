	//array containing data
	var image_array = new Array(
									{'image':'Images/8.jpg',  'title':'Image1'},
									{'image':'Images/7.jpg',  'title':'Image2'},
									{'image':'Images/2.jpg',  'title':'Image3'},
									{'image':'Images/5.jpg',  'title':'Image4'},
									{'image':'Images/4.jpg',  'title':'Image5'}
										
								);
								
			
	// function to display every element in an array        
	function build_dynamic(){                   
                    
        //for loop to traverse all the elements in the array           	
		for(var array_index=0; array_index < image_array.length; array_index++){
                        
							
						//a division for image and label is created 
						var container = document.createElement('DIV');
                        
						container.className = 'container';                                                                
								      
						// the image is created and appended to the div
						var img = document.createElement('img');
						
						img.src = image_array[array_index].image;
                        
						img.className = 'image';
								
						container.appendChild(img);
				
                        
						// the label for the image is created and appended to the div    
						var label = document.createElement('span');
                    
						label.innerHTML=image_array[array_index].title;
                        
						label.className="label";
                        
						container.appendChild(label);
                       

				// the whole container is appended to the main division 
                document.getElementById('stage').appendChild(container);
						
			}
                    
    } // function ends