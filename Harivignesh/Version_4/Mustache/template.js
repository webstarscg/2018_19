//template for a table
var tpl = "<table><tr><th><h2>Name</h2></th><th><h2>Institute</h2></th><th><h2>Degree</h2></th><th><h2>Year of Passing</h2></th><th><h2>Percentage</h2></th></tr>" +
          "{{#depts}}<tr><td>{{>employee}}</td></tr>{{/depts}}</table>";

//data are included in the partials		  
var partials = {employee: "{{Name}}<td>{{Institute}}</td><td>{{Degree}}</td><td>{{Year of Passing}}</td><td>{{Percentage}}</td>"};

//the data and template are joined together
var html = Mustache.to_html(tpl, data, partials);
$('#stageTable').html(html);

//template for table ends


//template for divisions inside division
var temp = "<div>{{#depts}}"
							+"<div> <b>Name:&emsp;</b>{{Name}}</div>"
							+"<div> <b>Institute:&emsp;</b>{{Institute}}</div>"
							+"<div> <b>Degree:&emsp;</b>{{Degree}}</div>"
							+"<div> <b>Year of Passing:&emsp;</b>{{Year of Passing}}</div>"
							+"<div> <b>Percentage:&emsp;</b>{{Percentage}}</div><br>"
							+"{{/depts}}";
//the data and template are joined together				
var param = Mustache.to_html(temp, data);
$('#stageDivision').html(param);

//template for division ends