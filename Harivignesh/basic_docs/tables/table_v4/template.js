


//template for divisions inside division
var temp = 	 "<table>"
			+"	<tr>"
			+"  	<th><b>Name</b></th>"
			+"		<th><b>Date of Birth</b></th>"
			+"		<th><b>Gender</b></th>"
			+"		<th><b>Address</b></th>"
			+"		<th><b>Mobile</b></th>"
			+"		<th><b>Email</b></th>"
			+"		<th><b>Institute Name</b></th>"
			+"  </tr>"
			+"{{#student_info}}"
			+"  <tr>"
			+"		<td> {{name}}</td>"
			+"		<td> {{dob}}</td>"
			+"		<td> {{gender}}</td>"
			+"		<td> {{address}}</td>"
			+"		<td> {{mobile}}</td>"
			+"		<td> {{email}}</td>"
			+"		<td> {{institute_name}}</td>"
			+"	</tr>"
			+"{{/student_info}}"
			+"</table>";
				
//the data and template are joined together				
var param = Mustache.to_html(temp, data);
$('#stageDivision').html(param);

//template for division ends


