function CreateTable(param) {
       
		var table_info = param.data;
		var stage      = document.getElementById(param.stage_id);
		
		
        // EXTRACT VALUE FOR HTML HEADER. 
        // ('Book ID', 'Book Name', 'Category' and 'Price')
        var col = [];
        for (var rows in table_info) {
            for (var key in table_info[rows]) {
                if (col.indexOf(key) == -1) {
                    col.push(key);
                }
            }
        }
		
        // CREATE DYNAMIC TABLE.
        var table = document.createElement("table");

        // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.

		 // TABLE ROW.

        var tr = table.insertRow(-1);                  

		// TABLE HEADER.
        for (var rows in col) {
            var th = document.createElement("th");      
            th.innerHTML = col[rows];
            tr.appendChild(th);
        }

        // ADD JSON DATA TO THE TABLE AS ROWS.
        for (var rows in table_info) {

            tr = table.insertRow(-1);

            for (var cols in col) {
                var tabCell = tr.insertCell(-1);
                tabCell.innerHTML = table_info[rows][col[cols]];
				//document.write("<br> - " + col[cols] + ": " +table_info[rows][col[cols]]);

            }
		} // 
		
        // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
        var divContainer = document.getElementById("showData");
		
        divContainer.innerHTML = "";
        
		divContainer.appendChild(table);
    
	} // end