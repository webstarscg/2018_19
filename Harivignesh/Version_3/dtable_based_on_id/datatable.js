function generate_table(param,stage_id) {
			
	
	
	//empty the div using innerHTML
	
	document.getElementById(stage_id).innerHTML = "";
		
	// get the reference for the body
	
	var stage_id= document.getElementById(stage_id);
	

	// creates a <table> element and a <tbody> element
	
	var tbl = document.createElement("table");
	
	var tblBody = document.createElement("tbody");

    // creating all cells
		
	for (var rows = 0; rows < param.length; rows++)
	{
  
		// creates a table row
		
		var row = document.createElement("tr");

		for (var cols = 0; cols < param[rows].length; cols++)
		{

			// Create a <td> element and a text node, make the text
			// node the contents of the <td>, and put the <td> at
			// the end of the table row
			
			var cell = document.createElement("td");
		 
			var heading = document.createElement("th");
		 
			var label = document.createElement('div');
			
			var img = document.createElement('img');
			
				label.innerHTML=param[rows][cols];
				
				if(rows==0)
				{
					heading.appendChild(label);
				}
				else
				{
						
					cell.appendChild(label);
				}	
			
			if(rows==0)
				{
					row.appendChild(heading);
					
				}
				else
				{
						
					row.appendChild(cell);
				}		
			   
		
		} //for loop for column ends

    // add the row to the end of the table body
	
    tblBody.appendChild(row);
	
	} // fro loop for row ends
  
    // put the <tbody> in the <table>
	
	tbl.appendChild(tblBody);
  
	// appends <table> into <body>
	
	stage_id.appendChild(tbl);
  
	tbl.setAttribute("border", "2");
	
	tbl.setAttribute("cellpadding", "10");
	
	tblBody.setAttribute("align", "center");
	
	
  
}