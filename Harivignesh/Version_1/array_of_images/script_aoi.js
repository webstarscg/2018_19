window.onload= buildImage;
		
		//multidimensional objects in JSON format
		var image_array = new Array(
										{'image':'Images/8.jpg',  'title':'Image1'},
										{'image':'Images/7.jpg',  'title':'Image2'},
										{'image':'Images/2.jpg',  'title':'Image3'},
										{'image':'Images/5.jpg',  'title':'Image4'},
										{'image':'Images/4.jpg',  'title':'Image5'}
									);
		  
		var array_index = 0;
		
		
		//function to initially load an image when page starts
		function buildImage(){
				
			var img = document.createElement('img')
			
			img.src = image_array[array_index].image;
			
			document.getElementById('content').appendChild(img);
			
			document.getElementById('heading').innerHTML = image_array[0].title;
		
		}
		
		
		//function to move to next image 
		function nextImage()
		{
		
			var img = document.getElementById('content').getElementsByTagName('img')[0]
							
			array_index++;
			
			array_index=array_index % image_array.length;
			
			img.src=image_array[array_index].image;
			
		    document.getElementById('heading').innerHTML = image_array[array_index].title;
			
		} //function to move to next image ends 
		
		
		//function to move to previous image 
		function previousImage()
		{
		
			if(array_index==0)//if array length is 0 it loops back to the last element
			{
				array_index=image_array.length;
			}

			var img = document.getElementById('content').getElementsByTagName('img')[0]
							
			array_index--;
			
			array_index=array_index % image_array.length;
			
			img.src=image_array[array_index].image;
			
		    document.getElementById('heading').innerHTML = image_array[array_index].title;
			
		}
		//function to move to previous image ends