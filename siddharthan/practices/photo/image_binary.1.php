<?php

$image = base64_encode(file_get_contents('images/something.png'));

?>

<!DOCTYPE html>
<html>
  <head>
    <title>Image-Encoding</title>
  </head>
  <body>
        <img src="data:image/png;base64,<?php echo $image ?>">
  </body>
</html>
