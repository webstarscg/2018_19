<?php

    function data_retrieve(){

      require_once  ("./lib/config.php");

    $conn           =   db_connection(); 

    //Query for fetch data from DB

    $query          =   "SELECT id , name, gender, dob, town, cgpa 
                                FROM student";                             

    $result         =   mysqli_query($conn, $query); //variable to execute our Query

    $data   =   array(); //Empty Array to store our DB


    //fetch the data from DB and create an array

      while($details = mysqli_fetch_assoc($result)){

            $data[] = $details;

        } //while ends here..
        
    return $data;

    $conn->close(); //DB Connection closed

  }//function close here....

?>