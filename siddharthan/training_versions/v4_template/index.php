<?php

    require_once    ("template.php");

    require_once    ("table_builder.php");

    $content    =   view();

    $template   =  new Template("page.html");

    $template   ->   AddParam('title','Student Table');
    $template   ->   AddParam('head','Student Details');
    $template   ->   AddParam('content',$content);

    $template->EchoOutput();

?>