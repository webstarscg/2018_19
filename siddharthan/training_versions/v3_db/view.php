<?php

function view($cont){

//Data Presentation Goes here...

$table_header =$cont['header'];//to fetch the values of $table_header to $cont

$details=$cont['data'];//to fetch the values of $student_detail to $cont

$table_content=''; //Empty variable to concatena

$table_content.='<table border="5" width="75%">'; //Table Declaration

$serial_number = 0; //variable to auto generate serial number

//foreach for header
foreach ($table_header as  $head_value) {

          if(is_array($head_value)){
                  //foreach for no of visits and purpose
                  foreach ($head_value as $no_visit_key => $no_visit_value) {

                    $table_content.="<th>".$no_visit_value."</th>";

                  } //visits foreach ends here

          }    //if ends here

          else{

            $table_content.="<th>".$head_value."</th>";

          } //else ends here

}//header foreach ends

//table body starts

$table_content.='<tr>'; //table row starts here

foreach ($details as $details_value) {

      if(is_array($details_value)){

        foreach ($details_value as $student_key => $student_value) {

          $table_content.="<td>".$student_value."</td>";

        }

        $table_content.='</tr>'; //table row starts here

      }
      else{

        $table_content.="<td>".$student_value."</td>";

      }

}

$table_content.='</table></center>';

  return "$table_content";

} //function ends here

?>
