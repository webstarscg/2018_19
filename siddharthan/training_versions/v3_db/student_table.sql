
-- Table Creation

CREATE TABLE student (
  id int(11) NOT NULL AUTO_INCREMENT,
  timestamp_punch timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  user_id int(11) NOT NULL,
  name varchar(50) NOT NULL,
  gender varchar(15) NOT NULL,
  dob date NOT NULL,
  town varchar(40) NOT NULL,
  cgpa varchar(100) NOT NULL,
  PRIMARY KEY (id)
);

--Insert Query

INSERT INTO student (name, gender, dob, town, cgpa)
	VALUES ('Siddharthan','Male','1996-12-08','Pattukkottai', 7.58);
