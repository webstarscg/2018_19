<?php

    include_once 'config.php';

    $table_header=array(

                    "s.no"        =>  "S.No",
                    "name"        =>  "Name",
                    "gender"      =>  "Gender",
                    "dob"         =>  "Date of Birth",
                    "town"        =>  "Town",
                    "cgpa"       =>  "CGPA",

                  ); //header array ends here...

    //student Details Array Delaration

    $query = "SELECT id,name,gender,dob,town,cgpa FROM student"; //Query for fetch data from DB

    // function to call the DB connection              
    $conn = connect_db(); 

    
    $result = mysqli_query($conn, $query); //variable to execute our Query

    $column_data = array(); //Empty Array to store our DB
    
      while($details = mysqli_fetch_array($result,MYSQLI_ASSOC)){

          array_push($column_data,s $details);

        } //while ends here..

      $conn->close(); //DB Connection closed



    $content = array(

                    "header" => $table_header,

                    "data"   => $column_data

                );//content array ends here...

    

?>
