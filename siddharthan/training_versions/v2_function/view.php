<?php

  function view($param){

          #Data Presentation Goes here...
          $table_header =$param['header'];//to fetch the values of $table_header to $cont
          
          $column_detail=$param['data'];//to fetch the values of $patient_detail to $cont
          
          $table_content='';
      
          $table_content.='<table border="5" width="80%" height="75%">'; //Table Declaration
  
          $no = 0;

            #foreach for header
            foreach ($table_header as  $head_value) {
                     
                        $table_content.="<th>$head_value</th>";
            
            }//header foreach ends

  $table_content.='<tr>'; //table row starts here

    #Patient details column starts here

          #foreach for data
          foreach ($column_detail as $column_data) {
          
            $table_content.="<td>".++$no."</td>";
          
                foreach ($column_data as $key => $value) {
          
                            $table_content.="<td>"; //Table Column starts here
          
                            $table_content.="&nbsp &nbsp &nbsp".$value;

                            $table_content.="</td>"; //Table Column ends here...
          
                        }  //data value foreach ends
          
              $table_content.='</tr>'; //table row ends here...
          
          } //data foreach ends

      $table_content.='</table></center>';

      return $table_content;

  } //function ends here
?>
