<?php 

        $TABLE=[


                'header' =>       array(

                                    array('COL_VALUE'    =>  'S.no'),
                                    array('COL_VALUE'    =>  'Name'),
                                    array('COL_VALUE'    =>  'Age'),
                                    array('COL_VALUE'    =>  'Gender'),
                                    array('COL_VALUE'    =>  'Town'),
                                    array('COL_VALUE'    =>  'Date'),
                                    array('COL_VALUE'    =>  'Visits'),

                ),

                'table_data' =>   array(

                                    array('field'=>'name'),
                                    array('field'=>'gender'),
                                    array('field'=>'age'),
                                    array('field'=>'town'),
                                    array('field'=>'date'),
                                    array('field'=>'visits'),
                ),

                'table_name'    => 'patient',

                'table_title'   => 'HMS DATA',

                'table_heading' => 'Patient Data',

            ];

?>