<?php

function patient_view($bind){

  include_once './view/header.php';

//Data Presentation Goes here...

$table_header = $bind['header'];

$button = $bind['button'];

$details = data_retrieve();//to fetch the values of $patient_detail to $cont

$table_content=''; //Empty variable to concatena

$table_content.="<title>Patient Table</title>";//title of the page goes here

$table_content.='<center><h1 position="center">Patient Details</h1>'; //Heading

$table_content.='<table border="5" width="75%">'; //Table Declaration

$serial_number = 0; //variable to auto generate serial number

//foreach for header
foreach ($table_header as  $head_value) {

          if(is_array($head_value)){
                  //foreach for no of visits and purpose
                  foreach ($head_value as $no_visit_key => $no_visit_value) {

                    $table_content.="<th>".$no_visit_value."</th>";

                  } //visits foreach ends here

          }    //if ends here

          else{

            $table_content.="<th>".$head_value."</th>";

          } //else ends here

}//header foreach ends

//table body starts

$table_content.='<tr>'; //table row starts here

foreach ($details as $details_value) {

      if(is_array($details_value)){

        foreach ($details_value as $patient_key => $patient_value) {

          $table_content.="<td>".$patient_value."</td>";

        }

      }
      else{

        $table_content.="<td>".$patient_value."</td>";

      }

      foreach ($button as $buttons) {

            if(is_array($buttons)){

              foreach ($buttons as $button_key => $button_value) {

                    $table_content.="<td>".$button_value."</td>";
              }

            }

            else {

                $table_content.="<td>".$buttons."</td>";
            }

      }
      
      $table_content.="</tr>";


}


$table_content.='</table></center>';

  return "$table_content";

} //function ends here

?>
