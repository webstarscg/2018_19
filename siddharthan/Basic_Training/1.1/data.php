<?php

    include_once 'config.php';

    $table_header=array(

                    "s.no"        =>  "S.No",
                    "name"        =>  "Name",
                    "gender"      =>  "Gender",
                    "age"         =>  "Age",
                    "town"        =>  "Town",
                    "visit"       =>  "Visits",

                  ); //header array ends here...

    //Patient Details Array Delaration

    $query = "SELECT * FROM patient"; //Query for fetch data from DB

    $conn = connect_db(); //Function to call the DB connection

    $result = mysqli_query($conn, $query); //variable to execute our Query

    $patient_data = array(); //Empty Array to store our DB

  //  if(mysqli_num_rows($result) > 0){

      while($details = mysqli_fetch_array($result,MYSQL_NUM)){

        $patient_data[] = $details;

        } //while ends here..



    $content = array(

                    "header" => $table_header,

                    "data"   => $patient_data

    );//content array ends here...

    $conn->close(); //DB Connection closed

?>
