<?php 

require_once    ("lib/template.php");

require_once    ("lib/data_retrieval.php");

$data_value   =   data_retrieve($data);

$TABLE_INFO =   array(

                    'title' => "HMS DATA",

                    'heading' => "Patient Details",

                    'header'=>array(

                        array('head'    =>  'S.no'),
                        array('head'    =>  'Name'),
                        array('head'    =>  'Age'),
                        array('head'    =>  'Gender'),
                        array('head'    =>  'Town'),
                        array('head'    =>  'Date'),
                        array('head'    =>  'Visits'),

                    ),//header array ends here...

                    'body'=>$data_value,

                    'button'=>"Add Patient"

);

$options = array(

                "filename"=>"static/table.html", 
                "debug"=>0,
                "global_vars"=>0,
                "loop_context_vars"=>1
            );

$template   =&  new Template($options);

$template   ->  AddParam('title'    ,  $TABLE_INFO['title']  );
$template   ->  AddParam('heading'  ,  $TABLE_INFO['heading'] );
$template   ->  AddParam('thead'    ,  $TABLE_INFO['header'] );//Parameter ends here..

$template   ->  AddParam('tbody'    ,  $TABLE_INFO['body'] );//Data accomodate to Parameter in templating


$template   -> EchoOutput();


?>