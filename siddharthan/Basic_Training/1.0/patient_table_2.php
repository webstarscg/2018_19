<?php

  // function created for patient table
  function patient_table(){

    //Header Array Declaration

    $table_header=array(

                    "name"        =>  "Name",
                    "gender"      =>  "Gender",
                    "age"         =>  "Age",
                    "town"        =>  "Town",
                    "visit"       =>  "Visits",
                  );

    //Patient Details Array Delaration

    $patient_detail=array(

                          array(
                            "name"        => "Siddharthan",
                            "gender"      => "Male",
                            "age"         => "23",
                            "address"     => "Pattukkottai",
                            "visits"      =>  array(
                                                  1 =>  "12.10.2016",
                                                              "purpose" =>  array(
                                                                                1 => "Checkup",
                                                                                            "prescription"  =>  array(
                                                                                                                    1 =>  "Scan & X-ray",
                                                                                                                  ),
                                                                              ), //first visit ends
                                                  2 =>  "13.07.2015",
                                                              "purpose1"  =>  array(
                                                                                2   => "Cold",
                                                                                            "prescription"  =>  array(
                                                                                                                    1 =>  "Vicks Action 500",
                                                                                                                    2 =>  "Amruthanjan"
                                                                                                                  ),
                                                                              ), //second visit ends
                                                  3 =>  "08.05.2018",
                                                              "purpose2" =>  array(
                                                                                3 => "Headache",
                                                                                              "prescription"  =>  array(
                                                                                                                      1 =>  "Axe Oil",
                                                                                                                      2 =>  "Dolo650"
                                                                                                                    ),
                                                                              ), //third visit ends

                                                ),  //visit time ends

                          ), //patient data ends

                          array(
                            "name"    =>"Dharmalingam",
                            "gender"  =>"Male",
                            "age"     =>"59",
                            "address" =>"Peravoorani",
                            "visit_time"  =>  array(
                                                  1 =>  "02.08.2014",
                                                                "purpose" =>  array(
                                                                                  1 => "Fever",
                                                                                            "prescription"  =>  array(
                                                                                                                    1 =>  "Paracetamol",
                                                                                                                  ),
                                                                              ), //first visit ends

                                                  2 =>  "19.05.2016",
                                                                "purpose1" =>  array(
                                                                                    2 => "Checkup",
                                                                                                "prescription"  =>  array(
                                                                                                                        1 =>  "X-Ray",
                                                                                                                      ),
                                                                              ), //second visit ends

                                                  3 =>  "01.07.2017",
                                                                "purpose2" =>  array(
                                                                                  3 => "Heard attack",
                                                                                                  "prescription"  =>  array(
                                                                                                                          1 =>  "Cardiogram",
                                                                                                                        ),
                                                                              ), //third visit ends
                                                ), //visit time ends
                          ),  //patient data ends

                          array(
                            "name"    =>"Ananthanayaki",
                            "gender"  =>"Female",
                            "age"     =>"46",
                            "address" =>"Anavayal",
                            "visit_time"  =>  array(
                                                  1 =>  "11.03.2010",
                                                                "purpose" =>  array(
                                                                                  1 => "Dengue Fever",
                                                                                                  "prescription"  =>  array(
                                                                                                                          1 =>  "calbol",
                                                                                                                        ),
                                                                              ), //first visit ends

                                                  2 =>  "15.05.2013",
                                                                "purpose1" =>  array(
                                                                                    2 => "Cold",
                                                                                            "prescription"  =>  array(
                                                                                                                    1 =>  "calbol",
                                                                                                                  ),
                                                                              ), //second visit ends
                                                  3 =>  "01.01.2016",
                                                                "purpose2" =>  array(
                                                                                    3 => "Checkup",
                                                                                                "prescription"  =>  array(
                                                                                                                        1 =>  "calbol",
                                                                                                                      ),
                                                                              ), //third visit ends
                                                ), //visit time ends
                          ), //patient data ends

                          array(
                            "name"    =>"Vishnu",
                            "gender"  =>"Female",
                            "age"     =>"22",
                            "address" =>"Mettupalayam",
                            "visit_time"  =>  array(
                                                  1         =>  "11.07.2001",
                                                                        "purpose" =>  array(
                                                                                          1 => "Cold",
                                                                                                    "prescription"  =>  array(
                                                                                                                            1 =>  "Pirandai",
                                                                                                                          ),
                                                                              ), //first visit ends

                                                  2 =>  "12.07.2006",
                                                                "purpose1" =>  array(
                                                                                  2 => "Fever",
                                                                                            "prescription"  =>  array(
                                                                                                                    1 =>  "Paracetamol",
                                                                                                                  ),
                                                                              ), //second visit ends
                                                  3 =>  "08.12.2008",
                                                                  "purpose2" =>  array(
                                                                                    3 => "Checkup",
                                                                                                "prescription"  =>  array(
                                                                                                                        1 =>  "ECG",
                                                                                                                      ),
                                                                              ), //third visit ends
                                                ), //visit time ends
                          ), //patient data ends

                          array(
                            "name"    =>"Sathish",
                            "gender"  =>"Male",
                            "age"     =>"23",
                            "address" =>"Sivakasi",
                            "visit_time"  =>  array(
                                                  1 =>  "10.01.2006",
                                                                "purpose" =>  array(
                                                                                  1 => "Back Pain",
                                                                                                "prescription"  =>  array(
                                                                                                                        1 =>  "Moove",
                                                                                                                     ),
                                                                              ), //first visit ends

                                                  2 =>  "03.09.2015",
                                                                "purpose1" =>  array(
                                                                                    2 => "Cold",
                                                                                              "prescription"  =>  array(
                                                                                                                      1 =>  "Thoothuvalai",
                                                                                                                    ),
                                                                              ), //second visit ends

                                                  3 =>  "08.05.2018",
                                                                "purpose2" =>  array(
                                                                                    3 => "Back Pain",
                                                                                                  "prescription"  =>  array(
                                                                                                                          1 =>  "Pirandai",
                                                                                                                        ),
                                                                              ), //third visit ends
                                                ),  //visit time ends
                          ), //patient data ends

                          array(
                            "name"    =>"Sivarathy",
                            "gender"  =>"Female",
                            "age"     =>"25",
                            "address" =>"Coimbatore",
                            "visit_time"  =>  array(
                                                  1         =>  "12.10.2016",
                                                                        "purpose" =>  array(
                                                                                          1 => "Headache",
                                                                                                      "prescription"  =>  array(
                                                                                                                              1 =>  "Jandu Balm",
                                                                                                                            ),
                                                                              ), //first visit ends

                                                  2 =>  "23.08.2017",
                                                                "purpose1" =>  array(
                                                                                    2 => "Cold",
                                                                                              "prescription"  =>  array(
                                                                                                                      1 =>  "Thoothuvalai",
                                                                                                                    ),
                                                                              ), //second visit Ends

                                                  3 =>  "08.05.2018",
                                                                  "purpose2" =>  array(
                                                                                      3 => "Stomach Pain",
                                                                                                        "prescription"  =>  array(
                                                                                                                                1 =>  "Omam Water",
                                                                                                                              ),
                                                                              ), //third visit ends
                                                ),  //visit time ends
                          ), //patient data ends

    );  //Overall Patient data ends here...


    //Data Presentation Goes here...

    $table_content='';

    $table_content.='<center><h1 position="center">Patient Details</h1>'; //Heading

    $table_content.='<table border="5">'; //Table Declaration


    //foreach for header
    foreach ($table_header as  $head_value) {

              if(is_array($head_value)){
                      //foreach for no of visits and purpose
                      foreach ($head_value as $no_visit_key => $no_visit_value) {

                        $table_content.="<th>$no_visit_value</th>";

                      } //visits foreach ends here

              }    //if ends here

              else{

                $table_content.="<th>$head_value</th>";

              } //else ends here

    }//header foreach ends

    //foreach for data
    foreach ($patient_detail as $patient_data) {

      $table_content.='<tr>';

          foreach ($patient_data as $data_key => $data_value) {

                if(is_array($data_value)){

                    $table_content.="<td>";

                      // for visting
                      foreach ($data_value as $visit_key => $visit_value) {

                          if(is_array($visit_value)){

                            //purpose of Visits

                            foreach ($visit_value as $purpose_key => $purpose_value) {

                                    if(is_array($purpose_value)){

                                      foreach ($purpose_value as $prescription_key => $prescription_value) {

                                          $table_content.="<br>&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp $prescription_value";

                                      } //prescription_value foreach ends

                                    }//if ends here...
                                    else{

                                        $table_content.="&nbsp &nbsp &nbsp".$purpose_value;

                                    } //else ends here....

                            } //purpose_value foreach ends

                          } //if ends here...
                          else{

                            $table_content.="<br><li>$visit_value<br></li>";

                          } //else ends here..

                      } // visit value foreach ends

                } // if ends
                else{

                  $table_content.="<td>$data_value</td>";

               } //else ends here..
                  $table_content.="</td>";

        }  //data value foreach ends

      $table_content.='</tr>';//table structure ends

    } //data foreach ends

      $table_content.='</table></center>';

      echo "$table_content";

      #print_r($patient_detail);

  } //function ends here

  patient_table();  //call the function

?>
