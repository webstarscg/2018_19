<?php

$s_no = 0;

    $table_header=array(
                    "s.no"        =>  "S.No",
                    "name"        =>  "Name",
                    "gender"      =>  "Gender",
                    "age"         =>  "Age",
                    "town"        =>  "Town",
                    "visit"       =>  "Visits",
                  ); //header array ends here...

    //Patient Details Array Delaration

    $patient_detail=array(

                          array(

                            "name"        =>  "Siddharthan",
                            "gender"      =>  "Male",
                            "age"         =>  "23",
                            "address"     =>  "Pattukkottai",
                            "visits"      =>    array(
                                                  1 =>  "12.10.2016",
                                                              "purpose" =>  array(
                                                                                1 => "Checkup",
                                                                                            "prescription"  =>  array(
                                                                                                                    1 =>  "Scan & X-ray",
                                                                                                                  ),
                                                                              ), //first visit ends
                                                  2 =>  "13.07.2015",
                                                              "purpose1"  =>  array(
                                                                                2   => "Cold",
                                                                                            "prescription"  =>  array(
                                                                                                                    1 =>  "Vicks Action 500",
                                                                                                                    2 =>  "Amruthanjan"
                                                                                                                  ),
                                                                              ), //second visit ends
                                                  3 =>  "08.05.2018",
                                                              "purpose2" =>  array(
                                                                                3 => "Headache",
                                                                                              "prescription"  =>  array(
                                                                                                                      1 =>  "Axe Oil",
                                                                                                                      2 =>  "Dolo650"
                                                                                                                    ),
                                                                              ), //third visit ends

                                                ),  //visit time ends

                          ), //patient data ends

                          array(

                            "name"        =>  "Dharmalingam",
                            "gender"      =>  "Male",
                            "age"         =>  "59",
                            "address"     =>  "Peravoorani",
                            "visit_time"  =>    array(
                                                  1 =>  "02.08.2014",
                                                                "purpose" =>  array(
                                                                                  1 => "Fever",
                                                                                            "prescription"  =>  array(
                                                                                                                    1 =>  "Paracetamol",
                                                                                                                  ),
                                                                              ), //first visit ends

                                                  2 =>  "19.05.2016",
                                                                "purpose1" =>  array(
                                                                                    2 => "Checkup",
                                                                                                "prescription"  =>  array(
                                                                                                                        1 =>  "X-Ray",
                                                                                                                      ),
                                                                              ), //second visit ends

                                                  3 =>  "01.07.2017",
                                                                "purpose2" =>  array(
                                                                                  3 => "Heard attack",
                                                                                                  "prescription"  =>  array(
                                                                                                                          1 =>  "Cardiogram",
                                                                                                                        ),
                                                                              ), //third visit ends
                                                ), //visit time ends
                          ),  //patient data ends

                          array(

                            "name"        =>  "Ananthanayaki",
                            "gender"      =>  "Female",
                            "age"         =>  "46",
                            "address"     =>  "Anavayal",
                            "visit_time"  =>  array(
                                                  1 =>  "11.03.2010",
                                                                "purpose" =>  array(
                                                                                  1 => "Dengue Fever",
                                                                                                  "prescription"  =>  array(
                                                                                                                          1 =>  "calbol",
                                                                                                                        ),
                                                                              ), //first visit ends

                                                  2 =>  "15.05.2013",
                                                                "purpose1" =>  array(
                                                                                    2 => "Cold",
                                                                                            "prescription"  =>  array(
                                                                                                                    1 =>  "calbol",
                                                                                                                  ),
                                                                              ), //second visit ends
                                                  3 =>  "01.01.2016",
                                                                "purpose2" =>  array(
                                                                                    3 => "Checkup",
                                                                                                "prescription"  =>  array(
                                                                                                                        1 =>  "calbol",
                                                                                                                      ),
                                                                              ), //third visit ends
                                                ), //visit time ends
                          ), //patient data ends

                          array(

                            "name"        =>  "Vishnu",
                            "gender"      =>  "Female",
                            "age"         =>  "22",
                            "address"     =>  "Mettupalayam",
                            "visit_time"  =>  array(
                                                  1 =>  "11.07.2001",
                                                                  "purpose" =>  array(
                                                                                    1 => "Cold",
                                                                                              "prescription"  =>  array(
                                                                                                                      1 =>  "Pirandai",
                                                                                                                    ),
                                                                              ), //first visit ends

                                                  2 =>  "12.07.2006",
                                                                "purpose1" =>  array(
                                                                                  2 => "Fever",
                                                                                            "prescription"  =>  array(
                                                                                                                    1 =>  "Paracetamol",
                                                                                                                  ),
                                                                              ), //second visit ends
                                                  3 =>  "08.12.2008",
                                                                  "purpose2" =>  array(
                                                                                    3 => "Checkup",
                                                                                                "prescription"  =>  array(
                                                                                                                        1 =>  "ECG",
                                                                                                                      ),
                                                                              ), //third visit ends
                                                ), //visit time ends
                          ), //patient data ends

                          array(

                            "name"        =>  "Sathish",
                            "gender"      =>  "Male",
                            "age"         =>  "23",
                            "address"     =>  "Sivakasi",
                            "visit_time"  =>  array(
                                                  1 =>  "10.01.2006",
                                                                "purpose" =>  array(
                                                                                  1 => "Back Pain",
                                                                                                "prescription"  =>  array(
                                                                                                                        1 =>  "Moove",
                                                                                                                     ),
                                                                              ), //first visit ends

                                                  2 =>  "03.09.2015",
                                                                "purpose1" =>  array(
                                                                                    2 => "Cold",
                                                                                              "prescription"  =>  array(
                                                                                                                      1 =>  "Thoothuvalai",
                                                                                                                    ),
                                                                              ), //second visit ends

                                                  3 =>  "08.05.2018",
                                                                "purpose2" =>  array(
                                                                                    3 => "Back Pain",
                                                                                                  "prescription"  =>  array(
                                                                                                                          1 =>  "Pirandai",
                                                                                                                        ),
                                                                              ), //third visit ends
                                                ),  //visit time ends
                          ), //patient data ends

                          array(

                            "name"        =>  "Sivarathy",
                            "gender"      =>  "Female",
                            "age"         =>  "25",
                            "address"     =>  "Coimbatore",
                            "visit_time"  =>  array(
                                                  1 =>  "12.10.2016",
                                                                  "purpose" =>  array(
                                                                                     1 => "Headache",
                                                                                                  "prescription"  =>  array(
                                                                                                                          1 =>  "Jandu Balm",
                                                                                                                        ),
                                                                              ), //first visit ends

                                                  2 =>  "23.08.2017",
                                                                "purpose1" =>  array(
                                                                                    2 => "Cold",
                                                                                              "prescription"  =>  array(
                                                                                                                      1 =>  "Thoothuvalai",
                                                                                                                    ),
                                                                              ), //second visit Ends

                                                  3 =>  "08.05.2018",
                                                                  "purpose2" =>  array(
                                                                                      3 => "Stomach Pain",
                                                                                                        "prescription"  =>  array(
                                                                                                                                1 =>  "Omam Water",
                                                                                                                              ),
                                                                              ), //third visit ends
                                                ),  //visit time ends
                          ), //patient data ends

                          array(

                            "name"        =>  "Dinesh",
                            "gender"      =>  "male",
                            "age"         =>  "24",
                            "address"     =>  "Namakkal",
                            "visit_time"  =>  array(
                                                  1 =>  "12.10.2016",
                                                                  "purpose" =>  array(
                                                                                    1 => "Fever",
                                                                                              "prescription"  =>  array(
                                                                                                                      1 =>  "Paracetomol",
                                                                                                                    ),
                                                                              ), //first visit ends

                                                  2 =>  "23.08.2017",
                                                                "purpose1" =>  array(
                                                                                    2 => "Headache",
                                                                                              "prescription"  =>  array(
                                                                                                                      1 =>  "Axe Oil",
                                                                                                                    ),
                                                                              ), //second visit Ends

                                                  3 =>  "08.05.2018",
                                                                  "purpose2" =>  array(
                                                                                      3 => "Ashthma",
                                                                                                 "prescription"  =>  array(
                                                                                                                        1 =>  "Nazivion",
                                                                                                                      ),
                                                                              ), //third visit ends
                                                ),  //visit time ends
                          ), //patient data ends

    );  //Overall Patient data ends here...

    //single array to merge two arrays

    $content = array(

                  "header" => $table_header,
                  "data"   => $patient_detail

    );

?>
