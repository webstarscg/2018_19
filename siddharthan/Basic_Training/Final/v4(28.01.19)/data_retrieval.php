<?php

    function data_retrieve(){

    require_once 'config.php';

    //Query for fetch data from DB

    $query          =   "SELECT patient_id, name, gender, age, town, date, visits 
                                FROM patient";                             

    $conn           =   db_connection(); //Function to call the DB connection

    $result         =   mysqli_query($conn, $query); //variable to execute our Query

    $patient_data   =   array(); //Empty Array to store our DB


    //fetch the data from DB and create an array

      while($details = mysqli_fetch_array($result,MYSQL_NUM)){

            $patient_data[] = $details;

        } //while ends here..


    return $patient_data;

    $conn->close(); //DB Connection closed

  }//function close here....

?>