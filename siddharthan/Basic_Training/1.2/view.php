<?php

function patient_table(){

//Data Presentation Goes here...

$details  = data_retrive();

$table_content   =  ''; //Empty variable to concatenate

$table_content  .=  "<title>Patient Table</title>";//title of the page goes here

$table_content  .=  '<center><h1 position="center">Patient Details</h1>'; //Heading

$table_content  .=  '<table border="5" width="75%">'; //Table Declaration

$serial_number   =  0; //variable to auto generate serial number

//table header view
$table_header=array(

                "s.no"        =>  "S.No",
                "name"        =>  "Name",
                "gender"      =>  "Gender",
                "age"         =>  "Age",
                "town"        =>  "Town",
                "visit"       =>  "Visits",

              ); //header array ends here...

    //foreach for header
    foreach ($table_header as  $head_value) {

              if(is_array($head_value)){
                      //foreach for no of visits and purpose
                      foreach ($head_value as $no_visit_key => $no_visit_value) {

                        $table_content.="<th>".$no_visit_value."</th>";

                      } //visits foreach ends here

              } //if ends here

              else{

                $table_content.="<th>".$head_value."</th>";

              } //else ends here

    }//header foreach ends

        //Patient Data Table body starts

        $table_content.='<tr>'; //table row starts here

        foreach ($details as $details_value) {

              if(is_array($details_value)){

                foreach ($details_value as $patient_key => $patient_value) {

                  $table_content.="<td>".$patient_value."</td>";

                } //patient data inner foreach ends...

                $table_content.='</tr>'; //table row starts here

              } //if ends...

                    else{

                        $table_content.="<td>".$patient_value."</td>";

                    } //else ends...

              } //patient data foreach ends here...

        $table_content.='</table></center>';

  return "$table_content";

} //function ends here

?>
