<?php

function db_connection() {

    $host       =   "localhost";
    $port       =    3306;
    $user       =   "root";
    $password   =   "";
    $dbname     =   "hms";

    $con = new mysqli($host, $user, $password, $dbname, $port)

            or die('Could not connect to the database server' . mysqli_connect_error());

    return $con;

}

?>