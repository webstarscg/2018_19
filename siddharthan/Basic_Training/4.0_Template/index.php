<?php

    require_once    ("template.php");

    require_once    ("table_builder.php");

    $content    =   patient_view();

    $template   =&  new Template("page.html");

    $template   ->   AddParam('title','Patient Table');
    $template   ->   AddParam('head','Patient Details');
    $template   ->   AddParam('content',$content);

    $template->EchoOutput();

?>