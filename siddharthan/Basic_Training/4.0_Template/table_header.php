<?php

function table_header(){

        //header array

        $table_header = array(

                        "s.no"        =>  "S.No",
                        "name"        =>  "Name",
                        "gender"      =>  "Gender",
                        "age"         =>  "Age",
                        "town"        =>  "Town",
                        "date"        =>  "Date",
                        "visit"       =>  "Visit Purpose",
                        "action"      =>  "Action",

        ); //header array ends here...

            return $table_header;

            } //function ends here...

?>