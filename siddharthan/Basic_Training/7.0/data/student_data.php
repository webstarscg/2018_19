<?php 

        $TABLE=[


                'header' =>       array(

                                    array('COL_VALUE'    =>  'S.no'),
                                    array('COL_VALUE'    =>  'Name'),
                                    array('COL_VALUE'    =>  'Age'),
                                    array('COL_VALUE'    =>  'Gender'),

                ),

                'table_data' =>   array(

                                    array('field'=>'name'),
                                    array('field'=>'gender'),
                                    array('field'=>'age'),

                ),

                'table_name'    => 'student',

                'table_title'   => 'Student Data',

                'table_heading' => 'Student Table',

            ];

?>