<?php 

require_once    ("template.php");
require_once    ("query.php");
require_once    ("table_data.php");
require_once    ("config.php");

$conn       =   db_connection();

$param = array( 
                'table_data' => $table_data,
                'table_name' => 'patient',
                'db_conn'    => $conn
            );

$temp = execute_query($param);

// $data_value    =   data($temp);

//print_r($data_value);

$options = array(
    
                "filename"=>"table.html", 
                "debug"=>0,
                "global_vars"=>0,
                "loop_context_vars"=>1
            );

$header =   array(

                array('head'    =>  'S.no'),
                array('head'    =>  'Name'),
                array('head'    =>  'Age'),
                array('head'    =>  'Gender'),
                array('head'    =>  'Town'),
                array('head'    =>  'Date'),
                array('head'    =>  'Visits'),

            );//header array ends here...


$template   =&  new Template($options);

$template   ->  AddParam('title'    ,   'HMS Data');

$template   ->  AddParam('heading'  ,   'Patient Data Table');

$template   ->  AddParam('thead'    ,    $header);//Parameter ends here..

$template   ->  AddParam('tbody'    ,    $temp);//Data accomodate to Parameter in templating


$template   -> EchoOutput();


?>