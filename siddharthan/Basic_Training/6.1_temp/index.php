<?php 

// lib
require_once    ("lib/template.php");
require_once    ("data/get_data.php");

// config
require_once    ("lib/config.php");

// data
require_once    ("data/table_data.php");


$conn = db_connection();

$param = array( 

                'table_data' => $TABLE['table_data'],
                'table_name' => $TABLE['table_name'],
                'db_conn'    => $conn
                
            );

$table_info  = get_table_data($param);

//=========================================================================================================
            

$T=new Template([ "filename"=>"table.html", 
                        "debug"=>0,
                        "global_vars"=>0,
                        "loop_context_vars"=>1
                    ]);

$T->AddParam('TABLE_TITLE',$TABLE['table_title']);

$T->AddParam('TABLE_HEADING',$TABLE['table_heading']);

$T->AddParam('HEAD_ROW_INFO',$TABLE['header']);//Header Data
 
$T->AddParam('table_info',$table_info);


$T->EchoOutput();


?>