<?php 

require_once    ("template.php");
require_once    ("data_retrieve.php");

$data_value   =   data_retrieve();

//print_r($data_value);

$options = array("filename"=>"table.html", "debug"=>0);

$template   =&  new Template($options);

$template   ->  AddParam('title'    ,   'Multi Loop');
$template   ->  AddParam('heading'  ,   'Multi Looping');
$template   ->  AddParam('thead'    ,   array(

                                            array('head'    =>  'Name'),
                                            array('head'    =>  'Age'),
                                            array('head'    =>  'Gender'),

                                        )
                                    );

$template   ->  AddParam('tbody',$data_value);


$template   -> EchoOutput();


?>