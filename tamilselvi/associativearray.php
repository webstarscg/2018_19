<?php


    $table_heading= array('reg_no'=> array('label'=>'Reg no'),
                           'name'=> array('label'=> 'Student Name'),
                           'address'=> array('label'=> 'Address'),
                           'age'=> array('label'=> 'Age'),
                           'course'=> array('label'=> 'Course')
                           );
    $table_content= array(array('reg_no'=> "14MCS047",
                                'name'=> "Tamilselvi",
                                'address'=> "CBE",
                                'age'=> 22,
                                'course'=>"MSc"
                               ),
                         array('reg_no'=> "14MCS037",
                                'name'=> "Priyanka",
                                'address'=> "CBE",
                                'age'=> 22,
                                'course'=>"MSc"
                               ),
                         array('reg_no'=> "14MCS032",
                                'name'=> "Priya",
                                'address'=> "CBE",
                                'age'=> 22,
                                'course'=>"MSc"
                                ),
                            );
    $table_info= array('heading'=>$table_heading,
                       'content'=> $table_content);
    $table_value= table($table_info);
    function table($table_info){
            $table_data='';		// To return array in table format
            $table_content= $table_info['content'];
            $table_head= $table_info['heading'];
            $table_data.='<tr>';
            foreach($table_head as $head_key=> $head_data){
                    $table_data.="<td>".$head_data['label']."</td>";
            }
            $table_data.='</tr>';
                    foreach($table_content as $content_key => $content_value){
                             $table_data.='<tr>';
                            foreach($table_head as $key=> $value){
                                     if(!empty($content_value[$key])){
                                         $table_data.='<td>'.$content_value[$key].'</td>';
                                
                                     }
                                    else{
                                        $table_data.='<td>'."Nil".'</td>';
                            }
                    }
                             $table_data.='</tr>';
            return '<table border="4" cellspacing="3" cellpadding="5" width="5%">'.$table_data.'</table>';
	
    }
    }
	
 echo $table_value;
?>