<?php

	$form_data= array(
		
		1=>array(	'label'  	=>'Textbox',
				'is_text'	=>1,
				'max_length'	=>15),
		
		2=>array(	'label'		=>'Textarea',
				'is_text_area'	=>1),
		
		3=>array( 	'label'         => 'Radio',
				'is_radio'      => 1,
                                'prop'    	=>['1' => ['label'=>'Male', 'option_value'=>'Male'],
						   '2' => ['label'=>'Female', 'option_value'=>'Female'],]),
		4=>array(       'label'       => 'Checkbox',
                                'is_checkbox' => 1,
				'prop'        => ['1' => ['label'=>'Programming', 'option_value'=>'Programming'],
					          '2' => ['label'=>'Designing', 'option_value'=>'Designing'],]),
                                    
                5=>array(	'label'      => 'Dropdown',
                                'is_dropdown'=> 1,
                                'prop'       =>['1' => ['label'=>'BC', 'option_value'=>'BC'],
					        '2' => ['label'=>'MBC', 'option_value'=>'MBC'],
					        '3' => ['label'=>'FC', 'option_value'=>'FC'] ]),
		
		
                        );                

                        
?>