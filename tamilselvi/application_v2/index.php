<?php

    require_once("../template.php");

    require_once("form_data.php");

    $options = array("filename"=>"loop.html", "debug"=>0);
    
    $template =& new Template($options);

    $template->AddParam('title', 'Student Table');
    
    $template->AddParam('form_data', array(
                                     
				     array('label'=>'Textbox',
                                           'is_text'=>1,
                                           'max_length'=>'2'),
                                 
                                    array('label'=>'Textarea',
                                          'is_text_area'=>1),
                                                                    
                                    array('label'=> 'Radio',
                                          'is_radio'=> 1,
                                          'prop'=>['1' => ['label'=>'Male', 'option_value'=>'Male'],
					           '2' => ['label'=>'Female', 'option_value'=>'Female'],]),
                       
                                    array('label' => 'Checkbox',
                                          'is_checkbox'=> 1,
                                          'prop'=> ['1' => ['label'=>'Programming', 'option_value'=>'Programming'],
						    '2' => ['label'=>'Designing', 'option_value'=>'Designing'],]),
                                    
                                    array('label'=> 'Dropdown',
                                          'is_dropdown'=> 1,
                                          'prop'=>['1' => ['label'=>'BC', 'option_value'=>'BC'],
						   '2' => ['label'=>'MBC', 'option_value'=>'MBC'],
						   '3' => ['label'=>'FC', 'option_value'=>'FC']
                                                   ]),
                        ));                

    $template->EchoOutput();
?>