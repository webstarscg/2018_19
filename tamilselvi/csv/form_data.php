<?php

	$form_data= array(
		
		1=>array(	'label'  	=>'TextBox',
				'type'		=>'text',
				'field'         => 'textbox',
				'is_max_length'	=>1,
				'max_length'	=>15),
				
		
		2=>array( 	'label'         => 'Radio',
				'type'          => 'radio',
				'field'         => 'radio',
                                'prop'    =>['1' => ['label'=>'Option A', 'option_value'=>'Male'],
					     '2' => ['label'=>'Option B', 'option_value'=>'Female'],]),
		
		
		3=>array( 	'label'      	=> 'Date',
			        'type'       	=> 'date',
				'field'         => 'date',
				'prop'       	=>['max'  => "2019-01-01" ,
						   'min'  => "2000-12-31" ,],
				'is_max'	=> 1,
				'is_max'	=> 1),
	
		
		4=>array( 	'label'       	=> 'Dropdown',
				'type'    	=> 'dropdown_single',
				'field'         => 'dropdown',
				'prop'   	=>['1' => ['label'=>'Option A', 'option_value'=>'Msc'],
						   '2' => ['label'=>'Option B', 'option_value'=>'Bsc'],
						   '3' => ['label'=>'Option C', 'option_value'=>'BCom']]),
		
		5=>array(	'label'		=>'Email',
				'field'         =>'Email',
				'type'		=>'text'),
		
		6=>array(	'label'         => 'Checkbox',
				'type'          => 'checkbox',
				'field'         => 'checkbox',
                                'prop'    	=> ['1' => ['label'=>'Option A', 'option_value'=>'Dancing'],
						   '2' => ['label'=>'Option B', 'option_value'=>'Singing'],]),

		
		7=>array(	'label'		=>'Textarea',
				'field'         =>'Textarea',
				'type'		=>'text_area'),
		
		8=>array( 	'label'  => 'Image',
			        'type'   => 'file',
				'field'  => 'image',
			        'prop'   => ['width'   => 50,
				             'height'  => 50,]
				),
		
		9=>array( 	   'label'   => 'Multilist',
				   'type'    => 'dropdown_multiple',
				   'field'   => 'Multilist',
				   'prop'    =>['1' => ['label'=>'Option A', 'option_value'=>'MSc'],
						'2' => ['label'=>'Option B', 'option_value'=>'MCA'],]
				),
	
		10=>array( 	   'label'     => 'Range',
			           'type'      => 'range',
				   'field'     => 'range',
				   'prop'      =>['max'  => 10,
						  'min'  => 0,],
				   'is_max'    => 1,
				   'is_max'    => 1,
				  ),

	);

?>
    	
	