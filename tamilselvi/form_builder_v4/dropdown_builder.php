<center>DROPDOWN BUILDER</center>
<?php


    $form_data=array(1=>array('label'  =>'Dropdown',
			      'type'   =>'drop_grid',
			      'columns'=>[1=>'Option  data',
				          2=>'Option value'],
			      'rows'   => '4'),
		    );
    
        function drop_builder($form_data){   
       
	 
	    $form_content = '';
	  	
	    foreach($form_data as $form_index => $row_attr){

		if($row_attr['type']=='drop_grid'){
	       
		    $grid_content=build_grid($row_attr);
		
		    $form_content.="Label:<input type='text'> ";
			 
		    $form_content.="<br><br>";
			 
		    $form_content.="Options:".$grid_content;
				    
		    $form_content.="<br><br>";
		
		 
	        }//if
		 
            } // end of foreach
	  
	return $form_content;
	
	} //  end of function
    
    
    // building
    
    function build_grid($row_attr){
		  
	// row 
	    
	    $rows = $row_attr['rows'];
			
        //cols
	
	    $cols = count($row_attr['columns']);

	    $grid_content    = ''; 
			
	    $grid_content.="<table border='1'></thead>";
				
			
	    for($col_idx=1;$col_idx<=$cols;$col_idx++){

			
		$grid_content.="<th>".$row_attr['columns'][$col_idx]."</th>";
				
	    }//end of for
			
	    $grid_content.='</thead>';
			
		for($row_idx=1;$row_idx<=$rows;$row_idx++){
		
		    $grid_content.="<tr>";
				
			for($col_idx=1;$col_idx<=$cols;$col_idx++){
								
			    $grid_content.='<td><input type="text" name="grid"></td>';
					
			}//end of for
				
		     $grid_content.="</tr>";
				
		}//end of for
			
			
	    $grid_content.='</table>';
			
		return $grid_content;
				
    } // end of function
	
    
?>

