<?php

	$form_data=array(1=>array( 'label'      => 'Text Box',
				   'type'       => 'text',
				   'prop'       => ['max_length' => 2,
						   ]
				  
				  ),
					 
			2=>array( 'label'      => 'Date ',
			          'type'       => 'date',
				  'prop'        =>['max'  => "2019-01-01" ,
						  'min'  => "2000-12-31" ,]
				  
				), 
				  
			3=>array( 'label'         => 'Radio',
			          'type'          => 'radio',
				  
				  'prop'          =>['1' => "Option A",
						     '2' => "Option B",]
				),
			
			4=>array( 'label'       => 'Checkbox',
				  'type'        => 'checkbox',
				  'prop'        =>['1'  => "Option 1",
						   '2'  => "Option 2", ]
				),
			
			5=>array( 'label'       => 'Dropdown',
				  'type'        => 'dropdown',
				  'prop'        =>['1' => "Option 1",
						   '2' => "Option 1", ]
				),
			
			6=>array( 'label'      => 'Email',
				  'type'       => 'email',
				  'prop'       => ['max_length' => 3,
						   'size'       => 50,]
				  
				),
				  
				
			7=>array( 'label'  => 'Image',
			          'type'   => 'file',
			          'prop'   => ['width'   => 50,
				               'height'  => 50,]
				),
			
			);

?>