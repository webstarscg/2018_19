<?php

    function form_builder($form_data){

	    $temp = [];
    
	    $form_content='';
	
		// traverse form_data
	
		foreach($form_data as $form_row_key => $form_row_data){
			
			if($form_row_data['type']=='text'){
			
			    $temp['text_size']=((@$form_row_data['properties']['size'])?@$form_row_data['prop']['size']:100);    
				
			    $form_content.= "$form_row_data[label]: <input type= ".$form_row_data['type']."
									
								    maxlength= ".$form_row_data['prop']['max_length']."
						
								    size=".$temp['text_size']."> <br><br>";
			
			} // end
                        
                        elseif($form_row_data['type']=='date'){
				
			    $form_content.= "$form_row_data[label]: <input type= ".$form_row_data['type']."
                                        
					                             max= ".$form_row_data['prop']['max']."
										
								     min= ".$form_row_data['prop']['min']."> <br><br>";
			} //
			
			elseif($form_row_data['type']=='radio'){
                            
                                $form_option_data = $form_row_data['prop'];
                               
                                        $temp = 0;
					
					$form_content.= "$form_row_data[label]:";
                                        
					    foreach($form_option_data as $key => $value){
                                        
						$temp++;
                                            
					        $form_content.= "<input type=".$form_row_data['type']."  name='option' value=$key> $form_option_data[$temp] <br><br>";
						
					    }
                                       
			}
			
			elseif($form_row_data['type']=='checkbox'){
                            
                                $form_option_data = $form_row_data['prop'];
                               
                                        $temp = 0;
					
					$form_content.= "$form_row_data[label]:";
                                        
					    foreach($form_option_data as $key => $value){
                                        
						$temp++;
                                            
					        $form_content.= "<input type=".$form_row_data['type']."  name='option' value=$key> $form_option_data[$temp] <br><br>"; 
						
					    }
                                       
			}
			
			elseif($form_row_data['type']=='dropdown'){
                            
                                $form_option_data = $form_row_data['prop'];
                               
                                        $temp = 0;
					
					$form_content.= "$form_row_data[label]:";
                                        
					    foreach($form_option_data as $key => $value){
                                        
						$temp++;
                                            
					        $form_content.= "<input type=".$form_row_data['type']."  name='option' value=$key> $form_option_data[$temp] <br><br>"; 
						
					    }
                                       
			}
			
			elseif($form_row_data['type']=='email'){
			    
                
			    $form_content.= "$form_row_data[label]: <input type= ".$form_row_data['type']."
									
								    maxlength= ".$form_row_data['prop']['max_length']."
						
								    size= ".$form_row_data['prop']['size']."> <br><br>";
			         	
			}
			 
			elseif($form_row_data['type']=='file'){
                
			    $form_content.= "$form_row_data[label]: <input type= ".$form_row_data['type']."
			    
								    width= ".$form_row_data['prop']['width']."
						
								    height= ".$form_row_data['prop']['height']."> <br><br>";
			}
		    
		}  // end    
	
	return $form_content;
	
    }
        
?>