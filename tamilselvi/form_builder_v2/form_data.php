<?php

	$form_data=array(1=>array( 'label'      => 'Text Box',
				   'type'       => 'text',
				   'prop'       => ['max_length' => 2,
						   ]
				  
				  ),
					 
			2=>array( 'label'      => 'Date ',
			           'type'      => 'date',
				   'prop'      =>['max'  => "2019-01-01" ,
						   'min'  => "2000-12-31" ,],
				   'is_max'    => 1,
				   'is_max'    => 1,
								
				  ),
				  
			3=>array( 'label'         => 'Radio',
			          'type'          => 'radio',
				  'prop'          =>['1' => ['label'=>'Option A', 'option_value'=>100],
						     '2' => ['label'=>'Option B', 'option_value'=>200],
						     '3' => ['label'=>'Option C', 'option_value'=>300],
						     ]
				),
			
			4=>array( 'label'       => 'Checkbox',
				  'type'        => 'checkbox',
				  'prop'        => ['1' => ['label'=>'Option A', 'option_value'=>100],
						    '2' => ['label'=>'Option B', 'option_value'=>200],]
				),
			
			5=>array( 'label'    => 'Dropdown',
				   'type'    => 'dropdown_single',
				   'prop'    =>['1' => ['label'=>'Option A', 'option_value'=>100],
						'2' => ['label'=>'Option B', 'option_value'=>200],]
				),
			
			6=>array( 'label'      => 'Email',
				  'type'       => 'email',
				  'prop'       => ['max_length' => 3,
						   'size'       => 50,]
				  
				),
				  
				
			7=>array( 'label'  => 'Image',
			          'type'   => 'file',
			          'prop'   => ['width'   => 50,
				               'height'  => 50,]
				),
			8=>array( 'label'    => 'Multilist',
				   'type'    => 'dropdown_multiple',
				   'prop'    =>['1' => ['label'=>'Option A', 'option_value'=>100],
						'2' => ['label'=>'Option B', 'option_value'=>200],]
				),
			
                        9=>array( 'label'      => 'Range',
			           'type'      => 'range',
				   'prop'      =>['max'  => 10,
						  'min'  => 0,],
				   'is_max'    => 1,
				   'is_max'    => 1,
				  ),
			
			);

?>