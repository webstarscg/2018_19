<?php

    function option_builder($form_data){

	    $temp = [];
	    
	    $dmax=[];
	
	    $dmin=[];
    
	    $form_content='';
	
		// traverse form_data
	
		foreach($form_data as $form_row_key => $form_row_data){
			
			if($form_row_data['type']=='text'){
			
			    $temp['text_size']=((@$form_row_data['properties']['size'])?@$form_row_data['prop']['size']:100);    
				
			    $form_content.= "$form_row_data[label]: <input type= ".$form_row_data['type']."
									
								    maxlength= ".$form_row_data['prop']['max_length']."
						
								    size=".$temp['text_size']."> <br><br>";
			
			} // end if
                        
                        elseif($form_row_data['type']=='date'){
		    
			    $dmax['max_date']=(@$form_row_data['is_max'])?" max='".@$form_row_data['prop']['max']."'":'';
			 
			    $dmin['min_date']=(@$form_row_data['is_min'])?" min='".@$form_row_data['prop']['min']."'":'';
			 
			    $form_content.=$form_row_data['label'].
					            "<input type=".$form_row_data['type']."
						      max=".@$form_row_data['prop']['max']."
						      min=".@$form_row_data['prop']['min'].">" ;
					
		
                       
			} // end if
			
			elseif($form_row_data['type']=='radio'){
                            
                                $form_option_data = $form_row_data['prop'];
        				
				$form_content.= "$form_row_data[label]:";
				
				    foreach($form_option_data as $key => $value){
				
					$form_content.= "<input type=".$form_row_data['type']."  name='option' value=".$form_option_data[$key]['option_value'].">".
							"".$form_option_data[$key]['label']; 
					      
				    } //end foreach
					
					$form_content.="<br><br>";	
                                       
			}//end if
			
			elseif($form_row_data['type']=='checkbox'){
                            
                               $form_option_data = $form_row_data['prop'];
        				
				$form_content.= "$form_row_data[label]:";
				
				    foreach($form_option_data as $key => $value){
				
					$form_content.= "<input type=".$form_row_data['type']."  name='option' value=".$form_option_data[$key]['option_value'].">".
					                 "".$form_option_data[$key]['label']; 
					
				    }//end foreach
				    
					$form_content.="<br><br>";  
			} // end if
			
			elseif($form_row_data['type']=='dropdown_single'){
                            
                                $form_option_data = $form_row_data['prop'];
        				
				$form_content.= "$form_row_data[label]:";
                                
				$form_content.="<select>";
                                
				    foreach($form_option_data as $key => $value){
				
					$form_content.= "<option value=".$form_option_data[$key]['option_value']."> ".$form_option_data[$key]['label']."</option>";
					
                                    }//foreach
                                    
                                $form_content.="</select><br><br>";       
			} // end if
			
			elseif($form_row_data['type']=='email'){
			    
				$form_content.= "$form_row_data[label]: <input type= ".$form_row_data['type']."
									
								        maxlength= ".$form_row_data['prop']['max_length']."
					    
								        size= ".$form_row_data['prop']['size']."> <br><br>";
			         	
			}// end if
			 
			elseif($form_row_data['type']=='file'){
                
			    $form_content.= "$form_row_data[label]: <input type= ".$form_row_data['type']."
			    
								    width= ".$form_row_data['prop']['width']."
						
								    height= ".$form_row_data['prop']['height']."> <br><br>";
			}// end if
			
			elseif($form_row_data['type']=='dropdown_multiple'){
                            
                                $form_option_data = $form_row_data['prop'];
        				
				$form_content.= "$form_row_data[label]:";
                                
				$form_content.=" <select multiple>";
                                
				    foreach($form_option_data as $key => $value){
				
					$form_content.= "<option value".$form_option_data[$key]['option_value']."> ".$form_option_data[$key]['label']."</option>";
					
                                    }//foreach
			}//end if 
				
                        elseif($form_row_data['type']=='range'){
			    
			    $dmax['max_range']=(@$form_row_data['is_max'])?" max='".@$form_row_data['prop']['max']."'":'';
			    
			    $dmin['min_range']=(@$form_row_data['is_min'])?" min='".@$form_row_data['prop']['min']."'":'';
			    
			    $form_content.= "$form_row_data[label]: <input type='".$form_row_data['type']."'
                                        
					                             max='".@$form_row_data['prop']['max']."'
										
								     min='".@$form_row_data['prop']['min']."'> <br><br>";
			
			} // end if
			
              
                                $form_content.="</select><br><br>";       
		    
    
		    
		}  // end    
	
	return $form_content;
	
    }
        
?>