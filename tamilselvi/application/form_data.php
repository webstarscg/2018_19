<?php

	$form_data= array(
		
		1=>array(	'label'  	=>'Student Name',
				'is_text'	=>1,
				'max_length'	=>15),
		
		2=>array(	'label'		=>'Title',
				'is_text_area'	=>1),
		
		3=>array(	'label'		=>'Title Description',
				'is_text_area'	=>1),
		
		3=>array( 	'label'         => 'Gender',
				'is_radio'          => 1,
                                'prop'    	=>['1' => ['label'=>'Male', 'option_value'=>'Male'],
						   '2' => ['label'=>'Female', 'option_value'=>'Female'],]),
                        );
?>