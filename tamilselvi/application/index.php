<?php
     
     //includes textbox_creator_data
     include('../application/textbox_creator/data.php');
     
     //includes textarea_creator_data
     include('../application/textarea_creator/data.php');
     
     //includes date_creator_data
     include('../application/date_creator/data.php');
     
     //includes option_builder_data
     include('../application/option_builder/data.php');
     
     //includes range_creator_data
     include('../application/range_creator/data.php');
    
     //includes image_creator_data
     include('../application/image_creator/data.php');

     //includes form_builder    
     include('../library/form_builder.php');
    
     //includes insert
     include ('../library/csv.php');
      
     //process the form
     $form_content = form_builder($form_data);
    
     // if add action
     if(@$_POST['ADD']){
        
	  // insert action
	  insert_action_csv(
			 ['post_data' => $_POST,
			  'form_data'  => $form_data,
			  ]
			 
		      );
        
     } //  end 
   
?>

<html>
     <head>
	  <title>FORM BUILDER</title>
     </head>
     <body>
	  <?php
            echo $form_content;
	  ?>
     </body>
</html>