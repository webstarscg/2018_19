<?php

	$form_data= array(
		
		1=>array( 	'label'         => 'Radio',
				'type'          => 'radio',
				'field'         => 'radio',
                                'prop'          =>['1' => ['label'=>'Option A', 'option_value'=>'Male'],
					           '2' => ['label'=>'Option B', 'option_value'=>'Female'],]),
		
		
		2=>array( 	'label'       	=> 'Dropdown',
				'type'    	=> 'dropdown_single',
				'field'         => 'dropdown',
				'prop'   	=>['1' => ['label'=>'Option A', 'option_value'=>'Msc'],
						   '2' => ['label'=>'Option B', 'option_value'=>'Bsc'],
						   '3' => ['label'=>'Option C', 'option_value'=>'BCom']]),
		
		3=>array(	'label'		=>'Email',
				'field'         =>'Email',
				'type'		=>'text'),
		
		4=>array(	'label'         => 'Checkbox',
				'type'          => 'checkbox',
				'field'         => 'checkbox',
                                'prop'    	=> ['1' => ['label'=>'Option A', 'option_value'=>'Dancing'],
						   '2' => ['label'=>'Option B', 'option_value'=>'Singing'],]),

		
		5=>array( 	   'label'   => 'Multilist',
				   'type'    => 'dropdown_multiple',
				   'field'   => 'Multilist',
				   'prop'    =>['1' => ['label'=>'Option A', 'option_value'=>'MSc'],
						'2' => ['label'=>'Option B', 'option_value'=>'MCA'],]
				),

	);

?>
    	
	