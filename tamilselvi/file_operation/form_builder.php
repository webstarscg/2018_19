<center>FORM BUILDER</center>
<?php
	
	$form_data= array(
		
		1=>array(	'label'  	=>'Textbox',
				'field'		=>'Textbox',
				'type'		=>'text',
				'is_max_length'	=>1,
				'max_length'	=>15),
				
		
		2=>array( 	'label'         => 'Radio',
				'field'         => 'radio',
				'type'          => 'radio',
                                'prop'    	=>['1' => ['label'=>'Option A', 'option_value'=>'Option A'],
						   '2' => ['label'=>'Option B', 'option_value'=>'Option B'],]),
                        );
                                             
    function form_build($temp){   
	  
	  $lv=[];
	  
	  $name='';
	  
	  $form_content = "<form  method='POST'>";
	  
	  $form_content.="<table style='width:100%'border='3'>";
	
          foreach($temp as $form_index => $row_attr){
              
	       if($row_attr['type']=='text'){
	      
		    $lv['text_max_len']=(@$row_attr['is_max_length'])?"maxlength='".@$row_attr['max_length']."'":'';   
				       
			 $form_content.='<tr>'.
					 "<td>".$row_attr['label']."</td>".
					 "<td><input type='".@$row_attr['type']."'$lv[text_max_len] id='X$form_index' name='X$form_index' ></td>".
				   '</tr>';
				   			   
			//$name=@$_POST["name"];
			//print_r($name);
			
			
	       }elseif($row_attr['type']=='radio'){
	       
			$form_radio=radio_builder(['row_index' => $form_index,
						  'row_data' => $row_attr]);
			      
			 $form_content.='<tr>'.
					     "<td>".@$row_attr['label']."</td>".
			                     "<td>$form_radio </td>".
					'</tr>';				
			
			//$gender=@$_POST["gender"];
			//print_r($gender);
				   
		}//if
                
                
         }//for
		
            $form_content.='<tr>'."<td></td>"."<td><input type='submit' id='ADD' name='ADD'></form></td>".
				   '</tr>';    
                
            return $form_content;
    }
    
  function radio_builder($row_attr){
	
	    $form_radio='';
				$param=$row_attr['row_index'];
				//print_r($param);
                                $form_option_data = $row_attr['row_data']['prop'];
				
				    foreach($form_option_data as $key => $value){
				
					 $form_radio.= "<input type=".$row_attr['row_data']['type']." id='X$param' name='X$param' value='".$form_option_data[$key]['option_value']."'>".
					                 "".$form_option_data[$key]['label']; 
				    }
				    
    
        return $form_radio;
	
	
} // end of
	
?>