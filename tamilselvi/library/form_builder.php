<?php

     function form_builder($temp){   
	  
	  $lv=[];
	  
	  $dmax=[];
	  
	  $dmin=[];
       
	  $form_content = "<form action='' method='post'>";
	  
	  $form_content.="<table style='width:100%'border='3'>";
	
          foreach($temp as $form_index => $row_attr){
       
	       
	       if($row_attr['type']=='text'){
	      
			$lv['text_max_len']=(@$row_attr['is_max_length'])?"maxlength='".@$row_attr['max_length']."'":'';   
				       
			$form_content.='<tr>'.
					 "<td>".$row_attr['label']."</td>".
					 "<td><input type='".@$row_attr['type']."'$lv[text_max_len] id='X$form_index' name='X$form_index' ></td>".
				       '</tr>';
				   
	       }elseif($row_attr['type']=='radio'){
	       
			$form_radio=radio_builder(['row_index' => $form_index,
						  'row_data' => $row_attr]);
			      
			$form_content.='<tr>'.
					     "<td>".@$row_attr['label']."</td>".
			                     "<td>$form_radio </td>".
					'</tr>';
	       }elseif($row_attr['type']=='checkbox'){
	       
			$form_checkbox=checkbox_builder(['row_index' => $form_index,
						  'row_data' => $row_attr]);
			      
			$form_content.='<tr>'.
					     "<td>".@$row_attr['label']."</td>".
			                     "<td>$form_checkbox </td>".
				        '</tr>';	
	       
	       }elseif($row_attr['type']=='date'){
		    
			$form_date=date_builder(['row_index' => $form_index,
						  'row_data' => $row_attr]);
			      
			$form_content.='<tr>'.
					     "<td>".@$row_attr['label']."</td>".
			                     "<td>$form_date </td>".
					'</tr>';
					
					
	       }elseif($row_attr['type']=='text_area'){
		
			$form_content.='<tr>'.
					     "<td>".@$row_attr['label']."</td>".			
					     "<td><textarea rows='3' id='X$form_index' name='X$form_index'></textarea></td>".
					'</tr>';
				
	       }elseif($row_attr['type']=='grid'){
	       
		
			$grid_content=build_grid(['row_index' => $form_index,
						  'row_data' => $row_attr]);
		
			$form_content.='<tr>'.
					     "<td>".@$row_attr['label']."</td>".
			                     "<td>$grid_content </td>".
					'</tr>';
			 
	       }elseif($row_attr['type']=='dropdown_single'){
		 
			$form_dropdown=dropdown_builder(['row_index' => $form_index,
						          'row_data' => $row_attr]);
			      
			$form_content.='<tr>'.
					     "<td>".@$row_attr['label']."</td>".
			                     "<td>$form_dropdown </td>".
					'</tr>';
					
	        }elseif($row_attr['type']=='file'){
				
			$form_option=image_builder(['row_index' => $form_index,
						      'row_data' => $row_attr]);
                
			$form_content.= '<tr>'.
				
					  "<td>".@$row_attr['label']."</td>".
					  "<td>$form_option </td>".
					
					'</tr>';
						
						
		}elseif($row_attr['type']=='dropdown_multiple'){
		 
			$form_dropdown=multiple_dropdown_builder(['row_index' => $form_index,
						          'row_data' => $row_attr]);
			      
			$form_content.='<tr>'.
					     "<td>".@$row_attr['label']."</td>".
			                     "<td>$form_dropdown </td>".
					'</tr>';
		
		}elseif($row_attr['type']=='range'){
		 
			$form_range=range_builder(['row_index' => $form_index,
						       'row_data' => $row_attr]);
			      
			$form_content.='<tr>'.
					     "<td>".@$row_attr['label']."</td>".
			                     "<td>$form_range </td>".
					'</tr>';
					
	       }//end if
                               
	  
	  } // end of for
	  
	  $form_content.='<tr>'."<td></td>"."<td><input type='submit' id='ADD' name='ADD'></form></td>".
				   '</tr>';
	  return $form_content;
     
	  
     } //  end of function
     
     // building 
     function build_grid($row_attr){
		  
			 $param=$row_attr['row_index'];
		    // row 
			$rows		  = $row_attr['row_data']['rows'];
			
                    //cols
			$cols		 = count($row_attr['row_data']['columns']);

			$grid_content    = ''; 
			
	       		$grid_content.="<table border='1'></thead>";
				
			if($row_attr['row_data']['is_sno']==1){
			      
			      $grid_content.="<th></th>";
					
			}//end of if
			
			for($col_idx=1;$col_idx<=$cols;$col_idx++){

			
				$grid_content.="<th>".$row_attr['row_data']['columns'][$col_idx]."</th>";
				
			}//end of for
			
			$grid_content.='</thead>';
			
			for($row_idx=1;$row_idx<=$rows;$row_idx++){
		
				$grid_content.="<tr>";
				
				if($row_attr['row_data']['is_sno']==1){
				
					$grid_content.="<td> $row_idx </td>";
				
				
			      }//end of if
			
			      for($col_idx=1;$col_idx<=$cols;$col_idx++){
								
									
				   $grid_content.="<td><input type='text' id='X$param' name='X$param'></td>";
					
			      }//end of for
				
			      $grid_content.="</tr>";
				
			}//end of for
			
			
			$grid_content.='</table>';
			
			return $grid_content;
				
     } // end of function
	
	function checkbox_builder($row_attr){
    
	        $form_checkbox='';
	    
	       $param=$row_attr['row_index'];     
                            
                                $form_option_data = @$row_attr['row_data']['prop'];
				
				    foreach($form_option_data as $key => $value){
				
					 $form_checkbox.= "<input type=".$row_attr['row_data']['type']."  name='X$param' id='X$param'  value=".$form_option_data[$key]['option_value'].">".
					                 "".$form_option_data[$key]['label']; 
					
				    }//foreach
                                       
    
        return $form_checkbox;
    	
     }//end of


	function dropdown_builder($row_attr){
	 
		 $form_dropdown='';
		 
		 $param=$row_attr['row_index'];
				 
				     $form_option_data = @$row_attr['row_data']['prop'];
				     
				     $form_dropdown.=" <select name='X$param'>";
				     
					 foreach($form_option_data as $key => $value){
				     
					     $form_dropdown.= "<option value=".$form_option_data[$key]['option_value']."  name='X$param'> ".$form_option_data[$key]['label']."</option>";
					     
					 }//foreach
					 
				     $form_dropdown.="</select><br><br>";       
	 
	return $form_dropdown;
	 
	     
     } // end of

     function radio_builder($row_attr){
    
	       $form_radio='';
	
	       $param=$row_attr['row_index'];
			 //print_r($param);
                    $form_option_data = $row_attr['row_data']['prop'];
				
				    foreach($form_option_data as $key => $value){
				
					 $form_radio.= "<input type=".$row_attr['row_data']['type']." id='X$param' name='X$param' value=".$form_option_data[$key]['option_value'].">".
				    	                 "".$form_option_data[$key]['label']; 
				    }
        return $form_radio;
    
	
     } // end of

     function date_builder($row_attr){

	    $dmax=[];
	    
	    $dmin=[];
	    
	    $form_date='';
	    
	    $param=$row_attr['row_index'];
		    
			    $dmax['row_data']['max_date']=(@$row_attr['row_data']['is_max'])?" max='".@$row_attr['row_data']['prop']['max']."'":'';
			 
			    $dmin['min_date']=(@$row_attr['row_data']['is_min'])?" min='".@$row_attr['row_data']['prop']['min']."'":'';
			 
			    $form_date.="<input type=".$row_attr['row_data']['type']."
					max=".@$row_attr['row_data']['prop']['max']."
					min=".@$row_attr['row_data']['prop']['min']."id='X$param' name='X$param'>" ;    
	
	return $form_date;
	
	
    }//end of
    
    function image_builder($row_attr){
    
	        $form_option='';
	    
	       $param=$row_attr['row_index'];     
                            
                                $form_option_data = @$row_attr['row_data']['prop'];
				
				    foreach($form_option_data as $key => $value){
				
					 $form_option.= "<input type=".$row_attr['row_data']['type']."  name='X$param' id='X$param'  value=".$form_option_data[$key]['option_value'].">".
					                 "".$form_option_data[$key]['label']; 
					
				    }//foreach
                                       
    
        return $form_option;
    	
     }//end of

     function multiple_dropdown_builder($row_attr){
	 
		 $form_dropdown='';
		 
		 $param=$row_attr['row_index'];
				 
				     $form_option_data = @$row_attr['row_data']['prop'];
				     
				     $form_dropdown.=" <select multiple name='X$param'>";
				     
					 foreach($form_option_data as $key => $value){
				     
					     $form_dropdown.= "<option value=".$form_option_data[$key]['option_value']."  name='X$param'> ".$form_option_data[$key]['label']."</option>";
					     
					 }//foreach
					 
				     $form_dropdown.="</select><br><br>";       
	 
	     return $form_dropdown;
	 
	     
     } // end of
     
     function range_builder($row_attr){
	
		$dmax=[];
	    
	        $dmin=[];
	    
		$form_range='';
		 
		$param=$row_attr['row_index'];
				
			    $dmax['max_range']=(@$row_attr['row_data']['is_max'])?" max='".@$row_attr['row_data']['prop']['max']."'":'';
			    
			    $dmin['min_range']=(@$row_attr['row_data']['is_min'])?" min='".@$row_attr['row_data']['prop']['min']."'":'';
			    
			    $form_range.= "<input type='".@$row_attr['row_data']['type']."'
                                        
					                             max='".@$row_attr['row_data']['prop']['max']."'
										
								     min='".@$row_attr['row_data']['prop']['min']."id='X$param' name='X$param''>";
	return $form_range;
	} // end of
	
     
?>