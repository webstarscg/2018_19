<?php

    function option_builder($form_data){
	    
	    $dmax=[];
	
	    $dmin=[];
	    
	    $form_content='';
	
		// traverse form_data
	
		foreach($form_data as $form_row_key => $form_row_data){
			
			if($form_row_data['type']=='range'){
			    
			    $dmax['max_range']=(@$form_row_data['is_max'])?" max='".@$form_row_data['prop']['max']."'":'';
			    
			    $dmin['min_range']=(@$form_row_data['is_min'])?" min='".@$form_row_data['prop']['min']."'":'';
			    
			    $form_content.= "$form_row_data[label]: <input type='".$form_row_data['type']."'
                                        
					                             max='".@$form_row_data['prop']['max']."'
										
								     min='".@$form_row_data['prop']['min']."'> <br><br>";
			
			} // end if
			
  
		}  // end    
	
	return $form_content;
	
    }
		
?>