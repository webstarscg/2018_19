<?php

    function option_builder($form_data){
    
	    $form_content='';
	
		// traverse form_data
	
		foreach($form_data as $form_row_key => $form_row_data){
                    
                    if($form_row_data['type']=='dropdown_single'){
                            
                                $form_option_data = $form_row_data['prop'];
        				
				$form_content.= "$form_row_data[label]:";
                                
				$form_content.=" <select>";
                                
				    foreach($form_option_data as $key => $value){
				
					$form_content.= "<option value=".$form_option_data[$key]['option_value']."> ".$form_option_data[$key]['label']."</option>";
					
                                    }//foreach
                                    
                                $form_content.="</select><br><br>";       
                    } 
					 
                } // for
    
        return $form_content;
    
	
    } // end of
        
?>                