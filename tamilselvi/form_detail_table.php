﻿<?php

	$form_data=array( 1=>array( 'label'             => 'Text Box',
				    'type'	        => 'text',
				    'max_length'        => 1,
				    'width'		=> 100,
				    'height'	        => 20,
				    'font_size'	        => 12,
				    'font_color'        => 'blue',
				    'is_manditory'      => 'yes',
				    'allowed_character' => '., ' ),
                     
                          2=>array( 'label'      => 'Date ',
				    'type'       => 'date',
				    'width'      => 100,
				    'height'     => 20,
				    'from_date'  => '11/12/2018',
				    'to_date'    => '31/12/2018'),
                      
                          3=>array( 'label'              => 'Image',
			            'type'               => 'file',
			            'width'              => 50,
				    'height'             => 50,
				    'image_size'         => 30,
				    'image_format'       => 'JPG,JPEG,PNG',
				    'image_width_height' => '35*35mm',
				    'is_manditory'       => 'yes'),
                       
                          4=>array( 'label'              => 'Document',
				    'type'               => 'doc',
				    'width'              => 50,  
				    'height'             => 50,
				    'document_size'      => 100,
				    'document_format'    => 'PDF',
			            'is_manditory'       => 'yes'),
                       
                          5=>array( 'label'             => 'Radio',
				    'type'              => 'radio',
				    'list_options'      => 'Option1, Option2',
				    'is_manditory'      => 'yes'),
					   
		          6=>array( 'label'             => 'Dropdown',
				    'type'              => 'dropdown',
				    'list_options'      => 'Option 1, Option 2',
				    'is_manditory'      => 'yes'),
					   
		          7=>array( 'label'             => 'Checkbox',
				    'type'              => 'checkbox',
				    'list_options'      => 'Option 1 , Option 2',
				    'is_manditory'      => 'yes'),
					   
		          8=>array( 'label'	     => 'Email',
				    'type'		     => 'email',
				    'max_length'	     => 100,
				    'width'	     => 100,
				    'height'	     => 20,
			            'font_size'	     => 12,
				    'font_color'	     => 'blue',
				    'is_manditory'      => 'yes',
				    'allowed_character' => "0–9 , a–z, A–Z, !,#, $, %, &, ', *, /, =, ?, ^, _, +, -, `, {, |, }, ~, ")
    
                         );
					  
					  
	$form_content="<table style= 'width:100%' border= '3'>";
	
	//traverse form_data
	
	foreach($form_data as $form_row_key => $form_row_data){
			
		if($form_row_data['type']=='text'){
				
			$form_content.='<tr>';
                
				$form_content.="<td>".$form_row_data['label']."</td>";
                    
				$form_content.="<td> <input type= 'text'> </td>";
                    
                    
			$form_content.='</tr>';
					
		} // end
            
                elseif($form_row_data['type']=='date'){
				
			$form_content.='<tr>';
                
				$form_content.="<td>".$form_row_data['label']."</td>";
                    
                                $form_content.="<td> <input type= 'date'   > </td>";
                    
			$form_content.='</tr>';
					
			} 
                 
                elseif($form_row_data['type']=='file'){
				
                        $form_content.='<tr>';
                
				$form_content.="<td>".$form_row_data['label']."</td>";
                    
				$form_content.="<td> <input type= 'file'> </td>";
                    
			$form_content.='</tr>';
					
		}
            
                elseif($form_row_data['type']=='doc'){
				
                        $form_content.='<tr>';
                
				$form_content.="<td>".$form_row_data['label']."</td>";
                    
				$form_content.="<td> <input type= 'file'> </td>";

			$form_content.='</tr>';
					
		}
            
                elseif($form_row_data['type']=='radio'){
				
                        $form_content.='<tr>';
                
				$form_content.="<td>".$form_row_data['label']."</td>";
                    
                                $form_content.="<td> <input type= 'radio' name= 'option1' value= 'option1'> Option 1
					
					      	     <input type= 'radio'  name= 'option1' value= 'option2'> Option 2  <br><br> </td>";
					 
			$form_content.='</tr>';
					
		}
            
		elseif($form_row_data['type']=='checkbox'){
				
			$form_content.='<tr>';
                
				$form_content.="<td>".$form_row_data['label']."</td>";
                    
                                $form_content.="<td> <input type='checkbox' name='option1' /> Option 1
					
						     <input type='checkbox' name='option2' /> Option 2 <br><br> </td>";
					
			$form_content.='</tr>';
					
		}
		
		elseif($form_row_data['type']=='dropdown'){
				
			$form_content.='<tr>';
                
				$form_content.="<td>".$form_row_data['label']."</td>";
                    
				$form_content.="<td> <select name='dropdown'>
						     <option>-select-</option>
						     <option>Option 1</option>
						     <option>Option 2</option>/> <br><br> </td>";
					
			$form_content.='</tr>';
					
		}
            
		elseif($form_row_data['type']=='email'){
				
			$form_content.='<tr>';
                
				$form_content.="<td>".$form_row_data['label']."</td>";
                    
				$form_content.="<td> <input type='email' name='email'> <br><br> </td>";
                    
			$form_content.='</tr>';
					
		}
			
        $form_content.'</table>';
            
    }  // end
	
	echo $form_content;
	
?>
