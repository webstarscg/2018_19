<?php
require_once("../template.php");

$options = array("filename"=>"if.html", "debug"=>0);
$template =& new Template($options);

$template->AddParam('title', 'Student Table');
$template->AddParam('msg1', 'Condition is true :)');
$template->AddParam('msg2', 'Condition is false :(');
$template->AddParam('condition', '0');

$template->EchoOutput();

?>