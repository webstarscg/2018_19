<?php

	$form_data= array(
		
		1=>array(	'label'  	=>'Student Name',
				'is_text'	=>1,
				'max_length'	=>15),
		
		2=>array(	'label'		=>'Title',
				'is_text_area'	=>1),
		
		3=>array(	'label'		=>'Title Description',
				'is_text_area'	=>1),
		
		4=>array( 	'label'         => 'Gender',
				'is_radio'      => 1,
                                'prop'    	=>['1' => ['label'=>'Male', 'option_value'=>'Male'],
						   '2' => ['label'=>'Female', 'option_value'=>'Female'],]),
		
		5=>array(	'label'         => 'Area of interest',
				'is_checkbox'   => 1,
                                'prop'    	=> ['1' => ['label'=>'Programming', 'option_value'=>'Programming'],
						   '2' => ['label'=>'Designing', 'option_value'=>'Designing'],]),
		
		6=>array( 	'label'       	=> 'Caste',
				'is_dropdown'  	=> 1,
				'prop'   	=>['1' => ['label'=>'BC', 'option_value'=>'BC'],
						   '2' => ['label'=>'MBC', 'option_value'=>'MBC'],
						   '3' => ['label'=>'FC', 'option_value'=>'FC']]),
                        );
?>