<?php

	// data input	
	$form_data= array(1=>array(             'label'		=> 'Budget',
				                'type'		=> 'grid',
				                'columns'	=> [1	=> 'Items',
								    2	=> 'Amount'],
								    
						'is_sno'       	=> 1,	    
								
				                'rows'		=>15)
					  );
    
	 // content variable
     $form_content='';
     
	 // form traverse
     foreach($form_data as $form_index => $row_attr){
		
		//echo $row_attr['type'];
		$form_content.=$form_index.")".$row_attr['label'] ;
        
		if($row_attr['type']=='grid'){
		
			// row 
			$rows		  = $row_attr['rows'];

			$cols		 = count($row_attr['columns']);

	
			//$form_content.=$cols['columns'];
			
			$form_content.="<table border='1'></thead>";
				
			if($row_attr['is_sno']==1){
					$form_content.="<th></th>";
					
			}
			
			for($col_idx=1;$col_idx<=$cols;$col_idx++){

			
				$form_content.="<th>".$row_attr['columns'][$col_idx]."</th>";
				
			}//end of for
			
			$form_content.='</thead>';
			
			for($row_idx=1;$row_idx<=$rows;$row_idx++){

		
				$form_content.="<tr>";
				
				if($row_attr['is_sno']==1){
				
					$form_content.="<td> $row_idx </td>";
				
				}
				
			
			
			for($col_idx=1;$col_idx<=$cols;$col_idx++){
								
									
					$form_content.='<td><input type="text" name="grid"></td>';
					
				}
				
				$form_content.="</tr>";
				
			}//end of for
			
			
			$form_content.='</table>';
			
		} // end of grid
			
	} // end of traverse


	echo $form_content;
	
?>