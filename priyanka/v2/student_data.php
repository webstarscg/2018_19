<?php

	$form_data= array(
		
		1=>array(	'label'  	=>'Name',
				'type'		=>'text',
				'field_id'      =>'student_name',
				'is_max_length'	=>1,
				'max_length'	=>15),
				
		
		2=>array( 	'label'         => 'Gender',
				'type'          => 'radio',
				'field_id'      => 'gender',
                                'prop'    =>['1' => ['label'=>'Male', 'option_value'=>'Male'],
					     '2' => ['label'=>'Female', 'option_value'=>'Female'],]),
		
		
//		3=>array( 	'label'      	=> 'Date ',
//			        'type'       	=> 'date',
//				'prop'       	=>['max'  => "2019-01-01" ,
//						   'min'  => "2000-12-31" ,],
//				'is_max'	=> 1,
//				'is_max'	=> 1),
//	
//		
//		4=>array( 	'label'       	=> 'Caste',
//				'type'    	=> 'dropdown',
//				'prop'   	=>['1' => ['label'=>'BC', 'option_value'=>100],
//						   '2' => ['label'=>'MBC', 'option_value'=>200],
//						   '3' => ['label'=>'FC', 'option_value'=>200]]),
//			 		
//		
//		5=>array(	'label'		=>'Registration Number',
//				'type'		=>'text'),
//		
//		6=>array(	'label'		=>'Institution',
//				'type'		=>'text'),
//		
//		7=>array(	'label'		=>'Email',
//				'type'		=>'text'),
//		
//		8=>array(	'label'		=>'Mobile No',
//				'type'		=>'text'),
//		
//		9=>array(	'label'         => 'Area of interest',
//				'type'          => 'checkbox',
//                                'prop'    	=>['1' => ['label'=>'Programming', 'option_value'=>100],
//						   '2' => ['label'=>'Designing', 'option_value'=>200],]),
//
//		
//		10=>array(	'label'		=>'Title',
//				'type'		=>'text_area'),
//		
//		11=>array(	'label'		=>'Title Description',
//				'type'		=>'text_area'),
//		
//		12=>array(	'label'		=>'Mentor Name',
//				'type'		=>'text'),
//		
//		13=>array(	'label'		=>'Budget',
//				'type'		=>'grid',
//				'columns'	=>[1=>'Items',
//						   2=>'Amount'],
//				'is_sno'	=> 1,
//				'rows'		=> '4'),
//		
//		14=>array(	'label'		=>'Student Details',
//				'type'		=>'grid',
//				'columns'	=>[1=>'Name',
//						   2=>'Email id',
//					           3=>'Course'],
//				'is_sno'	=>  1,
//				'rows'		=> '5'),

	);

?>
    	
	