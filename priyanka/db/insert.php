<?php

    function insert_action($param){
        
        $lv = [];
                
        $lv['fields']      = [];
        
        $lv['field_values']= [];
                
        // form data
        foreach($param['form_data'] as $form_row_id=>$form_row_data){
            
            // field
            array_push($lv['fields'],$form_row_data['field']);
            
            // value
            array_push($lv['field_values'],"'".$param['post_data']['X'.$form_row_id ]."'");
            
            
        } // end of form data
        
        //print_r($lv['fields']);
        //print_r($lv['field_values']);
        
        $lv['query'] = "INSERT INTO
                                    $param[table_name]
                                        (".implode(',',$lv['fields']).")
                                    VALUES
                                        (".implode(',',$lv['field_values']).")
                                    
                                ";
                                
        mysqli_query($param['db_conn'],$lv['query']);
        
        mysqli_close($param['db_conn']);
       // echo $lv['query'];
                         
        return 1;                                
        
    } // end
    
?>