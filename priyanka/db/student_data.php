<?php

	$form_data= array(

		1=>array(	'label'  				=>'Student Name',
							'type'					=>'text',
							'field'   			=>'name',
							'is_max_length'	=>1,
							'max_length'		=>15),


		2=>array( 'label'         => 'Gender',
							'type'          => 'radio',
							'field'         => 'gender',
              'prop'    			=>['1' => [
																					'label'=>'Male',
																					'option_value'=>'Male'],
						   									'2' => [
																					'label'=>'Female',
																					'option_value'=>'Female'],]),


		3=>array( 	'label'      	=> 'Date of Birth ',
			        	'type'       	=> 'date',
								'field'       => 'date',
								'prop'       	=>['max'  => "2019-01-01" ,
						   									 'min'  => "2000-12-31" ,],
																 'is_max'	=> 1,
																 'is_max'	=> 1),


		4=>array( 	'label'   => 'Caste',
								'type'    => 'dropdown',
								'field'   => 'caste',
								'prop'   	=>['1' => ['label'=>'BC', 'option_value'=>'BC'],
						   							 '2' => ['label'=>'MBC', 'option_value'=>'MBC'],
						   					 		 '3' => ['label'=>'FC', 'option_value'=>'FC']]),


		5=>array(	'label'		=>'Registration Number',
							'field'   =>'reg_no',
							'type'		=>'text'),

		6=>array(	'label'		=>'Institution',
							'field'   =>'institution',
							'type'		=>'text'),

		7=>array(	'label'		=>'Email',
							'field'   =>'email',
							'type'		=>'text'),

		8=>array(	'label'		=>'Mobile No',
							'field'   =>'mobile_no',
							'type'		=>'text'),

		9=>array(	'label'         => 'Area of interest',
							'type'          => 'checkbox',
							'field'         => 'area_of_interest',
              'prop'    			=> ['1' => ['label'=>'Programming', 'option_value'=>'Programming'],
						   										'2' => ['label'=>'Designing', 'option_value'=>'Designing'],]),


		10=>array(	'label'		=>'Title',
								'field'   =>'title',
								'type'		=>'text_area'),

		11=>array(	'label'		=>'Title Description',
								'field'   =>'description',
								'type'		=>'text_area'),

		12=>array(	'label'		=>'Mentor Name',
								'field'   =>'mname',
								'type'		=>'text'),

//		13=>array(	'label'		=>'Budget',
//				'type'		=>'grid',
//				'field'         =>'budget',
//				'columns'	=>[1=>'Items',
//						   2=>'Amount'],
//				'is_sno'	=> 0,
//				'rows'		=> '4'),
//
//		14=>array(	'label'		=>'Student Details',
//				'type'		=>'grid',
//				'columns'	=>[1=>'Name',
//						   2=>'Email id',
//					           3=>'Course'],
//				'is_sno'	=>  1,
//				'rows'		=> '5'),

	);

?>
