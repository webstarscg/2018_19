<?php

function connect_db() {

    $host = "localhost";
    $user = "root";
    $password = "";
    $dbname = "student";

    $con = new mysqli($host, $user, $password, $dbname)

            or die('Could not connect to the database server' . mysqli_connect_error());

    return $con;

}

?>