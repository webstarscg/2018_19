<?php

    //includes  student_data
    include "student_data.php";

    //includes student_content
    include('../library/form_builder.php');

    //includes insert
    include "insert.php";

    //includes config
    include "config.php";

    $form_content = form_builder($form_data);

    $conn         = connect_db();
    // if add action
    if(@$_POST['ADD']){

        // insert action
        insert_action(
          
		       ['post_data' => $_POST,
                        'form_data'  => $form_data,
			                  'table_name' => 'student_data',
			                  'db_conn'    => $conn
			                   ]

		    );

    }

?>

<html>
     <head>
	  <title>STUDENT FORM</title>
     </head>
     <body>
	    <center>STUDENT FORM</center>
	  <?php
            echo $form_content;
	  ?>
     </body>
</html>
